// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.time;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;

public class TimePeriodTest {
	private TimeStamp timeStampA = new TimeStamp(17);
	private TimeStamp timeStampB = new TimeStamp(19);
	private TimeSpan timeSpanA = new TimeSpan(23);
	private TimeSpan timeSpanB = new TimeSpan(29);

	private TimePeriod periodAA = new TimePeriod(timeStampA, timeSpanA);
	private TimePeriod periodAAStar = new TimePeriod(timeStampA, timeSpanA);
	private TimePeriod periodBB = new TimePeriod(timeStampB, timeSpanB);
	private TimePeriod periodAB = new TimePeriod(timeStampA, timeSpanB);
	private TimePeriod periodBA = new TimePeriod(timeStampB, timeSpanA);

	@Test
	public void lastValidTime_returnsLastIncludedTimStep() {
		long lastValidTimeStep = periodAA.getStartTime().getStep() + periodAA.getDuration().getSteps() - 1L;
		assertEquals(lastValidTimeStep, periodAA.getLastTime().getStep());
	}

	@Test
	public void shiftByDuration_negative_throws() {
		assertThrowsFatalMessage(TimeSpan.ERR_NOT_POSITIVE, () -> periodAA.shiftByDuration(-1));
	}

	@Test
	public void shiftByDuration_zero_returnsSameTimeSpan() {
		assertEquals(periodAA, periodAA.shiftByDuration(0));
	}

	@Test
	public void shiftByDuration_positive_shiftsTowardsFuture() {
		int shift = 5;
		TimePeriod result = periodAA.shiftByDuration(shift);
		TimeSpan duration = periodAA.getDuration();
		assertEquals(duration, result.getDuration());
		long newStart = periodAA.getStartTime().getStep() + duration.getSteps() * shift;
		assertEquals(newStart, result.getStartTime().getStep());
		long newEnd = periodAA.getLastTime().getStep() + duration.getSteps() * shift;
		assertEquals(newEnd, result.getLastTime().getStep());
	}

	@Test
	public void compareTo_differentDuration_throws() {
		assertThrowsFatalMessage(TimePeriod.UNALLOWED_COMPARISON, () -> periodAA.compareTo(periodAB));
	}

	@Test
	public void compareTo_identical_returnsTrue() {
		assertEquals(0, periodAA.compareTo(periodAA));
	}

	@Test
	public void compareTo_equivalent_returnsTrue() {
		assertEquals(0, periodAA.compareTo(periodAAStar));
	}

	@Test
	public void compareTo_laterEquivalent_returnsTrue() {
		assertTrue(periodAA.compareTo(periodBA) < 0);
	}

	@Test
	public void compareTo_earlierEquivalent_returnsTrues() {
		assertTrue(periodBA.compareTo(periodAA) > 0);
	}

	@Test
	public void isComparableTo_comparable_returnsTrue() {
		assertTrue(periodAA.isComparableTo(periodBA));
	}

	@Test
	public void isComparableTo_notComparable_returnsFalse() {
		assertFalse(periodAA.isComparableTo(periodAB));
	}

	@Test
	public void equals_null_returnsFalse() {
		assertFalse(periodAA.equals(null));
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void equals_wrongType_returnsFalse() {
		assertFalse(periodAA.equals(666));
	}

	@Test
	public void equals_identical_returnsTrue() {
		assertTrue(periodAA.equals(periodAA));
	}

	@Test
	public void equals_equivalent_returnsTrue() {
		assertTrue(periodAA.equals(periodAAStar));
	}

	@Test
	public void equals_notEquivalent_returnsFalse() {
		assertFalse(periodAA.equals(periodAB));
		assertFalse(periodAA.equals(periodBA));
		assertFalse(periodAA.equals(periodBB));
	}

	@Test
	public void toString_containsFirstTimeStamp() {
		String result = periodAA.toString();
		assertThat(result, containsString(timeStampA.toString()));
	}

	@Test
	public void toString_containsLastTimeStamp() {
		String result = periodAA.toString();
		assertThat(result, containsString(periodAA.getLastTime().toString()));
	}

	@Test
	public void hashCode_identical_returnsSameHash() {
		assertEquals(periodAA.hashCode(), periodAA.hashCode());
	}

	@Test
	public void hashCode_equivalent_returnsSameHash() {
		assertEquals(periodAA.hashCode(), periodAAStar.hashCode());
	}

	@Test
	public void addComponentsTo_timesAddedToCollector() {
		long start = 34;
		long duration = 55;
		ComponentCollector collector = mock(ComponentCollector.class);
		TimePeriod period = new TimePeriod(new TimeStamp(start), new TimeSpan(duration));
		period.addComponentsTo(collector);
		verify(collector, times(1)).storeLongs(start);
		verify(collector, times(1)).storeLongs(duration);
	}

	@Test
	public void populate_setsMembersCorrectly() {
		long start = 34;
		long duration = 55;
		ComponentProvider provider = mock(ComponentProvider.class);
		when(provider.nextLong()).thenReturn(start).thenReturn(duration);
		TimePeriod period = new TimePeriod();
		period.populate(provider);
		assertEquals(start, period.getStartTime().getStep());
		assertEquals(duration, period.getDuration().getSteps());
		assertEquals(start + duration - 1, period.getLastTime().getStep());
	}
}
