// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.time;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.time.Constants.Interval;

public class TimeSpanTest {

	@Test
	public void constructor_negative_throws() {
		assertThrowsMessage(IllegalArgumentException.class, TimeSpan.ERR_NOT_POSITIVE, () -> new TimeSpan(-1));
	}

	@Test
	public void constructorA_zero_works() {
		TimeSpan timeSpan = new TimeSpan(0);
		assertEquals(0, timeSpan.getSteps());
	}

	@Test
	public void constructorB_zero_works() {
		TimeSpan timeSpan = new TimeSpan(0, Interval.SECONDS);
		assertEquals(0, timeSpan.getSteps());
	}

	@Test
	public void constructor_intervalSeconds_interpretedAsSeconds() {
		TimeSpan timeSpan = new TimeSpan(14, Interval.SECONDS);
		assertEquals(14, timeSpan.getSteps());
	}

	@Test
	public void constructor_intervalMinutes_interpretedAsMinutes() {
		TimeSpan timeSpan = new TimeSpan(3, Interval.MINUTES);
		assertEquals(3 * 60, timeSpan.getSteps());
	}

	@Test
	public void constructor_intervalHours_interpretedAsHours() {
		TimeSpan timeSpan = new TimeSpan(2, Interval.HOURS);
		assertEquals(2 * 3600, timeSpan.getSteps());
	}

	@Test
	public void constructor_intervalDays_interpretedAsDays() {
		TimeSpan timeSpan = new TimeSpan(5, Interval.DAYS);
		assertEquals(5 * 3600 * 24, timeSpan.getSteps());
	}

	@Test
	public void constructor_intervalWeeks_interpretedAsWeeks() {
		TimeSpan timeSpan = new TimeSpan(99, Interval.WEEKS);
		assertEquals(99 * 3600 * 168, timeSpan.getSteps());
	}

	@Test
	public void constructor_intervalMonths_interpretedAsMonths() {
		TimeSpan timeSpan = new TimeSpan(15, Interval.MONTHS);
		assertEquals(15 * 3600 * 730, timeSpan.getSteps());
	}

	@Test
	public void constructor_intervalYears_interpretedAsYears() {
		TimeSpan timeSpan = new TimeSpan(88, Interval.YEARS);
		assertEquals(88L * 3600 * 24 * 365, timeSpan.getSteps());
	}

	@Test
	public void combine_createsCorrectTimeStamp() {
		TimeSpan timeSpan = TimeSpan.combine(new TimeSpan(1, Interval.YEARS), new TimeSpan(25, Interval.DAYS),
				new TimeSpan(11, Interval.HOURS), new TimeSpan(2, Interval.SECONDS));
		assertEquals(3600 * 24 * (365 + 25) + 3600 * 11 + 2, timeSpan.getSteps());
	}

	@Test
	public void equals_null_returnsFalse() {
		TimeSpan timeSpan = new TimeSpan(0);
		assertFalse(timeSpan.equals(null));
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void equals_foreignClass_returnsFalse() {
		TimeSpan timeSpan = new TimeSpan(0);
		assertFalse(timeSpan.equals(0));
	}

	@Test
	public void equals_this_returnsTrue() {
		TimeSpan timeSpan = new TimeSpan(21);
		assertTrue(timeSpan.equals(timeSpan));
	}

	@Test
	public void equals_equivalent_returnsTrue() {
		TimeSpan timeSpanA = new TimeSpan(21);
		TimeSpan timeSpanB = new TimeSpan(21);
		assertTrue(timeSpanA.equals(timeSpanB));
	}

	@Test
	public void equals_notEquaivalent_returnsFalse() {
		TimeSpan timeSpanA = new TimeSpan(21);
		TimeSpan timeSpanB = new TimeSpan(99);
		assertFalse(timeSpanA.equals(timeSpanB));
	}

	@Test
	public void hashCode_identicalObject_returnsSameCode() {
		TimeSpan timeSpan = new TimeSpan(54);
		assertEquals(timeSpan.hashCode(), timeSpan.hashCode());
	}

	@Test
	public void hashCode_equivalentObject_returnsSameCode() {
		TimeSpan timeSpanA = new TimeSpan(66);
		TimeSpan timeSpanB = new TimeSpan(66);
		assertEquals(timeSpanA.hashCode(), timeSpanB.hashCode());
	}

	@Test
	public void toString_callsFormatter() {
		TimeSpan timeSpan = new TimeSpan(0);
		try (MockedStatic<Formatter> formatterMock = Mockito.mockStatic(Formatter.class)) {
			timeSpan.toString();
			formatterMock.verify(() -> Formatter.formatTimeSpan(anyLong()), times(1));
		}
	}
	
	@Test
	public void addComponentsTo_storesSteps() {
		long value = 7584L; 
		ComponentCollector collector = mock(ComponentCollector.class);
		TimeSpan timeSpan = new TimeSpan(value);
		timeSpan.addComponentsTo(collector);
		verify(collector, times(1)).storeLongs(value);
	}
	
	@Test
	public void populate_setsStoredSteps() {
		long value = 7584L;
		TimeSpan timeSpan = new TimeSpan();
		ComponentProvider provider = mock(ComponentProvider.class);
		when(provider.nextLong()).thenReturn(value);
		timeSpan.populate(provider);
		assertEquals(value, timeSpan.getSteps());
	}
	
}