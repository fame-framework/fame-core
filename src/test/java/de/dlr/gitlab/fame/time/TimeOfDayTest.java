// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.time;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.testUtils.LogChecker;

public class TimeOfDayTest {
	private LogChecker logChecker = new LogChecker(TimeOfDay.class);

	@Before
	public void setUp() {
		logChecker.clear();
	}

	@Test
	public void testConstructor() {
		TimeOfDay timeOfDay = new TimeOfDay(13, 43, 59);
		assertEquals((long) (13 * 3600 + 43 * 60 + 59), timeOfDay.getSteps());
	}

	@Test
	public void testConstructorLastSecond() {
		TimeOfDay timeOfDay = new TimeOfDay(23, 59, 59);
		assertEquals((long) (23 * 3600 + 59 * 60 + 59), timeOfDay.getSteps());
	}

	@Test
	public void testConstructorTooLarge() {
		assertThrowsFatalMessage(TimeOfDay.TOO_BIG, () -> new TimeOfDay(24, 0, 0));
	}

	@Test
	public void testConstructorNegativeHourWarn() {
		new TimeOfDay(-1, 61, 1);
		logChecker.assertLogsContain(TimeOfDay.NEGATIVE);
	}

	@Test
	public void testConstructorNegativeMinuteWarn() {
		new TimeOfDay(1, -59, 1);
		logChecker.assertLogsContain(TimeOfDay.NEGATIVE);
	}

	@Test
	public void testConstructorNegativeSecondWarn() {
		new TimeOfDay(1, 59, -1);
		logChecker.assertLogsContain(TimeOfDay.NEGATIVE);
	}

	@Test
	public void testConstructorTooLargeSecondWarn() {
		new TimeOfDay(1, 59, 61);
		logChecker.assertLogsContain(TimeOfDay.INVALID);
	}

	@Test
	public void testConstructorTooLargeMinuteWarn() {
		new TimeOfDay(1, 60, 59);
		logChecker.assertLogsContain(TimeOfDay.INVALID);
	}

	@Test
	public void testConstructorTooLargeHourWarn() {
		new TimeOfDay(24, -5, 0);
		logChecker.assertLogsContain(TimeOfDay.INVALID);
	}

	@Test
	public void testToString() {
		TimeOfDay timeOfDay = new TimeOfDay(13, 02, 17);
		assertEquals("13:02:17h", timeOfDay.toString());
	}

	@Test
	public void testCalcHourOfDay() {
		long time = 4 * Constants.STEPS_PER_HOUR;
		assertEquals(4, TimeOfDay.calcHourOfDay(time));
		time = 2 * Constants.STEPS_PER_HOUR + 3 * Constants.STEPS_PER_DAY;
		assertEquals(2, TimeOfDay.calcHourOfDay(time));
		time = 23 * Constants.STEPS_PER_HOUR + 3 * Constants.STEPS_PER_WEEK;
		assertEquals(23, TimeOfDay.calcHourOfDay(time));
		time = 23 * Constants.STEPS_PER_HOUR + 59 * Constants.STEPS_PER_MINUTE + 59 * Constants.STEPS_PER_SECOND
				+ 5 * Constants.STEPS_PER_DAY;
		assertEquals(23, TimeOfDay.calcHourOfDay(time));
	}
}