// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.time;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import de.dlr.gitlab.fame.time.Constants.Interval;

/** Tests for {@link Formatter} Tests for null argument not needed as Formatter is called from existing TimeStamp objects only
 *
 * @author Christoph Schimeczek */
public class FormatterTest {
	@Test
	public void formatTimeStamp_zero() {
		assertEquals("2000-001(01/01)_00:00:00", Formatter.formatTimeStamp(0));
	}

	@Test
	public void formatTimeStamp_negativeSeconds() {
		assertEquals("1999-365(12/53)_23:59:55", Formatter.formatTimeStamp(-5));
	}

	@Test
	public void formatTimeStamp_negativeMinutes() {
		assertEquals("1999-365(12/53)_23:57:55", Formatter.formatTimeStamp(-125));
	}

	@Test
	public void formatTimeStamp_negativeHours() {
		assertEquals("1999-365(12/53)_20:57:55", Formatter.formatTimeStamp(-10925));
	}

	@Test
	public void formatTimeStamp_negativeDays() {
		assertEquals("1999-364(12/52)_20:57:55", Formatter.formatTimeStamp(-97325));
	}

	@Test
	public void formatTimeStamp_negativeMonths() {
		assertEquals("1999-334(11/48)_20:57:55", Formatter.formatTimeStamp(-2689325));
	}

	@Test
	public void formatTimeStamp_negativeYears() {
		assertEquals("1994-334(11/48)_20:57:55", Formatter.formatTimeStamp(-160369325L));
	}

	@Test
	public void formatTimeStamp_seconds() {
		assertEquals("2000-001(01/01)_00:00:17", Formatter.formatTimeStamp(17));
	}

	@Test
	public void formatTimeStamp_minutes() {
		assertEquals("2000-001(01/01)_00:23:17", Formatter.formatTimeStamp(1397));
	}

	@Test
	public void formatTimeStamp_hours() {
		assertEquals("2000-001(01/01)_07:23:17", Formatter.formatTimeStamp(26597));
	}

	@Test
	public void formatTimeStamp_days() {
		assertEquals("2000-005(01/01)_07:23:17", Formatter.formatTimeStamp(372197));
	}

	@Test
	public void formatTimeStamp_week() {
		assertEquals("2000-009(01/02)_07:23:17", Formatter.formatTimeStamp(717797));
	}

	@Test
	public void formatTimeStamp_months() {
		assertEquals("2000-040(02/06)_07:23:17", Formatter.formatTimeStamp(3396197));
	}

	@Test
	public void formatTimeStamp_years() {
		assertEquals("6331-040(02/06)_07:23:17", Formatter.formatTimeStamp(136585812197L));
	}

	@Test
	public void formatTimeSpan_zero() {
		assertEquals("0s", Formatter.formatTimeSpan(new TimeSpan(0).steps));
	}

	@Test
	public void formatTimeSpan_seconds() {
		assertEquals("59s", Formatter.formatTimeSpan(new TimeSpan(59, Interval.SECONDS).steps));
	}

	@Test
	public void formatTimeSpan_minutes() {
		assertEquals("2min", Formatter.formatTimeSpan(new TimeSpan(2, Interval.MINUTES).steps));
	}

	@Test
	public void formatTimeSpan_hours() {
		assertEquals("4h", Formatter.formatTimeSpan(new TimeSpan(4, Interval.HOURS).steps));
	}

	@Test
	public void formatTimeSpan_days() {
		assertEquals("77d", Formatter.formatTimeSpan(new TimeSpan(77, Interval.DAYS).steps));
	}

	@Test
	public void formatTimeSpan_years() {
		assertEquals("42a", Formatter.formatTimeSpan(new TimeSpan(42, Interval.YEARS).steps));
	}

	@Test
	public void formatTimeSpan_mixed() {
		TimeSpan span = TimeSpan.combine(new TimeSpan(33, Interval.SECONDS), new TimeSpan(43, Interval.MINUTES),
				new TimeSpan(2, Interval.YEARS));
		assertEquals("2a 43min 33s", Formatter.formatTimeSpan(span.steps));
	}
}
