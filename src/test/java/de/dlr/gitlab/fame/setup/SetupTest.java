// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.setup;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.testUtils.LogChecker;

/** Tests for class {@link Setup}
 * 
 * @author Christoph Schimeczek */
public class SetupTest {

	private LogChecker logChecker = new LogChecker(Setup.class);
	private Setup setup;

	@Before
	public void setup() {
		logChecker.clear();
	}

	@Test
	public void isTrackExchange_singleCoreTrue_returnsTrue() {
		setup = new Setup(MpiMode.SINGLE_CORE, true, false);
		assertTrue(setup.isTrackExchange());
		logChecker.assertLogsDoNotContain(Setup.WARN_PARALLEL_DISALLOWED);
	}

	@Test
	public void isTrackExchange_singleCoreFalse_returnsFalse() {
		setup = new Setup(MpiMode.SINGLE_CORE, false, true);
		assertFalse(setup.isTrackExchange());
		logChecker.assertLogsDoNotContain(Setup.WARN_PARALLEL_DISALLOWED);
	}

	@Test
	public void isTrackExchange_parallelFalse_returnsFalse() {
		setup = new Setup(MpiMode.PARALLEL, false, true);
		assertFalse(setup.isTrackExchange());
		logChecker.assertLogsDoNotContain(Setup.WARN_PARALLEL_DISALLOWED);
	}

	@Test
	public void isTrackExchange_parallelTrue_returnsFalse() {
		setup = new Setup(MpiMode.PARALLEL, true, true);
		assertFalse(setup.isTrackExchange());
		logChecker.assertLogsContain(Setup.WARN_PARALLEL_DISALLOWED);
	}

	@Test
	public void isTrackRuntime_singleCoreTrue_returnsTrue() {
		setup = new Setup(MpiMode.SINGLE_CORE, false, true);
		assertTrue(setup.isTrackRuntime());
		logChecker.assertLogsDoNotContain(Setup.WARN_PARALLEL_DISALLOWED);
	}

	@Test
	public void isTrackRuntime_singleCoreFalse_returnsFalse() {
		setup = new Setup(MpiMode.SINGLE_CORE, true, false);
		assertFalse(setup.isTrackRuntime());
		logChecker.assertLogsDoNotContain(Setup.WARN_PARALLEL_DISALLOWED);
	}

	@Test
	public void isTrackRuntime_parallelFalse_returnsFalse() {
		setup = new Setup(MpiMode.PARALLEL, true, false);
		assertFalse(setup.isTrackRuntime());
		logChecker.assertLogsDoNotContain(Setup.WARN_PARALLEL_DISALLOWED);
	}

	@Test
	public void isTrackRuntime_parallelTrue_returnsFalse() {
		setup = new Setup(MpiMode.PARALLEL, false, true);
		assertFalse(setup.isTrackRuntime());
		logChecker.assertLogsContain(Setup.WARN_PARALLEL_DISALLOWED);
	}
}
