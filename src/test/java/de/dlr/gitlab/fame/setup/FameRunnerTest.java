// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.setup;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.junit.Test;
import de.dlr.gitlab.fame.service.InputManager;

public class FameRunnerTest {

	@Test
	public void loadAndDistributeInputData_forwardsGivenPath() {
		InputManager inputManager = mock(InputManager.class);
		String path = "SomePath";
		FameRunner.loadAndDistributeInputData(inputManager, path);
		verify(inputManager, times(1)).read(path);
	}

	@Test
	public void loadAndDistributeInputData_callsDistribute() {
		InputManager inputManager = mock(InputManager.class);
		FameRunner.loadAndDistributeInputData(inputManager, "");
		verify(inputManager, times(1)).distribute();
	}

	@Test
	public void loadAndDistributeInputData_returnsGivenInputManager() {
		InputManager inputManager = mock(InputManager.class);
		InputManager result = FameRunner.loadAndDistributeInputData(inputManager, "");
		assert inputManager == result;
	}
}
