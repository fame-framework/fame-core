// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.setup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.testUtils.LogChecker;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Model.OptionSpec;
import picocli.CommandLine.Model.PositionalParamSpec;

/** Tests for {@link YamlDefaultsProvider}
 * 
 * @author Christoph Schimeczek */
public class YamlDefaultsProviderTest {

	private LogChecker logChecker = new LogChecker(YamlDefaultsProvider.class);
	private YamlDefaultsProvider provider;
	private Map<String, Object> defaults;
	private OptionSpec optionSpec;
	private PositionalParamSpec paramSpec;

	@Before
	public void setup() {
		defaults = new HashMap<>();
		defaults.put("MyString", "MyValue");
		defaults.put("MyInt", 42);
		defaults.put("MyList", Arrays.asList(new String[] {"A", "B", "C"}));
		defaults.put("MyBool", true);
		defaults.put("MyNull", null);
		Map<String, Object> subDefaults = new HashMap<>();
		defaults.put("MySubcommand", subDefaults);
		subDefaults.put("MyInnerString", "Inside");
		defaults.put("MyDanglingSubcommand", null);

		provider = new YamlDefaultsProvider(defaults);
		logChecker.clear();
	}

	@Test
	public void constructor_nullDefaultsFile_logsNoFile() {
		new YamlDefaultsProvider((File) null);
		logChecker.assertLogsContain(YamlDefaultsProvider.INFO_NO_FILE);
	}

	@Test
	public void constructor_noDefaultsFile_logsNoFile() {
		File file = mock(File.class);
		when(file.exists()).thenReturn(false);
		new YamlDefaultsProvider(file);
		logChecker.assertLogsContain(YamlDefaultsProvider.INFO_NO_FILE);
	}

	@Test
	public void defaultValue_nullDefaults_returnsNull() throws Exception {
		YamlDefaultsProvider provider = new YamlDefaultsProvider((Map<String, Object>) null);
		assertNull(provider.defaultValue(null));
	}

	@Test
	public void defaultValue_emptyDefaults_returnsNull() throws Exception {
		YamlDefaultsProvider provider = new YamlDefaultsProvider(new HashMap<>());
		assertNull(provider.defaultValue(null));
	}

	@Test
	public void defaultValue_optionNullDescriptionEmptyName_returnsNull() throws Exception {
		optionSpec = createOption(null, "", "");
		assertNull(provider.defaultValue(optionSpec));
	}

	/** Mock an optionSpec object with given description, command and longest-name */
	private OptionSpec createOption(String description, String command, String name) {
		OptionSpec optionSpec = mock(OptionSpec.class);
		when(optionSpec.isOption()).thenReturn(true);
		when(optionSpec.descriptionKey()).thenReturn(description);

		CommandSpec commandSpec = null;
		if (command != null) {
			commandSpec = mock(CommandSpec.class);
			when(commandSpec.qualifiedName(".")).thenReturn(command);
		}
		when(optionSpec.command()).thenReturn(commandSpec);

		when(optionSpec.longestName()).thenReturn(name);
		return optionSpec;
	}

	@Test
	public void defaultValue_optionEmptyDescriptionEmptyName_returnsNull() throws Exception {
		optionSpec = createOption("", "", "");
		assertNull(provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_matchingDescriptionEmptyCommand_returnsValue() throws Exception {
		optionSpec = createOption("MyString", "", "");
		assertEquals("MyValue", provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_matchingDescriptionSubcommand_returnsValue() throws Exception {
		optionSpec = createOption("MyInnerString", "MySubcommand", "");
		assertEquals("Inside", provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_matchingNameEmptyCommand_returnsValue() throws Exception {
		optionSpec = createOption("", "", "MyString");
		assertEquals("MyValue", provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_matchingNameNullCommand_returnsValue() throws Exception {
		optionSpec = createOption("", null, "MyString");
		assertEquals("MyValue", provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_nameWithDashEmptyCommand_returnsValue() throws Exception {
		optionSpec = createOption("", "", "-MyString");
		assertEquals("MyValue", provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_matchingNameWithCommand_returnsValue() throws Exception {
		optionSpec = createOption("", "MySubcommand", "MyInnerString");
		assertEquals("Inside", provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_nameNotMatching_returnsNull() throws Exception {
		optionSpec = createOption("", "", "NoMatchingName");
		assertNull(provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_nameNotMatching_logsInfo() throws Exception {
		optionSpec = createOption("", "", "NoMatchingName");
		provider.defaultValue(optionSpec);
		logChecker.assertLogsContain(YamlDefaultsProvider.INFO_NO_DEFAULT);
	}

	@Test
	public void defaultValue_subcommandNotAssigned_returnsNull() throws Exception {
		optionSpec = createOption("", "MyDanglingSubcommand", "SomeInnerValue");
		assertNull(provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_subcommandNotAssigned_logsWarning() throws Exception {
		optionSpec = createOption("", "MyDanglingSubcommand", "SomeInnerValue");
		provider.defaultValue(optionSpec);
		logChecker.assertLogsContain(YamlDefaultsProvider.WARN_NO_CHILDREN);
	}

	@Test
	public void defaultValue_nothingAssigned_returnsNull() throws Exception {
		optionSpec = createOption("", "", "MyNull");
		assertNull(provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_mapAssigned_returnsNull() throws Exception {
		optionSpec = createOption("", "", "MySubcommand");
		assertNull(provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_mapAssigned_logsError() throws Exception {
		optionSpec = createOption("", "", "MySubcommand");
		provider.defaultValue(optionSpec);
		logChecker.assertLogsContain(YamlDefaultsProvider.ERR_DISALLOWED_COMPLEX);
	}

	@Test
	public void defaultValue_list_returnNoBracketAndSpace() throws Exception {
		optionSpec = createOption("", "", "MyList");
		assertEquals("A,B,C", provider.defaultValue(optionSpec));
	}

	@Test
	public void defaultValue_paramNullDescriptionEmptyName_returnsNull() throws Exception {
		paramSpec = createParam(null, "", "");
		assertNull(provider.defaultValue(paramSpec));
	}

	/** Mock an optionSpec object with given description, command and longest-name */
	private PositionalParamSpec createParam(String description, String command, String label) {
		PositionalParamSpec spec = mock(PositionalParamSpec.class);
		when(spec.isOption()).thenReturn(false);
		when(spec.descriptionKey()).thenReturn(description);

		CommandSpec commandSpec = null;
		if (command != null) {
			commandSpec = mock(CommandSpec.class);
			when(commandSpec.qualifiedName(".")).thenReturn(command);
		}
		when(spec.command()).thenReturn(commandSpec);

		when(spec.paramLabel()).thenReturn(label);
		return spec;
	}

	@Test
	public void defaultValue_paramEmptyDescriptionEmptyName_returnsNull() throws Exception {
		paramSpec = createParam("", "", "");
		assertNull(provider.defaultValue(paramSpec));
	}

	@Test
	public void defaultValue_paramMatchingDescriptionEmptyCommand_returnsValue() throws Exception {
		paramSpec = createParam("MyString", "", "");
		assertEquals("MyValue", provider.defaultValue(paramSpec));
	}

	@Test
	public void defaultValue_paramMatchingDescriptionSubcommand_returnsValue() throws Exception {
		paramSpec = createParam("MyInnerString", "MySubcommand", "");
		assertEquals("Inside", provider.defaultValue(paramSpec));
	}

	@Test
	public void defaultValue_paramMatchingNameEmptyCommand_returnsValue() throws Exception {
		paramSpec = createParam("", "", "MyString");
		assertEquals("MyValue", provider.defaultValue(paramSpec));
	}

	@Test
	public void defaultValue_paramMatchingNameNullCommand_returnsValue() throws Exception {
		paramSpec = createParam("", null, "MyString");
		assertEquals("MyValue", provider.defaultValue(paramSpec));
	}

}
