// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.testUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/** Helpers to access private members of classes under test
 * 
 * @author Christoph Schimeczek */
public class AccessPrivates {
	/** Constructs and returns a new instance of the given class using a constructor with the specified arguments
	 * 
	 * @return new instance of the given class using a constructor with the specified arguments
	 * @throws {@link RuntimeException} that copies error messages in case an {@link InvocationTargetException} is caused within the
	 *           called constructor;
	 * @throws {@link RuntimeException} with a new error message in case the constructor cannot be found, called, instantiated,
	 *           accessed or in case of wrong arguments; */
	public static Object usePrivateConstructorOn(Class<?> clas, Object... arguments) {
		Class<?>[] argumentClasses = extractClassTypes(arguments);
		try {
			Constructor<?> constructor = clas.getDeclaredConstructor(argumentClasses);
			constructor.setAccessible(true);
			return constructor.newInstance(arguments);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not instantiate " + clas.getSimpleName());
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e.getCause().getMessage());
		}
	}

	/** @return array matching the classes of the specified objects */
	private static Class<?>[] extractClassTypes(Object... objects) {
		Class<?>[] classes = new Class<?>[objects.length];
		for (int index = 0; index < objects.length; index++) {
			classes[index] = unboxPrimitives(objects[index].getClass());
		}
		return classes;
	}

	/** Returns the corresponding primitive type (e.g. <code>integer</code>) if the specified class is a boxed primitive class (e.g.
	 * <code>Integer</code>); otherwise, the given class is returned;
	 * 
	 * @return corresponding primitive type if the specified class is a boxed primitive class; otherwise the specified class */
	private static Class<?> unboxPrimitives(Class<? extends Object> clas) {
		if (clas == Boolean.class) {
			return Boolean.TYPE;
		} else if (clas == Byte.class) {
			return Byte.TYPE;
		} else if (clas == Character.class) {
			return Character.TYPE;
		} else if (clas == Float.class) {
			return Float.TYPE;
		} else if (clas == Integer.class) {
			return Integer.TYPE;
		} else if (clas == Long.class) {
			return Long.TYPE;
		} else if (clas == Short.class) {
			return Short.TYPE;
		} else if (clas == Double.class) {
			return Double.TYPE;
		} else {
			return clas;
		}
	}

	/** Calls specified method on given target Object with specified parameters - even if the method is declared
	 * <code>private</code>;
	 * <p>
	 * Returns the return Object of the specified method, or null if the method is declared <code>void</code>
	 * 
	 * @return return Object of the specified method, or null if the method is declared <code>void</code> */
	public static Object callPrivateMethodOn(String methodName, Object target, Object... arguments) {
		Class<?>[] argumentClasses = extractClassTypes(arguments);
		Method method = accessMethod(methodName, target.getClass(), argumentClasses);
		return invokeMethodOn(method, target, arguments);
	}

	/** @return accessible {@link Method} of given declaringClass, specified by name and classes of its arguments;
	 * @throws {@link RuntimeException} if method cannot be found or is secured */
	public static Method accessMethod(String methodName, Class<?> declaringClass, Class<?>... argumentClasses) {
		try {
			Method method = declaringClass.getDeclaredMethod(methodName, argumentClasses);
			method.setAccessible(true);
			return method;
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			throw new RuntimeException(
					"Method '" + methodName + "' could not be found on class " + declaringClass.getSimpleName());
		}
	}

	/** @throws {@link RuntimeException} that copies error messages in case an {@link InvocationTargetException} is caused within
	 *           the called function;
	 * @throws {@link RuntimeException} with a new error message in case the function cannot be found, called, accessed or in case
	 *           of wrong arguments; */
	public static Object invokeMethodOn(Method method, Object target, Object... arguments) {
		try {
			return method.invoke(target, arguments);
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException e) {
			e.printStackTrace();
			throw new RuntimeException("Method '" + method.getName() + "' could not be invoked on " + target);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getCause().getMessage());
		}
	}

	/** Calls specified static method of given target class with specified parameters - even if the method is declared
	 * <code>private</code>;
	 * <p>
	 * Returns the return Object of the specified method, or null if the method is declared <code>void</code>
	 * 
	 * @return return Object of the specified method, or null if the method is declared <code>void</code> */
	public static Object callPrivateStaticMethod(String methodName, Class<?> targetClass, Object... arguments) {
		Class<?>[] argumentClasses = extractClassTypes(arguments);
		Method method = accessMethod(methodName, targetClass, argumentClasses);
		return invokeMethodOn(method, null, arguments);
	}

	/** Calls specified static method of given target Class that has the specified argument types and argument values */
	public static Object callPrivateStaticMethod(String methodName, Class<?> targetClass, Class<?>[] argumentClasses,
			Object... argumentValues) {
		Method method = accessMethod(methodName, targetClass, argumentClasses);
		return invokeMethodOn(method, null, argumentValues);
	}

	/** Calls method specified by name, declared in given class, on target object of child class using specified arguments
	 * 
	 * @param methodName name of the method to be called
	 * @param declaringClass type of the class that originally declares the method
	 * @param target object on which the method will be called; should be a child of declaringClass; if Object is of type
	 *          declaringClass, use {@link #callPrivateMethodOn(String, Object, Object...)} instead
	 * @param arguments all necessary arguments for the method call in correct order
	 * @return return Object of the specified method on target, or null if the method is declared <code>void</code> */
	public static Object callMethodOfClassXOnY(String methodName, Class<?> declaringClass, Object target,
			Object... arguments) {
		Class<?>[] argumentClasses = extractClassTypes(arguments);
		Method method = accessMethod(methodName, declaringClass, argumentClasses);
		return invokeMethodOn(method, target, arguments);
	}

	/** Calls method specified by name, declared in given class with argument types as specified on given target object using
	 * specified arguments
	 * 
	 * @param methodName name of the method to be called
	 * @param declaringClass type of the class that originally declares the method
	 * @param target object on which the method will be called; should be a child of declaringClass; if Object is of type
	 *          declaringClass, use {@link #callPrivateMethodOn(String, Object, Object...)} instead
	 * @param argumentTypes array of class types specifying the argument types of the method to be called
	 * @param arguments all necessary arguments for the method call in order matching argumentTypes
	 * @return returned Object of the specified method on target, or null if the method is declared <code>void</code> */
	public static Object callMethodOfClassXWithArgtypesOnY(String methodName, Class<?> declaringClass, Object target,
			Class<?>[] argumentTypes, Object... arguments) {
		Method method = accessMethod(methodName, declaringClass, argumentTypes);
		return invokeMethodOn(method, target, arguments);
	}

	/** Returns the value of the specified private field of the specified target object or its superclass
	 * 
	 * @return value of the specified private field of the specified target object or its superclass */
	public static Object getPrivateFieldOf(String fieldName, Object target) {
		try {
			Field field = getPrivateFieldOfClassOrSuperClass(fieldName, target.getClass());
			field.setAccessible(true);
			return field.get(target);
		} catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
			e.printStackTrace();
			throw new RuntimeException(
					"Could not access field " + fieldName + " on class " + target.getClass().getSimpleName());
		}
	}

	/** @return {@Field} specified by name, if present on the given class or any of its super-classes
	 * @throws {@link RuntimeException} if field could not be found or accessed */
	private static Field getPrivateFieldOfClassOrSuperClass(String fieldName, Class<?> clas) {
		try {
			return clas.getDeclaredField(fieldName);
		} catch (SecurityException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not access field " + fieldName + " on class " + clas.getSimpleName());
		} catch (NoSuchFieldException e) {
			Class<?> superClass = clas.getSuperclass();
			if (superClass != null) {
				return getPrivateFieldOfClassOrSuperClass(fieldName, superClass);
			} else {
				throw new RuntimeException("Could not find field " + fieldName + " on any super class.");
			}
		}
	}

	/** sets the given <b>private field</b> by name on the specified target (or one of its super-classes) to the given value; this
	 * works both on static and instance fields */
	public static void setPrivateField(String fieldName, Object target, Object value) {
		Field field = getPrivateFieldOfClassOrSuperClass(fieldName, target.getClass());
		try {
			field.setAccessible(true);
			field.set(target, value);
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not set field " + fieldName + " on class " + target.getClass().getSimpleName());
		}
	}
}