// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.testUtils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.List;
import org.mockito.AdditionalAnswers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;

public class MpiMock {
	/** @return new mocked {@link MpiManager}: thinks being root - all MPI functions return their given Bundle */
	@SuppressWarnings("rawtypes")
	public static MpiManager newMockMpi() {
		MpiManager mockedMpiManager = mock(MpiManager.class);
		when(mockedMpiManager.isRoot()).thenReturn(true);
		when(mockedMpiManager.getProcessCount()).thenReturn(1);
		when(mockedMpiManager.getRank()).thenReturn(0);
		when(mockedMpiManager.broadcast(any(Bundle.class), anyInt())).thenAnswer(AdditionalAnswers.returnsArgAt(0));
		when(mockedMpiManager.aggregateAll(any(Bundle.class), any(Tag.class))).thenAnswer(AdditionalAnswers.returnsArgAt(0));		
		when(mockedMpiManager.aggregateAt(any(Bundle.class), anyInt(), any(Tag.class))).thenAnswer(AdditionalAnswers.returnsArgAt(0)); 
		when(mockedMpiManager.individualAllToAll(any(), any(Tag.class))).thenAnswer(
			new Answer() {
				public Object answer(InvocationOnMock invocation) {
					List<Object> objects = invocation.getArgument(0);
					return objects.get(0);
				}
			});
		return mockedMpiManager;
	}
}