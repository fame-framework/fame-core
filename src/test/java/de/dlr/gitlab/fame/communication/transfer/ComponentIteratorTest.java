// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import org.junit.Before;
import org.junit.Test;

/** Tests for class {@link ComponentIterator} */
public class ComponentIteratorTest {
	ComponentIterator<Integer> iterator;
	ArrayList<Integer> list;

	@Before
	public void setUp() {
		list = new ArrayList<>();
		iterator = new ComponentIterator<Integer>();
		iterator.iterateOver(list);
	}

	@Test
	public void next_emptyList_throws() {
		assertThrowsMessage(NoSuchElementException.class, ComponentIterator.NO_MORE_ELEMENTS, () -> iterator.next());
	}

	@Test
	public void hasNext_empty_returnsFalse() {
		assertFalse(iterator.hasNext());
	}

	@Test
	public void hasNext_notEmpty_returnsTrue() {
		list.add(null);
		assertTrue(iterator.hasNext());
	}

	@Test
	public void hasNext_endOfList_returnsFalse() {
		add(1, 2, 3);
		iterator.next();
		iterator.next();
		iterator.next();
		assertFalse(iterator.hasNext());
	}

	/** adds given integers to list that backs the iterator */
	private void add(int... ints) {
		for (int i : ints) {
			list.add(i);
		}
	}

	@Test
	public void next_returnsElementsInOrder() {
		int[] order = new int[] {-6, 7, 9, 7, 8, 1000, -19, 0, 8, 0};
		add(order);
		for (int index = 0; index < order.length; index++) {
			assertEquals(order[index], (int) iterator.next());
		}
	}

	@Test
	public void next_endOfList_throws() {
		add(1, 2);
		iterator.next();
		iterator.next();
		assertThrowsMessage(NoSuchElementException.class, ComponentIterator.NO_MORE_ELEMENTS, () -> iterator.next());
	}

	@Test
	public void iterateOver_replacesList() {
		add(1, 2);
		iterator.iterateOver(Arrays.asList(7, 8));
		assertEquals(7, (int) iterator.next());
		assertEquals(8, (int) iterator.next());
	}

	@Test
	public void iterateOver_resetsReadingPosition() {
		add(1, 2);
		iterator.next();
		iterator.next();
		iterator.iterateOver(Arrays.asList(7, 8));
		assertEquals(7, (int) iterator.next());
		assertEquals(8, (int) iterator.next());
	}

	@Test
	public void previewNext_empty_throws() {
		assertThrowsMessage(NoSuchElementException.class, ComponentIterator.NO_MORE_ELEMENTS, () -> iterator.previewNext());
	}

	@Test
	public void previewNext_endOfList_throws() {
		add(1, 2);
		iterator.next();
		iterator.next();
		assertThrowsMessage(NoSuchElementException.class, ComponentIterator.NO_MORE_ELEMENTS, () -> iterator.previewNext());
	}

	@Test
	public void previewNext_returnsNext() {
		add(1, 2);
		assertEquals(1, (int) iterator.previewNext());
	}

	@Test
	public void previewNext_doesNotChangeReadingPosition() {
		add(1, 2);
		for (int index = 0; index < 100; index++) {
			assertEquals(1, (int) iterator.previewNext());
		}
	}
}
