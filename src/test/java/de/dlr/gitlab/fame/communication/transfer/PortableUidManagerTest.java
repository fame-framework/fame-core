// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.DummyBrokenPortable;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortablePrimitives;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableWithSubcomponents;
import de.dlr.gitlab.fame.communication.transfer.portablesB.DummyPortableSingleBoolean;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.LogChecker;

/** Tests for {@link PortableUidManager} */
public class PortableUidManagerTest {
	private PortableUidManager dataUidManager;
	private LogChecker logChecker = new LogChecker(PortableUidManager.class);

	@Before
	public void setUp() {
		dataUidManager = new PortableUidManager(asList("de.dlr.gitlab.fame.communication.transfer.portablesA"));
		logChecker.clear();
	}

	/** @return list of strings created from given arguments */
	private List<String> asList(String... strings) {
		return Arrays.asList(strings);
	}

	@Test
	public void testConstructor() {
		@SuppressWarnings("unchecked") HashMap<Class<? extends Portable>, Integer> mapClassToId = (HashMap<Class<? extends Portable>, Integer>) AccessPrivates
				.getPrivateFieldOf("mapClassToId", dataUidManager);
		assertTrue(mapClassToId.containsKey(DummyPortablePrimitives.class));
		assertTrue(mapClassToId.containsKey(DummyPortableWithSubcomponents.class));
		assertTrue(mapClassToId.containsKey(DummyPortableEmpty.class));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testSortClassByTypeName() {
		Set<Class<? extends Object>> setA = createHashSet(String.class, Composer.class, Portable.class, Integer.class,
				Math.class);
		ArrayList<Class<? extends Object>> result = PortableUidManager.sortClassTypesByName(setA);
		for (int i = 1; i < result.size(); i++) {
			String classNameBefore = result.get(i - 1).getName();
			String classNameAfter = result.get(i).getName();
			assertTrue(classNameAfter.compareTo(classNameBefore) > 0);
		}
	}

	@SuppressWarnings("unchecked")
	/** @return a HashSet with the given classes */
	private Set<Class<? extends Object>> createHashSet(Class<? extends Object>... classes) {
		HashSet<Class<? extends Object>> set = new HashSet<>();
		for (Class<? extends Object> clas : classes) {
			set.add(clas);
		}
		return set;
	}

	@Test
	public void testCreateClassToIdMap() {
		ArrayList<Class<? extends Object>> list = new ArrayList<>();
		list.add(String.class);
		list.add(Composer.class);
		list.add(Portable.class);
		list.add(Math.class);
		list.add(Integer.class);
		HashMap<Class<? extends Object>, Integer> result = PortableUidManager.createClassToIdMap(list);
		for (int i = 0; i < list.size(); i++) {
			Class<?> clas = list.get(i);
			int index = result.get(clas);
			assertEquals(i, index);
		}
	}

	@Test
	public void testCreateConstructorListNoConstructor() {
		ArrayList<Class<? extends Portable>> classes = new ArrayList<>();
		classes.add(DummyBrokenPortable.class);
		try {
			AccessPrivates.callPrivateMethodOn("createConstructorList", dataUidManager, classes);
			fail("Exception expected!");
		} catch (RuntimeException e) {
			assertEquals(PortableUidManager.NO_DEFAULT_CONSTRUCTOR + "DummyBrokenPortable", e.getMessage());
		}
	}

	@Test
	public void testGetTypeIdOfClass_UnknownClass() {
		try {
			dataUidManager.getTypeIdOfClass(DummyPortableSingleBoolean.class);
			fail("Exception expected!");
		} catch (RuntimeException e) {
			assertEquals(PortableUidManager.CLASS_NOT_KNOWN + DummyPortableSingleBoolean.class.getSimpleName(),
					e.getMessage());
		}
	}

	@Test
	public void testGetTypeIdOfClass_KnownClass_NoCrash() {
		dataUidManager = new PortableUidManager(asList("de.dlr.gitlab.fame.communication.transfer.portablesB"));
		dataUidManager.getTypeIdOfClass(DummyPortableSingleBoolean.class);
	}
}