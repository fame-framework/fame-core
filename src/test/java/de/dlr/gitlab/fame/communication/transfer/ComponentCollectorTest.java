// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem.Builder;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

/**
 * Tests for {@link ComponentCollector}
 *
 * @author Christoph Schimeczek
 */
public class ComponentCollectorTest {
	private Builder builder;
	private ComponentCollector componentCollector;

	@Before
	public void setup() {
		builder = NestedItem.newBuilder();
		componentCollector = new ComponentCollector(builder);
	}

	@Test
	public void testConstructor() {
		ComponentCollector componentCollector = new ComponentCollector(builder);
		Builder usedBuilder = (Builder) AccessPrivates.getPrivateFieldOf("builder", componentCollector);
		assertEquals(builder, usedBuilder);
	}

	@Test
	public void testStoreBooleans() {
		boolean[] input = new boolean[] {true, false, false, true};
		componentCollector.storeBooleans(input);
		List<Boolean> storedValues = builder.getBoolValuesList();
		for (int i = 0; i < input.length; i++) {
			assertEquals(input[i], storedValues.get(i));
		}
	}

	@Test
	public void testStoreInts() {
		int[] input = new int[] {1, 2, 3, 4, 5};
		componentCollector.storeInts(input);
		List<Integer> storedValues = builder.getIntValuesList();
		for (int i = 0; i < input.length; i++) {
			assertEquals(input[i], (int) storedValues.get(i));
		}
	}

	@Test
	public void testStoreLongs() {
		long[] input = new long[] {98723409812734904L, 321479810798309L, 98704597085L, 127908712094387L};
		componentCollector.storeLongs(input);
		List<Long> storedValues = builder.getLongValuesList();
		for (int i = 0; i < input.length; i++) {
			assertEquals(input[i], (long) storedValues.get(i));
		}
	}

	@Test
	public void testStoreFloats() {
		float[] input = new float[] {.1f, .2f, .0f, -4.2f};
		componentCollector.storeFloats(input);
		List<Float> storedValues = builder.getFloatValuesList();
		for (int i = 0; i < input.length; i++) {
			assertEquals(input[i], storedValues.get(i), 10 - 6);
		}
	}

	@Test
	public void testStoreDoubles() {
		double[] input = new double[] {.1d, .2d, .0d, -4.2d};
		componentCollector.storeDoubles(input);
		List<Double> storedValues = builder.getDoubleValuesList();
		for (int i = 0; i < input.length; i++) {
			assertEquals(input[i], storedValues.get(i), 10 - 14);
		}
	}

	@Test
	public void testStoreStrings() {
		String[] input = new String[] {"A", "BC", "DD", "1701D"};
		componentCollector.storeStrings(input);
		List<String> storedValues = builder.getStringValuesList();
		for (int i = 0; i < input.length; i++) {
			assertEquals(input[i], storedValues.get(i));
		}
	}

	@Test
	public void testStoreTimeSeries() {
		int[] ids = new int[] {4, 2, 1001};
		TimeSeries seriesA = mock(TimeSeries.class);
		TimeSeries seriesB = mock(TimeSeries.class);
		TimeSeries seriesC = mock(TimeSeries.class);
		when(seriesA.getId()).thenReturn(ids[0]);
		when(seriesB.getId()).thenReturn(ids[1]);
		when(seriesC.getId()).thenReturn(ids[2]);
		TimeSeries[] input = new TimeSeries[] {seriesA, seriesB, seriesC};

		componentCollector.storeTimeSeries(input);
		List<Integer> storedValues = builder.getTimeSeriesIdsList();
		for (int i = 0; i < input.length; i++) {
			assertEquals(ids[i], (int) storedValues.get(i));
		}
	}

	@Test
	public void testStoreComponent() {
		Portable[] input = new Portable[] {mock(Portable.class), mock(Portable.class), mock(Portable.class)};
		componentCollector.storeComponents(input);
		List<Portable> storedComponents = componentCollector.getSubComponents();
		for (int i = 0; i < input.length; i++) {
			assertEquals(input[i], storedComponents.get(i));
		}
	}
}