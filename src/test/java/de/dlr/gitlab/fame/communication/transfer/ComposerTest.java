// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortablePrimitives;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableWithSubcomponents;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;
import de.dlr.gitlab.fame.service.TimeSeriesProvider;
import de.dlr.gitlab.fame.service.TimeSeriesProvider.MissingSeriesException;
import de.dlr.gitlab.fame.testUtils.LogChecker;

/** Tests for {@link Composer}
 *
 * @author Christoph Schimeczek */
public class ComposerTest {
	private static final int COUNT = 3;
	private static final int HIERARCHY_LEVEL = 2;
	private static final String DUMMY_ERROR = "DUMMY_ERROR";
	public static final TimeSeries[] timeSeries = new TimeSeries[COUNT];

	private LogChecker logChecker = new LogChecker(Composer.class);
	private Composer composer;
	private PortableUidManager portableUidManager;

	@Before
	public void setup() throws MissingSeriesException {
		for (int i = 0; i < COUNT; i++) {
			timeSeries[i] = mock(TimeSeries.class);
			when(timeSeries[i].getId()).thenReturn(i);
		}
		prepareComposer(HIERARCHY_LEVEL);
		logChecker.clear();
	}

	private void prepareComposer(int hierarchyLevel) throws MissingSeriesException {
		TimeSeriesProvider tsProvider = mock(TimeSeriesProvider.class);
		when(tsProvider.getSeriesById(anyInt())).thenThrow(new MissingSeriesException(DUMMY_ERROR));
		for (int i = 0; i < COUNT; i++) {
			doReturn(timeSeries[i]).when(tsProvider).getSeriesById(i);
		}
		portableUidManager = new PortableUidManager(Arrays.asList("de.dlr.gitlab.fame.communication.transfer.portablesA"));
		composer = new Composer(portableUidManager, tsProvider, hierarchyLevel);
	}

	@Test
	public void testDecomposePrimitivesOnly() {
		DummyPortablePrimitives originalItem = new DummyPortablePrimitives(COUNT);
		NestedItem result = composer.decompose(originalItem);
		assertEquals(COUNT, result.getBoolValuesCount());
		assertEquals(COUNT + 1, result.getIntValuesCount()); // count-variable is transported as well
		assertEquals(COUNT, result.getLongValuesCount());
		assertEquals(COUNT, result.getFloatValuesCount());
		assertEquals(COUNT, result.getDoubleValuesCount());
		assertEquals(COUNT, result.getStringValuesCount());
		assertEquals(COUNT, result.getTimeSeriesIdsCount());
	}

	@Test
	public void testDecomposeSubcomponents() {
		DummyPortableWithSubcomponents originalItem = new DummyPortableWithSubcomponents(COUNT);
		NestedItem result = composer.decompose(originalItem);
		assertEquals(2, result.getComponentsCount());
	}

	@Test
	public void testDecomposeFailHierachyLevelExceeded() throws MissingSeriesException {
		prepareComposer(0);
		DummyPortableWithSubcomponents originalItem = new DummyPortableWithSubcomponents(COUNT);
		assertThrowsFatalMessage(Composer.MAX_DEPTH_EXCEEDED, () -> composer.decompose(originalItem));
		logChecker.assertLogsContain(Composer.MAX_DEPTH_EXCEEDED);
	}

	@Test
	public void testDecomposeHierarchyLevelExactMatch() throws MissingSeriesException {
		prepareComposer(0);
		DummyPortablePrimitives originalItem = new DummyPortablePrimitives(COUNT);
		composer.decompose(originalItem);
	}

	@Test
	public void testComposePrimitives() {
		NestedItem nestedItem = prepareNestedItemWithPrimivites(0);
		DummyPortablePrimitives result = (DummyPortablePrimitives) composer.compose(nestedItem);
		assertEquals(1, result.count);
	}

	private NestedItem prepareNestedItemWithPrimivites(int seriesId) {
		return NestedItem.newBuilder()
				.setDataTypeId(portableUidManager.getTypeIdOfClass(DummyPortablePrimitives.class))
				.addIntValues(1)
				.addBoolValues(true)
				.addIntValues(42)
				.addLongValues(420000000000000L)
				.addFloatValues(4.2f)
				.addDoubleValues(0.42d)
				.addStringValues("42")
				.addTimeSeriesIds(seriesId)
				.build();
	}

	@Test
	public void testComposeSubcomponents() {
		NestedItem nestedItem = prepareNestedItemSubcomponents();
		DummyPortableWithSubcomponents result = (DummyPortableWithSubcomponents) composer.compose(nestedItem);
		assertTrue(result.subcomponentA.getClass() == DummyPortablePrimitives.class);
		assertTrue(result.subcomponentB.getClass() == DummyPortablePrimitives.class);
	}

	private NestedItem prepareNestedItemSubcomponents() {
		return NestedItem.newBuilder()
				.setDataTypeId(portableUidManager.getTypeIdOfClass(DummyPortableWithSubcomponents.class))
				.addComponents(prepareNestedItemWithPrimivites(0))
				.addComponents(prepareNestedItemWithPrimivites(0))
				.build();
	}

	@Test
	public void testComposeMissingSeries() {
		int seriesId = COUNT + 10;
		NestedItem nestedItem = prepareNestedItemWithPrimivites(seriesId);
		assertThrowsFatalMessage(DUMMY_ERROR, () -> composer.compose(nestedItem));
		logChecker.assertLogsContain(DUMMY_ERROR);
	}
}