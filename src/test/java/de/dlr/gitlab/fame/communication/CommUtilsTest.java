// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;

public class CommUtilsTest {
	AutoCloseable closeable;

	@Before
	public void setUp() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void releaseMock() throws Exception {
		closeable.close();
	}

	@Test
	public void getExactlyOneEntry_null_throws() {
		assertThrowsFatalMessage(CommUtils.NOT_ONE_ENTRY, () -> CommUtils.getExactlyOneEntry(null));
	}

	@Test
	public void getExactlyOneEntry_empty_throws() {
		assertThrowsFatalMessage(CommUtils.NOT_ONE_ENTRY, () -> CommUtils.getExactlyOneEntry(new ArrayList<Double>()));
	}

	@Test
	public void getExactlyOneEntry_one_returnsEntry() {
		Object value = new Object();
		assertEquals(value, CommUtils.getExactlyOneEntry(Arrays.asList(value)));
	}

	@Test
	public void getExactlyOneEntry_moreThanOne_throws() {
		assertThrowsFatalMessage(CommUtils.NOT_ONE_ENTRY, () -> CommUtils.getExactlyOneEntry(Arrays.asList(5, 5)));
	}

	@Test
	public void extractMessagesFrom_null_returnsEmptyList() {
		assertTrue(CommUtils.extractMessagesFrom(null, 12L).isEmpty());
	}

	@Test
	public void extractMessageFrom_emptyList_returnsEmptyList() {
		assertTrue(CommUtils.extractMessagesFrom(makeMessagesFrom(), 12L).isEmpty());
	}

	/** @return new ArrayList of messages; one message from each given id */
	private ArrayList<Message> makeMessagesFrom(long... ids) {
		ArrayList<Message> list = new ArrayList<>();
		for (long id : ids) {
			Message message = mock(Message.class);
			when(message.getSenderId()).thenReturn(id);
			list.add(message);
		}
		return list;
	}

	@Test
	public void extractMessageFrom_notFound_returnsEmptyList() {
		ArrayList<Message> input = makeMessagesFrom(1, 2, 3);
		ArrayList<Message> result = CommUtils.extractMessagesFrom(input, 99L);
		assertTrue(result.isEmpty());
		assertEquals(3, input.size());
	}

	@Test
	public void extractMessageFrom_one_returnsOne() {
		ArrayList<Message> input = makeMessagesFrom(1, 2, 3);
		ArrayList<Message> result = CommUtils.extractMessagesFrom(input, 1L);
		assertEquals(1, result.size());
		assertEquals(2, input.size());
	}

	@Test
	public void extractMessageFrom_some_returnsMatching() {
		ArrayList<Message> input = makeMessagesFrom(1, 1, 3);
		ArrayList<Message> result = CommUtils.extractMessagesFrom(input, 1L);
		assertEquals(2, result.size());
		assertEquals(1, input.size());
	}

	@Test
	public void extractMessageFrom_allMatching_returnsAll() {
		ArrayList<Message> input = makeMessagesFrom(1, 1, 1);
		ArrayList<Message> result = CommUtils.extractMessagesFrom(input, 1L);
		assertEquals(3, result.size());
		assertEquals(0, input.size());
	}

	@Test
	public void extractMessagesWith_nullMessages_returnsEmptyList() {
		assertTrue(CommUtils.extractMessagesWith(null, DataItem.class).isEmpty());
	}

	@Test
	public void extractMessagesWith_emptyMessages_returnsEmptyList() {
		assertTrue(CommUtils.extractMessagesWith(new ArrayList<>(), DataItem.class).isEmpty());
	}

	@Test
	public void extractMessagesWith_nullType_throws() {
		assertThrowsFatalMessage(CommUtils.NO_TYPE,
				() -> CommUtils.extractMessagesWith(new ArrayList<>(), null));
	}

	@Test
	public void extractMessagesWith_notFound_returnsEmptyList() {
		ArrayList<Message> input = makeMessagesWith(DummyDataItem.class, false, false, false);
		ArrayList<Message> result = CommUtils.extractMessagesWith(input, DummyDataItem.class);
		assertEquals(0, result.size());
		assertEquals(3, input.size());
	}

	/** @return new List of Messages; each Message returning corresponding "haveItem" when asked containsType(given DataItem) */
	private ArrayList<Message> makeMessagesWith(Class<? extends DataItem> clas, boolean... haveItem) {
		ArrayList<Message> list = new ArrayList<>();
		for (boolean hasItem : haveItem) {
			Message message = mock(Message.class);
			when(message.containsType(clas)).thenReturn(hasItem);
			list.add(message);
		}
		return list;
	}

	@Test
	public void extractMessagesWith_oneMatching_returnsMatching() {
		ArrayList<Message> input = makeMessagesWith(DummyDataItem.class, false, true, false);
		ArrayList<Message> result = CommUtils.extractMessagesWith(input, DummyDataItem.class);
		assertEquals(1, result.size());
		assertEquals(2, input.size());
	}

	@Test
	public void extractMessagesWith_someMatching_returnsMatching() {
		ArrayList<Message> input = makeMessagesWith(DummyDataItem.class, false, true, true);
		ArrayList<Message> result = CommUtils.extractMessagesWith(input, DummyDataItem.class);
		assertEquals(2, result.size());
		assertEquals(1, input.size());
	}

	@Test
	public void extractMessagesWith_allMatching_returnsAll() {
		ArrayList<Message> input = makeMessagesWith(DummyDataItem.class, true, true, true);
		ArrayList<Message> result = CommUtils.extractMessagesWith(input, DummyDataItem.class);
		assertEquals(3, result.size());
		assertEquals(0, input.size());
	}

	@Test
	public void extractMessagesContaining_nullMessages_returnsEmptyList() {
		var result = CommUtils.extractMessagesContaining(null, Portable.class);
		assertTrue(result.isEmpty());
	}

	@Test
	public void extractMessagesContaining_emptyMessages_returnsEmptyList() {
		var result = CommUtils.extractMessagesContaining(Collections.emptyList(), Portable.class);
		assertTrue(result.isEmpty());
	}

	@Test
	public void extractMessagesContaining_nullType_throws() {
		assertThrowsFatalMessage(CommUtils.NO_TYPE,
				() -> CommUtils.extractMessagesContaining(Collections.emptyList(), null));
	}

	@Test
	public void extractMessagesContaining_notFound_returnsEmptyList() {
		ArrayList<Message> input = makeMessagesWithDummyPortable(false, false, false);
		ArrayList<Message> result = CommUtils.extractMessagesContaining(input, DummyPortableEmpty.class);
		assertTrue(result.isEmpty());
		assertEquals(3, input.size());
	}

	/** @return new List of Messages; each Message returning a {@link DummyPortableEmpty} if its corresponding 'haveItem' is true */
	private ArrayList<Message> makeMessagesWithDummyPortable(boolean... haveItem) {
		ArrayList<Message> list = new ArrayList<>();
		for (boolean hasItem : haveItem) {
			Message message = mock(Message.class);
			when(message.getFirstPortableItemOfType(DummyPortableEmpty.class))
					.thenReturn(hasItem ? mock(DummyPortableEmpty.class) : null);
			list.add(message);
		}
		return list;
	}

	@Test
	public void extractMessagesContaining_oneFound_extractsOne() {
		ArrayList<Message> input = makeMessagesWithDummyPortable(false, true, false);
		ArrayList<Message> result = CommUtils.extractMessagesContaining(input, DummyPortableEmpty.class);
		assertEquals(1, result.size());
		assertEquals(2, input.size());
	}
	
	@Test
	public void extractMessagesContaining_oneSome_extractsMatching() {
		ArrayList<Message> input = makeMessagesWithDummyPortable(true, true, false);
		ArrayList<Message> result = CommUtils.extractMessagesContaining(input, DummyPortableEmpty.class);
		assertEquals(2, result.size());
		assertEquals(1, input.size());
	}
	
	@Test
	public void extractMessagesContaining_allFound_extractsAll() {
		ArrayList<Message> input = makeMessagesWithDummyPortable(true, true, true);
		ArrayList<Message> result = CommUtils.extractMessagesContaining(input, DummyPortableEmpty.class);
		assertEquals(3, result.size());
		assertEquals(0, input.size());
	}
}
