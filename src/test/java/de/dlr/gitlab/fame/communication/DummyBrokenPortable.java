// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication;

import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;

/**
 * Class to simulate a missing public Constructor 
 *
 * @author Christoph Schimeczek
 */
public class DummyBrokenPortable implements Portable {

	private DummyBrokenPortable() {}
	
	@Override
	public void addComponentsTo(ComponentCollector collector) {}

	@Override
	public void populate(ComponentProvider provider) {}
}