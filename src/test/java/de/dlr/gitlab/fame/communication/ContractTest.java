// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.Collections;
import org.junit.Test;
import com.google.protobuf.ProtocolStringList;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.protobuf.Field.NestedField;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Test cases for class {@link Contract}
 * 
 * @author Christoph Schimeczek */
public class ContractTest {
	private enum Products {
		NoProduct, ProductA
	};

	private long defaultContractId = 65L;

	/** @return a mocked {@link ProtoContract} with the given parameters */
	public static ProtoContract mockProto(long senderId, long receiverId, String productName, long firstTime, long steps,
			long expiration, NestedField... fields) {
		ProtoContract proto = mock(ProtoContract.class);
		when(proto.getSenderId()).thenReturn(senderId);
		when(proto.getReceiverId()).thenReturn(receiverId);
		when(proto.getProductName()).thenReturn(productName);
		when(proto.getFirstDeliveryTime()).thenReturn(firstTime);
		when(proto.getDeliveryIntervalInSteps()).thenReturn(steps);
		when(proto.getExpirationTime()).thenReturn(expiration);
		when(proto.getFieldsCount()).thenReturn(fields.length);
		when(proto.getFieldsList()).thenReturn(Arrays.asList(fields));
		return proto;
	}

	@Test
	public void constructor_setsContractId() {
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 0);
		Contract contract = new Contract(mock, 21L, Products.ProductA);
		assertEquals(21L, contract.getContractId());
	}

	@Test
	public void constructor_setsProduct() {
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 0);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertEquals(Products.ProductA, contract.getProduct());
	}

	@Test
	public void constructor_setsSenderFromProto() {
		ProtoContract mock = mockProto(14L, 1, Products.ProductA.name(), 0, 0, 0);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertEquals(14L, contract.getSenderId());
	}

	@Test
	public void constructor_setsReceiverFromProto() {
		ProtoContract mock = mockProto(0, 23L, Products.ProductA.name(), 0, 0, 0);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertEquals(23L, contract.getReceiverId());
	}

	@Test
	public void constructor_setsFirstDeliveryTimeFromProto() {
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 99L, 0, 0);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertEquals(99L, contract.getFirstDeliveryTime().getStep());
	}

	@Test
	public void constructor_setsDeliveryIntervalFromProto() {
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 10L, 0);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertEquals(10L, contract.getDeliveryInterval().getSteps());
	}

	@Test
	public void constructor_setsExpirationTimeFromProto() {
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 100L);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertEquals(100L, contract.getExpirationTime().getStep());
	}

	@Test
	public void constructor_noFields_noPayload() {
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 100L);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertFalse(contract.hasPayload());
	}

	@Test
	public void constructor_withField_setsPayload() {
		NestedField mockField = mockField("A", Double.class);
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 100L, mockField);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertTrue(contract.hasPayload());
	}

	/** @return mock of NestedField with the given name */
	private NestedField mockField(String name, Class<?> valueType, NestedField... innerFields) {
		NestedField field = mock(NestedField.class);
		when(field.getFieldName()).thenReturn(name);
		when(field.getFieldsCount()).thenReturn(innerFields.length);
		when(field.getFieldsList()).thenReturn(Arrays.asList(innerFields));
		if (innerFields.length == 0) {
			if (valueType == Integer.class) {
				when(field.getIntValuesList()).thenReturn(Arrays.asList(1));
			} else if (valueType == Double.class) {
				when(field.getDoubleValuesList()).thenReturn(Arrays.asList(2.0));
			} else if (valueType == String.class) {
				ProtocolStringList list = mock(ProtocolStringList.class);
				when(list.get(0)).thenReturn("3");
				when(list.size()).thenReturn(1);
				when(field.getStringValuesList()).thenReturn(list);
			} else if (valueType == Long.class) {
				when(field.getLongValuesList()).thenReturn(Arrays.asList(4L));
			} else {
				throw new RuntimeException("Not allowed.");
			}
		}
		return field;
	}

	@Test
	public void constructor_setPlainFields() {
		NestedField mockFieldA = mockField("A", Double.class);
		NestedField mockFieldB = mockField("B", Double.class);
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 100L, mockFieldA, mockFieldB);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertTrue(contract.getPayload("A") != null);
		assertTrue(contract.getPayload("B") != null);
	}

	@Test
	public void constructor_setNestedFields() {
		NestedField mockFieldOuter = mockField("Outer", Object.class,
				mockField("Inner", Object.class, mockField("A", Double.class)));
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 100L, mockFieldOuter);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertTrue(contract.getPayload("Outer.Inner.A") != null);
	}

	@Test
	public void constructor_payloadIsTimeSeries_throws() {
		NestedField field = mock(NestedField.class);
		when(field.getFieldName()).thenReturn("Irrelevant");
		when(field.getFieldsCount()).thenReturn(0);
		when(field.getFieldsList()).thenReturn(Collections.emptyList());
		when(field.hasSeriesId()).thenReturn(true);
		when(field.getSeriesId()).thenReturn(0);
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 100L, field);
		assertThrowsFatalMessage(Contract.SERIES_NOT_ALLOWED, () -> new Contract(mock, 99L, Products.NoProduct));
	}

	@Test
	public void constructor_payloadFieldAmbiguous_throws() {
		NestedField field = mock(NestedField.class);
		when(field.getFieldName()).thenReturn("Irrelevant");
		when(field.getFieldsCount()).thenReturn(0);
		when(field.getFieldsList()).thenReturn(Collections.emptyList());
		when(field.getIntValuesList()).thenReturn(Arrays.asList(1));
		when(field.getDoubleValuesList()).thenReturn(Arrays.asList(2.0));
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 100L, field);
		assertThrowsFatalMessage(Contract.INCORRECT_PAYLOAD, () -> new Contract(mock, 99L, Products.NoProduct));
	}

	@Test
	public void getPayload_noPayload_returnsEmptyList() {
		ProtoContract mock = mockProto(0, 1, Products.ProductA.name(), 0, 0, 100L);
		Contract contract = new Contract(mock, 0, Products.ProductA);
		assertTrue(contract.getPayload("MissingPath").isEmpty());
	}

	@Test
	public void getNextTimeOfDeliveryAfter_beforeFirstDelivery_returnsFirstDelivery() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 100L, 1000L);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(3L));
		assertEquals(10L, nextDeliveryTime.getStep());
	}

	/** @return new Contract with {@link #defaultContractId} and parameters as specified */
	private Contract buildContract(long senderId, long receiverId, Enum<?> product, long firstTime, long steps,
			long expiration, NestedField... fields) {
		ProtoContract mock = mockProto(senderId, receiverId, product.name(), firstTime, steps, expiration, fields);
		return new Contract(mock, defaultContractId, product);
	}

	@Test
	public void getNextTimeOfDeliveryAfter_atFirstDelivery_returnsNextDelivery() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 100L, 1000L);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(10L));
		assertEquals(110L, nextDeliveryTime.getStep());
	}

	@Test
	public void getNextTimeOfDeliveryAfter_betweenDeliveries_returnsClosestNextDelivery() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 100L, 1000L);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(50L));
		assertEquals(110L, nextDeliveryTime.getStep());
	}

	@Test
	public void getNextTimeOfDeliveryAfter_nextIsBeyondExpiration_returnsEndOfTime() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 100L, 1000L);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(950L));
		assertEquals(TimeStamp.LATEST.getStep(), nextDeliveryTime.getStep());
	}

	@Test
	public void getNextTimeOfDeliveryAfter_nextIsOnExpiration_returnsExpirationTime() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 100L, 1010L);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(950L));
		assertEquals(1010L, nextDeliveryTime.getStep());
	}

	@Test
	public void getNextTimeOfDeliveryAfter_onFirstDeliveryNextAfterExpiration_returnsEndofTime() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 100L, 100L);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(10L));
		assertEquals(TimeStamp.LATEST.getStep(), nextDeliveryTime.getStep());
	}

	@Test
	public void isSender_matchesSenderId_returnTrue() {
		Contract contract = buildContract(42L, 1L, Products.NoProduct, 10L, 100L, 100L);
		assertTrue(contract.isSender(42L));
	}

	@Test
	public void isSender_matchesSenderIdAndReceiver_returnTrue() {
		Contract contract = buildContract(42L, 42L, Products.NoProduct, 10L, 100L, 100L);
		assertTrue(contract.isSender(42L));
	}

	@Test
	public void isSender_notMatchesSender_returnFalse() {
		Contract contract = buildContract(42L, 0L, Products.NoProduct, 10L, 100L, 100L);
		assertFalse(contract.isSender(0L));
	}

	@Test
	public void isActiveAt_beforeFirstDelivery_returnFalse() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 1L, 1000L);
		assertFalse(contract.isActiveAt(new TimeStamp(9L)));
	}

	@Test
	public void isActiveAt_atFirstDelivery_returnTrue() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 1L, 1000L);
		assertTrue(contract.isActiveAt(new TimeStamp(10L)));
	}

	@Test
	public void isActiveAt_beforeExpiration_returnTrue() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 1L, 1000L);
		assertTrue(contract.isActiveAt(new TimeStamp(999L)));
	}

	@Test
	public void isActiveAt_onExpiration_returnTrue() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 1L, 1000L);
		assertTrue(contract.isActiveAt(new TimeStamp(1000L)));
	}

	@Test
	public void isActiveAt_afterExpiration_returnFalse() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 10L, 1L, 1000L);
		assertFalse(contract.isActiveAt(new TimeStamp(1001L)));
	}

	@Test
	public void fulfilAfter_deliveryTime_MatchesNextDeliveryTime() {
		TimeStamp now = new TimeStamp(15L);
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 0L, 10L, 20L);
		ContractData result = contract.fulfilAfter(now);
		assertEquals(contract.getNextTimeOfDeliveryAfter(now).getStep(), result.deliveryTime.getStep());
	}

	@Test
	public void fulfilAfter_contractId_isOwnContractId() {
		TimeStamp now = new TimeStamp(15L);
		Contract contract = buildContract(0L, 0L, Products.NoProduct, 0L, 10L, 20L);
		ContractData result = contract.fulfilAfter(now);
		assertEquals(defaultContractId, result.contractId);
	}

	@Test
	public void toString_correctFormat() {
		Contract contract = buildContract(0L, 14L, Products.ProductA, 0L, 10L, 20L);
		assertEquals("Agent 0-->14 <ProductA> [steps 0:20 each 10]", contract.toString());
	}
}