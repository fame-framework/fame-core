// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.stats;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.util.AgentInteractionStatistic;

public class FullTrackingTest {
	private FullTracking tracking;
	private AgentInteractionStatistic mockStatisticA;
	private AgentInteractionStatistic mockStatisticB;

	@Before
	public void setUp() {
		mockStatisticA = mock(AgentInteractionStatistic.class);
		mockStatisticB = mock(AgentInteractionStatistic.class);
		tracking = new FullTracking(mockStatisticA, mockStatisticB);
	}

	@Test
	public void getStatsJson_combinesUnderlyingStatistics() {
		String contentA = "{SomeContent}";
		String contentB = "{OtherContent}";
		when(mockStatisticA.getJSON()).thenReturn(contentA);
		when(mockStatisticB.getJSON()).thenReturn(contentB);
		String result = tracking.getStatsJson();
		String expected = String.format("{%s,%s}", contentA, contentB);
		assert expected.equals(result);
	}

	@Test
	public void assessMessage_tracksSenderReceiverPair() {
		tracking.assessMessage(createProtoMessage(1, 2, 0));
		verify(mockStatisticA, times(1)).recordInteraction(1, 2, 1);
	}

	/** @return mocked ProtoMessage with senderId, receiverId and given SerialisedSize */
	private ProtoMessage createProtoMessage(long senderId, long receiverId, int sizeInBytes) {
		ProtoMessage message = mock(ProtoMessage.class);
		when(message.getSenderId()).thenReturn(senderId);
		when(message.getReceiverId()).thenReturn(receiverId);
		when(message.getSerializedSize()).thenReturn(sizeInBytes);
		return message;
	}

	@Test
	public void assessMessage_tracksSize() {
		tracking.assessMessage(createProtoMessage(1, 2, 10));
		verify(mockStatisticB, times(1)).recordInteraction(1, 2, 10);
	}
}
