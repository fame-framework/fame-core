// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.message;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;
import de.dlr.gitlab.fame.communication.transfer.Composer;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortablePrimitives;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;
import de.dlr.gitlab.fame.testUtils.LogChecker;

/** Tests for {@link MessageBuilder}
 * 
 * @author Christoph Schimeczek */
public class MessageBuilderTest {
	private long senderId = 42L;
	private long receiverId = 21L;
	private MessageBuilder builder;
	private ArrayList<DataItem> dataItems;
	private ArrayList<Portable> portables;
	private LogChecker logChecker = new LogChecker(MessageBuilder.class);
	private Composer mockComposer;

	@Before
	public void setup() {
		mockComposer = mock(Composer.class);
		var packageNames = Arrays.asList("de.dlr.gitlab.fame.communication.transfer");
		dataItems = new ArrayList<>();
		portables = new ArrayList<>();
		builder = new MessageBuilder(mockComposer, packageNames, dataItems, portables);

		builder.setReceiverId(receiverId).setSenderId(senderId);
		dataItems.add(new DummyDataItem());
		portables.add(new DummyPortableEmpty());
		logChecker.clear();
	}

	@Test
	public void add_dataItem_added() {
		Signal signal = new Signal(0);
		builder.add(signal);
		assertTrue(dataItems.contains(signal));
	}

	@Test
	public void add_nullDataItem_nothingAdded() {
		dataItems.clear();
		builder.add((DataItem) null);
		assertTrue(dataItems.isEmpty());
	}

	@Test
	public void add_portable_added() {
		Portable dummy = new DummyPortablePrimitives();
		builder.add(dummy);
		assertTrue(portables.contains(dummy));
	}

	@Test
	public void add_nullPortable_nothingAdded() {
		portables.clear();
		builder.add((Portable) null);
		assertTrue(portables.isEmpty());
	}

	@Test
	public void add_duplicateDataItemType_throws() {
		assertThrowsFatalMessage(MessageBuilder.ERR_TYPE_ALREADY_EXISTS, () -> builder.add(new DummyDataItem()));
		logChecker.assertLogsContain(MessageBuilder.ERR_TYPE_ALREADY_EXISTS);
	}

	@Test
	public void build_missingSender_throws() {
		builder.clear();
		builder.setReceiverId(1L).add(new DummyDataItem());
		assertThrowsFatalMessage(MessageBuilder.ERR_INCOMPLETE, () -> builder.build());
	}

	@Test
	public void build_missingReceiver_throws() {
		builder.clear();
		builder.setSenderId(0L).add(new DummyDataItem());
		assertThrowsFatalMessage(MessageBuilder.ERR_INCOMPLETE, () -> builder.build());
	}

	@Test
	public void build_missingDataItem_throws() {
		builder.clear();
		builder.setSenderId(0L).setReceiverId(1L);
		assertThrowsFatalMessage(MessageBuilder.ERR_INCOMPLETE, () -> builder.build());
	}

	@Test
	public void build_incomplete_logsError() {
		builder.clear();
		assertThrowsFatalMessage(MessageBuilder.ERR_INCOMPLETE, () -> builder.build());
		logChecker.assertLogsContain(MessageBuilder.ERR_INCOMPLETE);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void build_complete_containsAllData() {
		var item = mock(NestedItem.class);
		when(mockComposer.decompose(any(DummyPortableEmpty.class))).thenReturn(item);
		when(mockComposer.getIdOfDataType(any(Class.class))).thenReturn(0);
		when(item.getDataTypeId()).thenReturn(0);
		Message message = builder.build();
		assertEquals(senderId, message.senderId);
		assertEquals(receiverId, message.receiverId);
		ArrayList<DummyPortableEmpty> portableItems = message.getAllPortableItemsOfType(DummyPortableEmpty.class);
		assertEquals(portables.size(), portableItems.size());
	}

	@Test
	public void clear_removesStoredObjects() {
		builder.add(new DummyPortableEmpty());
		builder.clear();
		assertTrue(portables.isEmpty());
		assertTrue(dataItems.isEmpty());
	}

	@Test
	public void clear_resetsSenderAndReceiver() {
		builder.clear();
		assertEquals(Long.MIN_VALUE, builder.getSender());
		assertEquals(Long.MIN_VALUE, builder.getReceiver());
	}

	@Test
	public void setSender_getSender_equals() {
		builder.setSenderId(42L);
		assertEquals(42L, builder.getSender());
	}

	@Test
	public void setReceiver_getReceiver_equals() {
		builder.setReceiverId(42L);
		assertEquals(42L, builder.getReceiver());
	}
}