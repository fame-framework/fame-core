// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.message;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.DummyBrokenDataItem;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.reflection.DummyDataItemB;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

/** Tests for {@link DataItem}
 * 
 * @author Christoph Schimeczek */
public class DataItemTest {
	private class Dummy {
		@SuppressWarnings("unused")
		public Dummy() {}
	}

	@Before
	public void setUp() {
		DataItem.setDataItemClasses(Collections.emptyList());
	}

	@Test
	public void mapIdsToConstructors_missingConstructorFromProtoData_throws() {
		DummyDataItem ddi = new DummyDataItem();
		@SuppressWarnings("unchecked") HashMap<Class<?>, Integer> map = (HashMap<Class<?>, Integer>) AccessPrivates
				.getPrivateFieldOf("mapClassToId", ddi);
		map.put(Dummy.class, -1);
		assertThrowsFatalMessage(DataItem.ERR_MISSING_CONSTRUCTOR,
				() -> AccessPrivates.callMethodOfClassXOnY("mapIdsToConstructors", DataItem.class, ddi));
		map.remove(Dummy.class);
	}

	@Test
	public void getProtobuf_unknownClass_throws() {
		DummyBrokenDataItem ddi = new DummyBrokenDataItem();
		assertThrowsFatalMessage(DataItem.CLASS_NOT_KNOWN, () -> ddi.getProtobuf());
	}

	@Test
	public void constructFromDataAndId_unassociatedId_throws() throws NoSuchMethodException, SecurityException {
		DummyDataItem ddi = new DummyDataItem();
		@SuppressWarnings("unchecked") HashMap<Integer, Constructor<?>> map = (HashMap<Integer, Constructor<?>>) AccessPrivates
				.getPrivateFieldOf("mapIdToConstructor", ddi);
		Constructor<?> constructor = Dummy.class.getConstructor(DataItemTest.class);
		map.put(-1, constructor);
		ProtoDataItem data = ProtoDataItem.newBuilder().setDataTypeId(-1).build();
		assertThrowsFatalMessage(DataItem.ERR_CREATION_FAILED,
				() -> AccessPrivates.callMethodOfClassXOnY("constructFromDataAndId", DataItem.class, ddi, data, -1));
		map.remove(-1);
	}

	@Test
	public void getIdOfType_unknownClass_throws() {
		assertThrowsFatalMessage(DataItem.CLASS_NOT_KNOWN, () -> DataItem.getIdOfType(DummyDataItemB.class));
	}
}