// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.Before;
import org.junit.Test;
import com.google.protobuf.ByteString;
import com.google.protobuf.ProtocolStringList;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortablePrimitives;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableWithSubcomponents;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;

/** Tests for {@link ComponentProvider}
 *
 * @author Christoph Schimeczek */
public class ComponentProviderTest {
	private class StringList extends ArrayList<String> implements ProtocolStringList {
		private static final long serialVersionUID = 1L;

		/** Not implemented */
		@Override
		public List<ByteString> asByteStringList() {
			return null;
		}

		StringList(String[] input) {
			for (String s : input) {
				add(s);
			}
		}
	}

	private ComponentProvider provider;
	private Boolean[] boolInput = new Boolean[] {true, true, false, true};
	private Integer[] intInput = new Integer[] {1, 6, 2, 3};
	private Long[] longInput = new Long[] {1L, 23987729384L, 19019190910L, 44L};
	private Float[] floatInput = new Float[] {0f, -6f, 11f, 42.42f};
	private Double[] doubleInput = new Double[] {-0d, -6d, 11d, 42.42d};
	private String[] stringInput = new String[] {"Da", "Lai", "Lam", "A"};
	private TimeSeries[] timeSeriesInput = new TimeSeries[] {mock(TimeSeries.class), mock(TimeSeries.class),
			mock(TimeSeries.class)};
	private Portable[] portableInput = new Portable[] {new DummyPortableWithSubcomponents(),
			new DummyPortableWithSubcomponents(), new DummyPortableEmpty(), new DummyPortableWithSubcomponents()};

	@Before
	public void setUp() {
		provider = new ComponentProvider();

		NestedItem item = mock(NestedItem.class);
		when(item.getBoolValuesList()).thenReturn(Arrays.asList(boolInput));
		when(item.getIntValuesList()).thenReturn(Arrays.asList(intInput));
		when(item.getLongValuesList()).thenReturn(Arrays.asList(longInput));
		when(item.getFloatValuesList()).thenReturn(Arrays.asList(floatInput));
		when(item.getDoubleValuesList()).thenReturn(Arrays.asList(doubleInput));
		when(item.getStringValuesList()).thenReturn(new StringList(stringInput));

		provider.setPrimitives(item);
		provider.setTimeSeriesArray(Arrays.asList(timeSeriesInput));
		provider.setComponentArray(Arrays.asList(portableInput));
	}

	@Test
	public void nextBoolean_returnsValuesInOrder() {
		for (int i = 0; i < boolInput.length; i++) {
			assertEquals(boolInput[i], provider.nextBoolean());
		}
	}

	public void nextBoolean_noMoreElement_throws() {
		for (int i = 0; i < boolInput.length; i++) {
			provider.nextBoolean();
		}
		assertThrowsMessage(NoSuchElementException.class, "", () -> provider.nextBoolean());
	}

	@Test
	public void nextInt_returnsValuesInOrder() {
		for (int i = 0; i < intInput.length; i++) {
			assertEquals((int) intInput[i], provider.nextInt());
		}
	}

	public void nextInt_noMoreElement_throws() {
		for (int i = 0; i < intInput.length; i++) {
			provider.nextInt();
		}
		assertThrowsMessage(NoSuchElementException.class, "", () -> provider.nextInt());
	}

	@Test
	public void nextLong_returnsValuesInOrder() {
		for (int i = 0; i < longInput.length; i++) {
			assertEquals((long) longInput[i], provider.nextLong());
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void nextLong_noMoreElement_throws() {
		for (int i = 0; i < longInput.length; i++) {
			provider.nextLong();
		}
		provider.nextLong();
	}

	@Test
	public void nextFloat_returnsValuesInOrder() {
		for (int i = 0; i < floatInput.length; i++) {
			assertEquals(floatInput[i], provider.nextFloat(), 1E-6);
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void nextFloat_noMoreElement_throws() {
		for (int i = 0; i < floatInput.length; i++) {
			provider.nextFloat();
		}
		provider.nextFloat();
	}

	@Test
	public void nextDouble_returnsValuesInOrder() {
		for (int i = 0; i < doubleInput.length; i++) {
			assertEquals(doubleInput[i], provider.nextDouble(), 1E-14);
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void nextDouble_noMoreElement_throws() {
		for (int i = 0; i < doubleInput.length; i++) {
			provider.nextDouble();
		}
		provider.nextDouble();
	}

	@Test
	public void nextString_returnsValuesInOrder() {
		for (int i = 0; i < stringInput.length; i++) {
			assertEquals(stringInput[i], provider.nextString());
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void nextString_noMoreElement_throws() {
		for (int i = 0; i < stringInput.length; i++) {
			provider.nextString();
		}
		provider.nextString();
	}

	@Test
	public void nextTimeSeries_returnsValuesInOrder() {
		for (int i = 0; i < timeSeriesInput.length; i++) {
			assertEquals(timeSeriesInput[i], provider.nextTimeSeries());
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void nextTimeSeries_noMoreElement_throws() {
		for (int i = 0; i < timeSeriesInput.length; i++) {
			provider.nextTimeSeries();
		}
		provider.nextTimeSeries();
	}

	@Test
	public void hasNextComponent_hasComponent_returnsTrue() {
		for (int i = 0; i < portableInput.length; i++) {
			assertTrue(provider.hasNextComponent());
			provider.nextComponent(Portable.class);
		}
	}

	public void hasNextComponent_noMoreComponent_returnsFalse() {
		for (int i = 0; i < portableInput.length; i++) {
			provider.nextComponent(Portable.class);
		}
		assertFalse(provider.hasNextComponent());
	}

	@Test
	public void hasNextComponent_exactMatchOfClass_returnsTrue() {
		for (int i = 0; i < portableInput.length; i++) {
			Class<? extends Portable> targetClass = portableInput[i].getClass();
			assertTrue(provider.hasNextComponent(targetClass));
			provider.nextComponent(targetClass);
		}
	}

	@Test
	public void hasNextComponent_noMatchOfClass_returnsFalse() {
		for (int i = 0; i < portableInput.length; i++) {
			Class<? extends Portable> targetClass = portableInput[i].getClass();
			assertFalse(provider.hasNextComponent(DummyPortablePrimitives.class));
			provider.nextComponent(targetClass);
		}
	}

	@Test
	public void hasNextComponent_castableClass_returnsTrue() {
		for (int i = 0; i < portableInput.length; i++) {
			Class<? extends Portable> targetClass = portableInput[i].getClass();
			assertTrue(provider.hasNextComponent(Portable.class));
			provider.nextComponent(targetClass);
		}
	}

	@Test
	public void hasNextComponent_noComponent_returnsFalse() {
		for (int i = 0; i < portableInput.length; i++) {
			provider.nextComponent(Portable.class);
		}
		assertFalse(provider.hasNextComponent(Portable.class));
	}

	@Test
	public void nextComponentList_mixedList_cutOffByOtherElement() {
		ArrayList<DummyPortableWithSubcomponents> result = provider.nextComponentList(DummyPortableWithSubcomponents.class);
		assertEquals(2, result.size());
		assertEquals(portableInput[0], result.get(0));
		assertEquals(portableInput[1], result.get(1));
	}

	@Test
	public void nextComponentList_wrongType_returnsEmptyList() {
		provider.nextComponent(Portable.class);
		provider.nextComponent(Portable.class);
		ArrayList<DummyPortableWithSubcomponents> result = provider.nextComponentList(DummyPortableWithSubcomponents.class);
		assertTrue(result.isEmpty());
	}

	@Test
	public void nextComponentList_noMoreElements_returnsEmptyList() {
		provider.nextComponent(Portable.class);
		provider.nextComponent(Portable.class);
		provider.nextComponent(Portable.class);
		provider.nextComponent(Portable.class);
		ArrayList<DummyPortableWithSubcomponents> result = provider.nextComponentList(DummyPortableWithSubcomponents.class);
		assertTrue(result.isEmpty());
	}
}