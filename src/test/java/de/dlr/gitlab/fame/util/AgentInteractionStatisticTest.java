// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.util;

import org.junit.Before;
import org.junit.Test;

public class AgentInteractionStatisticTest {
	private static final String NAME = "MyStatsName";
	private AgentInteractionStatistic statistics;

	@Before
	public void setUp() {
		statistics = new AgentInteractionStatistic(NAME);
	}

	@Test
	public void getJSON_noData_returnsValidJson() {
		String result = statistics.getJSON();
		String expected = String.format("\"%s\":{}", NAME);
		assert expected.equals(result);
	}

	@Test
	public void recordInteraction_initialCall_setsInitialValue() {
		statistics.recordInteraction(1, 2, 5);
		String result = statistics.getJSON();
		assert result.contains("\":{\"1\":{\"2\":5}");
	}

	@Test
	public void recordInteraction_multipleCallsSamePair_addsUp() {
		statistics.recordInteraction(1, 2, 5);
		statistics.recordInteraction(1, 2, 15);
		String result = statistics.getJSON();
		assert result.contains("\":{\"1\":{\"2\":20}");
	}

	@Test
	public void recordInteraction_multipleCallsDifferentPair_separateRecords() {
		statistics.recordInteraction(1, 2, 5);
		statistics.recordInteraction(1, 3, 15);
		String result = statistics.getJSON();
		assert result.contains("\":{\"1\":{\"2\":5,\"3\":15}}");
	}

	@Test
	public void recordInteraction_pairsNotInOrder_recordsInOrder() {
		statistics.recordInteraction(2, 3, 15);
		statistics.recordInteraction(1, 5, 7);
		statistics.recordInteraction(1, 2, 3);
		String result = statistics.getJSON();
		assert result.contains("\":{\"1\":{\"2\":3,\"5\":7},\"2\":{\"3\":15}}");
	}
}
