// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.input.Make.Type;

public class MakeTest {

	private enum DummyEnum {
		A, B, C
	}

	@Test
	public void newGroup_hasGivenName() {
		String name = "MyGroup";
		GroupBuilder builder = Make.newGroup(name);
		assertEquals(name, builder.getName());
	}

	@Test
	public void newTree_hasNoName() {
		GroupBuilder builder = Make.newTree();
		assertNull(builder.getName());
	}

	@Test
	public void newInt_hasGivenNameAndCorrectType() {
		String name = "MyParameterName";
		ParameterBuilder builder = Make.newInt(name);
		assertEquals(name, builder.getName());
		assertEquals(Type.INTEGER, builder.getType());
	}

	@Test
	public void newDouble_hasGivenNameAndCorrectType() {
		String name = "MyParameterName";
		ParameterBuilder builder = Make.newDouble(name);
		assertEquals(name, builder.getName());
		assertEquals(Type.DOUBLE, builder.getType());
	}

	@Test
	public void newString_hasGivenNameAndCorrectType() {
		String name = "MyParameterName";
		ParameterBuilder builder = Make.newString(name);
		assertEquals(name, builder.getName());
		assertEquals(Type.STRING, builder.getType());
	}

	@Test
	public void newStringSet_hasGivenNameAndCorrectType() {
		String name = "MyParameterName";
		ParameterBuilder builder = Make.newStringSet(name);
		assertEquals(name, builder.getName());
		assertEquals(Type.STRINGSET, builder.getType());
	}

	@Test
	public void newSeries_hasGivenNameAndCorrectType() {
		String name = "MyParameterName";
		ParameterBuilder builder = Make.newSeries(name);
		assertEquals(name, builder.getName());
		assertEquals(Type.TIMESERIES, builder.getType());
	}

	@Test
	public void newLong_hasGivenNameAndCorrectType() {
		String name = "MyParameterName";
		ParameterBuilder builder = Make.newLong(name);
		assertEquals(name, builder.getName());
		assertEquals(Type.LONG, builder.getType());
	}

	@Test
	public void newTimeStamp_hasGivenNameAndCorrectType() {
		String name = "MyParameterName";
		ParameterBuilder builder = Make.newTimeStamp(name);
		assertEquals(name, builder.getName());
		assertEquals(Type.TIMESTAMP, builder.getType());
	}

	@Test
	public void newEnum_hasGivenNameAndCorrectType() {
		String name = "MyParameterName";
		ParameterBuilder builder = Make.newEnum(name, DummyEnum.class);
		assertEquals(name, builder.getName());
		assertEquals(Type.ENUM, builder.getType());
		assertArrayEquals(DummyEnum.values(), builder.getAllowedValues());
	}
}
