// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedConstruction;

/** Tests for {@link GroupBuilder}
 *
 * @author Christoph Schimeczek */
public class GroupBuilderTest {
	private GroupBuilder builder;
	private MockedConstruction<Group> createdGroupMock;
	private MockedConstruction<Tree> createdTreeMock;
	private List<TreeElement> innerElements;

	@Before
	public void setUp() {
		builder = new GroupBuilder();
		createdGroupMock = mockConstruction(Group.class);
		createdTreeMock = mockConstruction(Tree.class);
		innerElements = new ArrayList<>();
	}

	@After
	public void closeMocks() {
		createdGroupMock.close();
		createdTreeMock.close();
	}

	@Test
	public void add_nothing_returnsBuilder() {
		assertEquals(builder.add(), builder);
	}

	@Test
	public void add_nameNotUnique_throws() {
		builder.add(mockElement("NameA"), mockElement("OtherName"));
		assertThrowsFatalMessage(GroupBuilder.NAME_NOT_UNIQUE, () -> builder.add(mockElement("NameA")));
	}

	/** @return new mocked {@link ElementOrBuilder} that returns given String for getName() */
	private ElementOrBuilder mockElement(String name) {
		ElementOrBuilder element = mock(ElementOrBuilder.class);
		when(element.getName()).thenReturn(name);
		TreeElement actualElement = mock(TreeElement.class);
		innerElements.add(actualElement);
		when(element.get()).thenReturn(actualElement);
		return element;
	}

	@SuppressWarnings("unchecked")
	@Test
	public void add_multiple_addsAll() {
		builder.add(mockElement("NameA"), mockElement("NameB"));
		Group group = builder.build();
		verify(group, times(1)).setInnerElements(any(ArrayList.class));
		verify(innerElements.get(0)).setParent(group);
		verify(innerElements.get(1)).setParent(group);
	}

	@Test
	public void addAs_nameNotUnqiue_throws() {
		builder.add(mockElement("NameA"), mockElement("OtherName"));
		TreeElement element = mock(TreeElement.class);
		assertThrowsFatalMessage(GroupBuilder.NAME_NOT_UNIQUE, () -> builder.addAs("NameA", element));
	}

	@Test
	public void addAs_nameNull_throws() {
		TreeElement element = mock(TreeElement.class);
		assertThrowsFatalMessage(ElementBuilder.NAME_NEEDED, () -> builder.addAs(null, element));
	}

	@Test
	public void addAs_nameEmpty_throws() {
		TreeElement element = mock(TreeElement.class);
		assertThrowsFatalMessage(ElementBuilder.NAME_NEEDED, () -> builder.addAs("", element));
	}

	@Test
	public void addAs_valid_addsCopy() {
		TreeElement innerGroup = mock(TreeElement.class);
		TreeElement copiedInnerGroup = mock(TreeElement.class);
		when(innerGroup.deepCopy()).thenReturn(copiedInnerGroup);
		when(copiedInnerGroup.get()).thenReturn(copiedInnerGroup);
		builder.addAs("NewName", innerGroup);
		verify(copiedInnerGroup).setName("NewName");
		Group group = builder.build();
		verify(copiedInnerGroup).setParent(group);
	}

	@Test
	public void buildTree() {
		builder.setName("SomeName");
		builder.list();
		builder.optional();
		builder.add(mockElement("NameA"), mockElement("NameB"));
		Tree tree = builder.buildTree();
		verify(tree).setName(null);
		verify(tree).setIsList(true);
		verify(tree).setIsOptional(true);
		verify(innerElements.get(0)).setParent(tree);
		verify(innerElements.get(1)).setParent(tree);
	}
}
