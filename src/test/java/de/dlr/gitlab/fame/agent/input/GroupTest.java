// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/** Tests for {@link Group}
 *
 * @author Christoph Schimeczek */
public class GroupTest {
	private List<TreeElement> innerElements;
	private List<TreeElement> elementCopies;
	private Group group;

	@Before
	public void setUp() {
		group = new Group();
		innerElements = new ArrayList<>();
		group.setInnerElements(innerElements);
		elementCopies = new ArrayList<>();
	}

	@Test
	public void setInnerElements_getInnerElements_returnsAddedElements() {
		group = new Group();
		innerElements = new ArrayList<>();
		TreeElement elementA = mockElement();
		TreeElement elementB = mockElement();
		TreeElement elementC = mockElement();
		innerElements.add(elementA);
		innerElements.add(elementB);
		innerElements.add(elementC);
		group.setInnerElements(innerElements);
		List<TreeElement> result = group.getInnerElements();
		assertTrue(result.contains(elementA));
		assertTrue(result.contains(elementB));
		assertTrue(result.contains(elementC));
		assertEquals(3, result.size());
	}

	@Test
	public void deepCopy_NoInnerElements() {
		Group copy = group.deepCopy();
		assertNotEquals(group, copy);
	}

	@Test
	public void deepCopy() {
		addInnerElements(mockElement(), mockElement(), mockElement());
		setGroupAttributes("MyName", true, true, group);
		Group copy = group.deepCopy();

		assertEquals("MyName", copy.getName());
		assertTrue(copy.isList());
		assertNull(copy.getParent());
		assertThat(copy.getInnerElements(), hasItems(elementCopies.get(0), elementCopies.get(1), elementCopies.get(2)));
		verify(elementCopies.get(0), times(1)).setParent(copy);
		verify(elementCopies.get(1), times(1)).setParent(copy);
		verify(elementCopies.get(2), times(1)).setParent(copy);
	}

	/** Adds given inner elements to {@link #group} */
	private void addInnerElements(TreeElement... elementsToAdd) {
		for (TreeElement element : elementsToAdd) {
			innerElements.add(element);
		}
	}

	/** Creates a new {@link TreeElement} - also registers a potential copy in {@link #elementCopies} */
	private TreeElement mockElement() {
		TreeElement element = mock(TreeElement.class);
		TreeElement copy = mock(TreeElement.class);
		when(element.deepCopy()).thenReturn(copy);
		elementCopies.add(copy);
		return element;
	}

	/** sets {@link Group} Attributes */
	private void setGroupAttributes(String name, boolean isList, boolean isOptional, Group parent) {
		group.setName(name);
		group.setIsList(isList);
		group.setIsOptional(isOptional);
		group.setParent(parent);
	}
}
