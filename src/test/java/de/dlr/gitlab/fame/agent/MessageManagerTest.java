// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import de.dlr.gitlab.fame.agent.MessageManager.MessageManagementException;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.testUtils.ExceptionTesting;
import de.dlr.gitlab.fame.testUtils.LogChecker;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Test for {@link MessageManager}
 * 
 * @author Christoph Schimeczek */
public class MessageManagerTest {
	/** Dummy products for testing */
	private enum Products {
		A, B, C, D, X, Y
	}

	@Mock private TimeStamp before;
	@Mock private TimeStamp now;
	@Mock private TimeStamp after;
	AutoCloseable closeable;
	LogChecker logChecker = new LogChecker(MessageManager.class);

	private MessageManager messageManager;
	private ArrayList<Message> retrievedMessages;

	private final long overflowThreshold = (long) 1E3;

	@Before
	public void setUp() {
		closeable = MockitoAnnotations.openMocks(this);
		messageManager = new MessageManager(overflowThreshold);
		logChecker.clear();
		setupTime();
		retrievedMessages = null;
	}

	/** sets up time relations */
	private void setupTime() {
		when(before.isLessEqualTo(before)).thenReturn(true);
		when(before.isLessEqualTo(now)).thenReturn(true);
		when(before.isLessEqualTo(after)).thenReturn(true);
		when(now.isLessEqualTo(before)).thenReturn(false);
		when(now.isLessEqualTo(now)).thenReturn(true);
		when(now.isLessEqualTo(after)).thenReturn(true);
		when(after.isLessEqualTo(before)).thenReturn(false);
		when(after.isLessEqualTo(now)).thenReturn(false);
		when(now.isLessEqualTo(after)).thenReturn(true);
	}

	@After
	public void closeMocks() throws Exception {
		closeable.close();
	}

	@Test
	public void handleMessage_null_returnsNull() {
		assertNull(messageManager.handleMessage(null));
	}

	@Test
	public void handleMessage_noContractData_returnsMessage() {
		Message message = mock(Message.class);
		assertEquals(message, messageManager.handleMessage(message));
	}

	@Test
	public void handleMessage_unknownSenderContract_returnsNullAndLogsError() {
		assertNull(messageManager.handleMessage(makeMessage(now, 55)));
		logChecker.assertLogsContain(MessageManager.IGNORE_UNREGISTERED);
	}

	/** Creates a mocked message with given contractId */
	private Message makeMessage(TimeStamp deliveryTime, long contractId) {
		ContractData contractData = mock(ContractData.class);
		when(contractData.getContractId()).thenReturn(contractId);
		when(contractData.getDeliveryTime()).thenReturn(deliveryTime);
		Message message = mock(Message.class);
		when(message.getDataItemOfType(ContractData.class)).thenReturn(contractData);
		Message copy = mock(Message.class);
		when(message.deepCopy()).thenReturn(copy);
		when(copy.getDataItemOfType(ContractData.class)).thenReturn(contractData);
		return message;
	}

	@Test
	public void handleMessage_productNotUsedByAnyAction_returnsNullAndLogsError() {
		connect(Products.A);
		register(99, Products.A);
		assertNull(messageManager.handleMessage(makeMessage(now, 99)));
		logChecker.assertLogsContain(MessageManager.IGNORE_NO_FOLLOW_UP);
	}

	/** Connects given trigger at {@link #messageManager} with given inputs */
	private void connect(Enum<?> trigger, Enum<?>... inputs) {
		messageManager.connectInputsToTrigger(trigger, Arrays.asList(inputs));
	}

	/** Registers Contract with given Id and product at {@link #messageManager} */
	private void register(long contractId, Enum<?> product) {
		Contract contract = mock(Contract.class);
		when(contract.getContractId()).thenReturn(contractId);
		doReturn(product).when(contract).getProduct();
		messageManager.registerContract(contract);
	}

	@Test
	public void handleMessage_overflowThresholdExceeded_logs() {
		connect(Products.A, Products.A);
		register(99, Products.A);
		Message message = makeMessage(now, 99);
		for (long i = 0; i <= overflowThreshold + 10; i++) {
			messageManager.handleMessage(message);
		}
		logChecker.assertLogsContain(MessageManager.MESSAGE_OVERFLOW);
	}

	@Test
	public void handleMessage_allTriggerContractMissing_returnsNullAndLogsError() {
		connect(Products.A, Products.D);
		connect(Products.B, Products.D);
		connect(Products.C, Products.D);
		register(11, Products.D);
		assertNull(messageManager.handleMessage(makeMessage(before, 11)));
		logChecker.assertLogsContain(MessageManager.IGNORE_UNUSED);
	}

	@Test
	public void handleMessage_someTriggerContractMissing_returnsNullLogsNoError() {
		connect(Products.A, Products.B);
		connect(Products.B, Products.B);
		register(11, Products.B);
		assertNull(messageManager.handleMessage(makeMessage(before, 11)));
		logChecker.assertLogsDoNotContain(MessageManager.IGNORE_UNUSED);
	}

	@Test
	public void ActionOneInputDigestOneMessage() throws MessageManagementException {
		connect(Products.A, Products.A);
		register(99, Products.A);
		handle(makeMessages(before, 99));
		retrieve(Products.A, now);
		assertEquals(1, countReturnedMessages(before, 99));
	}

	/** Creates a mocked message for each given contractId with the given deliveryTime */
	private Message[] makeMessages(TimeStamp deliveryTime, long... contractIds) {
		Message[] messages = new Message[contractIds.length];
		for (int i = 0; i < contractIds.length; i++) {
			messages[i] = makeMessage(deliveryTime, contractIds[i]);
		}
		return messages;
	}

	/** Tasks {@link #messageManager} to handle the given Messages */
	private void handle(Message... messages) {
		for (Message message : messages) {
			messageManager.handleMessage(message);
		}
	}

	/** @return retrieve Messages for given trigger before or equals given TimeStamp and save them to {@link #retrievedMessages}
	 * @throws MessageManagementException */
	private void retrieve(Products trigger, TimeStamp timeStamp) throws MessageManagementException {
		retrievedMessages = messageManager.getMessagesForTrigger(trigger, timeStamp);
	}

	/** counts messages in {@link #retrievedMessages} which have given Contract and TimeStamp */
	private int countReturnedMessages(TimeStamp timeStamp, long contractId) {
		int matching = 0;
		for (Message message : retrievedMessages) {
			ContractData contractData = message.getDataItemOfType(ContractData.class);
			if (contractData.getContractId() == contractId && contractData.getDeliveryTime() == timeStamp) {
				matching++;
			}
		}
		return matching;
	}

	@Test
	public void ActionOneInputDigestMultipleMessages() throws MessageManagementException {
		connect(Products.A, Products.A);
		register(99, Products.A);
		handle(makeMessage(before, 99), makeMessage(now, 99), makeMessage(after, 99));
		retrieve(Products.A, now);
		assertEquals(1, countReturnedMessages(before, 99));
		assertEquals(1, countReturnedMessages(now, 99));
		assertEquals(0, countReturnedMessages(after, 99));
	}

	@Test
	public void rebindTrigger() throws MessageManagementException {
		connect(Products.D, Products.D);
		connect(Products.A, Products.A, Products.B);
		connect(Products.A, Products.C);
		register(11, Products.A);
		register(22, Products.B);
		register(33, Products.C);
		handle(makeMessages(before, 11, 22));
		logChecker.assertLogsContain(MessageManager.IGNORE_NO_FOLLOW_UP);
		handle(makeMessage(before, 33));
		retrieve(Products.A, now);
		assertEquals(0, countReturnedMessages(before, 11));
		assertEquals(0, countReturnedMessages(before, 22));
		assertEquals(1, countReturnedMessages(before, 33));
	}

	@Test
	public void bindMultipleInputs() throws MessageManagementException {
		connect(Products.A, Products.A, Products.B);
		register(11, Products.A);
		register(22, Products.B);
		handle(makeMessages(before, 11, 22));
		retrieve(Products.A, now);
		assertEquals(1, countReturnedMessages(before, 11));
		assertEquals(1, countReturnedMessages(before, 22));
	}

	@Test
	public void getMessagesForTrigger_NoTrigger() throws MessageManagementException {
		retrieve(Products.A, now);
		assertTrue(retrievedMessages.isEmpty());
		logChecker.assertLogsDoNotContain(MessageManager.ERR_NO_MESSAGES);
	}

	@Test
	public void getMessagesForTrigger_NothingStored() throws MessageManagementException {
		connect(Products.A, Products.B);
		register(22, Products.B);
		ExceptionTesting.assertThrowsMessage(MessageManagementException.class, MessageManager.ERR_NO_MESSAGES,
				() -> retrieve(Products.A, now));
	}

	@Test
	public void bindMultipleContracts() throws MessageManagementException {
		connect(Products.A, Products.A);
		register(11, Products.A);
		register(12, Products.A);
		handle(makeMessages(before, 11, 12));
		retrieve(Products.A, now);
		assertEquals(1, countReturnedMessages(before, 11));
		assertEquals(1, countReturnedMessages(before, 12));
	}

	@Test
	public void bindInputToMultipleTriggers() throws MessageManagementException {
		connect(Products.A, Products.B);
		connect(Products.B, Products.B);
		register(11, Products.A);
		register(12, Products.B);
		assertNull(messageManager.handleMessage(makeMessage(before, 12)));
		ArrayList<Message> messagesToA = messageManager.getMessagesForTrigger(Products.A, now);
		assertEquals(1, messagesToA.size());
		ArrayList<Message> messagesToB = messageManager.getMessagesForTrigger(Products.B, now);
		assertEquals(1, messagesToB.size());
		assertNotEquals(messagesToA.get(0), messagesToB.get(0));
	}

	@Test
	public void rebindTriggerOnMultipleInputBinds() throws MessageManagementException {
		connect(Products.A, Products.B, Products.X);
		connect(Products.B, Products.B);
		connect(Products.A, Products.C, Products.Y);
		register(11, Products.A);
		register(12, Products.B);
		register(13, Products.C);
		register(14, Products.Y);
		handle(makeMessages(now, 12, 13, 14));
		retrieve(Products.A, now);
		assertEquals(1, countReturnedMessages(now, 13));
		assertEquals(1, countReturnedMessages(now, 14));
		retrieve(Products.B, now);
		assertEquals(1, countReturnedMessages(now, 12));
		register(15, Products.X);
		handle(makeMessages(now, 15));
		logChecker.assertLogsContain(MessageManager.IGNORE_NO_FOLLOW_UP);
	}
}
