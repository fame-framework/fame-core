// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class TreeElementTest {
	private static class MyElement extends TreeElement {
		public int deepCopied = 0;
		public static final TreeElement deepCopy = new MyElement();

		@Override
		TreeElement deepCopy() {
			deepCopied++;
			return deepCopy;
		}

		@Override
		List<TreeElement> getInnerElements() {
			return null;
		}
	}

	private TreeElement element;

	@Before
	public void SetUp() {
		element = new MyElement();
	}

	@Test
	public void setAttributes_copiesName() {
		TreeElement other = new MyElement();
		other.setName("SomeName");
		other.setAttributes(element);
		assert element.getName().equals(other.getName());
	}

	@Test
	public void setAttributes_copiesList() {
		TreeElement other = new MyElement();
		other.setIsList(true);
		other.setAttributes(element);
		assert element.isList() == other.isList();
	}

	@Test
	public void setAttributes_copiesOptional() {
		TreeElement other = new MyElement();
		other.setIsOptional(true);
		other.setAttributes(element);
		assert element.isOptional() == other.isOptional();
	}

	@Test
	public void setParent_getParent_returnsNewParent() {
		TreeElement other = new MyElement();
		element.setParent(other);
		assert other == element.getParent();
	}

	@Test
	public void setName_getName_returnsNewName() {
		String name = "mySpecialName";
		element.setName(name);
		assert name.equals(element.getName());
	}

	@Test
	public void setIsList_isList_returnsSetted() {
		element.setIsList(true);
		assertTrue(element.isList());
		element.setIsList(false);
		assertFalse(element.isList());
	}

	@Test
	public void setIsOptional_isOptional_returnsSetted() {
		element.setIsOptional(true);
		assertTrue(element.isOptional());
		element.setIsOptional(false);
		assertFalse(element.isOptional());
	}

	@Test
	public void isRoot_noParent_returnsTrue() {
		element.setParent(null);
		assertTrue(element.isRoot());
	}

	@Test
	public void isRoot_hasParent_returnsFalse() {
		TreeElement other = new MyElement();
		element.setParent(other);
		assertFalse(element.isRoot());
	}

	@Test
	public void getRoot_noParent_returnsSelf() {
		element.setParent(null);
		assert element == element.getRoot();
	}

	@Test
	public void getRoot_oneParent_returnParent() {
		TreeElement parent = new MyElement();
		element.setParent(parent);
		assert parent == element.getRoot();
	}

	@Test
	public void getRoot_ancestors_returnsEldestAncestor() {
		buildAncestorsFor(element, "A", "B", "C", "D");
		assert element.getRoot().getName().equals("A");
	}

	/** Creates line of TreeElement ancestors with given names and adds them as parent to given element */
	private void buildAncestorsFor(TreeElement element, String... names) {
		TreeElement parent = null;
		for (String name : names) {
			TreeElement child = new MyElement();
			child.setName(name);
			child.setParent(parent);
			parent = child;
		}
		element.setParent(parent);
	}

	@Test
	public void getAddress_noParent_returnsEmptyString() {
		element.setParent(null);
		element.setName("ThisNameIsIgnored");
		assertTrue(element.getAddress().isEmpty());
	}

	@Test
	public void getAddress_singleParent_returnsName() {
		String name = "ThisIsMyName";
		buildAncestorsFor(element, "A");
		element.setName(name);
		assert name.equals(element.getAddress());
	}

	@Test
	public void getAddress_manyAncestors_returnsElementAddressIgnoringRootName() {
		buildAncestorsFor(element, "root", "A", "B", "C");
		String name = "ThisIsMyName";
		element.setName(name);
		String sep = TreeElement.ADDRESS_SEPARATOR;
		String expected = "A" + sep + "B" + sep + "C" + sep + name;
		assert expected.equals(element.getAddress());
	}

	@Test
	public void get_returnsDeepCopied() {
		MyElement myElement = (MyElement) this.element;
		MyElement result = (MyElement) myElement.get();
		assertEquals(1, myElement.deepCopied);
		assertEquals(MyElement.deepCopy, result);
	}

	@Test
	public void toString_noParent_returnsEmptyString() {
		element.setParent(null);
		element.setName("ThisNameIsIgnored");
		assertTrue(element.getAddress().isEmpty());
	}

	@Test
	public void toString_singleParent_address() {
		String name = "ThisIsMyName";
		buildAncestorsFor(element, "A");
		element.setName(name);
		assert name.equals(element.toString());
	}

	@Test
	public void toString_manyAncestors_returnsElementAddressIgnoringRootName() {
		buildAncestorsFor(element, "root", "A", "B", "C");
		String name = "ThisIsMyName";
		element.setName(name);
		String sep = TreeElement.ADDRESS_SEPARATOR;
		String expected = "A" + sep + "B" + sep + "C" + sep + name;
		assert expected.equals(element.toString());
	}
}
