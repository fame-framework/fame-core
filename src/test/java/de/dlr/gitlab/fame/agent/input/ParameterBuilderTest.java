// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.verify;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedConstruction;
import de.dlr.gitlab.fame.agent.input.Make.Type;

/** Tests for {@link ParameterBuilder}
 *
 * @author Christoph Schimeczek */
public class ParameterBuilderTest {
	private enum Dummy {
		A, B, C
	}

	private ParameterBuilder builder;
	private MockedConstruction<Parameter> mockedConstruction;

	@Before
	public void setUp() {
		builder = new ParameterBuilder();
		mockedConstruction = mockConstruction(Parameter.class);
	}

	@After
	public void closeMocks() {
		mockedConstruction.close();
	}

	@Test
	public void setType_null_throws() {
		assertThrowsFatalMessage(ParameterBuilder.DATA_TYPE_NEEDED, () -> builder.setType(null));
	}

	@Test
	public void setType_validType_returnsBuilder() {
		assertEquals(builder, builder.setType(Type.DOUBLE));
	}

	@Test
	public void setType_build_setsDataType() {
		builder.setType(Type.DOUBLE);
		verify(builder.build()).setDataType(Type.DOUBLE);
	}

	@Test
	public void setType_getType_match() {
		Type type = Type.DOUBLE;
		builder.setType(type);
		assertEquals(type, builder.getType());
	}

	@Test
	public void checkListAbility_timeseries_throws() {
		builder.setType(Type.TIMESERIES);
		assertThrowsFatalMessage(ParameterBuilder.LIST_DISALLOWED, () -> builder.checkListAbility());
	}

	@Test
	public void setType_enumNotProvided_throws() {
		builder.setType(Type.ENUM);
		assertThrowsFatalMessage(ParameterBuilder.ENUM_REQUIRED, () -> builder.build());
	}

	@Test
	public void setType_enumProvided_build_setsAllowedValues() {
		builder.setType(Type.ENUM);
		assertEquals(builder, builder.setEnum(Dummy.class));
		verify(builder.build()).setAllowedValues(any(Dummy[].class));
	}

	@Test
	public void setEnum_getAllowedValues_matchEnumValues() {
		builder.setType(Type.ENUM);
		assertEquals(builder, builder.setEnum(Dummy.class));
		assertArrayEquals(Dummy.values(), builder.getAllowedValues());
	}

	@Test
	public void fill_build_setsDefaultValue() {
		String defaultValue = "Some value";
		assertEquals(builder, builder.fill(defaultValue));
		verify(builder.build()).setDefaultValue(defaultValue);
	}

	@Test
	public void help_build_setsHelpValue() {
		String helpText = "Some comment";
		assertEquals(builder, builder.help(helpText));
		verify(builder.build()).setComment(helpText);
	}

	@Test
	public void build_setsParentMembers() {
		String name = "MyName";
		builder.setName(name);
		builder.list();
		builder.optional();
		Parameter parameter = builder.build();
		verify(parameter).setName(name);
		verify(parameter).setIsList(true);
		verify(parameter).setIsOptional(true);
	}
}
