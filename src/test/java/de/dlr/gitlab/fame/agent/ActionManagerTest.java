// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.LogChecker;

public class ActionManagerTest {
	private enum Dummy{None, One, Two};
	private ActionManager actionManager;
	private LogChecker logChecker = new LogChecker(ActionManager.class);
	private int functionCallCounterA;
	private int functionCallCounterB;

	@Before
	public void setup() {
		actionManager = new ActionManager();
		logChecker.clear();
		functionCallCounterA = 0;
		functionCallCounterB = 0;
	}
	
	private void dummyA(ArrayList<Message> m, List<Contract> a) {
		functionCallCounterA++;
	};
	
	private void dummyB(ArrayList<Message> m, List<Contract> a) {
		functionCallCounterB++;
	};
	
	@Test
	public void testConfigure() {
		actionManager.configure(Dummy.One, this::dummyA);
		@SuppressWarnings("unchecked")
		HashMap<Enum<?>, BiConsumer<ArrayList<Message>, List<Contract>>> reasonToAction = 
		 (HashMap<Enum<?>, BiConsumer<ArrayList<Message>, List<Contract>>>) AccessPrivates.getPrivateFieldOf("reasonToAction", actionManager);
		assertTrue(reasonToAction.containsKey(Dummy.One));
		reasonToAction.get(Dummy.One).accept(null, null); 
		assertEquals(1, functionCallCounterA);
	}
	
	@Test
	public void testConfigureDifferentEnums() {
		actionManager.configure(Dummy.One, this::dummyA);
		actionManager.configure(Dummy.Two, this::dummyB);
		@SuppressWarnings("unchecked")
		HashMap<Enum<?>, BiConsumer<ArrayList<Message>, List<Contract>>> reasonToAction = 
		 (HashMap<Enum<?>, BiConsumer<ArrayList<Message>, List<Contract>>>) AccessPrivates.getPrivateFieldOf("reasonToAction", actionManager);
		assertTrue(reasonToAction.containsKey(Dummy.One));
		assertTrue(reasonToAction.containsKey(Dummy.Two));
		reasonToAction.get(Dummy.One).accept(null, null);
		reasonToAction.get(Dummy.Two).accept(null, null);
		assertEquals(1, functionCallCounterA);
		assertEquals(1, functionCallCounterB);
	}
	
	@Test
	public void testConfigureOverrideEnums() {
		actionManager.configure(Dummy.One, this::dummyA);
		actionManager.configure(Dummy.One, this::dummyB);
		@SuppressWarnings("unchecked")
		HashMap<Enum<?>, BiConsumer<ArrayList<Message>, List<Contract>>> reasonToAction = 
		 (HashMap<Enum<?>, BiConsumer<ArrayList<Message>, List<Contract>>>) AccessPrivates.getPrivateFieldOf("reasonToAction", actionManager);
		reasonToAction.get(Dummy.One).accept(null, null);
		assertEquals(0, functionCallCounterA);
		assertEquals(1, functionCallCounterB);
		logChecker.assertLogsContain(ActionManager.OVERRIDE);
	}
	
	@Test
	public void testExecute() {
		actionManager.configure(Dummy.One, this::dummyA);
		BiConsumer<ArrayList<Message>, List<Contract>> action = actionManager.getActionFor(Dummy.One);
		action.accept(null, null);
		assertEquals(1, functionCallCounterA);
	}
	
	@Test
	public void testExecuteNoMethod() {
		BiConsumer<ArrayList<Message>, List<Contract>> action = actionManager.getActionFor(Dummy.One);
		assertNull(action);
	}
}