// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.Random;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.agent.input.Make;
import de.dlr.gitlab.fame.agent.input.ParameterData;
import de.dlr.gitlab.fame.agent.input.ParameterData.MissingDataException;
import de.dlr.gitlab.fame.agent.input.Tree;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.message.Signal;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.service.output.AllColumnBuffer;
import de.dlr.gitlab.fame.time.TimeStamp;

/**
 * Dummy implementation of an agent for testing purposes
 * 
 * @author Christoph Schimeczek
 */
public class SpecialAgent extends Agent {
	private static Tree parameters = Make.newTree().add(Make.newSeries("DummySeries")).buildTree();

	protected enum Columns {
		TestColumnA, TestColumnB
	};

	private TimeSeries dummySeries;

	/** total number of agents in the simulation */
	public static final int AGENT_COUNT = 4;
	/** number of messages sent per action of each agent */
	public static final int MESSAGE_COUNT = 5;
	private long targetId;

	public SpecialAgent(DataProvider dataProvider) {
		super(dataProvider);
		ParameterData inputData = parameters.join(dataProvider);
		
		Random random = new Random(getId());
		this.targetId = random.nextInt(AGENT_COUNT);
		try {
			this.dummySeries = inputData.getTimeSeries("DummySeries");
		} catch (MissingDataException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/** sends {@link #MESSAGE_COUNT} messages to a random target agent, stores 2 values in its {@link AllColumnBuffer} */
	public void execute(TimeStamp currentTime, ArrayList<Message> incomingMessages) {
		for (int i = 0; i < MESSAGE_COUNT; i++) {
			sendMessageTo(targetId, new Signal(0));
		}
		store(Columns.TestColumnA, targetId);
		store(Columns.TestColumnB, 1);
	}

	public TimeSeries getDummySeries() {
		return dummySeries;
	}
}