// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import org.junit.Before;
import org.junit.Test;

public class ElementBuilderTest {
	private static class MyElementBuilder extends ElementBuilder {
		public int callsToBuild = 0;
		public static final TreeElement element = mock(TreeElement.class);
		public int callsToCheckListAbility = 0;

		@Override
		protected TreeElement build() {
			callsToBuild++;
			return element;
		}

		@Override
		protected void checkListAbility() {
			callsToCheckListAbility++;
		}
	}

	private MyElementBuilder builder;

	@Before
	public void setUp() {
		builder = new MyElementBuilder();
	}

	@Test
	public void get_callsBuild() {
		builder.get();
		assertEquals(1, builder.callsToBuild);
	}

	@Test
	public void get_returnsBuildResult() {
		TreeElement result = builder.get();
		assertEquals(MyElementBuilder.element, result);
	}

	@Test
	public void setName_getName_returnsSettedValue() {
		String name = "MyFancyName";
		builder.setName(name);
		assertEquals(name, builder.getName());
	}

	@Test
	public void setName_illegalName_throws() {
		assertThrowsFatalMessage(ElementBuilder.NAME_ILLEGAL, () -> builder.setName("public"));
	}

	@Test
	public void list_checksListAbility() {
		builder.list();
		assertEquals(1, builder.callsToCheckListAbility);
	}

	@Test
	public void list_setAttributes_valueSet() {
		TreeElement element = mock(TreeElement.class);
		builder.setAttributes(element);
		verify(element, times(1)).setIsList(false);

		element = mock(TreeElement.class);
		builder.list();
		builder.setAttributes(element);
		verify(element, times(1)).setIsList(true);
	}

	@Test
	public void optional_setAttributes_valueSet() {
		TreeElement element = mock(TreeElement.class);
		builder.setAttributes(element);
		verify(element, times(1)).setIsOptional(false);

		element = mock(TreeElement.class);
		builder.optional();
		builder.setAttributes(element);
		verify(element, times(1)).setIsOptional(true);
	}

	@Test
	public void setAttribute_name_valueSet() {
		TreeElement element = mock(TreeElement.class);
		String name = "MyElementName";
		builder.setName(name);
		builder.setAttributes(element);
		verify(element, times(1)).setName(name);
	}

	@Test
	public void ensureNotNullOrEmpty_null_throws() {
		assertThrowsFatalMessage(ElementBuilder.NAME_NEEDED, () -> builder.ensureNotNullOrEmpty(null));
	}

	@Test
	public void ensureNotNullOrEmpty_empty_throws() {
		assertThrowsFatalMessage(ElementBuilder.NAME_NEEDED, () -> builder.ensureNotNullOrEmpty(""));
	}
}
