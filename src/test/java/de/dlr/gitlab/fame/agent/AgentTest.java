// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.BiConsumer;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent.WarmUpStatus;
import de.dlr.gitlab.fame.agent.MessageManager.MessageManagementException;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.service.LocalServices;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.LogChecker;
import de.dlr.gitlab.fame.time.TimeStamp;

public class AgentTest {
	private class DummyAgent extends Agent {
		public boolean warmedUp = false;

		public DummyAgent(AgentDao dao, LocalServices services) {
			super(new DataProvider(dao, null, services));
		}

		@Override
		protected WarmUpStatus warmUp(TimeStamp t) {
			warmedUp = true;
			return WarmUpStatus.COMPLETED;
		}
	}

	private DummyAgent agent;
	private ContractManager mockContractManager;
	private ActionManager mockActionManager;
	private MessageManager mockMessageManager;
	private LocalServices mockLocalServices;
	private long id = 12L;
	private LogChecker logChecker = new LogChecker(Agent.class);

	private enum Reasons {
		A, B, C
	};

	@Before
	public void setup() {
		AgentDao agentDaoMock = mock(AgentDao.class);
		when(agentDaoMock.getId()).thenReturn(id);
		mockLocalServices = mock(LocalServices.class);
		agent = new DummyAgent(agentDaoMock, mockLocalServices);
		logChecker.clear();

		mockContractManager = mock(ContractManager.class);
		AccessPrivates.setPrivateField("contractManager", agent, mockContractManager);
		mockActionManager = mock(ActionManager.class);
		AccessPrivates.setPrivateField("actionManager", agent, mockActionManager);
		mockMessageManager = mock(MessageManager.class);
		AccessPrivates.setPrivateField("messageManager", agent, mockMessageManager);
	}

	@Test
	public void toString_returnsCorrectString() {
		assertEquals("Agent(DummyAgent, " + id + ")", agent.toString());
	}

	@Test
	public void getId_returnsId() {
		assertEquals(id, agent.getId());
	}

	@Test
	public void constructor_registersAgent() {
		verify(mockLocalServices, times(1)).registerAgent(agent);
	}

	@Test
	public void now_returnsCurrentTimeStamp() {
		TimeStamp time = new TimeStamp(76L);
		when(mockLocalServices.getCurrentTime()).thenReturn(time);
		assertEquals(time, agent.now());
	}

	@Test
	public void getNextRandom_returnsRngForAgent() {
		Random rng = mock(Random.class);
		when(mockLocalServices.getNewRandomNumberGeneratorForAgent(id)).thenReturn(rng);
		Random result = agent.getNextRandomNumberGenerator();
		assertEquals(rng, result);
	}

	@Test
	public void executeWarmUpAndReportNeed_returnsCompleted() {
		TimeStamp currentTime = new TimeStamp(0);
		assertEquals(WarmUpStatus.COMPLETED, agent.executeWarmUp(currentTime));
		assertTrue(agent.warmedUp);
	}

	@Test
	public void sendMessageTo_noPayload_forwardedToLocalServices() {
		long receiver = 88L;
		agent.sendMessageTo(receiver);
		verify(mockLocalServices).sendMessage(eq(id), eq(receiver), eq(null), any(DataItem[].class));
	}

	@Test
	public void addContract_contractAddedToContractManager() {
		Contract contract = mock(Contract.class);
		agent.addContract(contract);
		verify(mockContractManager, times(1)).addContract(contract);
	}

	@Test
	public void addContract_contractAddedToMessageManager() {
		Contract contract = mock(Contract.class);
		agent.addContract(contract);
		verify(mockMessageManager, times(1)).registerContract(contract);
	}

	@Test
	public void executeActions_noFollowUpAction_logsInfo() {
		TimeStamp time = new TimeStamp(666);
		when(mockLocalServices.getCurrentTime()).thenReturn(time);
		mockContractManagerReturn();
		agent.executeActions(new ArrayList<>());
		logChecker.assertLogsContain(Agent.INFO_NO_FOLLOW_UP);
		verify(mockLocalServices, times(0)).addActionAt(any(), any());
	}

	private void mockContractManagerReturn(PlannedAction... actions) {
		ArrayList<PlannedAction> actionList = new ArrayList<>();
		for (PlannedAction action : actions) {
			actionList.add(action);
		}
		when(mockContractManager.updateAndGetNextContractActions(any())).thenReturn(actionList);
	}

	@Test
	public void executeActions_withFollowUpAction_actionsAdded() {
		TimeStamp time = new TimeStamp(666);
		when(mockLocalServices.getCurrentTime()).thenReturn(time);
		PlannedAction mockActionA = mock(PlannedAction.class);
		PlannedAction mockActionB = mock(PlannedAction.class);
		mockContractManagerReturn(mockActionA, mockActionB);
		agent.executeActions(new ArrayList<>());
		logChecker.assertLogsDoNotContain(Agent.INFO_NO_FOLLOW_UP);
		verify(mockLocalServices, times(1)).addActionAt(agent, mockActionA);
		verify(mockLocalServices, times(1)).addActionAt(agent, mockActionB);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void executeActions_missingMessages_logsWarning() throws MessageManagementException {
		TimeStamp time = new TimeStamp(666);
		when(mockLocalServices.getCurrentTime()).thenReturn(time);
		mockContractManagerReturn();
		ArrayList<Enum<?>> reasons = new ArrayList<>();
		reasons.add(Reasons.A);
		when(mockActionManager.getActionFor(Reasons.A)).thenReturn(mock(BiConsumer.class));
		when(mockMessageManager.getMessagesForTrigger(Reasons.A, time)).thenThrow(MessageManagementException.class);
		agent.executeActions(reasons);
		logChecker.assertLogsContain(Agent.ERR_MESSAGES_MISSING);
	}
}