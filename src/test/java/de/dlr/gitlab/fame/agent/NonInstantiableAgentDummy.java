// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.Product;

/** 
 * Dummy Agent class that is not instantiable, used in {@link ContractManagerTest} 
 * 
 * @author Christoph Schimeczek
 */
public abstract class NonInstantiableAgentDummy extends Agent {
	@Product
	public enum MyProducts{Prod1, Prod2};
	
	public NonInstantiableAgentDummy(DataProvider param) {
		super(param);
	}
}