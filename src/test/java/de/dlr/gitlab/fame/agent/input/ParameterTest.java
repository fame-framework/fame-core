// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.input.Make.Type;

/** Tests for {@link Parameter}
 * 
 * @author Christoph Schimeczek */
public class ParameterTest {
	private enum Dummy {
		A, B, C
	}

	private Parameter parameter;

	@Before
	public void setUp() {
		parameter = new Parameter();
	}

	@Test
	public void getInnerElements_returnsEmptyList() {
		assertTrue(parameter.getInnerElements().isEmpty());
	}

	@Test
	public void deepCopy_ReturnsOtherObject() {
		Parameter copy = parameter.deepCopy();
		assertNotEquals(parameter, copy);
	}

	@Test
	public void deepCopy_copiesAttributes() {
		parameter.setDataType(Type.ENUM);
		parameter.setAllowedValues(Dummy.values());
		parameter.setComment("Comment");
		parameter.setDefaultValue("Default");
		parameter.setIsList(true);
		parameter.setIsOptional(true);
		parameter.setName("MyName");
		parameter.setParent(mock(TreeElement.class));

		Parameter copy = parameter.deepCopy();
		assertEquals(Type.ENUM, copy.getDataType());
		assertThat(Arrays.asList(copy.getAllowedValues()), hasItems(Dummy.A, Dummy.B, Dummy.C));
		assertEquals("Comment", copy.getComment());
		assertEquals("Default", copy.getDefaultValue());
		assertTrue(copy.isList());
		assertTrue(copy.isOptional());
		assertEquals("MyName", copy.getName());
		assertNull(copy.getParent());
	}

	@Test
	public void setDetaultValue_getDefaultValue_returnsSettedValue() {
		String myDefault = "SomeValue";
		parameter.setDefaultValue(myDefault);
		assertEquals(myDefault, parameter.getDefaultValue());
	}

	@Test
	public void setComment_getComment_returnsSettedValue() {
		String myComment = "SomeComment";
		parameter.setComment(myComment);
		assertEquals(myComment, parameter.getComment());
	}

	@Test
	public void setAllowedValues_getAllowedValues_returnsSettedValues() {
		parameter.setAllowedValues(Dummy.values());
		assertArrayEquals(Dummy.values(), parameter.getAllowedValues());
	}

	@Test
	public void setDataType_getDataType_returnsSettedvalue() {
		for (Type type : Type.values()) {
			parameter.setDataType(type);
			assertEquals(type, parameter.getDataType());
		}
	}
}
