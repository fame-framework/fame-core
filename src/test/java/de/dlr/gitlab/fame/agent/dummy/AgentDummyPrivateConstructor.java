// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.dummy;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;

public class AgentDummyPrivateConstructor extends Agent {
	private AgentDummyPrivateConstructor(DataProvider param) {
		super(param);
	}
}