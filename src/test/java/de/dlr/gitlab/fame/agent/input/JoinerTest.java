// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import de.dlr.gitlab.fame.agent.input.ParameterData.MissingDataException;
import de.dlr.gitlab.fame.protobuf.Field.NestedField;

/** Tests for {@link Joiner} Since join is called from package only it can be safely assumed that given ParameterTree and
 * DataProvider at not null
 *
 * @author Christoph Schimeczek */
public class JoinerTest {
	private Integer value1 = 1;
	private Integer value2 = 2;
	private Integer value3 = 3;

	@Mock private DataProvider dataProviderMock;
	@Mock private Tree treeMock;
	private AutoCloseable closeable;

	private HashMap<String, Object> result;

	@Before
	public void setUp() {
		closeable = MockitoAnnotations.openMocks(this);
		result = null;
	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Test
	public void testJoinNoFieldNoParam() {
		fillTreeMock();
		fillDataProviderFields();
		callJoin();
		assertTrue(result.isEmpty());
	}

	/** adds all given ParameterTrees to the root {@link #treeMock} */
	private void fillTreeMock(TreeElement... innerParams) {
		ArrayList<TreeElement> params = new ArrayList<>();
		for (TreeElement innerParam : innerParams) {
			params.add(innerParam);
		}
		when(treeMock.getInnerElements()).thenReturn(params);
	}

	/** lets {@link #dataProviderMock} return the given top-level fields */
	private void fillDataProviderFields(NestedField... fields) {
		List<NestedField> fieldsToReturn = new ArrayList<NestedField>();
		for (NestedField field : fields) {
			fieldsToReturn.add(field);
		}
		when(dataProviderMock.getFields()).thenReturn(fieldsToReturn);
	}

	/** Mocks constructor of ParameterData to store HashMap created by Joiner into {@link #result}, then calls join() */
	private void callJoin() {
		try (@SuppressWarnings("unchecked") MockedConstruction<ParameterData> mocked = Mockito.mockConstruction(
				ParameterData.class,
				(mock, context) -> {
					result = (HashMap<String, Object>) context.arguments().get(0);
				})) {
			Joiner.join(treeMock, dataProviderMock);
		}
	}

	@Test
	public void testJoinOneFieldNoParam() {
		fillDataProviderFields(mockField("A"));
		fillTreeMock();
		callJoin();
		assertTrue(result.isEmpty());
	}

	/** lets given field return the given name and any specified inner fields */
	private NestedField mockField(String name, NestedField... innerFields) {
		NestedField field = mock(NestedField.class);
		when(field.getFieldName()).thenReturn(name);
		List<NestedField> fields = new ArrayList<>();
		for (NestedField innerField : innerFields) {
			fields.add(innerField);
		}
		when(field.getFieldsList()).thenReturn(fields);
		return field;
	}

	@Test
	public void testJoinMissingMandatoryField() {
		fillDataProviderFields();
		fillTreeMock(mockParam("MissingParam", true, value1));
		assertThrowsFatalMessage(Joiner.EX_PARAM_MISSING, () -> callJoin());
	}

	/** @return newly created {@link Parameter} mock with the given address, mandatoriness, value */
	private Parameter mockParam(String address, boolean mandatory, Object value) {
		Parameter param = mock(Parameter.class);
		doReturn(value).when(dataProviderMock).getValue(eq(param), any(NestedField.class));
		when(param.getAddress()).thenReturn(address);
		when(param.getInnerElements()).thenReturn(Collections.emptyList());
		when(param.isOptional()).thenReturn(!mandatory);
		when(param.isList()).thenReturn(false);
		when(param.getName()).thenReturn(getNameFromAddress(address));
		return param;
	}

	/** Returns name extracted from the given address String */
	private String getNameFromAddress(String address) {
		String[] addressParts = address.split("\\.");
		return addressParts.length > 0 ? addressParts[addressParts.length - 1] : address;
	}

	@Test
	public void testJoinMissingOptionalField() {
		fillDataProviderFields();
		fillTreeMock(mockParam("MissingParam", false, value1));
		callJoin();
		assertTrue(result.isEmpty());
	}

	@Test
	public void testJoinOneField() {
		fillDataProviderFields(mockField("A"));
		fillTreeMock(mockParam("A", true, value1));
		callJoin();
		assertThat(result.keySet(), hasItem("A"));
	}

	@Test
	public void testJoinMultipleFields() throws MissingDataException {
		fillDataProviderFields(mockField("A"), mockField("B"), mockField("C"));
		fillTreeMock(mockParam("A", true, value1), mockParam("B", true, value2), mockParam("C", true, value3));
		callJoin();
		assertThat(result.keySet(), hasItems("A", "B", "C"));
		assertEquals(1, result.get("A"));
		assertEquals(2, result.get("B"));
		assertEquals(3, result.get("C"));
	}

	@Test
	public void testJoinInnerField() throws MissingDataException {
		fillDataProviderFields(mockField("A", mockField("B")));
		fillTreeMock(mockGroup("A", true, false, mockParam("A.B", true, value2)));
		callJoin();
		assertThat(result.keySet(), hasItem("A.B"));
		assertEquals(2, result.get("A.B"));
	}

	/** @return newly created {@link Group} mock with given name, mandatoriness, isList, and inner elements */
	private Group mockGroup(String name, boolean mandatory, boolean isList, TreeElement... elementsToAdd) {
		Group group = mock(Group.class);
		when(group.getName()).thenReturn(name);
		when(group.isOptional()).thenReturn(!mandatory);
		when(group.isList()).thenReturn(isList);
		ArrayList<TreeElement> innerElements = new ArrayList<>();
		for (TreeElement element : elementsToAdd) {
			innerElements.add(element);
		}
		when(group.getInnerElements()).thenReturn(innerElements);
		return group;
	}

	@Test
	public void testJoinInnerInnerField() throws MissingDataException {
		fillDataProviderFields(mockField("A", mockField("B", mockField("C"))));
		fillTreeMock(mockGroup("A", true, false, mockGroup("B", true, false, mockParam("A.B.C", true, value3))));
		callJoin();
		assertThat(result.keySet(), hasItem("A.B.C"));
		assertEquals(3, result.get("A.B.C"));
	}

	@Test
	public void testJoinMixedLevels() throws MissingDataException {
		fillDataProviderFields(mockField("A"), mockField("L0", mockField("B"), mockField("L1", mockField("C"))));
		fillTreeMock(mockParam("A", true, value1), mockGroup("L0", true, false, mockParam("L0.B", true, value2),
				mockGroup("L1", true, false, mockParam("L0.L1.C", true, value3))));
		callJoin();
		assertThat(result.keySet(), hasItems("A", "L0.B", "L0.L1.C"));
		assertEquals(1, result.get("A"));
		assertEquals(2, result.get("L0.B"));
		assertEquals(3, result.get("L0.L1.C"));
	}

	@Test
	public void testJoinListGroup() throws MissingDataException {
		fillDataProviderFields(mockField("MyGroup", mockField("0", mockField("A")), mockField("1", mockField("A"))));
		fillTreeMock(mockGroup("MyGroup", true, true, mockParam("MyGroup.A", true, value1)));
		callJoin();
		assertThat(result.keySet(), hasItems("MyGroup.0.A", "MyGroup.1.A"));
	}
}
