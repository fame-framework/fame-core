// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.ExceptionTesting;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Tests for {@link ContractManager}
 * 
 * @author Christoph Schimeczek */
public class ContractManagerTest {
	private enum Dummy {
		None, One, Two
	};

	private final long agentId = 0L;
	private ContractManager contractManager;
	private ArrayList<Contract> spyOnContracts;
	private ProductTimes spyOnProductTimes;
	private HashMap<Enum<?>, ArrayList<Contract>> spyOnProductContracts;
	private HashSet<Enum<?>> spyOnProductsForSchedule;

	@SuppressWarnings("unchecked")
	@Before
	public void setup() {
		contractManager = new ContractManager(agentId);
		spyOnContracts = (ArrayList<Contract>) AccessPrivates.getPrivateFieldOf("contracts", contractManager);
		spyOnProductTimes = (ProductTimes) AccessPrivates.getPrivateFieldOf("productTimes", contractManager);
		spyOnProductContracts = (HashMap<Enum<?>, ArrayList<Contract>>) AccessPrivates.getPrivateFieldOf("productContracts",
				contractManager);
		spyOnProductsForSchedule = (HashSet<Enum<?>>) AccessPrivates.getPrivateFieldOf("productsForSchedule",
				contractManager);
	}

	@Test
	public void testContructor() {
		long spiedAgentId = (long) AccessPrivates.getPrivateFieldOf("ownAgentId", contractManager);
		assertEquals(agentId, spiedAgentId);
	}

	@Test
	public void testAddContract() {
		Contract mockContract = mock(Contract.class);
		doReturn(Dummy.None).when(mockContract).getProduct();
		contractManager.addContract(mockContract);
		assertTrue(spyOnContracts.contains(mockContract));
		assertTrue(spyOnProductContracts.containsKey(Dummy.None));
		assertTrue(spyOnProductContracts.get(Dummy.None).contains(mockContract));
	}

	@Test
	public void testAddContractsContractNotSameDeliveryInterval() {
		Contract contractOne = buildContract(0L, 1L, 99L, Dummy.None, 0L, 10L);
		contractManager.addContract(contractOne);
		Contract contractTwo = buildContract(0L, 1L, 99L, Dummy.None, 0L, 6L);
		try {
			contractManager.addContract(contractTwo);
			fail("Exception expected.");
		} catch (RuntimeException e) {
			assertEquals(ContractManager.ERR_CONTRACTS_OUT_OF_SYNC, e.getMessage());
		}
	}

	@Test
	public void testAddContractsNotSameScheduleTime() {
		Contract contractOne = buildContract(0L, 1L, 99L, Dummy.None, 0L, 10L);
		contractManager.addContract(contractOne);
		Contract contractTwo = buildContract(0L, 1L, 99L, Dummy.None, 1L, 10L);
		try {
			contractManager.addContract(contractTwo);
			fail("Exception expected.");
		} catch (RuntimeException e) {
			assertEquals(ContractManager.ERR_CONTRACTS_OUT_OF_SYNC, e.getMessage());
		}
	}

	@Test
	public void testAddContractsMatchingTimes() {
		Contract contractOne = buildContract(0L, 1L, 99L, Dummy.None, 0L, 10L);
		contractManager.addContract(contractOne);
		Contract contractTwo = buildContract(0L, 1L, 99L, Dummy.None, 10L, 10L);
		contractManager.addContract(contractTwo);
		ArrayList<Contract> contracts = spyOnProductContracts.get(Dummy.None);
		assertTrue(contracts.contains(contractOne));
		assertTrue(contracts.contains(contractTwo));
	}

	@Test
	public void testAddContractNotOverrideListAtSameProduct() {
		Contract mockContract = mock(Contract.class);
		doReturn(Dummy.None).when(mockContract).getProduct();
		when(mockContract.getDeliveryInterval()).thenReturn(new TimeSpan(1L));
		when(mockContract.getFirstDeliveryTime()).thenReturn(new TimeStamp(0L));
		contractManager.addContract(mockContract);
		ArrayList<Contract> contracts = spyOnProductContracts.get(Dummy.None);
		contractManager.addContract(mockContract);
		assertEquals(contracts, spyOnProductContracts.get(Dummy.None));
	}

	@Test
	public void testGetContractPartners() {
		spyOnContracts.add(buildContract(agentId, 1L, true));
		spyOnContracts.add(buildContract(2L, agentId, true));
		spyOnContracts.add(buildContract(3L, agentId, false));
		spyOnContracts.add(buildContract(agentId, 4L, false));
		ArrayList<Long> agents = contractManager.getContractPartners(new TimeStamp(0L));
		assertEquals(2, agents.size());
		assertFalse(agents.contains(agentId));
		assertTrue(agents.contains(1L));
		assertTrue(agents.contains(2L));
	}

	/** Support method: Creates contract */
	private Contract buildContract(long sender, long receiver, boolean active) {
		Contract mockContract = mock(Contract.class);
		when(mockContract.getSenderId()).thenReturn(sender);
		when(mockContract.getReceiverId()).thenReturn(receiver);
		when(mockContract.isActiveAt(any())).thenReturn(active);
		return mockContract;
	}

	@Test
	public void testGetContractsForProduct() {
		ArrayList<Contract> contracts = new ArrayList<Contract>();
		contracts.add(buildContract(0L, 1L, true));
		contracts.add(buildContract(2L, 0L, true));
		spyOnProductContracts.put(Dummy.One, contracts);
		List<Contract> result = contractManager.getContractsForProduct(Dummy.One);
		assertEquals(contracts, result);
	}

	@Test
	public void testGetContractsForProductEmpty() {
		List<Contract> result = contractManager.getContractsForProduct(Dummy.One);
		assertTrue(result != null);
	}

	@Test
	public void testGetContractsForProductIsNotModifiable() {
		List<Contract> result = contractManager.getContractsForProduct(Dummy.One);
		try {
			result.add(buildContract(0L, 1L, true));
			fail("Exception expected");
		} catch (UnsupportedOperationException e) {}
	}

	@Test
	public void testConsider() {
		contractManager.consider(Dummy.One);
		assertTrue(spyOnProductsForSchedule.contains(Dummy.One));
	}

	@Test
	public void testBuildActionAfterIsSender() {
		TimeStamp now = new TimeStamp(5L);
		Contract contract = buildContract(0L, 1L, 99L, Dummy.None, 0L, 10L);
		PlannedAction result = (PlannedAction) AccessPrivates.callPrivateMethodOn("buildActionAfter", contractManager,
				contract, now);
		Enum<?> reason = result.getSchedulingReason();
		assertEquals(Dummy.None, reason);
		assertEquals(9L, result.getTimeStamp().getStep());
	}

	private Contract buildContract(long sender, long receiver, long contractId, Enum<?> product, long start, long step) {
		ProtoContract protoType = ProtoContract.newBuilder().setSenderId(sender).setReceiverId(receiver)
				.setProductName(product.name()).setFirstDeliveryTime(start).setDeliveryIntervalInSteps(step).build();
		return new Contract(protoType, contractId, product);
	}

	@Test
	public void testBuildActionAfterIsReceiver() {
		TimeStamp now = new TimeStamp(5L);
		Contract contract = buildContract(1L, 0L, 99L, Dummy.None, 0L, 10L);
		PlannedAction result = (PlannedAction) AccessPrivates.callPrivateMethodOn("buildActionAfter", contractManager,
				contract, now);
		Enum<?> reason = result.getSchedulingReason();
		assertEquals(Dummy.None, reason);
		assertEquals(10L, result.getTimeStamp().getStep());
	}

	@Test
	public void scheduleDeliveriesBeginningAt() {
		contractManager.addContract(buildContract(agentId, 1L, 99L, Dummy.Two, 0L, 5L));
		contractManager.consider(Dummy.Two);
		ArrayList<PlannedAction> result = contractManager.scheduleDeliveriesBeginningAt(new TimeStamp(0L));
		assertEquals(1, result.size());
		PlannedAction action = result.get(0);
		assertEquals(Dummy.Two, action.getSchedulingReason());
		assertEquals(4L, action.getTimeStamp().getStep());
	}

	@Test
	public void scheduleDeliveriesBeginningAt_multipleCall_fail() {
		contractManager.addContract(buildContract(agentId, 1L, 99L, Dummy.Two, 0L, 5L));
		contractManager.consider(Dummy.Two);
		contractManager.scheduleDeliveriesBeginningAt(new TimeStamp(0L));
		ExceptionTesting.assertThrowsFatalMessage(ContractManager.ERR_MULTIPLE_INITIALISATION,
				() -> contractManager.scheduleDeliveriesBeginningAt(new TimeStamp(0L)));
	}

	@Test
	public void testScheduleDeliveriesBeginningAtNotConsidered() {
		contractManager.addContract(buildContract(agentId, 1L, 99L, Dummy.Two, 0L, 5L));
		ArrayList<PlannedAction> result = contractManager.scheduleDeliveriesBeginningAt(new TimeStamp(0L));
		assertEquals(0, result.size());
	}

	@Test
	public void testUpdateAndGetNextContractActionsNone() {
		ArrayList<PlannedAction> result = contractManager.updateAndGetNextContractActions(new TimeStamp(0L));
		assertNotNull(result);
	}

	@Test
	public void testUpdateAndGetNextContractActions() {
		TimeStamp now = new TimeStamp(5L);
		spyOnProductTimes.linkProductToTime(now, Dummy.One);
		spyOnProductTimes.linkProductToTime(now, Dummy.Two);

		Contract mockContractA = mock(Contract.class);
		when(mockContractA.getNextTimeOfDeliveryAfter(any())).thenReturn(new TimeStamp(11L));
		Contract mockContractB = mock(Contract.class);
		when(mockContractB.getNextTimeOfDeliveryAfter(any())).thenReturn(new TimeStamp(12L));
		spyOnProductContracts.put(Dummy.One, new ArrayList<>());
		spyOnProductContracts.get(Dummy.One).add(mockContractA);
		spyOnProductContracts.put(Dummy.Two, new ArrayList<>());
		spyOnProductContracts.get(Dummy.Two).add(mockContractB);

		ArrayList<PlannedAction> result = contractManager.updateAndGetNextContractActions(now);
		assertEquals(2, result.size());
		PlannedAction actionA = result.get(0);
		PlannedAction actionB = result.get(1);

		assertEquals(Dummy.One, actionA.getSchedulingReason());
		assertEquals(11L, actionA.getTimeStamp().getStep());
		assertEquals(Dummy.Two, actionB.getSchedulingReason());
		assertEquals(12L, actionB.getTimeStamp().getStep());

		assertFalse(spyOnProductTimes.drawProductsForTime(new TimeStamp(11L)).isEmpty());
		assertFalse(spyOnProductTimes.drawProductsForTime(new TimeStamp(12L)).isEmpty());
	}
}