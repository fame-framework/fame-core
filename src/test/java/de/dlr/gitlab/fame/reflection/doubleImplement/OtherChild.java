// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.doubleImplement;

import de.dlr.gitlab.fame.agent.input.DataProvider;

public class OtherChild extends OtherMother implements AbilityX {

	public OtherChild(DataProvider dataProvider) {
		super(dataProvider);
	}
}