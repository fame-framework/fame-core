// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.doubleAnnotation;

import de.dlr.gitlab.fame.reflection.Dummy;

public class DoublyAnnotatedEnums {
	@Dummy
	public enum FirstEnum {
		A, B, C
	}

	@Dummy
	public enum SecondEnum {
		X, Y, Z
	}
}