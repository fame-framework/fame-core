// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection;

import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;

public class DummyDataItemB extends DataItem {
	public DummyDataItemB() {}

	public DummyDataItemB(ProtoDataItem protoData) {}

	@Override
	protected void fillDataFields(Builder builder) {}
}