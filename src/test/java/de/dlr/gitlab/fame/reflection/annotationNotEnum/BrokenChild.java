// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.annotationNotEnum;

import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.reflection.packA.Mother;

@Dummy
public abstract class BrokenChild extends Mother{

	public BrokenChild(DataProvider dataProvider) {
		super(dataProvider);
	}

}