// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.indexNonStatic;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;

public class NonStatic extends Agent {
	@Output
	public enum Columns {
		A, B, C
	};

	public enum Subindex {
		T, L
	}

	public final ComplexIndex<Subindex> complexIndexMother = ComplexIndex.build(Columns.A, Subindex.class);

	public NonStatic(DataProvider dataProvider) {
		super(dataProvider);
	}
}
