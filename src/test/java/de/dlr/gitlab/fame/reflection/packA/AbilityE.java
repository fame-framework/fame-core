// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.AgentAbility;
import de.dlr.gitlab.fame.reflection.Dummy;

public interface AbilityE extends AgentAbility {
	@Dummy
	public enum AbilityEEnum {
		AbilityE1
	}

}
