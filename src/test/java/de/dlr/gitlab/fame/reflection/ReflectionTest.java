// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.reflection.broken.BrokenAgentDummy;
import de.dlr.gitlab.fame.reflection.doubleAnnotation.DoublyAnnotatedEnums;
import de.dlr.gitlab.fame.reflection.doubleEnum.AbilityB.AbilityBEnum;
import de.dlr.gitlab.fame.reflection.doubleEnum.GrandSon;
import de.dlr.gitlab.fame.reflection.doubleImplement.AbilityX.AbilityXEnum;
import de.dlr.gitlab.fame.reflection.doubleImplement.OtherChild;
import de.dlr.gitlab.fame.reflection.doubleImplement.OtherMother;
import de.dlr.gitlab.fame.reflection.packA.AbilityA.AbilityAColumns;
import de.dlr.gitlab.fame.reflection.packA.AbilityA.AbilityAEnum;
import de.dlr.gitlab.fame.reflection.packA.AbilityAPlus.AbilityAPlusEnum;
import de.dlr.gitlab.fame.reflection.packA.AbilityC;
import de.dlr.gitlab.fame.reflection.packA.AbilityC.AbilityCEnum;
import de.dlr.gitlab.fame.reflection.packA.AbilityD;
import de.dlr.gitlab.fame.reflection.packA.AbilityD.AbilityDEnum;
import de.dlr.gitlab.fame.reflection.packA.AbilityE.AbilityEEnum;
import de.dlr.gitlab.fame.reflection.packA.Child;
import de.dlr.gitlab.fame.reflection.packA.GrandChild;
import de.dlr.gitlab.fame.reflection.packA.GrandChild.ChildColumns;
import de.dlr.gitlab.fame.reflection.packA.GrandChild.ChildSubindex1;
import de.dlr.gitlab.fame.reflection.packA.GrandChild.GrandChildEnum;
import de.dlr.gitlab.fame.reflection.packA.GrandDaughter;
import de.dlr.gitlab.fame.reflection.packA.Mother;
import de.dlr.gitlab.fame.reflection.packA.Mother.MotherColumns;
import de.dlr.gitlab.fame.reflection.packA.Mother.MotherEnum;
import de.dlr.gitlab.fame.reflection.packA.NoAbility.NoAbilityEnum;
import de.dlr.gitlab.fame.reflection.packA.NoChild;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.util.Reflection;
import de.dlr.gitlab.fame.util.Reflection.ReflectionType;

/** Tests for {@link Reflection}
 * 
 * @author Christoph Schimeczek, Achraf El Ghazi */
public class ReflectionTest {
	private List<String> packagesNames = new ArrayList<>();

	@Before
	public void setup() {
		packagesNames.clear();
	}

	@Test
	public void constructor_agentPackagesEmpty_works() {
		createdReflectionHasGivenPackageCount(Collections.emptyList(), ReflectionType.AGENT, 0);
	}

	/** Creates Reflection of given Type with specified packageNames and tests if number of considered packages matches given valid
	 * entries in given packagesNames list (also considering potential FAME default packages) */
	private void createdReflectionHasGivenPackageCount(List<String> packagesNames, ReflectionType type,
			int ownPackageCount) {
		Reflection reflection = new Reflection(type, packagesNames);
		int defaultPackageCount = Reflection.getFamePackageNames(type) != null ? 1 : 0;
		int expectedPackageCount = ownPackageCount + defaultPackageCount;
		assertEquals(expectedPackageCount, reflection.getPackageCount());
	}

	@Test
	public void constructor_communicationPackagesEmpty_works() {
		createdReflectionHasGivenPackageCount(Collections.emptyList(), ReflectionType.COMMUNICATION, 0);
	}

	@Test
	public void constructor_portablePackagesEmpty_works() {
		createdReflectionHasGivenPackageCount(Collections.emptyList(), ReflectionType.PORTABLE, 0);
	}

	@Test
	public void constructor_onePackage_addedToReflection() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		createdReflectionHasGivenPackageCount(packagesNames, ReflectionType.AGENT, 1);
	}

	@Test
	public void constructor_twoPackages_addedToReflection() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		packagesNames.add("de.dlr.gitlab.fame.reflection.doubleEnum");
		createdReflectionHasGivenPackageCount(packagesNames, ReflectionType.COMMUNICATION, 2);
	}

	@Test
	public void constructor_nullPackage_works() {
		createdReflectionHasGivenPackageCount(null, ReflectionType.AGENT, 0);
	}

	@Test
	public void constructor_nullStringInPackageNames_works() {
		packagesNames.add(null);
		createdReflectionHasGivenPackageCount(packagesNames, ReflectionType.AGENT, 0);
	}

	@Test
	public void constructor_illegalPackageName_throws() {
		packagesNames.add("IllegalPackageName");
		assertThrowsFatalMessage(Reflection.ERR_ILLEGAL_PACKAGE, () -> new Reflection(ReflectionType.AGENT, packagesNames));
	}

	@Test
	public void mapChildrenToClassName_directChild_considered() {
		assertEquals(Child.class, getChildrenOfMotherInPackAByName().get("Child"));
	}

	/** @return Children of {@link Mother} in package 'packA' mapped to their SimpleName */
	private HashMap<String, Class<? extends Mother>> getChildrenOfMotherInPackAByName() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		return reflection.mapChildrenToClassName(Mother.class);
	}

	@Test
	public void mapChildrenToClassName_grandChild_considered() {
		assertEquals(GrandChild.class, getChildrenOfMotherInPackAByName().get("GrandChild"));
	}

	@Test
	public void mapChildrenToClassName_childNotInPackage_ignored() {
		assertNull(getChildrenOfMotherInPackAByName().get("GrandSon"));
	}

	@Test
	public void mapChildrenToClassName_noChild_ignored() {
		assertNull(getChildrenOfMotherInPackAByName().get("NoChild"));
	}

	@Test
	public void getSubTypesOf_directChild_considered() {
		assertTrue(getChildrenOfMotherInPackA().contains(Child.class));
	}

	/** @return set of all children of class {@link Mother} in package 'packA' */
	private Set<Class<? extends Mother>> getChildrenOfMotherInPackA() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		return reflection.getSubTypesOf(Mother.class);
	}

	@Test
	public void getSubTypesOf_grandChild_considered() {
		assertTrue(getChildrenOfMotherInPackA().contains(GrandChild.class));
	}

	@Test
	public void getSubTypesOf_childNotInPackage_ignored() {
		assertFalse(getChildrenOfMotherInPackA().contains(GrandSon.class));
	}

	@Test
	public void getSubTypesOf_noChild_ignored() {
		assertFalse(getChildrenOfMotherInPackA().contains(NoChild.class));
	}

	@Test
	public void findAllInstantiableChildrenOf_abstractChild_ignored() {
		assertFalse(getInstantiableChildsOfMotherInPackA().contains(Child.class));
	}

	/** @return list of all instantiable children of class {@link Mother} in package 'packA' */
	private ArrayList<Class<? extends Mother>> getInstantiableChildsOfMotherInPackA() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		return reflection.findAllInstantiableChildrenOf(Mother.class);
	}

	@Test
	public void findAllInstantiableChildrenOf_noChild_ignored() {
		assertFalse(getInstantiableChildsOfMotherInPackA().contains(NoChild.class));
	}

	@Test
	public void findAllInstantiableChildrenOf_instantiableChild_considered() {
		assertTrue(getInstantiableChildsOfMotherInPackA().contains(GrandChild.class));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_annotatedParent_inherited() {
		assertTrue(getEnumsForChildOfMotherInPackA(Dummy.class, Child.class).contains(MotherEnum.MotherA));
	}

	/** @return list of all enums annotated with given annotation found for given childClass in package 'packA' of {@link Mother} */
	private List<Enum<?>> getEnumsForChildOfMotherInPackA(Class<? extends Annotation> annotation,
			Class<? extends Mother> childClass) {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		return reflection.findAnnotatedEnumsInHierarchyForChildrenOf(annotation, Mother.class).get(childClass);
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_otherParentAnnotation_ignored() {
		assertFalse(getEnumsForChildOfMotherInPackA(Dummy.class, Child.class).contains(MotherColumns.A));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_notAnnotatedParent_ignored() {
		assertFalse(getEnumsForChildOfMotherInPackA(Dummy.class, Child.class).contains(MotherColumns.A));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_notAnnotatedOwn_ignored() {
		assertFalse(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(ChildSubindex1.U));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_otherOwnAnnotation_ignored() {
		assertFalse(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(ChildColumns.X));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_annotatedOwn_considered() {
		assertTrue(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(GrandChildEnum.GrandChildA));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_annotatedGrandParent_inherited() {
		assertTrue(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(MotherEnum.MotherA));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_multipleAnnotationsOnSingleClass_throws() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.doubleAnnotation");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_MULTI_USE,
				() -> reflection.findAnnotatedEnumsInHierarchyForChildrenOf(Dummy.class, DoublyAnnotatedEnums.class));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_doubleEnumDeclaration_throws() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		packagesNames.add("de.dlr.gitlab.fame.reflection.doubleEnum");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_DOUBLE_DECLARATION,
				() -> reflection.findAnnotatedEnumsInHierarchyForChildrenOf(Dummy.class, Mother.class));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_annotationOnOtherThanEnum_throws() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.annotationNotEnum");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_NOT_ENUM,
				() -> reflection.findAnnotatedEnumsInHierarchyForChildrenOf(Dummy.class, Mother.class));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_ownPackageInterface_considered() {
		assertTrue(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(AbilityAPlusEnum.AbilityAPlus1));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_ownMultipleInterfaces_considered() {
		assertTrue(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(AbilityDEnum.AbilityD1));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_otherPackageInterface_ignored() {
		assertFalse(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(AbilityBEnum.AbilityB1));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_interfaceInheritance_ignored() {
		assertFalse(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(AbilityAEnum.AbilityA1));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_otherInterface_ignored() {
		assertFalse(getEnumsForChildOfMotherInPackA(Dummy.class, GrandDaughter.class).contains(NoAbilityEnum.NoAbility));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_interfaceInheritedFromParent_considered() {
		assertTrue(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(AbilityCEnum.AbilityC1));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_multipleInterfacesInheritedFromParent_considered() {
		assertTrue(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(AbilityEEnum.AbilityE1));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_interfaceOtherAnnotation_ignored() {
		assertFalse(getEnumsForChildOfMotherInPackA(Dummy.class, GrandChild.class).contains(AbilityAColumns.ColumnA1));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_interfaceDoubleImplemented_consideredOnce() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.doubleImplement");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		List<Enum<?>> enums = reflection.findAnnotatedEnumsInHierarchyForChildrenOf(Dummy.class, OtherMother.class)
				.get(OtherChild.class);
		assertTrue(enums.contains(AbilityXEnum.AbilityX1));
	}

	@Test
	public void findAnnotatedEnumsInHierarchyForChildrenOf_brokenCode_raises() {
		// This test requires a class with syntax errors named "BrokenAgentDummy"
		// in package de.dlr.gitlab.fame.reflection.broken
		// The class must include an enum annotated with @Dummy
		packagesNames.add("de.dlr.gitlab.fame.reflection.broken");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		// assertThrowsFatalMessage(Reflection.ERR_BROKEN_CODE,
		// () -> reflection.findAnnotatedEnumsInHierarchyForChildrenOf(Dummy.class, BrokenAgentDummy.class));
	}

	@Test
	public void findComplexIndexHierarchyForAgents_indexNonStatic_throws() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.indexNonStatic");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_MODIFIERS_MISSING,
				() -> reflection.findComplexIndexHierarchyForAgents());
	}

	@Test
	public void findComplexIndexHierarchyForAgents_indexNonFinal_throws() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.indexNonFinal");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_MODIFIERS_MISSING,
				() -> reflection.findComplexIndexHierarchyForAgents());
	}

	@Test
	public void findComplexIndexHierarchyForAgents_oneOwnIndex_considered() {
		assertTrue(getComplexIndicesInPackAForAgent(Mother.class).contains(Mother.complexIndexMother));
	}

	/** @return list of all {@link ComplexIndex}es found on given class */
	private List<ComplexIndex<? extends Enum<?>>> getComplexIndicesInPackAForAgent(Class<? extends Agent> clas) {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		return reflection.findComplexIndexHierarchyForAgents().get(clas.getSimpleName());
	}

	@Test
	public void findComplexIndexHierarchyForAgents_multipleOwnIndexes_considered() {
		assertTrue(getComplexIndicesInPackAForAgent(GrandChild.class).contains(GrandChild.complexIndexChild1));
		assertTrue(getComplexIndicesInPackAForAgent(GrandChild.class).contains(GrandChild.complexIndexChild2));
	}

	@Test
	public void findComplexIndexHierarchyForAgents_parentIndex_inherited() {
		assertTrue(getComplexIndicesInPackAForAgent(GrandChild.class).contains(Mother.complexIndexMother));
	}

	@Test
	public void findComplexIndexHierarchyForAgents_ownInterface_inherited() {
		assertTrue(getComplexIndicesInPackAForAgent(Mother.class).contains(AbilityC.complexIndexAbilityC));
	}

	@Test
	public void findComplexIndexHierarchyForAgents_parentInterface_inherited() {
		assertTrue(getComplexIndicesInPackAForAgent(GrandChild.class).contains(AbilityC.complexIndexAbilityC));
	}

	@Test
	public void findComplexIndexHierarchyForAgents_multipleInterfaces_considered() {
		assertTrue(getComplexIndicesInPackAForAgent(Mother.class).contains(AbilityC.complexIndexAbilityC));
		assertTrue(getComplexIndicesInPackAForAgent(GrandChild.class).contains(AbilityD.complexIndexAbilityD));
	}

}