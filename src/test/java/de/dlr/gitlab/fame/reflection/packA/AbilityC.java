// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.AgentAbility;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;

public interface AbilityC extends AgentAbility {
	@Dummy
	public enum AbilityCEnum {
		AbilityC1
	}

	@Output
	public enum AbilityCColumns {
		Cout
	}

	public enum AbilityCSubindex {
		K, V
	}

	public static final ComplexIndex<AbilityCSubindex> complexIndexAbilityC = ComplexIndex.build(AbilityCColumns.Cout,
			AbilityCSubindex.class);

}
