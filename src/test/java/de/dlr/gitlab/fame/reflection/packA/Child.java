// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.input.DataProvider;

public abstract class Child extends Mother{

	public Child(DataProvider dataProvider) {
		super(dataProvider);
	}

}