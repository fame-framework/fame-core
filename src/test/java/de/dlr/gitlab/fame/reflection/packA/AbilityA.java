// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.AgentAbility;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;

public interface AbilityA extends AgentAbility {
	@Dummy
	public enum AbilityAEnum {
		AbilityA1, AbilityA2
	}

	@Output
	public enum AbilityAColumns {
		ColumnA1, ColumnA2
	}
	
	public enum AbilityASubindex {
		K, V
	}

	public static final ComplexIndex<AbilityASubindex> complexIndexAbilityA = ComplexIndex.build(AbilityAColumns.ColumnA1,
			AbilityASubindex.class);
	
}
