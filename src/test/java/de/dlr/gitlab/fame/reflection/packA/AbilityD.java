// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.AgentAbility;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;

public interface AbilityD extends AgentAbility {
	@Dummy
	public enum AbilityDEnum {
		AbilityD1
	}

	@Output
	public enum AbilityDColumns {
		Dout
	}

	public enum AbilityDSubindex {
		K, V
	}

	public static final ComplexIndex<AbilityDSubindex> complexIndexAbilityD = ComplexIndex.build(AbilityDColumns.Dout,
			AbilityDSubindex.class);

}
