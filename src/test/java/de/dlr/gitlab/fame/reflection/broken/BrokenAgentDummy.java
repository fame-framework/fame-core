// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.broken;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.reflection.Dummy;

/** This class requires syntax errors if you w */
public class BrokenAgentDummy extends Agent {
	@Dummy
	public enum MyProducts {
		ProdA, ProdB
	};

	public BrokenAgentDummy(DataProvider param) {
		super(param);
	}

	// add some broken code somewhere, e.g. by un-commenting this line
}