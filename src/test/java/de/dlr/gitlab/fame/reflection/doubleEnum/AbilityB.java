// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.reflection.doubleEnum;

import de.dlr.gitlab.fame.agent.AgentAbility;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;

public interface AbilityB extends AgentAbility {
	@Dummy
	public enum AbilityBEnum {
		AbilityB1, AbilityB2
	}

	@Output
	public enum AbilityBColumns {
		Bout
	}

	public enum AbilityBSubindex {
		K, V
	}

	public static final ComplexIndex<AbilityBSubindex> complexIndexAbilityB = ComplexIndex.build(AbilityBColumns.Bout,
			AbilityBSubindex.class);

}
