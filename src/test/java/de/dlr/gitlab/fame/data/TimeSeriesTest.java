// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.data;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao.Builder;
import de.dlr.gitlab.fame.testUtils.LogChecker;
import de.dlr.gitlab.fame.time.TimeStamp;

public class TimeSeriesTest {
	private Builder builder = TimeSeriesDao.newBuilder();
	private TimeSeries timeSeries;
	private String seriesName = "MySeries";
	private int seriesId = 15;

	private LogChecker logChecker = new LogChecker(TimeSeries.class);

	@Before
	public void setup() {
		logChecker.clear();
		builder.clear();
		builder.addAllTimeSteps(Arrays.asList(new Long[] {-10L, 0L, 10L}));
		builder.addAllValues(Arrays.asList(new Double[] {-20.0, -10.0, 30.0}));
		builder.setSeriesId(seriesId);
		builder.setSeriesName(seriesName);
		timeSeries = new TimeSeries(builder.build());
		TimeSeries.resetExtrapolationWarningCount();
	}

	@Test
	public void constructor_duplicateTimes_raises() {
		builder.addTimeSteps(0L).addValues(5.0);
		assertThrowsFatalMessage(TimeSeries.ERR_DUPLICATION, () -> new TimeSeries(builder.build()));
	}

	@Test
	public void test_toString() {
		assertEquals("Series(" + seriesId + ": " + seriesName + ")", timeSeries.toString());
	}

	@Test
	public void getValueLinear_onTime_returnsExactValue() {
		assertEquals(-20.0, timeSeries.getValueLinear(new TimeStamp(-10L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueLinear(new TimeStamp(0L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueLinear(new TimeStamp(10L)), 1E-12);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueLinear_betweenTimes_Interpolated() {
		assertEquals(-15.0, timeSeries.getValueLinear(new TimeStamp(-5L)), 1E-12);
		assertEquals(-6.0, timeSeries.getValueLinear(new TimeStamp(1L)), 1E-12);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueLinear_beforeFirst_returnsFirst() {
		assertEquals(-20.0, timeSeries.getValueLinear(new TimeStamp(-20L)), 1E-12);
		logChecker.assertLogsContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
	}

	@Test
	public void getValueLinear_afterLast_returnsLast() {
		assertEquals(30.0, timeSeries.getValueLinear(new TimeStamp(15L)), 1E-12);
		logChecker.assertLogsContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueEarlierEqual_onTime_returnsExactValue() {
		assertEquals(-20.0, timeSeries.getValueEarlierEqual(new TimeStamp(-10L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueEarlierEqual(new TimeStamp(0L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueEarlierEqual(new TimeStamp(10L)), 1E-12);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueEarlierEqual_betweenTimes_returnsEarlier() {
		assertEquals(-20.0, timeSeries.getValueEarlierEqual(new TimeStamp(-5L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueEarlierEqual(new TimeStamp(1L)), 1E-12);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueEarlierEqual_beforeFirst_returnsFirst() {
		assertEquals(-20.0, timeSeries.getValueEarlierEqual(new TimeStamp(-15L)), 1E-12);
		logChecker.assertLogsContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
	}

	@Test
	public void getValueEarlierEqual_afterLast_returnsLast() {
		assertEquals(30.0, timeSeries.getValueEarlierEqual(new TimeStamp(15L)), 1E-12);
		logChecker.assertLogsContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueLaterEqual_onTime_returnsExactValue() {
		assertEquals(-20.0, timeSeries.getValueLaterEqual(new TimeStamp(-10L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueLaterEqual(new TimeStamp(0L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueLaterEqual(new TimeStamp(10L)), 1E-12);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueLaterEqualbetweenTimes_returnsNext() {
		assertEquals(-10.0, timeSeries.getValueLaterEqual(new TimeStamp(-5L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueLaterEqual(new TimeStamp(1L)), 1E-12);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueLaterEqual_beforeFirst_returnsFirst() {
		assertEquals(-20.0, timeSeries.getValueLaterEqual(new TimeStamp(-15L)), 1E-12);
		logChecker.assertLogsContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueLaterEqual_afterLast_returnsLast() {
		assertEquals(30.0, timeSeries.getValueLaterEqual(new TimeStamp(15L)), 1E-12);
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_LOWER);
		logChecker.assertLogsContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getId_returnsId() {
		assertEquals(seriesId, timeSeries.getId());
	}

	@Test
	public void getValueX_extrapolationAfterMaximumCount_notLogged() {
		for (int i = 0; i < TimeSeries.MAX_WARNING_COUNT; i++) {
			timeSeries.getValueLaterEqual(new TimeStamp(15L));
			logChecker.assertLogsContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
		}
		logChecker.clear();
		timeSeries.getValueLaterEqual(new TimeStamp(15L));
		logChecker.assertLogsDoNotContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
	}

	@Test
	public void getValueX_extrapolationAtMaximumCount_logsError() {
		for (int i = 0; i < TimeSeries.MAX_WARNING_COUNT; i++) {
			timeSeries.getValueLaterEqual(new TimeStamp(15L));
			logChecker.assertLogsContain(TimeSeries.WARN_EXTRAPOLATING_HIGHER);
		}
		logChecker.clear();
		timeSeries.getValueLaterEqual(new TimeStamp(15L));
		logChecker.assertLogsContain(TimeSeries.ERR_TOO_MANY_EXTRAPOLATIONS);
	}
}