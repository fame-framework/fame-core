// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.output;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Services.Output.Builder;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series;

public class BufferManagerTest {
	private BufferManager bufferManager;
	private List<String> activeClasses = Arrays.asList(ActiveClassA.class.getSimpleName(),
			ActiveClassB.class.getSimpleName());

	private enum Columns {
		A, B, C
	};

	private enum OtherColumns {
		X, Y, Z
	};

	private List<Enum<?>> columnList = new ArrayList<>();
	private List<Enum<?>> otherColumnList = new ArrayList<>();
	private HashMap<Class<? extends Agent>, List<Enum<?>>> outputColumnLists;
	private HashMap<String, List<ComplexIndex<? extends Enum<?>>>> complexColumnLists;

	public class ActiveClassA extends Agent {
		public ActiveClassA(DataProvider param) {
			super(param);
		}
	};

	public class ActiveClassB extends Agent {
		public ActiveClassB(DataProvider param) {
			super(param);
		}
	};

	public class InactiveClass extends Agent {
		public InactiveClass(DataProvider param) {
			super(param);
		}
	};

	@Before
	public void setUp() {
		bufferManager = new BufferManager();
		outputColumnLists = new HashMap<>();
		complexColumnLists = new HashMap<>();

		bufferManager.setBufferParameters(activeClasses, outputColumnLists, complexColumnLists, true);
		columnList.clear();
		columnList.addAll(Arrays.asList(Columns.values()));
		otherColumnList.clear();
		otherColumnList.addAll(Arrays.asList(OtherColumns.values()));
	}

	@Test
	public void createBuffer_agentNotRegistered_returnsNull() {
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 0);
		assertNull(buffer);
	}

	@Test
	public void createBuffer_agentHasNoOutputColumns_returnsNull() {
		outputColumnLists.put(ActiveClassA.class, new ArrayList<Enum<?>>());
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 0);
		assertNull(buffer);
	}

	@Test
	public void createBuffer_flushAllBuffersToOutputBuilder_containsAgentClassHeader() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		bufferManager.createBuffer(ActiveClassA.class, 4);
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		AgentType agentType = outputBuilder.getAgentTypes(0);
		assertEquals("ActiveClassA", agentType.getClassName());
		assertEquals("A", agentType.getFields(0).getFieldName());
		assertEquals("B", agentType.getFields(1).getFieldName());
		assertEquals("C", agentType.getFields(2).getFieldName());
	}

	@Test
	public void createBuffer_flushAllBuffersToOutputBuilder_notRepeatClassHeader() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		bufferManager.createBuffer(ActiveClassA.class, 4);
		bufferManager.flushAllBuffersToOutputBuilder();
		bufferManager.createBuffer(ActiveClassA.class, 2);
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		assertEquals(0, outputBuilder.getAgentTypesCount());
	}

	@Test
	public void createBuffer_flushAllBuffersToOutputBuilder_noHeaderForInactiveClass() {
		outputColumnLists.put(InactiveClass.class, columnList);
		bufferManager.createBuffer(InactiveClass.class, 4);
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		assertEquals(0, outputBuilder.getAgentTypesCount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void createBuffer_flushAllBuffersToOutputBuilder_HeaderHasComplexColumn() {
		ArrayList<ComplexIndex<? extends Enum<?>>> indices = new ArrayList<>();
		ComplexIndex<OtherColumns> complexIndex = mock(ComplexIndex.class);
		indices.add(complexIndex);
		complexColumnLists.put(ActiveClassA.class.getSimpleName(), indices);
		when(complexIndex.getFieldName()).thenReturn("A");
		when(complexIndex.getKeyLabels()).thenReturn(OtherColumns.values());

		outputColumnLists.put(ActiveClassA.class, columnList);
		bufferManager.createBuffer(ActiveClassA.class, 4);
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		AgentType agentType = outputBuilder.getAgentTypes(0);
		assertTrue(agentType.getFields(0).getIndexNamesList().contains("X"));
		assertFalse(agentType.getFields(1).getIndexNamesList().contains("X"));
		assertFalse(agentType.getFields(2).getIndexNamesList().contains("X"));
	}

	@Test
	public void createBuffer_inactiveClass_returnsIgnoreBuffer() {
		outputColumnLists.put(InactiveClass.class, columnList);
		OutputBuffer buffer = bufferManager.createBuffer(InactiveClass.class, 4);
		assertTrue(buffer instanceof IgnoreBuffer);
	}

	@Test
	public void createBuffer_activeClass_returnsAllColumnBuffer() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 4);
		assertTrue(buffer instanceof AllColumnBuffer);
	}

	@Test
	public void createBuffer_activeClassComplexDisabled_returnsSimpleColumnBuffer() {
		bufferManager.setBufferParameters(activeClasses, outputColumnLists, complexColumnLists, false);
		outputColumnLists.put(ActiveClassA.class, columnList);
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 4);
		assertTrue(buffer instanceof SimpleColumnBuffer);
	}

	@Test
	public void createBuffer_forwardsAgentId() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 99);
		assertEquals(99L, buffer.getAgentId());
	}

	@Test
	public void flushAllBuffersToOutputBuilder_fetchesSeriesForActiveClass() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		OutputBuffer mockedBuffer;
		try (MockedConstruction<AllColumnBuffer> mocked = Mockito.mockConstruction(AllColumnBuffer.class)) {
			mockedBuffer = bufferManager.createBuffer(ActiveClassA.class, 4);
		}
		when(mockedBuffer.getSeries()).thenReturn(mock(Series.class));

		bufferManager.flushAllBuffersToOutputBuilder();
		verify(mockedBuffer, times(1)).getSeries();
	}

	@Test
	public void flushAllBuffersToOutputBuilder_ignoresInactiveClass() {
		outputColumnLists.put(InactiveClass.class, columnList);
		OutputBuffer mockedBuffer;
		try (MockedConstruction<IgnoreBuffer> mocked = Mockito.mockConstruction(IgnoreBuffer.class)) {
			mockedBuffer = bufferManager.createBuffer(InactiveClass.class, 4);
		}
		when(mockedBuffer.getSeries()).thenReturn(mock(Series.class));

		bufferManager.flushAllBuffersToOutputBuilder();
		verify(mockedBuffer, times(0)).getSeries();
	}

	@Test
	public void isActive_activeClass_returnsTrue() {
		assertTrue(bufferManager.isActive(ActiveClassA.class.getSimpleName()));
	}

	@Test
	public void isActive_inactiveClass_returnsFalse() {
		assertFalse(bufferManager.isActive(InactiveClass.class.getSimpleName()));
	}

	@Test
	public void isActive_listIsNull_returnsTrueForAllClasses() {
		bufferManager.setBufferParameters(null, outputColumnLists, complexColumnLists, false);
		assertTrue(bufferManager.isActive(InactiveClass.class.getSimpleName()));
		assertTrue(bufferManager.isActive(ActiveClassA.class.getSimpleName()));
		assertTrue(bufferManager.isActive(ActiveClassB.class.getSimpleName()));
	}

	@Test
	public void isActive_listIsEmpty_returnsTrueForAllClasses() {
		bufferManager.setBufferParameters(new ArrayList<String>(), outputColumnLists, complexColumnLists, false);
		assertTrue(bufferManager.isActive(InactiveClass.class.getSimpleName()));
		assertTrue(bufferManager.isActive(ActiveClassA.class.getSimpleName()));
		assertTrue(bufferManager.isActive(ActiveClassB.class.getSimpleName()));
	}

	@Test
	public void finishTick_activeBuffers_ticked() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		outputColumnLists.put(ActiveClassB.class, otherColumnList);

		OutputBuffer bufferA;
		OutputBuffer bufferB;
		try (MockedConstruction<AllColumnBuffer> mocked = Mockito.mockConstruction(AllColumnBuffer.class)) {
			bufferA = bufferManager.createBuffer(ActiveClassA.class, 4);
			bufferB = bufferManager.createBuffer(ActiveClassA.class, 2);
		}
		bufferManager.finishTick(42L);
		verify(bufferA, times(1)).tick(42L);
		verify(bufferB, times(1)).tick(42L);
	}

	@Test
	public void finishTick_inactiveBuffers_notTicked() {
		outputColumnLists.put(InactiveClass.class, columnList);
		OutputBuffer mockedBuffer;
		try (MockedConstruction<IgnoreBuffer> mocked = Mockito.mockConstruction(IgnoreBuffer.class)) {
			mockedBuffer = bufferManager.createBuffer(InactiveClass.class, 4);
		}
		bufferManager.finishTick(42L);
		verify(mockedBuffer, times(0)).tick(42L);
	}
}