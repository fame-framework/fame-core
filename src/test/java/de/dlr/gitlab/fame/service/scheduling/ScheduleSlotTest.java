// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.scheduling;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.mock;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;

public class ScheduleSlotTest {

	private enum Reasons {Reason1, Reason2}
	
	private ScheduleSlot scheduleSlot;
	
	@Before
	public void setUp() {
		scheduleSlot = new ScheduleSlot();
	}
	
	@Test
	public void getScheduleAgents_noEntries_returnsEmptySetOfAgents() {
		assert scheduleSlot.getScheduleAgents().isEmpty();
	}
	
	@Test
	public void getReasons_noReasons_returnsUnmodifiableEmptyList() {
		List<Enum<?>> result = scheduleSlot.getReasons(mock(Agent.class));
		assert result.isEmpty();
		assertThrows(RuntimeException.class, () -> result.add(Reasons.Reason1));
	}
	
	@Test
	public void addEntry_agentAddedToSet() {
		Agent agent = mock(Agent.class);
		scheduleSlot.addEntry(agent, Reasons.Reason1);
		assert scheduleSlot.getScheduleAgents().contains(agent);
	}
	
	@Test
	public void addEntry_reasonAddedToList() {
		Agent agent = mock(Agent.class);
		scheduleSlot.addEntry(agent, Reasons.Reason1);
		scheduleSlot.addEntry(agent, Reasons.Reason2);
		List<Enum<?>> result = scheduleSlot.getReasons(agent);
		assert result.contains(Reasons.Reason1);
		assert result.contains(Reasons.Reason2);
	}
}
