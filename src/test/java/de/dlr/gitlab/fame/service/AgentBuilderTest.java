// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.dummy.AgentDummy;
import de.dlr.gitlab.fame.agent.dummy.AgentDummyAbstract;
import de.dlr.gitlab.fame.agent.dummy.AgentDummyConstructorThrows;
import de.dlr.gitlab.fame.agent.dummy.AgentDummyPrivateConstructor;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.LogChecker;

public class AgentBuilderTest {
	private AgentBuilder agentBuilder;
	private MpiManager mockMpiManager;
	private LogChecker logChecker = new LogChecker(AgentBuilder.class);
	private LocalServices mockedServices;

	public class DummyClass {}

	@Before
	public void beforeEachTest() {
		this.mockMpiManager = mock(MpiManager.class);
		this.mockedServices = mock(LocalServices.class);
		this.agentBuilder = new AgentBuilder(mockMpiManager, new ArrayList<>(), null, mockedServices);
		logChecker.clear();
	}

	@Test
	public void testBuildAgentOnThisProcess() {
		when(mockMpiManager.getRank()).thenReturn(1);
		when(mockMpiManager.getProcessCount()).thenReturn(5);
		this.agentBuilder = new AgentBuilder(mockMpiManager, new ArrayList<>(), null, null);

		assertTrue((boolean) AccessPrivates.callPrivateMethodOn("buildAgentOnThisProcess", agentBuilder, 11));
		assertTrue((boolean) AccessPrivates.callPrivateMethodOn("buildAgentOnThisProcess", agentBuilder, 21));
		assertTrue((boolean) AccessPrivates.callPrivateMethodOn("buildAgentOnThisProcess", agentBuilder, 1));
		assertFalse((boolean) AccessPrivates.callPrivateMethodOn("buildAgentOnThisProcess", agentBuilder, 2));
		assertFalse((boolean) AccessPrivates.callPrivateMethodOn("buildAgentOnThisProcess", agentBuilder, 9));
		assertFalse((boolean) AccessPrivates.callPrivateMethodOn("buildAgentOnThisProcess", agentBuilder, 13));
	}

	@Test
	public void testGetClassFor() {
		Class<?> clas = (Class<?>) AccessPrivates.callPrivateMethodOn("getClassFor", agentBuilder, "AgentDummy");
		assertEquals(AgentDummy.class, clas);
	}

	@Test
	public void testGetClassForFailUnknown() {
		assertThrowsFatalMessage(AgentBuilder.ERR_CLASS_NOT_FOUND,
				() -> AccessPrivates.callPrivateMethodOn("getClassFor", agentBuilder, "AXAXAX"));
	}

	@Test
	public void testGetConstructorFor() throws NoSuchMethodException, SecurityException {
		Constructor<?> constructor = (Constructor<?>) AccessPrivates.callPrivateMethodOn("getConstructorFor", agentBuilder,
				AgentDummy.class);
		assertEquals(AgentDummy.class.getConstructor(DataProvider.class), constructor);
	}

	@Test
	public void testGetConstructorForFailWrongClass() {
		assertThrowsFatalMessage(AgentBuilder.ERR_NO_CONSTRUCTOR,
				() -> AccessPrivates.callPrivateMethodOn("getConstructorFor", agentBuilder, DummyClass.class));
	}

	@Test
	public void testCreateAgentFromConfig() {
		AgentDao dao = AgentDao.newBuilder().setId(0L).setClassName("AgentDummy").build();
		Agent agent = (Agent) AccessPrivates.callPrivateMethodOn("createAgentFromConfig", agentBuilder, dao);
		assertTrue(agent instanceof AgentDummy);
	}

	@Test
	public void testInstantiate() throws NoSuchMethodException, SecurityException {
		Constructor<?> constructor = AgentDummy.class.getConstructor(DataProvider.class);
		AgentDao dao = AgentDao.newBuilder().setId(0L).setClassName("Irrelevant").build();
		Agent agent = (Agent) AccessPrivates.callPrivateMethodOn("instantiate", agentBuilder, constructor, dao);
		assertTrue(agent instanceof AgentDummy);
	}

	@Test
	public void testInstatiateConstructorAccessFail() throws NoSuchMethodException, SecurityException {
		Constructor<?> constructor = AgentDummyPrivateConstructor.class.getDeclaredConstructor(DataProvider.class);
		AgentDao dao = AgentDao.newBuilder().setId(0L).setClassName("Irrelevant").build();
		assertThrowsFatalMessage(AgentBuilder.ERR_CREATION_FAILED,
				() -> AccessPrivates.callPrivateMethodOn("instantiate", agentBuilder, constructor, dao));
	}

	@Test
	public void testInstatiateAbstractClassFail() throws NoSuchMethodException, SecurityException {
		Constructor<?> constructor = AgentDummyAbstract.class.getDeclaredConstructor(DataProvider.class);
		AgentDao dao = AgentDao.newBuilder().setId(0L).setClassName("Irrelevant").build();
		assertThrowsFatalMessage(AgentBuilder.ERR_CREATION_FAILED,
				() -> AccessPrivates.callPrivateMethodOn("instantiate", agentBuilder, constructor, dao));
	}

	@Test
	public void testInstantiateConstructionFail() throws NoSuchMethodException, SecurityException {
		Constructor<?> constructor = AgentDummyConstructorThrows.class.getDeclaredConstructor(DataProvider.class);
		AgentDao dao = AgentDao.newBuilder().setId(0L).setClassName("Irrelevant").build();
		assertThrowsFatalMessage(AgentBuilder.ERR_CREATION_FAILED,
				() -> AccessPrivates.callPrivateMethodOn("instantiate", agentBuilder, constructor, dao));
	}

	@Test
	public void testInstantiateBrokenConstructor() throws NoSuchMethodException, SecurityException {
		Constructor<?> constructor = AgentBuilderTest.class.getDeclaredConstructor();
		AgentDao dao = AgentDao.newBuilder().setId(0L).setClassName("Irrelevant").build();
		assertThrowsFatalMessage(AgentBuilder.ERR_FAME_BROKEN,
				() -> AccessPrivates.callPrivateMethodOn("instantiate", agentBuilder, constructor, dao));
	}

	@Test
	public void testCreateAgentsFromConfig() {
		when(mockMpiManager.getRank()).thenReturn(0);
		when(mockMpiManager.getProcessCount()).thenReturn(2);
		agentBuilder = new AgentBuilder(mockMpiManager, new ArrayList<>(), null, mockedServices);

		List<AgentDao> agentConfigs = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			AgentDao dao = AgentDao.newBuilder().setId((long) i).setClassName("AgentDummy").build();
			agentConfigs.add(dao);
		}
		agentBuilder.createAgentsFromConfig(agentConfigs);
		logChecker.assertLogsContain(AgentBuilder.AGENT_CREATED);
	}
}