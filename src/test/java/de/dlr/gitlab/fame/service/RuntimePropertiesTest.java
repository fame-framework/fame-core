// SPDX-FileCopyrightText: 2025 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData.VersionData;
import de.dlr.gitlab.fame.testUtils.LogChecker;

public class RuntimePropertiesTest {

	private LogChecker logChecker = new LogChecker(RuntimeProperties.class);

	@Before
	public void setup() {
		logChecker.clear();
	}

	@Test
	public void constructor_error_logsError() throws IOException {
		Properties mockProperties = mock(Properties.class);
		doThrow(new IOException()).when(mockProperties).load(any(InputStream.class));
		new RuntimeProperties(mockProperties);
		logChecker.assertLogsContain(RuntimeProperties.ERR_IO);
	}

	@Test
	public void constructor_notFound_logsError() throws IOException {
		Properties mockProperties = mock(Properties.class);
		doThrow(new NullPointerException()).when(mockProperties).load(any(InputStream.class));
		new RuntimeProperties(mockProperties);
		logChecker.assertLogsContain(RuntimeProperties.ERR_VERSION_NOT_FOUND);
	}

	@Test
	public void constructor_illegalCharacter_logsError() throws IOException {
		Properties mockProperties = mock(Properties.class);
		doThrow(new IllegalArgumentException()).when(mockProperties).load(any(InputStream.class));
		new RuntimeProperties(mockProperties);
		logChecker.assertLogsContain(RuntimeProperties.ERR_VERSION_MISFORMAT);
	}

	@Test
	public void constructor_setsOneProperties() {
		RuntimeProperties properties = new RuntimeProperties();
		String result = properties.getProperty("os.name");
		assert !result.equals(RuntimeProperties.MISSING_PLACEHOLDER);
	}

	@Test
	public void getProperty_missing_returnsPlaceholder() {
		RuntimeProperties properties = new RuntimeProperties();
		String result = properties.getProperty("this.is.Not.A.Valid.Property.Name");
		assert result.equals(RuntimeProperties.MISSING_PLACEHOLDER);
	}

	@Test
	public void getVersions_allItemsPresent() {
		RuntimeProperties properties = new RuntimeProperties();
		VersionData result = properties.getVersions();
		assert !result.getFameCore().equals(RuntimeProperties.MISSING_PLACEHOLDER);
		assert !result.getFameProtobuf().equals(RuntimeProperties.MISSING_PLACEHOLDER);
		assert !result.getOs().equals(RuntimeProperties.MISSING_PLACEHOLDER);
		assert !result.getJvm().equals(RuntimeProperties.MISSING_PLACEHOLDER);
	}
}
