// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.scheduling.stats;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.util.AgentInteractionStatistic;

public class FullRuntimeTrackingTest {
	private RuntimeTracking runtimeTracking;
	private AgentInteractionStatistic mockStatistic;
	private long callCounter;

	@Before
	public void setUp() {
		mockStatistic = mock(AgentInteractionStatistic.class);
		runtimeTracking = new FullRuntimeTracking(this::mockTimer, mockStatistic);
		callCounter = 0L;
	}

	/** mocks calls to a time: returns values increased by one per each call */
	private long mockTimer() {
		return callCounter++;
	}

	@Test
	public void startActionsforAgent_startedTwice_raises() {
		runtimeTracking.startActionsForAgent(0L);
		assertThrowsFatalMessage(FullRuntimeTracking.ERR_NOT_ENDED, () -> runtimeTracking.startActionsForAgent(0L));
	}

	@Test
	public void endCurrentActions_endedTwice_raises() {
		runtimeTracking.startActionsForAgent(0L);
		runtimeTracking.endCurrentActions();
		assertThrowsFatalMessage(FullRuntimeTracking.ERR_NOT_STARTED, () -> runtimeTracking.endCurrentActions());
	}

	@Test
	public void getStatsJson_builtFromStatistic() {
		String statisticsContent = "MyStats";
		when(mockStatistic.getJSON()).thenReturn(statisticsContent);
		String expected = String.format("{%s}", statisticsContent);
		String result = runtimeTracking.getStatsJson();
		assert result.equals(expected);
	}
	
	@Test
	public void oneAgent_oneCall_logged() {
		runtimeTracking.startActionsForAgent(0L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.endTick();
		verify(mockStatistic, times(1)).recordInteraction(0, 0, 1);
	}

	@Test
	public void oneAgent_twoCalls_addedUp() {
		runtimeTracking.startActionsForAgent(0L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.startActionsForAgent(0L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.endTick();
		verify(mockStatistic, times(1)).recordInteraction(0, 0, 2);
	}

	@Test
	public void twoAgents_oneTick_oneCallEach_overlap() {
		runtimeTracking.startActionsForAgent(0L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.startActionsForAgent(1L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.endTick();
		verify(mockStatistic, times(1)).recordInteraction(0, 0, 1);
		verify(mockStatistic, times(1)).recordInteraction(0, 1, 1);
		verify(mockStatistic, times(1)).recordInteraction(1, 0, 1);
		verify(mockStatistic, times(1)).recordInteraction(1, 1, 1);
	}

	@Test
	public void twoAgents_twoTicks_oneCallEach_noOverlap() {
		runtimeTracking.startActionsForAgent(0L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.endTick();
		runtimeTracking.startActionsForAgent(1L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.endTick();
		verify(mockStatistic, times(1)).recordInteraction(0, 0, 1);
		verify(mockStatistic, times(1)).recordInteraction(1, 1, 1);
	}

	@Test
	public void twoAgents_twoTicks_twoCallsEach_addedUp() {
		runtimeTracking.startActionsForAgent(0L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.startActionsForAgent(1L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.endTick();
		runtimeTracking.startActionsForAgent(0L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.startActionsForAgent(1L);
		runtimeTracking.endCurrentActions();
		runtimeTracking.endTick();
		verify(mockStatistic, times(2)).recordInteraction(0, 0, 1);
		verify(mockStatistic, times(2)).recordInteraction(0, 1, 1);
		verify(mockStatistic, times(2)).recordInteraction(1, 0, 1);
		verify(mockStatistic, times(2)).recordInteraction(1, 1, 1);
	}
}
