// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.output;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsMessage;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import de.dlr.gitlab.fame.service.input.FileConstants;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.LogChecker;

public class FileWriterTest {
	public static final String WORKING_PATH = "./src/test/resources/keepThisFolder/";
	private final String fileName = "test.pb";

	private Setup mockSetup;
	private FileOutputStream mockFos;
	private FileWriter fileWriter;
	private LogChecker logChecker = new LogChecker(FileWriter.class);

	@Before
	public void setup() {
		mockFos = mock(FileOutputStream.class);
		fileWriter = new FileWriter(mockFos);
		logChecker.clear();
	}

	/** Updates the mocked Setup with given parameters */
	private void initialiseSetup(String fileName, boolean withTimeStamp) {
		mockSetup = mock(Setup.class);
		when(mockSetup.getOutputFile()).thenReturn(fileName);
		when(mockSetup.isAddTimeStamp()).thenReturn(withTimeStamp);
	}

	@Test
	public void FileWriter_overrideExistingFile() throws IOException {
		initialiseSetup(WORKING_PATH + fileName, false);
		FileWriter fileWriter = new FileWriter(mockSetup);
		String outFileName = FileWriter.fullFileName(mockSetup);
		fileWriter.close();

		try (FileOutputStream fos = new FileOutputStream(outFileName)) {
			fos.write(new byte[] {1, 2, 3, 1, 1, 2, 33, 22, 11, 1});
		}

		fileWriter = new FileWriter(mockSetup);
		fileWriter.close();
		int expected_length = FileConstants.getLatestHeader().length;
		assertEquals(expected_length, Files.size(Paths.get(outFileName)));
	}

	@Test
	public void FileWriter_nullPath_throws() {
		initialiseSetup(null, false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_FILE_NAME,
				() -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_emptyPath_throws() {
		initialiseSetup("", false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_FILE_NAME,
				() -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_blankPath_throws() {
		initialiseSetup(" 	 ", false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_FILE_NAME,
				() -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_pathToDirectory_throws() {
		initialiseSetup(WORKING_PATH, false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_FILE_IS_DIR,
				() -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_outputPathMissing_createsPath() throws IOException {
		String fileString = WORKING_PATH + "abc.pb";
		initialiseSetup(fileString, false);
		try (MockedStatic<Files> files = Mockito.mockStatic(Files.class)) {
			files.when(() -> Files.exists(any(Path.class))).thenReturn(false);
			files.when(() -> Files.createDirectories(any(Path.class))).thenReturn(mock(Path.class));
			new FileWriter(mockSetup);
		}
		logChecker.assertLogsContain(FileWriter.INFO_CREATE_FOLDER);
	}

	@Test
	public void FileWriter_outputPathCreationFail_throws() throws IOException {
		String fileString = "abc.pb";
		initialiseSetup(fileString, false);
		try (MockedStatic<Files> files = Mockito.mockStatic(Files.class)) {
			files.when(() -> Files.createDirectories(any(Path.class))).thenThrow(IOException.class);
			files.when(() -> Files.exists(any(Path.class))).thenReturn(false);

			assertThrowsFatalMessage(FileWriter.ERR_FOLDER_CREATE, () -> new FileWriter(mockSetup));
		}
	}

	@Test
	public void fullFileName_noTimeStamp_returnsPathWithoutTimeStamp() {
		initialiseSetup(WORKING_PATH + "abc.pb", false);
		String result = FileWriter.fullFileName(mockSetup).replace("\\", "/");
		assertEquals(WORKING_PATH + "abc.pb", result);
	}

	@Test
	public void fullFileName_withTimeStamp_returnsPathWithTimeStamp() {
		initialiseSetup(WORKING_PATH + "abc.pb", true);
		String result = FileWriter.fullFileName(mockSetup).replace("\\", "/");
		assertTrue(result.matches(WORKING_PATH + "[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}-[0-9]{2}-[0-9]{2}.abc.pb"));
	}

	@Test
	public void fullFileName_missingEnding_logsWarning() {
		initialiseSetup(WORKING_PATH + "abc", false);
		FileWriter.fullFileName(mockSetup);
		logChecker.assertLogsContain(FileWriter.WARN_FILE_ENDING);
	}

	@Test
	public void fullFileName_missingEnding_endingAttached() {
		initialiseSetup(WORKING_PATH + "abc", false);
		String newFileName = FileWriter.fullFileName(mockSetup);
		assertTrue(newFileName.endsWith(FileWriter.OUT_FILE_ENDING));
	}

	@Test
	public void fullFileName_correctEnding_noWarning() {
		initialiseSetup(WORKING_PATH + "abc" + FileWriter.OUT_FILE_ENDING, false);
		FileWriter.fullFileName(mockSetup);
		logChecker.assertLogsDoNotContain(FileWriter.WARN_FILE_ENDING);
	}

	@Test
	public void fullFileName_noParentPath_fileNameUnchanged() {
		String myFileName = "abc" + FileWriter.OUT_FILE_ENDING;
		initialiseSetup(myFileName, false);
		String result = FileWriter.fullFileName(mockSetup);
		assertEquals(myFileName, result);
	}

	@Test
	public void FileWriter_writesHeader() throws IOException {
		ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
		verify(mockFos, times(1)).write(captor.capture());
		byte[] result = captor.getValue();
		assertArrayEquals(FileConstants.getLatestHeader(), result);
	}

	@Test
	public void FileWriter_writeHeaderFail_throws() throws IOException {
		doThrow(IOException.class).when(mockFos).write(any(byte[].class));
		assertThrowsFatalMessage(FileWriter.ERR_WRITE_FAILED, () -> new FileWriter(mockFos));
	}

	@Test
	public void write_nullData_logsWarning() {
		fileWriter.write(null);
		logChecker.assertLogsContain(FileWriter.WARN_EMPTY_CONTENT);
	}

	@Test
	public void write_emptyData_logsWarning() {
		fileWriter.write(new byte[] {});
		logChecker.assertLogsContain(FileWriter.WARN_EMPTY_CONTENT);
	}

	@Test
	public void write_singleByte_writesHeaderFirst_oneByte() throws IOException {
		byte[] payload = new byte[] {1};
		fileWriter.write(payload);
		ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
		verify(mockFos, times(3)).write(captor.capture());
		List<byte[]> results = captor.getAllValues();
		int bytesAsInt = ByteBuffer.wrap(results.get(1)).getInt();
		assertEquals(payload.length, bytesAsInt);
		assertEquals(payload, results.get(2));
	}

	@Test
	public void write_singleByte_writesHeaderFirst_manyBytes() throws IOException {
		byte[] payload = new byte[] {1, 22, 1, 3, 2, 3, 4};
		fileWriter.write(payload);
		ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
		verify(mockFos, times(3)).write(captor.capture());
		List<byte[]> results = captor.getAllValues();
		int bytesAsInt = ByteBuffer.wrap(results.get(1)).getInt();
		assertEquals(payload.length, bytesAsInt);
		assertEquals(payload, results.get(2));
	}

	@Test
	public void write_ioexceptionHeader_throwsRuntimeException() throws IOException {
		doThrow(IOException.class).when(mockFos).write(any(byte[].class));
		assertThrowsFatalMessage(FileWriter.ERR_WRITE_FAILED, () -> fileWriter.write(new byte[] {1}));
	}

	@Test
	public void write_ioexceptionContent_throwsRuntimeException() throws IOException {
		doNothing().doThrow(IOException.class).when(mockFos).write(any(byte[].class));
		assertThrowsFatalMessage(FileWriter.ERR_WRITE_FAILED, () -> fileWriter.write(new byte[] {1}));
	}

	@Test
	public void close_fail_throws() throws IOException {
		doThrow(IOException.class).when(mockFos).close();
		assertThrowsFatalMessage(FileWriter.ERR_CLOSE_FAILED, () -> fileWriter.close());
	}
}