// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.message.MessageBuilder;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;
import de.dlr.gitlab.fame.communication.stats.CommTracking;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle.Builder;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.service.messaging.OutBox;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.MpiMock;

public class PostOfficeTest {
	private static final int MY_RANK = 0;
	private static final int PROCESS_COUNT = 2;

	private PostOffice postOffice;
	private MpiManager mockMpi;
	private MessageBuilder messageBuilder;
	private CommTracking commTracking;
	private AddressBook addressBook;
	private ArrayList<OutBox> outBoxes;
	private ArrayList<Bundle> bundles;
	private HashMap<Long, Agent> localAgents;
	private Builder bundleBuilder;

	public class DummyAgent extends Agent {
		public DummyAgent(AgentDao dao) {
			super(new DataProvider(dao, null, mock(LocalServices.class)));
		}
	}

	@Before
	public void setUp() {
		mockMpi();
		messageBuilder = mock(MessageBuilder.class);
		commTracking = mock(CommTracking.class);
		addressBook = mock(AddressBook.class);
		outBoxes = mockOutBoxes(PROCESS_COUNT);
		bundles = new ArrayList<>();
		localAgents = new HashMap<>();
		bundleBuilder = Bundle.newBuilder();

		postOffice = new PostOffice(mockMpi, messageBuilder, commTracking, addressBook, outBoxes, bundles,
				localAgents, bundleBuilder);

		when(messageBuilder.setSenderId(any(Long.class))).thenReturn(messageBuilder);
		when(messageBuilder.setReceiverId(any(Long.class))).thenReturn(messageBuilder);
		when(messageBuilder.add(any(DataItem.class))).thenReturn(messageBuilder);
		when(messageBuilder.add(any(Portable.class))).thenReturn(messageBuilder);

		Message message = mock(Message.class);
		when(messageBuilder.build()).thenReturn(message);

		AccessPrivates.callPrivateStaticMethod("setDataItemClasses", DataItem.class, new Class<?>[] {List.class},
				Arrays.asList(""));
	}

	private void mockMpi() {
		mockMpi = MpiMock.newMockMpi();
		when(mockMpi.getProcessCount()).thenReturn(PROCESS_COUNT);
		when(mockMpi.getRank()).thenReturn(MY_RANK);
	}

	private ArrayList<OutBox> mockOutBoxes(int processCount) {
		ArrayList<OutBox> outBoxes = new ArrayList<>();
		for (int index = 0; index < processCount; index++) {
			outBoxes.add(mock(OutBox.class));
		}
		return outBoxes;
	}

	@Test
	public void prepareOutBoxes_createsOneBoxPerProcess() {
		var result = PostOffice.prepareOutBoxes(3);
		assertEquals(3, result.size());
	}

	@Test
	public void prepareOutBoxes_createsOneBoxForEachProcess() {
		var result = PostOffice.prepareOutBoxes(140);
		assertEquals(140, result.size());
	}

	@Test
	public void sendMessage_clearsMessageBuilderFirst() {
		postOffice.sendMessage(0L, 1L, null);
		InOrder inOrder = inOrder(messageBuilder);
		inOrder.verify(messageBuilder).clear();
		inOrder.verify(messageBuilder).setSenderId(any(Long.class));
	}

	@Test
	public void sendMessage_addsSenderAndReceiver() {
		postOffice.sendMessage(0L, 1L, null);
		verify(messageBuilder).setSenderId(0L);
		verify(messageBuilder).setReceiverId(1L);
	}

	@Test
	public void sendMessage_withDataItem_addsDataItem() {
		DataItem dummyItem = new DummyDataItem();
		postOffice.sendMessage(0L, 1L, null, dummyItem);
		verify(messageBuilder).add(dummyItem);
	}

	@Test
	public void sendMessage_withPortable_addsPortable() {
		Portable dummyPortable = new DummyPortableEmpty();
		postOffice.sendMessage(2L, 13L, dummyPortable);
		verify(messageBuilder).add(dummyPortable);
	}

	@Test
	public void sendMessage_storesMessageToCorrectOutbox() {
		when(addressBook.getProcessId(0L)).thenReturn(0);
		when(addressBook.getProcessId(1L)).thenReturn(1);
		when(addressBook.getProcessId(2L)).thenReturn(0);
		postOffice.sendMessage(0L, 0L, null);
		postOffice.sendMessage(0L, 1L, null);
		postOffice.sendMessage(0L, 2L, null);
		verify(outBoxes.get(0), times(2)).storeMessage(any());
		verify(outBoxes.get(1), times(1)).storeMessage(any());
	}

	@Test
	public void deliverMessages_ignoresPreviousBundleContent() {
		bundles.add(mock(Bundle.class));
		Bundle response = mock(Bundle.class);
		when(mockMpi.individualAllToAll(any(), any())).thenReturn(response);
		when(response.getMessagesList()).thenReturn(Collections.emptyList());

		postOffice.deliverMessages();
		assertEquals(2, bundles.size());
		assertNull(bundles.get(0));
		assertNull(bundles.get(1));
	}

	@Test
	public void deliverMessages_multipleBundles_inCorrectOrder() {
		Bundle response = mock(Bundle.class);
		when(mockMpi.individualAllToAll(any(), any())).thenReturn(response);
		when(response.getMessagesList()).thenReturn(Collections.emptyList());

		Bundle bundle0 = mock(Bundle.class);
		when(outBoxes.get(0).bundle(bundleBuilder)).thenReturn(bundle0);
		Bundle bundle1 = mock(Bundle.class);
		when(outBoxes.get(1).bundle(bundleBuilder)).thenReturn(bundle1);

		postOffice.deliverMessages();
		assertEquals(bundle0, bundles.get(0));
		assertEquals(bundle1, bundles.get(1));
	}

	@Test
	public void deliverMessages_assignedToCorrectAgent() {
		Bundle response = createBundleWithAgentMessages(2, 0, 1, 1, 0, 2, 1);
		when(mockMpi.individualAllToAll(any(), any())).thenReturn(response);

		localAgents.put(0L, mock(Agent.class));
		localAgents.put(1L, mock(Agent.class));
		localAgents.put(2L, mock(Agent.class));

		postOffice.deliverMessages();

		verify(localAgents.get(0L), times(2)).receive(any(Message.class));
		verify(localAgents.get(1L), times(3)).receive(any(Message.class));
		verify(localAgents.get(2L), times(1)).receive(any(Message.class));
	}

	/** @return one bundle with one or multiple {@link MpiMessage}s which each contain up to messagesPerMpiMessage
	 *         {@link ProtoMessage}s addressed to given receivers */
	private Bundle createBundleWithAgentMessages(int messagesPerMpiMessage, int... receivers) {
		Bundle.Builder builder = Bundle.newBuilder();
		MpiMessage.Builder messageBuilder = null;
		int index = 0;
		while (index < receivers.length) {
			if (index % messagesPerMpiMessage == 0) {
				if (messageBuilder != null) {
					builder.addMessages(messageBuilder);
				}
				messageBuilder = MpiMessage.newBuilder();
			}
			messageBuilder.addMessages(ProtoMessage.newBuilder().setSenderId(0L).setReceiverId(receivers[index]));
			index++;
		}
		builder.addMessages(messageBuilder);
		return builder.build();
	}

	@Test
	public void isLocalAgent_agentRegistered_returnsTrue() {
		registerAgents(1L, 5L, 11L);
		assertTrue(localAgents.containsKey(1L));
		assertTrue(localAgents.containsKey(5L));
		assertTrue(localAgents.containsKey(11L));
	}

	/** registers any amount of mocked agents with given IDs */
	private void registerAgents(Long first, Long... agentIds) {
		Agent mockAgent = mock(Agent.class);
		when(mockAgent.getId()).thenReturn(first, agentIds);
		for (int i = 0; i < agentIds.length + 1; i++) {
			postOffice.registerAgent(mockAgent);
		}
	}

	@Test
	public void deliverMessages_handsMessageToTracking() {
		Bundle response = createBundleWithAgentMessages(2, 0);
		when(mockMpi.individualAllToAll(any(), any())).thenReturn(response);
		localAgents.put(0L, mock(Agent.class));

		postOffice.deliverMessages();

		verify(commTracking, times(1)).assessMessage(any());
	}

	@Test
	public void finalise_returnsCommStats() {
		String expectedString = "abc";
		when(commTracking.getStatsJson()).thenReturn(expectedString);
		String result = postOffice.getStats();
		assert result == expectedString;
	}
}
