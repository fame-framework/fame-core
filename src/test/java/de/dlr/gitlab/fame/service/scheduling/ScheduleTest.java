// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.scheduling;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;

public class ScheduleTest {
	public enum Reasons {
		Reason1, Reason2, Reason3
	}

	private Schedule schedule;
	private TimeStamp startTime = new TimeStamp(0);
	private TimeStamp endTime = new TimeStamp(10);
	private ArrayList<Agent> mockAgents = new ArrayList<>();

	@Before
	public void setup() {
		schedule = new Schedule(startTime.getStep(), endTime.getStep());
		mockAgents.add(mock(Agent.class));
		mockAgents.add(mock(Agent.class));
		mockAgents.add(mock(Agent.class));
	}

	@Test
	public void testGetInitialTime() {
		assertEquals(startTime.getStep(), schedule.getInitialTime().getStep());
	}

	@Test
	public void testAddSingleActionFailPast() {
		assertThrowsFatalMessage(Schedule.ERR_SCHEDULED_TO_PAST,
				() -> schedule.addSingleActionAt(mockAgents.get(0), startTime.earlierBy(new TimeSpan(1)), null));
	}

	@Test
	public void testAddSingleActionFailCurrent() {
		assertThrowsFatalMessage(Schedule.ERR_SCHEDULED_TO_PAST,
				() -> schedule.addSingleActionAt(mockAgents.get(0), schedule.getCurrentTime(), null));
	}

	@Test
	public void testAddSingleAction() {
		schedule.addSingleActionAt(mockAgents.get(0), startTime, Reasons.Reason1);
		ScheduleSlot slot = schedule.getNextScheduledEntry();
		assertScheduledAgentHasReasons(slot, mockAgents.get(0), Reasons.Reason1);
	}

	/** succeeds if given ScheduleSlot contains given agent and all of the given reasons */
	private void assertScheduledAgentHasReasons(ScheduleSlot slot, Agent agent, Enum<?>... reasons) {
		List<Enum<?>> scheduledReasons = slot.getReasons(agent);
		assertEquals(reasons.length, scheduledReasons.size());
		assertThat(scheduledReasons, hasItems(reasons));
	}

	@Test
	public void testAddSingleActionSimultaneousActions() {
		schedule.addSingleActionAt(mockAgents.get(0), startTime, Reasons.Reason1);
		schedule.addSingleActionAt(mockAgents.get(1), startTime, Reasons.Reason1);
		schedule.addSingleActionAt(mockAgents.get(2), startTime, Reasons.Reason2);
		ScheduleSlot slot = schedule.getNextScheduledEntry();
		assertScheduledAgentHasReasons(slot, mockAgents.get(0), Reasons.Reason1);
		assertScheduledAgentHasReasons(slot, mockAgents.get(1), Reasons.Reason1);
		assertScheduledAgentHasReasons(slot, mockAgents.get(2), Reasons.Reason2);
	}

	@Test
	public void testGetNextScheduledEntryNoEntry() {
		ScheduleSlot slot = schedule.getNextScheduledEntry();
		assertNotNull(slot);
	}

	@Test
	public void testAddSingleActionMultipleReasons() {
		schedule.addSingleActionAt(mockAgents.get(0), startTime, Reasons.Reason1);
		schedule.addSingleActionAt(mockAgents.get(0), startTime, Reasons.Reason2);
		schedule.addSingleActionAt(mockAgents.get(0), startTime, Reasons.Reason3);
		ScheduleSlot slot = schedule.getNextScheduledEntry();
		assertScheduledAgentHasReasons(slot, mockAgents.get(0), Reasons.Reason1, Reasons.Reason2, Reasons.Reason3);
	}

	@Test
	public void testGetNextScheduledEntryMixedTimes() {
		TimeStamp scheduledTime = startTime.laterBy(new TimeSpan(5));
		schedule.addSingleActionAt(mockAgents.get(2), scheduledTime.laterBy(new TimeSpan(1)), Reasons.Reason1);
		schedule.addSingleActionAt(mockAgents.get(0), scheduledTime, Reasons.Reason2);
		schedule.addSingleActionAt(mockAgents.get(1), scheduledTime.laterBy(new TimeSpan(2)), Reasons.Reason3);
		ScheduleSlot slot = schedule.getNextScheduledEntry();
		assertScheduledAgentHasReasons(slot, mockAgents.get(0), Reasons.Reason2);
	}

	@Test
	public void testGetNextScheduledEntryRemovedAfterUsage() {
		TimeStamp scheduledTime = startTime.laterBy(new TimeSpan(5));
		schedule.addSingleActionAt(mockAgents.get(2), scheduledTime.laterBy(new TimeSpan(1)), Reasons.Reason1);
		schedule.addSingleActionAt(mockAgents.get(0), scheduledTime, Reasons.Reason2);
		schedule.addSingleActionAt(mockAgents.get(1), scheduledTime.laterBy(new TimeSpan(2)), Reasons.Reason3);
		schedule.getNextScheduledEntry();
		ScheduleSlot slot = schedule.getNextScheduledEntry();
		assertScheduledAgentHasReasons(slot, mockAgents.get(2), Reasons.Reason1);
	}

	@Test
	public void testGetNextScheduledEntryUpdateTime() {
		TimeStamp scheduledTime = startTime.laterBy(new TimeSpan(5));
		schedule.addSingleActionAt(mockAgents.get(0), scheduledTime, null);
		schedule.getNextScheduledEntry();
		TimeStamp currentTime = schedule.getCurrentTime();
		assertEquals(0, currentTime.compareTo(scheduledTime));
	}

	@Test
	public void testgetNextTimeInSchedule() {
		TimeStamp scheduledTime = startTime.laterBy(new TimeSpan(3));
		schedule.addSingleActionAt(mockAgents.get(2), scheduledTime.laterBy(new TimeSpan(1)), null);
		schedule.addSingleActionAt(mockAgents.get(0), scheduledTime, null);
		schedule.addSingleActionAt(mockAgents.get(1), scheduledTime.laterBy(new TimeSpan(2)), null);
		TimeStamp nextTime = schedule.getNextTimeInSchedule();
		assertEquals(0, nextTime.compareTo(scheduledTime));
	}

	@Test
	public void testGetNextTimeInScheduleIsFarFutured() {
		assertEquals(Long.MAX_VALUE, schedule.getNextTimeInSchedule().getStep());
	}

	@Test
	public void testHasTasksRemainingBeyondFinal() {
		schedule.addSingleActionAt(mockAgents.get(0), endTime.laterBy(new TimeSpan(1)), null);
		schedule.addSingleActionAt(mockAgents.get(1), endTime.laterBy(new TimeSpan(2)), null);
		schedule.getNextScheduledEntry();
		assertFalse(schedule.hasTasksRemaining());
	}

	@Test
	public void testHasTasksRemainingEmpty() {
		assertFalse(schedule.hasTasksRemaining());
	}

	@Test
	public void testHasTasksRemaining() {
		schedule.addSingleActionAt(mockAgents.get(0), startTime.laterBy(new TimeSpan(3)), null);
		assertTrue(schedule.hasTasksRemaining());
	}

	@Test
	public void testInsertTimeWhenNecessaryKeyNotReplaced() {
		TimeStamp time = startTime.laterBy(new TimeSpan(1));
		schedule.addSingleActionAt(mockAgents.get(0), time, Reasons.Reason1);
		schedule.insertTimeWhenNecessary(time);
		assertScheduledAgentHasReasons(schedule.getNextScheduledEntry(), mockAgents.get(0), Reasons.Reason1);
	}

	@Test
	public void testInsertTimeWhenNecessaryKeyMissing() {
		TimeStamp time = startTime.laterBy(new TimeSpan(1));
		schedule.insertTimeWhenNecessary(time);
		assertEquals(time, schedule.getNextTimeInSchedule());
		assertNotNull(schedule.getNextScheduledEntry());
	}

	@Test
	public void testInsertTimeWhenNecessaryTooLate() {
		schedule.insertTimeWhenNecessary(endTime.laterByOne());
		assertFalse(schedule.hasTasksRemaining());
	}
}