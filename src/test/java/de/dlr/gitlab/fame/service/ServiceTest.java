// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.protobuf.Model.ModelData;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Services.AddressBook;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType.Field;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line.Column;
import de.dlr.gitlab.fame.protobuf.Services.ScheduledTime;
import de.dlr.gitlab.fame.protobuf.Services.WarmUpMessage;

/** Tests for class {@link Service} */
public class ServiceTest {
	private Service service;

	@Before
	public void setup() {
		service = new DummyService(mock(MpiManager.class));
	}

	@Test
	public void createBundleForInput_addsInputAndModelData() {
		InputData inputData = InputData.newBuilder()
				.setRunId(0L)
				.setSimulation(SimulationParam.newBuilder()
						.setStartTime(0L)
						.setStopTime(1L)
						.setRandomSeed(0L))
				.build();
		ModelData modelData = ModelData.newBuilder().build();
		Bundle bundle = service.createBundleForInput(inputData, modelData);
		assertEquals(inputData, bundle.getMessages(0).getInput());
		assertEquals(modelData, bundle.getMessages(0).getModel());
	}

	@Test
	public void testCreateBundleForScheduledTime() {
		ScheduledTime.Builder data = ScheduledTime.newBuilder().setTimeStep(11L);
		Bundle bundle = service.createBundleForScheduledTime(data);
		ScheduledTime result = bundle.getMessages(0).getScheduledTime();
		assertEquals(data.build(), result);
	}

	@Test
	public void testCreateBundleForWarmUpMessage() {
		WarmUpMessage.Builder data = WarmUpMessage.newBuilder().setNeeded(true);
		Bundle bundle = service.createBundleForWarmUpMessage(data);
		WarmUpMessage result = bundle.getMessages(0).getWarmUp();
		assertEquals(data.build(), result);
	}

	@Test
	public void testCreateBundleForAddressBook() {
		AddressBook.Builder data = AddressBook.newBuilder().setProcessId(55).addAgentIds(21L).addAgentIds(42L);
		Bundle bundle = service.createBundleForAddressBook(data);
		AddressBook result = bundle.getMessages(0).getAddressBook();
		assertEquals(data.build(), result);
	}

	@Test
	public void testCreateBundleForOutput() {
		Output.Builder data = Output.newBuilder()
				.addAgentTypes(AgentType.newBuilder()
						.setClassName("DummyClass")
						.addFields(Field.newBuilder().setFieldId(0).setFieldName("EmptyField")))
				.addSeries(Series.newBuilder()
						.setClassName("DummyClass")
						.setAgentId(33L)
						.addLines(Line.newBuilder()
								.setTimeStep(15L)
								.addColumns(Column.newBuilder().setFieldId(13).setValue(15.55d))));
		Bundle bundle = service.createBundleForOutput(data);
		Output result = bundle.getMessages(0).getOutput();
		assertEquals(data.build(), result);
	}
}