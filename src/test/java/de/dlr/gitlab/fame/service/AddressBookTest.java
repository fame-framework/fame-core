// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services;
import de.dlr.gitlab.fame.protobuf.Services.AddressBook.Builder;
import de.dlr.gitlab.fame.testUtils.MpiMock;

public class AddressBookTest {
	private static final int MY_RANK = 0;
	private static final int PROCESS_COUNT = 2;

	private MpiManager mockMpi;
	private AddressBook addressBook;
	private HashMap<Long, Integer> processById;
	private ArrayList<Long> newAgents;
	private Builder addressBookBuilder;
	private MpiMessage.Builder mpiMessageBuilder;
	private Bundle.Builder bundleBuilder;

	@Before
	public void setUp() {
		mockMpi();
		processById = new HashMap<>();
		addressBookBuilder = Services.AddressBook.newBuilder();
		bundleBuilder = Bundle.newBuilder();
		mpiMessageBuilder = MpiMessage.newBuilder();

		newAgents = new ArrayList<>();
		addressBook = new AddressBook(mockMpi, processById, addressBookBuilder, newAgents, mpiMessageBuilder,
				bundleBuilder);
	}

	private void mockMpi() {
		mockMpi = MpiMock.newMockMpi();
		when(mockMpi.getProcessCount()).thenReturn(PROCESS_COUNT);
		when(mockMpi.getRank()).thenReturn(MY_RANK);
		when(mockMpi.aggregateAll(any(), any())).then(call -> call.getArguments()[0]);
	}

	@Test
	public void getProcessId_unknownId_throws() {
		assertThrowsFatalMessage(AddressBook.ERR_UNKNOWN_AGENT_ID, () -> addressBook.getProcessId(5L));
	}

	@Test
	public void getProcessId_knownId_returnsId() {
		processById.put(42L, 0);
		assertEquals(0, addressBook.getProcessId(42L));
	}

	@Test
	public void registerAgent_agent_idAddedToNewAgents() {
		Agent agent = mock(Agent.class);
		when(agent.getId()).thenReturn(99L);
		addressBook.registerAgent(agent);
		assertTrue(newAgents.contains(99L));
	}

	@Test
	public void synchroniseAddressBookUpdates_noNewAgents_keepsAddressBook() {
		processById.put(42L, 0);
		addressBook.synchroniseAddressBookUpdates();
		assertEquals(1, processById.size());
		assertEquals(0, (int) processById.get(42L));
	}

	@Test
	public void synchroniseAddressBookUpdates_newAgents_addedToAddressBook() {
		processById.put(42L, 1);
		newAgents.add(99L);
		addressBook.synchroniseAddressBookUpdates();
		assertEquals(2, processById.size());
		assertEquals(1, (int) processById.get(42L));
		assertEquals(0, (int) processById.get(99L));
	}

	@Test
	public void synchroniseAddressBookUpdates_newAgents_removedFromNewAgentList() {
		newAgents.add(99L);
		addressBook.synchroniseAddressBookUpdates();
		assertTrue(newAgents.isEmpty());
	}

	@Test
	public void synchroniseAddressBookUpdates_newAgents_ignoresAnythingOnBuilder() {
		addressBookBuilder.setProcessId(55).addAgentIds(88L);
		newAgents.add(99L);
		addressBook.synchroniseAddressBookUpdates();
		assertEquals(1, processById.size());
		assertEquals(0, (int) processById.get(99L));
	}
}
