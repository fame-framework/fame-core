// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertNotEquals;
import java.util.Random;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests for {@link RandomNumberGeneratorProvider}
 *
 * @author Christoph Schimeczek
 */
public class RandomNumberGeneratorProviderTest {
	private final long default_random_seed = 12345L;
	private RandomNumberGeneratorProvider rngProvider;
	
	@Before
	public void setup() {
		rngProvider = new RandomNumberGeneratorProvider(default_random_seed);
	}
	
	@Test
	public void testGetNewRandomNumberGeneratorForAgent_DifferentRandomsPerAgent() {
		Random r1 = rngProvider.getNewRandomNumberGeneratorForAgent(0);
		Random r2 = rngProvider.getNewRandomNumberGeneratorForAgent(10);
		Random r3 = rngProvider.getNewRandomNumberGeneratorForAgent(42);
		
		assertNotEquals(r1, r2);
		assertNotEquals(r2, r3);
		assertNotEquals(r1, r3);
	}
	
	@Test
	public void testGetNewRandomNumberGeneratorForAgent_DifferentRandomsPerCall() {
		Random r1 = rngProvider.getNewRandomNumberGeneratorForAgent(0);
		Random r2 = rngProvider.getNewRandomNumberGeneratorForAgent(0);
		Random r3 = rngProvider.getNewRandomNumberGeneratorForAgent(0);
		
		assertNotEquals(r1, r2);
		assertNotEquals(r2, r3);
		assertNotEquals(r1, r3);
	}
	
	
	@Test
	public void testGetNewRandomNumberGeneratorForAgent_DifferentSeedsPerAgent() {
		Random r1 = rngProvider.getNewRandomNumberGeneratorForAgent(0);
		Random r2 = rngProvider.getNewRandomNumberGeneratorForAgent(10);
		Random r3 = rngProvider.getNewRandomNumberGeneratorForAgent(42);
		
		assertNotEquals(r1.nextLong(), r2.nextLong());
		assertNotEquals(r2.nextLong(), r3.nextLong());
		assertNotEquals(r1.nextLong(), r3.nextLong());
	}
	
	@Test
	public void testGetNewRandomNumberGeneratorForAgent_DifferentSeedsPerCall() {
		Random r1 = rngProvider.getNewRandomNumberGeneratorForAgent(0);
		Random r2 = rngProvider.getNewRandomNumberGeneratorForAgent(0);
		Random r3 = rngProvider.getNewRandomNumberGeneratorForAgent(0);
		
		assertNotEquals(r1.nextLong(), r2.nextLong());
		assertNotEquals(r2.nextLong(), r3.nextLong());
		assertNotEquals(r1.nextLong(), r3.nextLong());
	}	
}