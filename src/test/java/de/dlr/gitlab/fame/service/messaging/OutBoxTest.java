// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.messaging;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage.Builder;

/** Tests for class {@link OutBox} */
public class OutBoxTest {

	private Builder builder;
	private OutBox outBox;
	private Bundle.Builder bundleBuilder;

	@Before
	public void setUp() {
		builder = MpiMessage.newBuilder();
		outBox = new OutBox(builder);
		bundleBuilder = Bundle.newBuilder();
	}

	@Test
	public void storeMessage_addedToBuilder() {
		builder = mock(MpiMessage.Builder.class);
		outBox = new OutBox(builder);

		ProtoMessage message = mock(ProtoMessage.class);
		outBox.storeMessage(message);
		verify(builder, times(1)).addMessages(message);
	}

	@Test
	public void bundle_previousContentRemoved() {
		bundleBuilder.addMessages(MpiMessage.newBuilder()).addMessages(MpiMessage.newBuilder());
		Bundle result = outBox.bundle(bundleBuilder);
		assertEquals(1, result.getMessagesCount());
	}

	@Test
	public void bundle_addMessageToBundle() {
		bundleBuilder = mock(Bundle.Builder.class);
		when(bundleBuilder.addMessages(builder)).thenReturn(bundleBuilder);
		outBox.bundle(bundleBuilder);
		verify(bundleBuilder, times(1)).addMessages(builder);
	}

	@Test
	public void bundle_clearsLocalBuilder() {
		builder = mock(MpiMessage.Builder.class);
		outBox = new OutBox(builder);
		bundleBuilder = mock(Bundle.Builder.class);
		when(bundleBuilder.addMessages(builder)).thenReturn(bundleBuilder);

		outBox.bundle(bundleBuilder);

		verify(builder, times(1)).clear();
	}
}
