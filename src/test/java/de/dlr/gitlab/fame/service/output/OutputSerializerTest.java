// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.output;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;

public class OutputSerializerTest {
	private FileWriter fileWriterMock;
	private OutputSerializer serializer;
	private DataStorage.Builder dataStorageBuilderMock;
	private Output.Builder outputBuilderMock;
	private DataStorage dataStorageMock;

	@Before
	public void setUp() {
		fileWriterMock = mock(FileWriter.class);
		dataStorageBuilderMock = mock(DataStorage.Builder.class);
		outputBuilderMock = mock(Output.Builder.class);
		serializer = new OutputSerializer(fileWriterMock, dataStorageBuilderMock, outputBuilderMock, new HashMap<>());
		dataStorageMock = mock(DataStorage.class);
		when(dataStorageBuilderMock.build()).thenReturn(dataStorageMock);
	}

	@Test
	public void close_closesFilewriter() throws IOException {
		serializer.close();
		verify(fileWriterMock, times(1)).close();
	}

	@Test
	public void writeInputData_forwardsBytesToFileWriter() throws IOException {
		DataStorage mockInput = mock(DataStorage.class);
		byte[] byteArray = new byte[] {0, 1, 2};
		when(mockInput.toByteArray()).thenReturn(byteArray);
		serializer.writeInputData(mockInput);
		verify(fileWriterMock).write(byteArray);
	}

	@Test
	public void writeExecutionData_forwardsCreatedBundleToFileWriter() throws IOException {
		ExecutionData mockExecutionData = mock(ExecutionData.class);
		serializer.writeExecutionData(mockExecutionData);
		verify(dataStorageBuilderMock).setExecution(mockExecutionData);
		verify(dataStorageBuilderMock).build();
		verify(fileWriterMock).write(any());
	}

	@Test
	public void writeBundledOutput_noData_noCrash() {
		serializer.writeBundledOutput(createBundleMock());
	}

	/** @return mocked Bundle for the given output messages */
	private Bundle createBundleMock(Output... outputs) {
		Bundle bundleMock = mock(Bundle.class);
		ArrayList<MpiMessage> messages = new ArrayList<>();
		for (Output output : outputs) {
			MpiMessage messageMock = mock(MpiMessage.class);
			when(messageMock.getOutput()).thenReturn(output);
			messages.add(messageMock);
		}
		when(bundleMock.getMessagesList()).thenReturn(messages);
		return bundleMock;
	}

	@Test
	public void writeBundledOutput_newAgentType_addedOnceOnly() {
		Output outputMock = mock(Output.class);
		AgentType agentTypeMock = createAgentMock("MyClassName");
		when(outputMock.getAgentTypesList()).thenReturn(Arrays.asList(agentTypeMock, agentTypeMock, agentTypeMock));
		serializer.writeBundledOutput(createBundleMock(outputMock));
		verify(outputBuilderMock, times(1)).addAgentTypes(agentTypeMock);
	}

	/** @return mock of AgentType answering to "getClassName()" with given String */
	private AgentType createAgentMock(String className) {
		AgentType agentTypeMock = mock(AgentType.class);
		when(agentTypeMock.getClassName()).thenReturn(className);
		return agentTypeMock;
	}

	@Test
	public void writeBundledOutput_multipleAgentTypes_allAgentHeadersWritten() {
		AgentType agentTypeMock1 = createAgentMock("MyClassName1");
		AgentType agentTypeMock2 = createAgentMock("MyClassName2");
		AgentType agentTypeMock3 = createAgentMock("MyClassName3");
		Output outputMock = mock(Output.class);
		when(outputMock.getAgentTypesList()).thenReturn(Arrays.asList(agentTypeMock1, agentTypeMock2, agentTypeMock3));
		serializer.writeBundledOutput(createBundleMock(outputMock));
		verify(outputBuilderMock, times(1)).addAgentTypes(agentTypeMock1);
		verify(outputBuilderMock, times(1)).addAgentTypes(agentTypeMock2);
		verify(outputBuilderMock, times(1)).addAgentTypes(agentTypeMock3);
	}
}