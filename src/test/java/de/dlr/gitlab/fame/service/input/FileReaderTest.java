// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.input;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.when;
import java.io.FileInputStream;
import java.io.IOException;
import org.junit.Test;
import org.mockito.MockedConstruction;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;

public class FileReaderTest {
	final String inputFileNoHeaderPath = "./src/test/resources/inputData/InputDataNoHeader.bin";
	public final String inputFileOldHeaderPath = "./src/test/resources/inputData/InputDataOldHeader.bin";
	public final String inputFileHeaderPath = "./src/test/resources/inputData/InputDataHeader.bin";
	private final String corruptFilePath = "./src/test/resources/inputData/corrupt.bin";
	private final String illegalPath = "KK:/illegal/path/missing.pb";

	@Test
	public void read_fileWithoutHeaders_throws() {
		assertThrowsFatalMessage(FileReader.ERR_NO_HEADER, () -> FileReader.read(inputFileNoHeaderPath));
	}

	@Test
	public void read_fileWithOldHeaders_throws() {
		assertThrowsFatalMessage(FileReader.ERR_OLD_HEADER, () -> FileReader.read(inputFileOldHeaderPath));
	}

	@Test
	public void read_fileWithHeaders_returnsInput() {
		DataStorage data = FileReader.read(inputFileHeaderPath);
		assertNotNull(data);
	}

	@Test
	public void read_noFile_throws() {
		assertThrowsFatalMessage(FileReader.ERR_INPUT_FILE_NOT_FOUND, () -> FileReader.read(illegalPath));
	}

	@Test
	public void read_fileCorrupt_throws() {
		assertThrowsFatalMessage(FileReader.ERR_PARSER_ERROR, () -> FileReader.read(corruptFilePath));
	}

	@Test
	public void read_readError_throws() {
		try (MockedConstruction<FileInputStream> mockFileInputStream = mockConstruction(FileInputStream.class,
				(mock, context) -> {
					when(mock.read(any(byte[].class))).thenThrow(new IOException());
				})) {
			assertThrowsFatalMessage(FileReader.ERR_READ_ERROR, () -> FileReader.read(inputFileHeaderPath));
		}
	}
}
