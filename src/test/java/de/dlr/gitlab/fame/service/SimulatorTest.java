// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import org.junit.Test;

public class SimulatorTest {

	@Test
	public void getCompletionMessage_containsTicksFormattedEN() {
		String message1 = Simulator.getCompletionMessage(0, 55);
		String message2 = Simulator.getCompletionMessage(0, 1200);
		assert message1.contains("55 ticks");
		assert message2.contains("1,200 ticks");
	}

	@Test
	public void getCompletionMessage_containsWalltimeFormattedEN() {
		String message1 = Simulator.getCompletionMessage(55, 0);
		String message2 = Simulator.getCompletionMessage(1200, 0);
		String message3 = Simulator.getCompletionMessage(1231200, 0);
		assert message1.contains("0.06 seconds");
		assert message2.contains("1.20 seconds");
		assert message3.contains("1,231.20 seconds");
	}

	@Test
	public void formatDate() {
		String result = Simulator.formatDate(0);
		assert result.equals("UTC 1970-01-01 00:00:00");
	}
}
