// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import java.util.Random;
import org.junit.Test;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

public class UuidGeneratorTest {
	
	
	@Test
	public void testConstructor0() {
		UuidGenerator uuidManager = (UuidGenerator) AccessPrivates.usePrivateConstructorOn(UuidGenerator.class, 0);
		long processIdOffset = (long) AccessPrivates.getPrivateFieldOf("processIdOffset", uuidManager);
		assertEquals(UuidGenerator.ADDRESS_SPACE, processIdOffset);
	}
	
	@Test
	public void testConstructor4() {
		UuidGenerator uuidManager = (UuidGenerator) AccessPrivates.usePrivateConstructorOn(UuidGenerator.class, 4);
		long processIdOffset = (long) AccessPrivates.getPrivateFieldOf("processIdOffset", uuidManager);
		assertEquals(5L * UuidGenerator.ADDRESS_SPACE, processIdOffset);
	}
	
	@Test
	public void testGenerateNext() {
		UuidGenerator uuidManager = (UuidGenerator) AccessPrivates.usePrivateConstructorOn(UuidGenerator.class, 0);
		Random random = new Random(173954739L);
		long expectedUuid = UuidGenerator.ADDRESS_SPACE - 1;
		for (int i = 0; i < 10; i++) {
			long uuid = 0L;
			for (int numberOfGenerations = 0; numberOfGenerations <= random.nextInt(3); numberOfGenerations++) {
				uuid = (long) AccessPrivates.callPrivateMethodOn("generateNext", uuidManager);
				expectedUuid++;
			}
			assertEquals(expectedUuid, uuid);
		}
	}
}