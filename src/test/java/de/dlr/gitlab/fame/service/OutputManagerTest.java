// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.mpi.Constants;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;
import de.dlr.gitlab.fame.service.output.BufferManager;
import de.dlr.gitlab.fame.service.output.OutputSerializer;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.MpiMock;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Tests for class {@link OutputManager}
 *
 * @author Christoph Schimeczek */
public class OutputManagerTest {
	private final int outputInterval = 21;

	private TimeStamp dummyTime = new TimeStamp(14L);

	private MpiManager mpi;
	private BufferManager bufferManager;
	private OutputSerializer outputSerializer;
	private OutputManager outputManager;
	private Setup setup;

	@Before
	public void setup() {
		mpi = MpiMock.newMockMpi();
		when(mpi.isInputOutputProcess()).thenReturn(true);

		bufferManager = mock(BufferManager.class);
		outputSerializer = mock(OutputSerializer.class);
		outputManager = new OutputManager(mpi, bufferManager, outputSerializer);

		setup = mock(Setup.class);
		when(setup.getOutputInterval()).thenReturn(outputInterval);
		when(setup.getOutputAgents()).thenReturn(new ArrayList<>());
		when(setup.isOutputComplex()).thenReturn(true);

		outputManager.setOutputParameters(setup, Collections.emptyList());
	}

	@Test
	public void setOutputParameters_bufferManagerInitialised() {
		verify(bufferManager).setBufferParameters(any(), any(), any(), eq(true));
	}

	@Test
	public void setOutputParameters_outputIntervalSet() {
		assertEquals(outputInterval, outputManager.getOutputInterval());
	}

	@Test
	public void writeInputData_canWrite_forwardsDataStorage() {
		DataStorage dataStorage = mock(DataStorage.class);
		outputManager.writeInputData(dataStorage);
		verify(outputSerializer, times(1)).writeInputData(dataStorage);
	}

	@Test
	public void writeInputData_canNotWrite_forwardsDataStorage() {
		initPassiveOutputManager();
		DataStorage dataStorage = mock(DataStorage.class);
		outputManager.writeInputData(dataStorage);
		verify(outputSerializer, times(0)).writeInputData(dataStorage);
	}

	/** Initialises {@link #outputManager} without OutputSerialiser */
	private void initPassiveOutputManager() {
		outputManager = new OutputManager(mpi, bufferManager, null);
		outputManager.setOutputParameters(setup, Collections.emptyList());
		when(mpi.isInputOutputProcess()).thenReturn(false);
	}

	@Test
	public void tickBuffersAndWriteOutData_call_timeStepForwardedToBufferManager() {
		outputManager.tickBuffersAndWriteOutData(3L, dummyTime);
		verify(bufferManager).finishTick(14L);
	}

	@Test
	public void tickBuffersAndWriteOutData_lastTickInInterval_aggregatesAtRoot() {
		when(bufferManager.flushAllBuffersToOutputBuilder()).thenReturn(Output.newBuilder());
		outputManager.tickBuffersAndWriteOutData(outputInterval, dummyTime);
		verify(mpi, times(1)).aggregateAt(any(), eq(Constants.ROOT), eq(Tag.OUTPUT));
	}

	@Test
	public void tickBuffersAndWriteOutData_lastTickInIntervalAndCanWrite_writesOutput() {
		when(bufferManager.flushAllBuffersToOutputBuilder()).thenReturn(Output.newBuilder());
		outputManager.tickBuffersAndWriteOutData(outputInterval, dummyTime);
		verify(outputSerializer, times(1)).writeBundledOutput(any());
	}

	@Test
	public void tickBuffersAndWriteOutData_lastTickInIntervalAndCanNotWrite_doesNotWrite() {
		initPassiveOutputManager();
		when(bufferManager.flushAllBuffersToOutputBuilder()).thenReturn(Output.newBuilder());
		outputManager.tickBuffersAndWriteOutData(outputInterval, dummyTime);
		verify(outputSerializer, times(0)).writeBundledOutput(any());
	}

	@Test
	public void flushAll_flushesBuffers() {
		when(bufferManager.flushAllBuffersToOutputBuilder()).thenReturn(Output.newBuilder());
		outputManager.flushAll(dummyTime);
		verify(bufferManager, times(1)).flushAllBuffersToOutputBuilder();
	}

	@Test
	public void flushAll_aggregatesAtRoot() {
		when(bufferManager.flushAllBuffersToOutputBuilder()).thenReturn(Output.newBuilder());
		outputManager.flushAll(dummyTime);
		verify(mpi, times(1)).aggregateAt(any(), eq(Constants.ROOT), eq(Tag.OUTPUT));
	}

	@Test
	public void flushAll_canWrite_writesOutput() {
		when(bufferManager.flushAllBuffersToOutputBuilder()).thenReturn(Output.newBuilder());
		outputManager.flushAll(dummyTime);
		verify(outputSerializer, times(1)).writeBundledOutput(any());
	}

	@Test
	public void flushAll_canNotWrite_doesNotWrite() {
		initPassiveOutputManager();
		when(bufferManager.flushAllBuffersToOutputBuilder()).thenReturn(Output.newBuilder());
		outputManager.flushAll(dummyTime);
		verify(outputSerializer, times(0)).writeBundledOutput(any());
	}

	@Test
	public void close_canWrite_closesFile() {
		outputManager.close();
		verify(outputSerializer, times(1)).close();
	}

	@Test
	public void close_canNotWrite_doesNotCloseFile() {
		initPassiveOutputManager();
		outputManager.close();
		verify(outputSerializer, times(0)).close();
	}

	@Test
	public void registerAgent_createsBuffer() {
		Agent agent = mock(Agent.class);
		when(agent.getId()).thenReturn(42L);
		outputManager.registerAgent(agent);
		verify(bufferManager, times(1)).createBuffer(any(), eq(42L));
	}

	@Test
	public void logEnvironmentData_canWrite_writes() {
		outputManager.logEnvironmentData();
		verify(outputSerializer, times(1)).writeExecutionData(any(ExecutionData.class));
	}

	@Test
	public void logEnvironmentData_canNotWrite_doesNotWrite() {
		initPassiveOutputManager();
		outputManager.logEnvironmentData();
		verify(outputSerializer, times(0)).writeExecutionData(any());
	}

	@Test
	public void logRuntimeData_canWrite_writes() {
		outputManager.logRuntimeData("MyDate", 1, 2);
		verify(outputSerializer, times(1)).writeExecutionData(any(ExecutionData.class));
	}

	@Test
	public void logRuntimeData_canNotWrite_doesNotWrite() {
		initPassiveOutputManager();
		outputManager.logRuntimeData("MyDate", 1, 2);
		verify(outputSerializer, times(0)).writeExecutionData(any());
	}
}