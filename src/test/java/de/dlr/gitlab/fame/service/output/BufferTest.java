// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.output;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line.Column;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line.Column.Map;
import de.dlr.gitlab.fame.testUtils.ExceptionTesting;

public class BufferTest {
	enum Columns {
		ONE, TWO, THREE, FOUR, FIVE
	};

	enum ColumnsX {
		UNKNOWN, FOREIGN, STRANGE
	};

	private long myId = 11L;

	@Test
	public void constructor_savesClassname() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName1", myId, Arrays.asList(Columns.values()));
		assertEquals("ClassName1", buffer.getClassName());
	}

	@Test
	public void constructor_savesId() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName2", myId, Arrays.asList(Columns.values()));
		assertEquals(myId, buffer.getAgentId());
	}

	@Test
	public void constructor_withNullColumnList() {
		new AllColumnBuffer("ClassName3", myId, null);
	}

	@Test
	public void constructor_withEmptyColumnList() {
		new AllColumnBuffer("ClassName4", myId, new ArrayList<Enum<?>>());
	}

	@Test
	public void getSeries_returnsEmptySeriesOnDefault() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName5", myId, new ArrayList<Enum<?>>());
		Series series = buffer.getSeries();
		assertValidSeries(series, "ClassName5");
		assertTrue(series.getLinesCount() == 0);
	}

	/** asserts that given Series is properly initialised */
	private void assertValidSeries(Series series, String expectedClassName) {
		assertTrue(series.isInitialized());
		assertEquals(myId, series.getAgentId());
		assertEquals(expectedClassName, series.getClassName());
	}

	@Test
	public void tick_getSeries_returnsEmptySeries() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName6", myId, new ArrayList<Enum<?>>());
		buffer.tick(5L);
		Series series = buffer.getSeries();
		assertValidSeries(series, "ClassName6");
		assertTrue(series.getLinesCount() == 0);
	}

	@Test
	public void store_FailsIfNoTypeRegistered() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName7", myId, new ArrayList<Enum<?>>());
		ExceptionTesting.assertThrowsFatalMessage(AllColumnBuffer.EX_UNKNOWN_COLUMN, () -> buffer.store(Columns.ONE, 99.));
	}

	@Test
	public void store_FailsIfUnknownType() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName8", myId, Arrays.asList(Columns.values()));
		ExceptionTesting.assertThrowsFatalMessage(AllColumnBuffer.EX_UNKNOWN_COLUMN,
				() -> buffer.store(ColumnsX.UNKNOWN, 99.));
	}

	@Test
	public void store_FailsIfDoubleWriteOnSameColumn() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName88", myId, Arrays.asList(Columns.values()));
		buffer.store(Columns.ONE, 99.);
		ExceptionTesting.assertThrowsFatalMessage(AllColumnBuffer.EX_OVERWRITE_COLUMN,
				() -> buffer.store(Columns.ONE, 99.));
	}

	@Test
	public void store_FailsIfMixedWriteOnSameColumn() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName201", myId, Arrays.asList(Columns.values()));
		ComplexIndex<?> complexIndex = mock(ComplexIndex.class);
		when(complexIndex.getFieldName()).thenReturn(Columns.ONE.name());
		when(complexIndex.getKeyValues()).thenReturn(new String[] {"A", "B"});

		buffer.store(complexIndex, 99.);
		ExceptionTesting.assertThrowsFatalMessage(AllColumnBuffer.EX_MIXED_COLUMN, () -> buffer.store(Columns.ONE, 88));
	}

	@Test
	public void store_tick_store_WorksOnSameColumn() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName77", myId, Arrays.asList(Columns.values()));
		buffer.store(Columns.ONE, 99.);
		buffer.tick(55L);
		buffer.store(Columns.ONE, 100.);
		buffer.tick(66L);
		Series series = buffer.getSeries();
		Line line1 = series.getLines(0);
		Line line2 = series.getLines(1);
		assertEquals(55L, line1.getTimeStep());
		assertEquals(66L, line2.getTimeStep());
		assertEquals(99., line1.getColumns(0).getValue(), 1E-14);
		assertEquals(100., line2.getColumns(0).getValue(), 1E-14);
	}

	@Test
	public void store_tick_getSeries_returnsSeriesWithLineAndColumn() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName9", myId, Arrays.asList(Columns.values()));
		buffer.store(Columns.ONE, 99.);
		buffer.tick(5L);
		Series series = buffer.getSeries();
		assertValidSeries(series, "ClassName9");
		assertEquals(5L, series.getLines(0).getTimeStep());
		Column storedColumn = series.getLines(0).getColumns(0);
		assertEquals(Columns.ONE.ordinal(), storedColumn.getFieldId());
		assertEquals(99., storedColumn.getValue(), 1E-14);
	}

	@Test
	public void constructor_secondInstanceUsesStaticData() {
		AllColumnBuffer bufferA = new AllColumnBuffer("ClassName100", myId, Arrays.asList(Columns.values()));
		AllColumnBuffer bufferB = new AllColumnBuffer("ClassName100", 6767L, null);
		bufferA.store(Columns.ONE, 99.);
		bufferB.store(Columns.ONE, 99.);
	}

	@Test
	public void store_ComplexFailsIfNoTypeRegistered() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName200", myId, new ArrayList<Enum<?>>());
		ComplexIndex<?> complexIndex = mock(ComplexIndex.class);
		when(complexIndex.getFieldName()).thenReturn("NotAColumn");
		ExceptionTesting.assertThrowsFatalMessage(AllColumnBuffer.EX_UNKNOWN_COLUMN, () -> buffer.store(complexIndex, 99.));
	}

	@Test
	public void store_ComplexFailsAfterSimpleOnSameColumn() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName201", myId, Arrays.asList(Columns.values()));
		ComplexIndex<?> complexIndex = mock(ComplexIndex.class);
		when(complexIndex.getFieldName()).thenReturn(Columns.ONE.name());

		buffer.store(Columns.ONE, 88);
		ExceptionTesting.assertThrowsFatalMessage(AllColumnBuffer.EX_MIXED_COLUMN, () -> buffer.store(complexIndex, 99.));
	}

	@Test
	public void store_ComplexWorksAfterSimpleOnDifferentColumns() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName202", myId, Arrays.asList(Columns.values()));
		ComplexIndex<?> complexIndex = mock(ComplexIndex.class);
		when(complexIndex.getFieldName()).thenReturn(Columns.TWO.name());
		when(complexIndex.getKeyValues()).thenReturn(new String[] {"A", "B"});

		buffer.store(Columns.ONE, 88);
		buffer.store(complexIndex, 99.);
	}

	@Test
	public void store_tick_getSeries_ComplexIndex() {
		AllColumnBuffer buffer = new AllColumnBuffer("ClassName203", myId, Arrays.asList(Columns.values()));
		ComplexIndex<?> complexIndex = mock(ComplexIndex.class);
		when(complexIndex.getFieldName()).thenReturn(Columns.TWO.name());
		when(complexIndex.getKeyValues()).thenReturn(new String[] {"valueA1", "valueB1"},
				new String[] {"valueA2", "valueB2"});
		buffer.store(complexIndex, 1.);
		buffer.store(complexIndex, 2.);
		buffer.store(Columns.ONE, 88.);

		buffer.tick(5L);
		Series series = buffer.getSeries();
		Column storedColumnOne = series.getLines(0).getColumns(0);
		assertEquals(Columns.ONE.ordinal(), storedColumnOne.getFieldId());
		assertEquals(88., storedColumnOne.getValue(), 1E-14);

		Column storedColumnTwo = series.getLines(0).getColumns(1);
		assertEquals(Columns.TWO.ordinal(), storedColumnTwo.getFieldId());
		List<Map> entries = storedColumnTwo.getEntriesList();
		assertEquals(((Double) 1.0).toString(), entries.get(0).getValue());
		assertEquals("valueA1", entries.get(0).getIndexValues(0));
		assertEquals("valueB1", entries.get(0).getIndexValues(1));

		assertEquals(((Double) 2.0).toString(), entries.get(1).getValue());
		assertEquals("valueA2", entries.get(1).getIndexValues(0));
		assertEquals("valueB2", entries.get(1).getIndexValues(1));
	}
}