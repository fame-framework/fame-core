// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.output;

import static org.junit.Assert.assertEquals;
import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import org.junit.Test;

public class ComplexIndexTest {
	@Output
	private enum Columns {
		A, B
	};

	private enum NoOutputEnum {
		A, B
	}

	private enum Keys {
		X, Y, Z
	}

	@Test
	public void build_FailOnNonOutputColumn() {
		assertThrowsFatalMessage(ComplexIndex.ERR_NO_OUTPUT, () -> ComplexIndex.build(NoOutputEnum.A, Keys.class));
	}

	@Test
	public void getFieldName_ReturnsFieldName() {
		ComplexIndex<Keys> complexIndex = ComplexIndex.build(Columns.A, Keys.class);
		assertEquals(Columns.A.name(), complexIndex.getFieldName());
	}

	@Test
	public void getKeyLabels() {
		ComplexIndex<Keys> complexIndex = ComplexIndex.build(Columns.A, Keys.class);
		assertEquals(Keys.values()[0], complexIndex.getKeyLabels()[0]);
		assertEquals(Keys.values()[1], complexIndex.getKeyLabels()[1]);
		assertEquals(Keys.values()[2], complexIndex.getKeyLabels()[2]);
	}

	@Test
	public void getKeyValues_FailsIfNotAllKeysSet() {
		ComplexIndex<Keys> complexIndex = ComplexIndex.build(Columns.A, Keys.class);
		complexIndex.key(Keys.X, "X");
		complexIndex.key(Keys.Y, "Y");
		assertThrowsFatalMessage(ComplexIndex.ERROR_MISSING_KEY, () -> complexIndex.getKeyValues());
	}

	@Test
	public void getKeyValues_WorksIfAllKeysSet() {
		ComplexIndex<Keys> complexIndex = ComplexIndex.build(Columns.A, Keys.class);
		complexIndex.key(Keys.X, "X");
		complexIndex.key(Keys.Y, "Y");
		complexIndex.key(Keys.Z, "Z");
		Object[] result = complexIndex.getKeyValues();
		assertEquals("X", result[0]);
		assertEquals("Y", result[1]);
		assertEquals("Z", result[2]);
	}

	@Test
	public void clear_getKeyValues_FailsIfNotAllValuesSetAgain() {
		ComplexIndex<Keys> complexIndex = ComplexIndex.build(Columns.A, Keys.class);
		complexIndex.key(Keys.X, "X").key(Keys.Y, "Y").key(Keys.Z, "Z");
		complexIndex.clear();
		complexIndex.key(Keys.X, "A");
		assertThrowsFatalMessage(ComplexIndex.ERROR_MISSING_KEY, () -> complexIndex.getKeyValues());
	}

	@Test
	public void getKeyValues_KeepIndexOrder() {
		ComplexIndex<Keys> complexIndex = ComplexIndex.build(Columns.A, Keys.class);
		complexIndex.key(Keys.Y, "Y").key(Keys.X, "X").key(Keys.Z, "Z");
		Object[] result = complexIndex.getKeyValues();
		assertEquals("X", result[0]);
		assertEquals("Y", result[1]);
		assertEquals("Z", result[2]);
	}
}
