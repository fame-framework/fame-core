// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.input;

import static org.junit.Assert.assertEquals;
import java.nio.charset.StandardCharsets;
import org.junit.Test;

public class FileConstantsTest {

	@Test
	public void getHeaderVersion_unidentified_returns0() {
		assertEquals(0, FileConstants.getHeaderVersion(new byte[30]));
	}

	@Test
	public void getHeaderVersion_headerv1_returns1() {
		byte[] headerV1 = FileConstants.HEADER_V1.getBytes(StandardCharsets.UTF_8);
		assertEquals(1, FileConstants.getHeaderVersion(headerV1));
	}

	@Test
	public void getHeaderVersion_headerv2_returns2() {
		byte[] headerV2 = FileConstants.HEADER_V2.getBytes(StandardCharsets.UTF_8);
		assertEquals(2, FileConstants.getHeaderVersion(headerV2));
	}
}
