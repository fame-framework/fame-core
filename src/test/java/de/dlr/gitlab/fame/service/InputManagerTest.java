// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import de.dlr.gitlab.fame.communication.ContractTest;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao;
import de.dlr.gitlab.fame.protobuf.Model.ModelData;
import de.dlr.gitlab.fame.protobuf.Model.ModelData.JavaPackages;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;
import de.dlr.gitlab.fame.service.InputManager.JavaPackageNames;
import de.dlr.gitlab.fame.service.input.FileReader;
import de.dlr.gitlab.fame.testUtils.MpiMock;

public class InputManagerTest {
	private static final String AGENT_PACKAGE = "MyAgent";
	private static final String DATAITEM_PACKAGE = "MyDataItem";
	private static final String PORTABLE_PACKAGE = "MyPortable";

	private InputManager inputManager;
	private MpiManager mockMpiManager;
	private DataStorage mockedDataStorage;
	private InputData mockedInputData;
	private ModelData mockedModelData;

	@Before
	public void setup() {
		mockMpiManager = MpiMock.newMockMpi();
		when(mockMpiManager.isInputOutputProcess()).thenReturn(true);
		inputManager = new InputManager(mockMpiManager);
		mockedInputData = mock(InputData.class);
		mockedDataStorage = mock(DataStorage.class);
		when(mockedDataStorage.getInput()).thenReturn(mockedInputData);
		mockedModelData = mock(ModelData.class);
		when(mockedDataStorage.getModel()).thenReturn(mockedModelData);
		var packages = JavaPackages.newBuilder().addAgents(AGENT_PACKAGE).addDataItems(DATAITEM_PACKAGE)
				.addPortables(PORTABLE_PACKAGE).build();
		when(mockedModelData.getPackageDefinition()).thenReturn(packages);
	}

	@Test
	public void read_isRoot_setsInputData() {
		readMockedInput();
		assertTrue(inputManager.inputIsSet());
	}

	/** calls {@link InputManager#read(String)} and thus mocks the return from file reader */
	private void readMockedInput() {
		try (MockedStatic<FileReader> reader = mockStatic(FileReader.class)) {
			reader.when(() -> FileReader.read(any(String.class))).thenReturn(mockedDataStorage);
			inputManager.read("DummyFileName");
		}
	}

	@Test
	public void read_isNotRoot_inputNotSet() {
		when(mockMpiManager.isInputOutputProcess()).thenReturn(false);
		readMockedInput();
		assertFalse(inputManager.inputIsSet());
	}

	@Test
	public void read_isRoot_setsModelData() {
		readMockedInput();
		assertTrue(inputManager.modelIsSet());
	}

	@Test
	public void read_isNotRoot_modelNotSet() {
		when(mockMpiManager.isInputOutputProcess()).thenReturn(false);
		readMockedInput();
		assertFalse(inputManager.modelIsSet());		
	}

	@Test
	public void getContractPrototypes_returnsPrototypesFromInput() {
		readMockedInput();
		ProtoContract proto1 = ContractTest.mockProto(1L, 2L, "ProductA", 3L, 4L, 5L);
		ProtoContract proto2 = ContractTest.mockProto(6L, 7L, "ProductB", 8L, 9L, 10L);
		when(mockedInputData.getContractsList()).thenReturn(Arrays.asList(proto1, proto2));

		List<ProtoContract> contracts = inputManager.getContractPrototypes();
		assertEquals(2, contracts.size());
		assertTrue(contracts.contains(proto1));
		assertTrue(contracts.contains(proto2));
	}

	@Test
	public void getTimeSeries_seriesIndexUseTwice_throws() {
		readMockedInput();
		List<TimeSeriesDao> seriesList = Arrays.asList(mockSeries(15, "A"), mockSeries(15, "B"));
		when(mockedInputData.getTimeSeriesList()).thenReturn(seriesList);
		assertThrowsFatalMessage(InputManager.ERR_TIME_SERIES_INDEX_COLLISION, () -> inputManager.getTimeSeries());
	}

	/** @return a mocked TimeSeriesDao with given name and id and empty RowList */
	private TimeSeriesDao mockSeries(int id, String name) {
		TimeSeriesDao series = mock(TimeSeriesDao.class);
		when(series.getSeriesId()).thenReturn(id);
		when(series.getSeriesName()).thenReturn(name);
		return series;
	}

	@Test
	public void getTimeSeries_noIndexCollision_returnsSeriesMap() {
		readMockedInput();
		List<TimeSeriesDao> seriesList = Arrays.asList(mockSeries(15, "A"), mockSeries(16, "B"));
		when(mockedInputData.getTimeSeriesList()).thenReturn(seriesList);
		Map<Integer, TimeSeries> tsMap = inputManager.getTimeSeries();
		assertTrue(tsMap.containsKey(15));
		assertTrue(tsMap.containsKey(16));
	}

	@Test
	public void getJavaPackagesNames_returnsReadJavaPackages() {
		readMockedInput();
		JavaPackageNames packagesNames = inputManager.getPackageNames();
		assert packagesNames.getAgentPackages().contains(AGENT_PACKAGE);
		assert packagesNames.getDataItemPackages().contains(DATAITEM_PACKAGE);
		assert packagesNames.getPortablePackages().contains(PORTABLE_PACKAGE);
	}
}