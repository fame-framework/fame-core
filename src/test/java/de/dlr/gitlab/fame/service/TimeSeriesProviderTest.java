// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.service.TimeSeriesProvider.MissingSeriesException;

/**
 * Test cases for {@link TimeSeriesProvider}
 *
 * @author Christoph Schimeczek
 */
public class TimeSeriesProviderTest {
	private final int numberOfTestCases = 5;
	private TimeSeriesProvider timeSeriesProvider;
	private Map<Integer, TimeSeries> mapOfSeries;
	
	@Before
	public void setup() {
		mapOfSeries = createNewMap(numberOfTestCases);
		timeSeriesProvider = new TimeSeriesProvider(mapOfSeries);
	}

	/** @return new Map of Integers to TimeSeries */
	private Map<Integer, TimeSeries> createNewMap(int entryCount) {
		Map<Integer, TimeSeries> map = new HashMap<>();
		for (int timeSeriesId = 0; timeSeriesId < entryCount; timeSeriesId++) {
			map.put(timeSeriesId, mock(TimeSeries.class));
		}
		return map;
	}

	@Test
	public void testConstructorAndGets() throws MissingSeriesException {
		for (Integer timeSeriesId : mapOfSeries.keySet()) {
			TimeSeries result = timeSeriesProvider.getSeriesById(timeSeriesId);
			assertEquals(mapOfSeries.get(timeSeriesId), result);
		}
	}
	
	@Test (expected = MissingSeriesException.class)
	public void testGetSeriesByIdMissingId() throws MissingSeriesException {
		timeSeriesProvider.getSeriesById(numberOfTestCases + 10);
	}
}