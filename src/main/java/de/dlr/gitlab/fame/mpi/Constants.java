// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.mpi;

/** Constants of package mpi
 * 
 * @author Christoph Schimeczek */
public class Constants {
	/** ID of the root process */
	public static final int ROOT = 0;
	/** ID of the process that reads inputs and writes outputs */
	public static final int INPUT_OUTPUT_PROCESS = ROOT;

	/** ID representing a purpose of the MPI communication */
	public static enum Tag {
		/** Update of scheduling times in simulation */
		SCHEDULE,
		/** Update of agent process locations */
		ADDRESSES,
		/** Transmission of agent messages between processes */
		POST,
		/** Transmission of output data to output process */
		OUTPUT,
		/** Warm-up of agents before starting the simulation */
		WARM_UP,
		/** Exchange of Contract Data */
		CONTRACTS
	}
}