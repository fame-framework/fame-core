// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.scheduling.stats;

/** Creates runtime statistics from assessed actions
 * 
 * @author Christoph Schimeczek */
public interface RuntimeTracking {

	/** Begin logging of action duration for the specified agent
	 * 
	 * @param agentId id of agent who will now undertake actions */
	public void startActionsForAgent(long agentId);

	/** End logging of action duration for the specified agent */
	public void endCurrentActions();

	/** End scheduling during a tick */
	public void endTick();

	/** @return action statistics in JSON format */
	public String getStatsJson();
}
