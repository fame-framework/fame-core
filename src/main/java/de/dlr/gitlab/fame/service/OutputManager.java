// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.mpi.Constants;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData.ProcessConfiguration;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData.Simulation;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData.VersionData;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Services.Output.Builder;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;
import de.dlr.gitlab.fame.service.output.AllColumnBuffer;
import de.dlr.gitlab.fame.service.output.BufferManager;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;
import de.dlr.gitlab.fame.service.output.OutputBuffer;
import de.dlr.gitlab.fame.service.output.OutputSerializer;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.time.TimeStamp;
import de.dlr.gitlab.fame.util.Reflection;
import de.dlr.gitlab.fame.util.Reflection.ReflectionType;

/** Coordinates {@link BufferManager}s on all processes and the {@link OutputSerializer} for output files
 * 
 * @author Christoph Schimeczek */
public class OutputManager extends Service {
	static Logger logger = LoggerFactory.getLogger(OutputManager.class);

	private final BufferManager bufferManager;
	private final OutputSerializer outputSerializer;
	private int outputInterval = 0;

	OutputManager(MpiManager mpi, BufferManager bufferManager, OutputSerializer outputSerializer) {
		super(mpi);
		this.bufferManager = bufferManager;
		this.outputSerializer = outputSerializer;
	}

	/** Constructs an {@link OutputManager} that uses the specified {@link MpiManager} and {@link OutputSerializer}
	 * 
	 * @param mpi {@link MpiManager} to organise data exchange between nodes
	 * @param outputSerializer to write out data to file - if null, no output is written on this process<br>
	 *          Make sure to assign an OutputSerialiser to - and only to - the <b>root</b> process */
	OutputManager(MpiManager mpi, OutputSerializer outputSerializer) {
		this(mpi, new BufferManager(), outputSerializer);
	}

	/** Set relevant output parameters */
	void setOutputParameters(Setup setup, List<String> agentPackageNames) {
		outputInterval = setup.getOutputInterval();

		Reflection reflection = new Reflection(ReflectionType.AGENT, agentPackageNames);
		HashMap<Class<? extends Agent>, List<Enum<?>>> outputColumnLists = reflection
				.findAnnotatedEnumsInHierarchyForChildrenOf(Output.class, Agent.class);
		HashMap<String, List<ComplexIndex<? extends Enum<?>>>> complexColumnLists = reflection
				.findComplexIndexHierarchyForAgents();
		bufferManager.setBufferParameters(setup.getOutputAgents(), outputColumnLists, complexColumnLists,
				setup.isOutputComplex());
	}

	/** @returns output interval */
	int getOutputInterval() {
		return outputInterval;
	}

	/** Writes given DataStorage to the output file
	 * 
	 * @param inputDataStorage DataStorage to be written
	 * @throws RuntimeException if given inputDataStorage is null */
	void writeInputData(DataStorage inputDataStorage) {
		if (mpi.isInputOutputProcess()) {
			outputSerializer.writeInputData(inputDataStorage);
		}
	}

	/** tick {@link AllColumnBuffer} indices and gather and write data to output files if buffers are full */
	void tickBuffersAndWriteOutData(long tick, TimeStamp timeStamp) {
		bufferManager.finishTick(timeStamp.getStep());
		if (isLastTickInInverval(tick)) {
			writeAllBufferDataToFile();
		}
	}

	/** @return true {@link #outputInterval} ticks have passed in interval */
	private boolean isLastTickInInverval(long tick) {
		return tick % outputInterval == 0;
	}

	/** gather output from all processes and write to file at central process */
	private void writeAllBufferDataToFile() {
		Bundle bundledOutputData = gatherOutputDataAtOutputProcess();
		if (mpi.isInputOutputProcess()) {
			outputSerializer.writeBundledOutput(bundledOutputData);
		}
	}

	/** gather output data from all processes at central output process */
	private Bundle gatherOutputDataAtOutputProcess() {
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		Bundle bundle = createBundleForOutput(outputBuilder);
		return mpi.aggregateAt(bundle, Constants.INPUT_OUTPUT_PROCESS, Tag.OUTPUT);
	}

	/** Flushes all buffered data to the output file
	 * 
	 * @param timeStamp time of the last tick */
	public void flushAll(TimeStamp timeStamp) {
		bufferManager.finishTick(timeStamp.getStep());
		writeAllBufferDataToFile();
	}

	/** registers given agent and returns dedicated output buffer.
	 * 
	 * @param agent to register
	 * @return created buffer for that agent */
	public OutputBuffer registerAgent(Agent agent) {
		return bufferManager.createBuffer(agent.getClass(), agent.getId());
	}

	/** Writes MPI configuration and version strings to output file */
	void logEnvironmentData() {
		if (mpi.isInputOutputProcess()) {
			VersionData versionData = new RuntimeProperties().getVersions();
			ProcessConfiguration mpiConfig = ProcessConfiguration.newBuilder()
					.setCoreCount(mpi.getProcessCount())
					.setOutputProcess(Constants.INPUT_OUTPUT_PROCESS)
					.setOutputInterval(outputInterval)
					.build();
			ExecutionData data = ExecutionData.newBuilder().setConfiguration(mpiConfig).setVersionData(versionData).build();
			outputSerializer.writeExecutionData(data);
		}
	}

	/** Write given simulation runtime data to the output file
	 * 
	 * @param startDateTime formatted date-time string telling when the simulation began
	 * @param walltimeInMillis number of milliseconds the simulation ran
	 * @param ticks number of ticks executed in the simulation */
	void logRuntimeData(String startDateTime, long walltimeInMillis, long ticks) {
		if (mpi.isInputOutputProcess()) {
			Simulation.Builder simulationStats = Simulation.newBuilder()
					.setStart(startDateTime)
					.setDurationInMs(walltimeInMillis)
					.setTickCount(ticks);
			ExecutionData data = ExecutionData.newBuilder().setSimulation(simulationStats).build();
			outputSerializer.writeExecutionData(data);
		}
	}

	/** Closes the open file handlers - no more writing can be done afterwards */
	void close() {
		if (mpi.isInputOutputProcess()) {
			outputSerializer.close();
		}
	}
}