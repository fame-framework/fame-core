// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.mpi.Constants;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao;
import de.dlr.gitlab.fame.protobuf.Model.ModelData;
import de.dlr.gitlab.fame.protobuf.Model.ModelData.JavaPackages;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;
import de.dlr.gitlab.fame.service.input.FileReader;

/** Reads input files on root and distributes data to other processes
 * 
 * @author Christoph Schimeczek */
public class InputManager extends Service {
	static final String ERR_TIME_SERIES_INDEX_COLLISION = "TimeSeries index already used: ";

	static Logger logger = LoggerFactory.getLogger(InputManager.class);
	private DataStorage allData;
	private InputData inputData;
	private ModelData modelData;

	/** Contains lists of Java packages names that contain {@link Agent}s, {@link DataItem}s and {@link Portable}s */
	public class JavaPackageNames {
		private final JavaPackages javaPackages;

		private JavaPackageNames(ModelData modelData) {
			this.javaPackages = modelData.getPackageDefinition();
		}

		/** @return Names of all packages that may contain {@link Agent}s for this simulation */
		public List<String> getAgentPackages() {
			return javaPackages.getAgentsList();
		}

		/** @return Names of all packages that may contain {@link DataItem}s for this simulation */
		public List<String> getDataItemPackages() {
			return javaPackages.getDataItemsList();
		}

		/** @return Names of all packages that may contain {@link Portable}s for this simulation */
		public List<String> getPortablePackages() {
			return javaPackages.getPortablesList();
		}
	}

	/** Create new InputManager connected to a given {@link MpiManager}
	 * 
	 * @param mpi MpiManager used to exchange Data with other InputManager instances */
	public InputManager(MpiManager mpi) {
		super(mpi);
	}

	/** Reads given file to root's {@link InputData} storage
	 * 
	 * @param inputFileName path of the File to be read */
	public void read(String inputFileName) {
		if (mpi.isInputOutputProcess()) {
			allData = FileReader.read(inputFileName);
			inputData = allData.getInput();
			modelData = allData.getModel();
		}
	}

	/** Distributes previous read {@link InputData} from root to all other processes */
	public void distribute() {
		Bundle bundle = null;
		if (mpi.isInputOutputProcess()) {
			bundle = createBundleForInput(inputData, modelData);
		}
		var message = mpi.broadcast(bundle, Constants.INPUT_OUTPUT_PROCESS).getMessages(0);
		inputData = message.getInput();
		modelData = message.getModel();
	}

	/** Returns simulation parameters read from input file
	 * 
	 * @return {@link SimulationParam} read from input file */
	public SimulationParam getConfig() {
		return inputData.getSimulation();
	}

	/** Returns an unmodifiable Map linking {@link TimeSeries} to their ID as specified in the input file
	 * 
	 * @return unmodifiable {@link Map} linking {@link TimeSeries} to their ID as specified in the input file */
	public Map<Integer, TimeSeries> getTimeSeries() {
		HashMap<Integer, TimeSeries> timeSeriesMap = new HashMap<>();
		for (TimeSeriesDao series : inputData.getTimeSeriesList()) {
			TimeSeries timeSeries = new TimeSeries(series);
			Integer id = series.getSeriesId();
			if (timeSeriesMap.containsKey(id)) {
				throw Logging.logFatalException(logger, ERR_TIME_SERIES_INDEX_COLLISION + id);
			}
			timeSeriesMap.put(id, timeSeries);
		}
		return Collections.unmodifiableMap(timeSeriesMap);
	}

	/** Returns List of all {@link AgentDao AgentConfigurations} read from protobuf file
	 * 
	 * @return List of all agent configurations read from protobuf file */
	public List<AgentDao> getAgentConfigs() {
		return inputData.getAgentsList();
	}

	/** Returns a List of all {@link ProtoContract}s read from protobuf file
	 * 
	 * @return List of all {@link ProtoContract}s read from protobuf file */
	public ArrayList<ProtoContract> getContractPrototypes() {
		ArrayList<ProtoContract> prototypes = new ArrayList<>();
		prototypes.addAll(inputData.getContractsList());
		return prototypes;
	}

	/** @return true if input data is set */
	boolean inputIsSet() {
		return inputData != null;
	}

	/** @return true if model data is set */
	boolean modelIsSet() {
		return modelData != null;
	}

	/** @return first DataStorage read from input file - only available on root node, return null on other nodes */
	DataStorage getInputDataStorage() {
		return allData;
	}

	/** Returns Java package names for model classes containing Agents, DataItems, and Portables
	 * 
	 * @return wrapper of Java package names */
	public JavaPackageNames getPackageNames() {
		return new JavaPackageNames(modelData);
	}
}