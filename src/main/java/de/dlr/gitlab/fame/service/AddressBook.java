// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.protobuf.Services;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.AddressBook.Builder;

/** Hosts the process-location of each agent and exchanges this information with other processes
 * 
 * @author Christoph Schimeczek */
public class AddressBook extends Service {
	static final String ERR_UNKNOWN_AGENT_ID = "Unknown agent with id ";

	/** Stores processID for every {@link Agent} in simulation */
	private final HashMap<Long, Integer> processIdByAgent;
	/** Builds protobuf messages for exchanging updates with other processes */
	private final Builder builder;
	/** Holds IDs of {@link Agent}s that registered to this process's {@link AddressBook} after the last inter-process update */
	private final List<Long> newAgents;

	/** Construct new AddressBook
	 * 
	 * @param mpi to coordinate inter-process communication */
	public AddressBook(MpiManager mpi) {
		this(mpi, new HashMap<>(), Services.AddressBook.newBuilder(), new ArrayList<>(), MpiMessage.newBuilder(),
				Bundle.newBuilder());
	}

	/** Construct new AddressBook
	 * 
	 * @param mpi to coordinate inter-process communication
	 * @param processIdByAgent mapping of agent IDs to the ID of the process they reside on
	 * @param builder to build protobuf AddressBook messages
	 * @param newAgents List to hold newly registered agents
	 * @param messageBuilder to build {@link MpiMessage}s
	 * @param bundleBuilder to build {@link Bundle}s */
	AddressBook(MpiManager mpi, HashMap<Long, Integer> processIdByAgent, Builder builder, ArrayList<Long> newAgents,
			MpiMessage.Builder messageBuilder, Bundle.Builder bundleBuilder) {
		super(mpi, messageBuilder, bundleBuilder);
		this.processIdByAgent = processIdByAgent;
		this.builder = builder;
		this.newAgents = newAgents;
	}

	/** Returns ID of process that the given Agent resides on
	 * 
	 * @param agentId ID of agent whose process ID is searched
	 * @return id of the process the given agent resides on
	 * @throws RuntimeException if the given agent's ID is unknown */
	public int getProcessId(long agentId) {
		Integer processId = processIdByAgent.get(agentId);
		if (processId == null) {
			throw new RuntimeException(ERR_UNKNOWN_AGENT_ID + agentId);
		}
		return processId;
	}

	/** Register an {@link Agent} at this {@link AddressBook}; requires call to {@link #synchroniseAddressBookUpdates()} to make
	 * agent available on this and other processes
	 * 
	 * @param agent to register */
	void registerAgent(Agent agent) {
		newAgents.add(agent.getId());
	}

	/** synchronises updates for {@link AddressBook}s from all processes */
	void synchroniseAddressBookUpdates() {
		builder.clear();
		builder.setProcessId(mpi.getRank()).addAllAgentIds(newAgents);
		Bundle bundle = createBundleForAddressBook(builder);
		bundle = mpi.aggregateAll(bundle, Tag.ADDRESSES);
		updateAddressBook(bundle);
		newAgents.clear();
	}

	/** update own addressBook {@link #processIdByAgent} using the given {@link Bundle} that contains new agents and the processes
	 * they reside on */
	private void updateAddressBook(Bundle bundle) {
		for (MpiMessage mpiMessage : bundle.getMessagesList()) {
			Services.AddressBook addresses = mpiMessage.getAddressBook();
			Integer processId = addresses.getProcessId();
			for (Long agentId : addresses.getAgentIdsList()) {
				processIdByAgent.put(agentId, processId);
			}
		}
	}
}
