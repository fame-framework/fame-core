// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.scheduling.stats;

/** Dummy implementation of {@link RuntimeTracking} without any actual tracking of statistics
 * 
 * @author Christoph Schimeczek */
public class NoRuntimeTracking implements RuntimeTracking {

	@Override
	public void startActionsForAgent(long agentId) {}

	@Override
	public void endCurrentActions() {}

	@Override
	public void endTick() {}

	@Override
	public String getStatsJson() {
		return "";
	}
}
