// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.messaging;

import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage.Builder;

/** Stores messages to be sent to agents on one specific process
 * 
 * @author Christoph Schimeczek */
public class OutBox {
	private final Builder builder;

	/** Construct new {@link OutBox}
	 * 
	 * @param builder will be used to build {@link MpiMessage}s */
	public OutBox(Builder builder) {
		this.builder = builder;
	}

	/** Stores given message to this {@link OutBox} - will be bundled on call of {@link #bundle(Bundle.Builder)}
	 * 
	 * @param message to be sent to agents residing on the process associated with this {@link OutBox} */
	public void storeMessage(ProtoMessage message) {
		builder.addMessages(message);
	}

	/** Puts all currently stored messages to the given {@link Bundle} builder, which will be clears first; clears message storage
	 * and returns created {@link Bundle}
	 * 
	 * @param bundleBuilder to write the messages to
	 * @return created Bundle with all messages stored in this PostBox after the previous call to this method */
	public Bundle bundle(Bundle.Builder bundleBuilder) {
		bundleBuilder.clear();
		Bundle bundle = bundleBuilder.addMessages(builder).build();
		builder.clear();
		return bundle;
	}
}
