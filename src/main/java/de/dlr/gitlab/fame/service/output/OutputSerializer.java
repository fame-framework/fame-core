// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.output;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;

/** Manages writing operations of output data to file stream
 * 
 * @author Christoph Schimeczek */
public class OutputSerializer {
	private final HashMap<String, Boolean> headersWrittenForAgent;
	private final DataStorage.Builder dataStorageBuilder;
	private final Output.Builder outputBuilder;
	private final FileWriter fileWriter;

	/** Constructs a {@link OutputSerializer} using given FileWriter to write to output files
	 * 
	 * @param fileWriter manages streaming data to output file */
	public OutputSerializer(FileWriter fileWriter) {
		this(fileWriter, DataStorage.newBuilder(), Output.newBuilder(), new HashMap<>());
	}

	/** Constructs a {@link OutputSerializer} with given dependencies
	 * 
	 * @param fileWriter manages streaming data to output file
	 * @param dataStorageBuilder constructs DataStorage messages
	 * @param outputBuilder creates Output messages
	 * @param headersWrittenForAgent */
	OutputSerializer(FileWriter fileWriter, DataStorage.Builder dataStorageBuilder, Output.Builder outputBuilder,
			HashMap<String, Boolean> headersWrittenForAgent) {
		this.fileWriter = fileWriter;
		this.dataStorageBuilder = dataStorageBuilder;
		this.outputBuilder = outputBuilder;
		this.headersWrittenForAgent = headersWrittenForAgent;
	}

	/** Writes all output data from this Bundle to file
	 * 
	 * @param bundledOutputData Bundle containing data for output */
	public void writeBundledOutput(Bundle bundledOutputData) {
		dataStorageBuilder.clear();
		outputBuilder.clear();
		List<Output> outputs = extractOutputList(bundledOutputData);
		for (Output output : outputs) {
			updateAndAddNewHeadersToOutput(output);
			outputBuilder.addAllSeries(output.getSeriesList());
		}
		dataStorageBuilder.setOutput(outputBuilder);
		buildAndWriteDataStorage();
	}

	/** @return List of {@link Output} protobuf extracted from given {@link Bundle} of messages */
	private List<Output> extractOutputList(Bundle bundledOutputData) {
		return bundledOutputData
				.getMessagesList()
				.stream()
				.map(MpiMessage::getOutput)
				.collect(Collectors.toList());
	}

	/** adds new {@link AgentType headers} to {@link #dataStorageBuilder} and marks them as written in
	 * {@link #headersWrittenForAgent} */
	private void updateAndAddNewHeadersToOutput(Output output) {
		for (AgentType agentType : output.getAgentTypesList()) {
			String className = agentType.getClassName();
			if (!headersWrittenForAgent.containsKey(className)) {
				outputBuilder.addAgentTypes(agentType);
				headersWrittenForAgent.put(className, true);
			}
		}
	}

	/** Builds DataStorage from {@link #dataStorageBuilder} and writes it to file */
	private void buildAndWriteDataStorage() {
		DataStorage dataStorage = dataStorageBuilder.build();
		fileWriter.write(dataStorage.toByteArray());
	}

	/** Closes the associated file stream */
	public void close() {
		fileWriter.close();
	}

	/** Stores content of given DataStorage to the output file
	 * 
	 * @param inputDataStorage to be written to the output file */
	public void writeInputData(DataStorage inputDataStorage) {
		fileWriter.write(inputDataStorage.toByteArray());
	}

	/** Stores content of given ExecutionData to the output file
	 * 
	 * @param executionData to be written to the output file */
	public void writeExecutionData(ExecutionData executionData) {
		dataStorageBuilder.clear();
		dataStorageBuilder.setExecution(executionData);
		buildAndWriteDataStorage();
	}
}