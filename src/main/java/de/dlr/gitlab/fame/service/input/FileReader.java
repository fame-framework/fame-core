// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.input;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.protobuf.InvalidProtocolBufferException;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;

/** Reads given input file
 *
 * @author Christoph Schimeczek */
public abstract class FileReader {
	static final String ERR_INPUT_FILE_NOT_FOUND = "Could not find input file: ";
	static final String ERR_READ_ERROR = "Reading failed for file: ";
	static final String ERR_PARSER_ERROR = "Could not parse content of input file: ";
	static final String ERR_NO_HEADER = "Cannot read input file: If the input file was create by FAME-Io, version <1.6 was used but requires FAME-Io version >= 3.0.";
	static final String ERR_OLD_HEADER = "Cannot read input file: This input file was created with a old version of FAME-Io. FAME-Io version >= 3.0 is required.";

	private static Logger logger = LoggerFactory.getLogger(FileReader.class);

	/** Reads the predefined protobuf file and returns contained input data
	 * 
	 * @param inputFileName path to input file
	 * @return first DataStorage read from file */
	public static DataStorage read(String inputFileName) {
		File inputFile = new File(inputFileName);
		try {
			byte[] inputAsBytes = readFirstDataStorageInFile(inputFile);
			return parseByteInput(inputAsBytes);
		} catch (InvalidProtocolBufferException e) {
			throw Logging.logFatalException(logger, ERR_PARSER_ERROR + inputFileName);
		} catch (IOException e) {
			throw Logging.logFatalException(logger, ERR_READ_ERROR + inputFileName);
		}
	}

	/** Reads first (or only) DataStorage in given file. Throws errors if file has no or deprecated header.
	 * 
	 * @param inputFile to be read
	 * @return one DataStorage read from specified file
	 * @throws IOException if an I/O error occurs
	 * @throws InvalidProtocolBufferException if file could not be parsed in total
	 * @throws RuntimeException if file has no or deprecated header */
	private static byte[] readFirstDataStorageInFile(File inputFile) throws IOException {
		try (FileInputStream fileStream = new FileInputStream(inputFile)) {
			ensureSupportedHeader(fileStream);
			int messageLength = readMessageLength(fileStream);
			return readMessage(messageLength, fileStream);
		} catch (FileNotFoundException e) {
			throw Logging.logFatalException(logger, ERR_INPUT_FILE_NOT_FOUND + inputFile.getAbsolutePath());
		}
	}

	/** Ensures that the header on the file matches the latest header
	 * 
	 * @throws RuntimeException if no header is found or the header is no longer supported */
	private static void ensureSupportedHeader(FileInputStream fileStream) throws IOException {
		byte[] firstBytes = readHeaderBytes(fileStream);
		int headerVersion = FileConstants.getHeaderVersion(firstBytes);
		if (headerVersion == 0) {
			throw new RuntimeException(ERR_NO_HEADER);
		} else if (headerVersion < FileConstants.LATEST_HEADER_VERSION) {
			throw new RuntimeException(ERR_OLD_HEADER);
		}
	}

	/** Reads header bytes from given file stream advancing its reading position
	 * 
	 * @param fileStream to read the header from
	 * @return first header bytes of given fileStream
	 * @throws IOException if an I/O error occurs */
	private static byte[] readHeaderBytes(FileInputStream fileStream) throws IOException {
		byte[] headerBytes = new byte[FileConstants.HEADER_LENGTH];
		fileStream.read(headerBytes);
		return headerBytes;
	}

	/** @param fileStream to read the message length from - reading position will be advanced
	 * @return length of the next DataStorage element in the given fileStream
	 * @throws IOException if an I/O error occurs */
	private static int readMessageLength(FileInputStream fileStream) throws IOException {
		byte[] messageLengthBytes = new byte[FileConstants.MESSAGE_LENGTH_BYTE_COUNT];
		fileStream.read(messageLengthBytes);
		return ByteBuffer.wrap(messageLengthBytes).getInt();
	}

	/** @param messageLength number of bytes to read from fileStream
	 * @param fileStream to be read and advanced by given messageLength bytes
	 * @return bytes of given length read from given fileStream
	 * @throws IOException if an I/O error occurs */
	private static byte[] readMessage(int messageLength, FileInputStream fileStream) throws IOException {
		byte[] message = new byte[messageLength];
		fileStream.read(message);
		return message;
	}

	/** @return {@link DataStorage} of its given binary representation */
	private static DataStorage parseByteInput(byte[] inputAsBytes) throws InvalidProtocolBufferException {
		return DataStorage.parseFrom(inputAsBytes);
	}
}
