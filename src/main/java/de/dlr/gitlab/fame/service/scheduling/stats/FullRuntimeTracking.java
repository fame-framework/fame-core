// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.scheduling.stats;

import java.util.HashMap;
import java.util.function.Supplier;
import de.dlr.gitlab.fame.util.AgentInteractionStatistic;

/** Tracks duration of actions for each agent and tick-wise overlap with each other agent
 * 
 * @author Christoph Schimeczek */
public class FullRuntimeTracking implements RuntimeTracking {
	static final String ERR_NOT_ENDED = "RuntimeTracking: Action started before ending a previus one.";
	static final String ERR_NOT_STARTED = "RuntimeTracking: Action ended before starting one earlier.";

	static final String KEY = "ActionOverlapInNs";
	static final long UNSET_ID = Long.MIN_VALUE;

	private long actionStartTime;
	private long actingAgent = UNSET_ID;

	private HashMap<Long, Long> durationTotalInTickPerAgent = new HashMap<>();
	private final AgentInteractionStatistic durationOverlapBetweenAgents;

	private Supplier<Long> timer;

	/** Create a new {@link FullRuntimeTracking} tool using the specified timer function for duration tracking
	 * 
	 * @param timerFunction used to track duration of agent actions */
	FullRuntimeTracking(Supplier<Long> timerFunction, AgentInteractionStatistic durationOverlapBetweenAgents) {
		this.timer = timerFunction;
		this.durationOverlapBetweenAgents = durationOverlapBetweenAgents;
	}

	/** Create a new {@link FullRuntimeTracking} tool using {@link System#nanoTime()} for duration tracking */
	public FullRuntimeTracking() {
		this(System::nanoTime, new AgentInteractionStatistic(KEY));
	}

	@Override
	public void startActionsForAgent(long agentId) {
		if (actingAgent != UNSET_ID) {
			throw new RuntimeException(ERR_NOT_ENDED);
		}
		actingAgent = agentId;
		actionStartTime = timer.get();
	}

	@Override
	public void endCurrentActions() {
		long actionDuration = timer.get() - actionStartTime;
		if (actingAgent == UNSET_ID) {
			throw new RuntimeException(ERR_NOT_STARTED);
		}
		durationTotalInTickPerAgent.compute(actingAgent, (__, v) -> (v == null) ? actionDuration : v + actionDuration);
		actingAgent = UNSET_ID;
	}

	@Override
	public void endTick() {
		for (Long agentA : durationTotalInTickPerAgent.keySet()) {
			for (Long agentB : durationTotalInTickPerAgent.keySet()) {
				long overlap = Math.min(durationTotalInTickPerAgent.get(agentA), durationTotalInTickPerAgent.get(agentB));
				durationOverlapBetweenAgents.recordInteraction(agentA, agentB, overlap);
			}
		}
		durationTotalInTickPerAgent.clear();
	}

	@Override
	public String getStatsJson() {
		return "{" + durationOverlapBetweenAgents.getJSON() + "}";
	}
}
