// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.HashMap;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.message.MessageBuilder;
import de.dlr.gitlab.fame.communication.stats.CommTracking;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle.Builder;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.service.messaging.OutBox;

/** Manages delivery of messages between agents on any processes
 * 
 * @author Christoph Schimeczek */
public class PostOffice extends Service {
	private final AddressBook addressBook;
	private final MessageBuilder agentMessageBuilder;
	private final CommTracking commTracking;
	private final ArrayList<OutBox> outBoxes;
	private final ArrayList<Bundle> bundles;
	private final Builder bundleBuilder;
	private final HashMap<Long, Agent> localAgents;

	/** Construct a {@link PostOffice}
	 * 
	 * @param mpi MpiManager to coordinate inter-process communication
	 * @param messageBuilder for building new Messages
	 * @param commTracking gathers statistics of inter-agent communication
	 * @param addressBook hosts process-addresses of all agents */
	PostOffice(MpiManager mpi, MessageBuilder messageBuilder, CommTracking commTracking, AddressBook addressBook) {
		this(mpi, messageBuilder, commTracking, addressBook, prepareOutBoxes(mpi.getProcessCount()),
				new ArrayList<>(mpi.getProcessCount()), new HashMap<>(), Bundle.newBuilder());
	}

	PostOffice(MpiManager mpi, MessageBuilder messageBuilder, CommTracking commTracking, AddressBook addressBook,
			ArrayList<OutBox> outBoxes, ArrayList<Bundle> postBoxes, HashMap<Long, Agent> localAgents,
			Builder bundleBuilder) {
		super(mpi);
		this.agentMessageBuilder = messageBuilder;
		this.commTracking = commTracking;
		this.addressBook = addressBook;
		this.outBoxes = outBoxes;
		this.bundles = postBoxes;
		this.localAgents = localAgents;
		this.bundleBuilder = bundleBuilder;
	}

	/** @return a new OutBox for each active process, including own process */
	static ArrayList<OutBox> prepareOutBoxes(int processCount) {
		ArrayList<OutBox> outBoxes = new ArrayList<>();
		for (int processId = 0; processId < processCount; processId++) {
			outBoxes.add(new OutBox(MpiMessage.newBuilder()));
		}
		return outBoxes;
	}

	/** delivers messages to their respective receivers on any process; distributes messages for {@link Agent}s on this process */
	void deliverMessages() {
		Bundle myMessageBundle = exchangeMessagesWithOtherProcesses();
		distributeLocalMessages(myMessageBundle);
	}

	/** Exchanges messages with other processes
	 * 
	 * @return messages for local distribution from all processes */
	private Bundle exchangeMessagesWithOtherProcesses() {
		buildBundles();
		return mpi.individualAllToAll(bundles, Tag.POST);
	}

	/** Builds {@link Bundle}s from messages stored in {@link OutBox}es */
	private void buildBundles() {
		bundles.clear();
		for (OutBox outBox : outBoxes) {
			bundles.add(outBox.bundle(bundleBuilder));
		}
	}

	/** Distribute given messages among local agents */
	private void distributeLocalMessages(Bundle myMessageBundle) {
		for (MpiMessage mpiMessage : myMessageBundle.getMessagesList()) {
			for (ProtoMessage message : mpiMessage.getMessagesList()) {
				commTracking.assessMessage(message);
				long receiverId = message.getReceiverId();
				Agent receiver = localAgents.get(receiverId);
				receiver.receive(new Message(message));
			}
		}
	}

	/** Register an {@link Agent} at this {@link PostOffice};
	 * 
	 * @param agent to register */
	void registerAgent(Agent agent) {
		localAgents.put(agent.getId(), agent);
	}

	/** Send a {@link Message} to specified receiver containing all given {@link Portable}s and {@link DataItem}s
	 * 
	 * @param senderId UUID of sending agent
	 * @param receiverId UUID of receiving agent
	 * @param portable 0..1 {@link Portable} to include in message
	 * @param dataItems 0..N {@link DataItem}s to include in message */
	public void sendMessage(long senderId, long receiverId, Portable portable, DataItem... dataItems) {
		agentMessageBuilder.clear();
		agentMessageBuilder.setSenderId(senderId).setReceiverId(receiverId);
		addItems(portable, dataItems);
		storeInOutbox(addressBook.getProcessId(receiverId), agentMessageBuilder.build());
	}

	/** adds {@link DataItem}s and {@link Portable}s to the {@link #agentMessageBuilder} */
	private void addItems(Portable portable, DataItem... dataItems) {
		for (DataItem dataItem : dataItems) {
			agentMessageBuilder.add(dataItem);
		}
		if (portable != null) {
			agentMessageBuilder.add(portable);
		}
	}

	/** saves given {@link Message} to the {@link OutBox} of the receiver {@link Agent}'s process */
	private void storeInOutbox(int processId, Message message) {
		outBoxes.get(processId).storeMessage(message.createProtobufRepresentation());
	}

	/** @return communication statistics */
	public String getStats() {
		return commTracking.getStatsJson();
	}
}