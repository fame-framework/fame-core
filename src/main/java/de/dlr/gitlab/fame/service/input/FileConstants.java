// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.input;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

/** Holds constants relevant for reading FAME-protobuf messages from file
 * 
 * @author Christoph Schimeczek */
public class FileConstants {
	static final int HEADER_LENGTH = 30;
	static final String HEADER_V1 = "famecoreprotobufstreamfilev001";
	static final String HEADER_V2 = "fameprotobufstreamfilev002    ";
	static final int LATEST_HEADER_VERSION = 2;

	static final Map<Integer, String> headerMap = Map.of(1, HEADER_V1, 2, HEADER_V2);

	/** Number of bytes in data byte stream that encode the length of the next protobuf binary message */
	public static final int MESSAGE_LENGTH_BYTE_COUNT = 4;

	/** Returns version of the header encoded in the given test bytes, or 0 if no header is found.
	 * 
	 * @param test bytes to check for their encoded header version
	 * @return version of the header identified, or 0 if no header is found */
	public static int getHeaderVersion(byte[] test) {
		for (Entry<Integer, String> entry : headerMap.entrySet()) {
			byte[] headerInBytes = stringToBytes(entry.getValue());
			if (Arrays.equals(headerInBytes, test)) {
				return entry.getKey();
			}
		}
		return 0;
	}

	/** @return given string converted to UTF-8 bytes */
	private static byte[] stringToBytes(String toConvert) {
		return toConvert.getBytes(StandardCharsets.UTF_8);
	}

	/** @return latest file header in binary format */
	public static byte[] getLatestHeader() {
		return stringToBytes(headerMap.get(LATEST_HEADER_VERSION));
	}
}
