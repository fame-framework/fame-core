// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.Agent.WarmUpStatus;
import de.dlr.gitlab.fame.agent.PlannedAction;
import de.dlr.gitlab.fame.mpi.Constants;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.ScheduledTime;
import de.dlr.gitlab.fame.protobuf.Services.ScheduledTime.Builder;
import de.dlr.gitlab.fame.protobuf.Services.WarmUpMessage;
import de.dlr.gitlab.fame.service.scheduling.Schedule;
import de.dlr.gitlab.fame.service.scheduling.ScheduleSlot;
import de.dlr.gitlab.fame.service.scheduling.stats.RuntimeTracking;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Manages the activation of all {@link Agent Agents} on its process and the synchronisation of all agents across the simulation.
 * Keeps track of count of scheduled actions per agent and warns if agents "drop out of simulation" by not having actions
 * scheduled.
 * 
 * @author Christoph Schimeczek, Benjamin Fuchs */
public class Scheduler extends Service {
	static final String ERR_NO_MORE_ACTIONS = " has no remaining actions.";
	static final String ERR_ACTION_COUNT_NEGATIVE = "Negative count of scheduled actions for agent ";

	private static final Logger logger = LoggerFactory.getLogger(Scheduler.class);

	private final Schedule schedule;
	private final HashMap<Long, Integer> scheduledActionsByAgentId;
	private final ArrayList<Agent> agentsForWarmUp = new ArrayList<>();
	private final RuntimeTracking runtimeTracking;

	private boolean needsFurtherWarmUp = true;

	/** Constructs a {@link Scheduler}
	 * 
	 * @param mpi MpiManager to coordinate inter-process communication
	 * @param runtimeTracking to track performance of agent actions
	 * @param schedule to hold all local Agent actions */
	Scheduler(MpiManager mpi, RuntimeTracking runtimeTracking, Schedule schedule) {
		this(mpi, new HashMap<>(), runtimeTracking, schedule);
	}

	/** Constructs a {@link Scheduler}
	 * 
	 * @param mpi MpiManager to coordinate inter-process communication
	 * @param runtimeTracking to track performance of agent actions
	 * @param schedule to hold all local Agent actions
	 * @param scheduledActionsByAgentId to hold number of actions currently to be scheduled per agent */
	Scheduler(MpiManager mpi, HashMap<Long, Integer> scheduledActionsByAgentId, RuntimeTracking runtimeTracking,
			Schedule schedule) {
		super(mpi);
		this.scheduledActionsByAgentId = scheduledActionsByAgentId;
		this.runtimeTracking = runtimeTracking;
		this.schedule = schedule;
	}

	/** register an {@link Agent} at this {@link Scheduler} */
	void registerAgent(Agent agent) {
		agentsForWarmUp.add(agent);
	}

	/** Task this {@link Scheduler} to call the specified {@link Agent} when the given {@link PlannedAction} is due
	 * 
	 * @param agent that schedules the given Action
	 * @param action to be scheduled */
	void addActionAt(Agent agent, PlannedAction action) {
		schedule.addSingleActionAt(agent, action.getTimeStamp(), action.getSchedulingReason());
		addActionToRegister(agent.getId());
	}

	/** Increase action counter of the specified {@link Agent} by ID */
	private void addActionToRegister(long agentId) {
		scheduledActionsByAgentId.compute(agentId, (__, v) -> (v == null) ? 1 : v + 1);
	}

	/** Steps to next entry in {@link #schedule}, updates {@link #currentTime}, performs all actions scheduled for currentTime,
	 * removes entry from {@link #schedule} */
	void scheduleNext() {
		ScheduleSlot scheduleSlot = schedule.getNextScheduledEntry();
		for (Agent agent : scheduleSlot.getScheduleAgents()) {
			removeActionFromRegister(agent.getId());
			runtimeTracking.startActionsForAgent(agent.getId());
			agent.executeActions(scheduleSlot.getReasons(agent));
			runtimeTracking.endCurrentActions();
			checkHasActionsRemaining(agent);
		}
		runtimeTracking.endTick();
	}

	/** Decrease action counter of the specified {@link Agent} by ID */
	private void removeActionFromRegister(long agentId) {
		int newScheduledActionCount = getNumOfScheduledActions(agentId) - 1;
		if (newScheduledActionCount < 0) {
			logger.error(ERR_ACTION_COUNT_NEGATIVE + agentId);
			newScheduledActionCount = 0;
		}
		scheduledActionsByAgentId.put(agentId, newScheduledActionCount);
	}

	/** @return number of pending actions for specified {@link Agent} by ID */
	private int getNumOfScheduledActions(long agentId) {
		return scheduledActionsByAgentId.getOrDefault(agentId, 0);
	}

	/** logs error if Agent runs out of actions */
	private void checkHasActionsRemaining(Agent agent) {
		if (getNumOfScheduledActions(agent.getId()) <= 0) {
			logger.error(agent + ERR_NO_MORE_ACTIONS);
		}
	}

	/** Returns the {@link TimeStamp} of the next timeStep - call this only after synchronisation of all {@link Scheduler
	 * schedulers}
	 * 
	 * @return {@link TimeStamp} of the next timeStep */
	TimeStamp getNextTimeStep() {
		return schedule.getNextTimeInSchedule();
	}

	/** Called by {@link Simulator}; synchronise with {@link Scheduler schedulers} from other processes, updates next higher entry
	 * in schedule */
	void sychronise() {
		TimeStamp nextScheduledTime = schedule.getNextTimeInSchedule();
		Bundle bundle = reportNextScheduledTimeToRoot(nextScheduledTime);
		bundle = whenRootThenBundleNextTime(bundle);
		nextScheduledTime = receiveNextScheduledTimeFromRoot(bundle);
		schedule.insertTimeWhenNecessary(nextScheduledTime);
	}

	/** Sends next scheduled time to root
	 * 
	 * @return {@link Bundle} of all (root) or own (other processes) scheduled next time(s) */
	private Bundle reportNextScheduledTimeToRoot(TimeStamp nextScheduledTime) {
		Builder scheduledTimeBuilder = ScheduledTime.newBuilder().setTimeStep(nextScheduledTime.getStep());
		Bundle bundle = super.createBundleForScheduledTime(scheduledTimeBuilder);
		return mpi.aggregateAt(bundle, Constants.ROOT, Tag.SCHEDULE);
	}

	/** Root defines next TimeStamp to be addressed by schedulers
	 * 
	 * @return {@link Bundle} of next TimeStamp closest to and higher than current time (root) or own next scheduled time (other
	 *         processes) */
	private Bundle whenRootThenBundleNextTime(Bundle bundle) {
		if (mpi.isRoot()) {
			long nextScheduledTimeStep = findLowestNextScheduledTime(bundle.getMessagesList());
			bundle = super.createBundleForScheduledTime(ScheduledTime.newBuilder().setTimeStep(nextScheduledTimeStep));
		}
		return bundle;
	}

	/** Returns the closest {@link TimeStamp} later than {@link #currentTime} from a list of {@link MpiMessage}s
	 * 
	 * @return the lowest next scheduled time step from a list of {@link ScheduledTime} messages */
	private long findLowestNextScheduledTime(List<MpiMessage> messages) {
		long nextScheduledTime = Long.MAX_VALUE;
		for (MpiMessage message : messages) {
			long time = message.getScheduledTime().getTimeStep();
			nextScheduledTime = Math.min(nextScheduledTime, time);
		}
		return nextScheduledTime;
	}

	/** Receive next scheduled time broadcasted from root
	 * 
	 * @return the next scheduled time higher than and closest to the current time from any of the schedulers */
	private TimeStamp receiveNextScheduledTimeFromRoot(Bundle bundle) {
		bundle = mpi.broadcast(bundle, Constants.ROOT);
		ScheduledTime scheduledTimeMessage = bundle.getMessages(0).getScheduledTime();
		return new TimeStamp(scheduledTimeMessage.getTimeStep());
	}

	/** Returns true if any process has remaining tasks to be scheduled up to final time of the simulation
	 * 
	 * @return true if any process has remaining tasks to be scheduled up to final time of the simulation */
	boolean hasTasksRemaining() {
		return schedule.hasTasksRemaining();
	}

	/** Returns current simulation time
	 * 
	 * @return current simulation time */
	public TimeStamp getCurrentTime() {
		return schedule.getCurrentTime();
	}

	/** Returns true if any process needs further warm-up for its local agents
	 * 
	 * @return true, if any process needs further warm-up for its local agents */
	public boolean needsFurtherWarmUp() {
		return needsFurtherWarmUp;
	}

	/** Initialises schedules of all initially registered agents */
	public void initialiseSchedules() {
		TimeStamp initialTime = schedule.getInitialTime();
		for (Agent agent : agentsForWarmUp) {
			agent.initialiseActions(initialTime);
		}
	}

	/** Executes warm-up for all local agents, if necessary
	 * 
	 * @return number of Agents that actually performed a warm-up in this warm-up tick */
	public int executeWarmUp() {
		TimeStamp initialTime = schedule.getInitialTime();
		int numberOfActualWarmUps = 0;
		for (int agentIndex = (agentsForWarmUp.size() - 1); agentIndex >= 0; agentIndex--) {
			WarmUpStatus warmUpStatus = agentsForWarmUp.get(agentIndex).executeWarmUp(initialTime);
			if (warmUpStatus == WarmUpStatus.COMPLETED || warmUpStatus == WarmUpStatus.UNNECESSARY) {
				agentsForWarmUp.remove(agentIndex);
			}
			if (warmUpStatus == WarmUpStatus.COMPLETED) {
				numberOfActualWarmUps++;
			}
		}
		return numberOfActualWarmUps;
	}

	/** Synchronises need for warm-up among all processes */
	public void sychroniseWarmUp() {
		WarmUpMessage.Builder warmUpBuilder = WarmUpMessage.newBuilder().setNeeded(agentsForWarmUp.size() > 0);
		Bundle bundle = super.createBundleForWarmUpMessage(warmUpBuilder);
		bundle = mpi.aggregateAt(bundle, Constants.ROOT, Tag.WARM_UP);
		if (mpi.isRoot()) {
			bundle = anyProcessNeedsWarmUp(bundle);
		}
		bundle = mpi.broadcast(bundle, Constants.ROOT);
		needsFurtherWarmUp = bundle.getMessages(0).getWarmUp().getNeeded();
	}

	/** @return a {@link Bundle} with {@link WarmUpMessage}, that equals true if any process needs another warm-up iteration */
	private Bundle anyProcessNeedsWarmUp(Bundle bundle) {
		boolean warmUpNeeded = false;
		for (MpiMessage message : bundle.getMessagesList()) {
			if (message.getWarmUp().getNeeded()) {
				warmUpNeeded = true;
			}
		}
		WarmUpMessage.Builder warmUpBuilder = WarmUpMessage.newBuilder().setNeeded(warmUpNeeded);
		return super.createBundleForWarmUpMessage(warmUpBuilder);
	}

	/** Returns the set of local {@link Agent agents} that haven't yet completed their warm-up phase
	 * 
	 * @return the set of local {@link Agent agents} that haven't yet completed their warm-up phase */
	public ArrayList<Agent> getRemainingAgentsForWarmUp() {
		return agentsForWarmUp;
	}

	/** @return runtime statistics */
	public String getStats() {
		return runtimeTracking.getStatsJson();
	}
}