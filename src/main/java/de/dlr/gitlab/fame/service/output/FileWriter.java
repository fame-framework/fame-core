// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.output;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.service.input.FileConstants;
import de.dlr.gitlab.fame.setup.Setup;

/** Writes binary data to an output file
 *
 * @author Christoph Schimeczek, Achraf A. El Ghazi */
public final class FileWriter {
	static final String ERR_WRITE_FAILED = "Could not write to output file!";
	static final String ERR_FILE_FAILED = "Could not access output file: ";
	static final String ERR_CLOSE_FAILED = "Could not close output file!";
	static final String ERR_MISSING_FILE_NAME = "Output file name is missing, empty or all whitespaces.";
	static final String ERR_FILE_IS_DIR = "Output file points to a directory - please add a file name.";
	static final String ERR_FOLDER_CREATE = "Could not create all folders to output file: ";
	static final String WARN_EMPTY_CONTENT = "Output FileWriter received no data to write - nothing written.";
	static final String WARN_FILE_ENDING = "Output file name does not end in '.pb' - proper ending attached.";
	static final String INFO_CREATE_FOLDER = "Output folder created: ";

	static final String OUT_FILE_ENDING = ".pb";
	static final String TIMESTAMP_SEPARATOR = ".";
	static final String TIMESTAMP_FORMAT = "yyyy-MM-dd_HH-mm-ss";

	static Logger logger = LoggerFactory.getLogger(FileWriter.class);

	private final FileOutputStream fileStream;

	/** Create a FileWriter based on the given Setup parameters
	 * 
	 * @param setup defining the output parameters */
	public FileWriter(Setup setup) {
		this(getFileOutputStream(fullFileName(setup)));
	}

	/** @return the full file name, including its path and name, an optional TimeStamp, and the file ending */
	static String fullFileName(Setup setup) {
		String outputFile = setup.getOutputFile();
		ensureFileNameNotEmpty(outputFile);
		Path outputFilePath = Paths.get(outputFile.trim());
		ensureNoDirectory(outputFilePath);
		String fileName = outputFilePath.getFileName().toString();
		fileName = appendFileEndingIfMissing(fileName);
		fileName = prependTimeStampIfRequested(fileName, setup.isAddTimeStamp());
		Path parent = outputFilePath.getParent();
		String folder = parent != null ? parent.toString() : "";
		return Paths.get(folder, fileName.toString()).toString();
	}

	/** @throws IllegalArgumentException if given file name is null or empty */
	private static void ensureFileNameNotEmpty(String fileName) {
		if (fileName == null || fileName.isBlank()) {
			throw new IllegalArgumentException(ERR_MISSING_FILE_NAME);
		}
	}

	/** @throws IllegalArgumentException if given Path points to a directory */
	private static void ensureNoDirectory(Path filePath) {
		if (Files.isDirectory(filePath)) {
			throw new IllegalArgumentException(ERR_FILE_IS_DIR);
		}
	}

	/** @return given fileName with an appended proper file ending if it was missing */
	private static String appendFileEndingIfMissing(String fileName) {
		if (!fileName.endsWith(OUT_FILE_ENDING)) {
			fileName = fileName + OUT_FILE_ENDING;
			logger.warn(WARN_FILE_ENDING);
		}
		return fileName;
	}

	private static String prependTimeStampIfRequested(String fileName, boolean addTimeStamp) {
		return addTimeStamp ? getCurrentTimeStamp() + TIMESTAMP_SEPARATOR + fileName : fileName;
	}

	/** @return {@link String} representing the wall-time in human readable format */
	private static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(TIMESTAMP_FORMAT);
		return sdfDate.format(new Date());
	}

	/** Creates a FileOutputStream for writing data to. If the file already exists, overwrites it. If folder(s) do not yet exist,
	 * creates them.
	 * 
	 * @param filePath path to output file
	 * @return created file output stream */
	private static FileOutputStream getFileOutputStream(String filePath) {
		createParentFolders(filePath);
		try {
			return new FileOutputStream(filePath);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(ERR_FILE_FAILED + filePath);
		}
	}

	/** Creates all folders - if they do not yet exist - to store the output file in */
	private static void createParentFolders(String file) {
		Path folderPath = Paths.get(file).toAbsolutePath().getParent();
		if (!Files.exists(folderPath)) {
			try {
				Path dir = Files.createDirectories(folderPath);
				logger.info(INFO_CREATE_FOLDER + dir.toAbsolutePath());
			} catch (IOException e) {
				throw new RuntimeException(ERR_FOLDER_CREATE + folderPath);
			}
		}
	}

	/** Creates a FileWriter for the given fileStream and writes file header
	 * 
	 * @param fileStream to write to */
	FileWriter(FileOutputStream fileStream) {
		this.fileStream = fileStream;
		try {
			fileStream.write(FileConstants.getLatestHeader());
		} catch (IOException e) {
			throw new RuntimeException(ERR_WRITE_FAILED);
		}
	}

	/** Write an array of bytes to {@link #fileStream}, preceded by the length of the bytes */
	void write(byte[] bytes) {
		if (bytes == null || bytes.length == 0) {
			logger.warn(WARN_EMPTY_CONTENT);
		} else {
			try {
				fileStream.write(byteRepresentationOf(bytes.length));
				fileStream.write(bytes);
			} catch (IOException e) {
				throw new RuntimeException(ERR_WRITE_FAILED, e);
			}
		}
	}

	/** @return byte[] representation of given int in BigEndian */
	private byte[] byteRepresentationOf(int number) {
		ByteBuffer buffer = ByteBuffer.allocate(FileConstants.MESSAGE_LENGTH_BYTE_COUNT);
		buffer.putInt(number);
		return buffer.array();
	}

	/** Closes the associated file stream */
	void close() {
		try {
			fileStream.close();
		} catch (IOException e) {
			throw new RuntimeException(ERR_CLOSE_FAILED, e);
		}
	}
}
