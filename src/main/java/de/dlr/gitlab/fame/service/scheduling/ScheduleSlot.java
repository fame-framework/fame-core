// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.scheduling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import de.dlr.gitlab.fame.agent.Agent;

/** Associates {@link Agent}s with scheduling reasons
 * 
 * @author Christoph Schimeczek */
public class ScheduleSlot {
	private static final List<Enum<?>> EMPTY_REASONS = Collections.unmodifiableList(new ArrayList<>());
	private final HashMap<Agent, List<Enum<?>>> entries = new HashMap<>();

	/** Add a new entry in this {@link ScheduleSlot}
	 * 
	 * @param agent that requests scheduling in that slot
	 * @param reason for which the {@link Agent} wants to be scheduled */
	public void addEntry(Agent agent, Enum<?> reason) {
		entries.putIfAbsent(agent, new ArrayList<Enum<?>>());
		entries.get(agent).add(reason);
	}

	/** Get scheduling reasons stored for given Agent
	 * 
	 * @param agent that is scheduled
	 * @return List of reasons for scheduling - empty if no reasons are stored for this agent */
	public List<Enum<?>> getReasons(Agent agent) {
		return entries.getOrDefault(agent, EMPTY_REASONS);
	}

	/** @return List of all Agents that registered scheduling reasons - empty if entry was added yet */
	public Set<Agent> getScheduleAgents() {
		return entries.keySet();
	}
}
