// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service.output;

import java.util.List;

/** A data output buffer of a single agent that buffers only data for simple columns and ingores complex column data
 * 
 * @author Christoph Schimeczek */
public class SimpleColumnBuffer extends AllColumnBuffer {

	/** Create new {@link SimpleColumnBuffer}
	 * 
	 * @param className name of the class that this buffer is associated with
	 * @param agentId unique ID of the agent that this buffer holds data for
	 * @param columnList names of output columns available to the corresponding agent */
	public SimpleColumnBuffer(String className, long agentId, List<Enum<?>> columnList) {
		super(className, agentId, columnList);
	}

	@Override
	public void store(ComplexIndex<?> complexField, Object value) {}
}
