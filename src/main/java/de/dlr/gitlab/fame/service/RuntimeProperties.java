// SPDX-FileCopyrightText: 2025 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.io.IOException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.protobuf.Execution.ExecutionData.VersionData;

/** Extract version properties of runtime environment and FAME components
 * 
 * @author Christoph Schimeczek */
public class RuntimeProperties {
	static final String ERR_VERSION_NOT_FOUND = "Could not find FAME version properties - versions not logged to output.";
	static final String ERR_VERSION_MISFORMAT = "Illegal character in FAME version properties - versions not logged to output.";
	static final String ERR_IO = "Unknown error when reading FAME version properties - versions not logged to output.";

	private static final Logger logger = LoggerFactory.getLogger(RuntimeProperties.class);

	private final Properties properties;

	static final String MISSING_PLACEHOLDER = "n/a";

	/** Create a new RuntimeProperties object
	 * 
	 * @param properties to be store FAME properties in */
	RuntimeProperties(Properties properties) {
		this.properties = properties;
		try {
			var inputStream = getClass().getClassLoader().getResourceAsStream("fame.properties");
			properties.load(inputStream);
		} catch (NullPointerException e) {
			logger.error(ERR_VERSION_NOT_FOUND);
		} catch (IOException e) {
			logger.error(ERR_IO);
		} catch (IllegalArgumentException e) {
			logger.error(ERR_VERSION_MISFORMAT);
		}
		for (String name : System.getProperties().stringPropertyNames()) {
			properties.setProperty(name, System.getProperty(name));
		}
	}

	/** Create a new RuntimeProperties object */
	RuntimeProperties() {
		this(new Properties());
	}

	/** Get value for property with given name
	 * 
	 * @param name of the property requested
	 * @return value of the property, or "n/a" if property is not set */
	String getProperty(String name) {
		String value = properties.getProperty(name);
		return value != null ? value : MISSING_PLACEHOLDER;
	}

	/** @return version information about runtime environment and FAME components */
	VersionData getVersions() {
		return VersionData.newBuilder()
				.setOs(getProperty("os.name"))
				.setJvm(getProperty("java.vm.version"))
				.setFameCore(getProperty("fame.core.version"))
				.setFameProtobuf(getProperty("fame.protobuf.version"))
				.build();
	}
}
