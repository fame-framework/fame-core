// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.util.Random;
import de.dlr.gitlab.fame.agent.ActionActivator;
import de.dlr.gitlab.fame.agent.ActionBuilder;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.PlannedAction;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.service.output.OutputBuffer;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Holds services of this computation node
 *
 * @author Christoph Schimeczek */
public class LocalServices {
	private final Scheduler scheduler;
	private final PostOffice postOffice;
	private final OutputManager outputManager;
	private final Notary notary;
	private final TimeSeriesProvider timeSeriesProvider;
	private final RandomNumberGeneratorProvider rngProvider;
	private final AddressBook addressBook;
	private static final ActionActivator actionActivator = new ActionActivator();

	/** Create new {@link LocalServices}
	 * 
	 * @param scheduler to schedule {@link Agent}s
	 * @param postOffice to organise message exchange
	 * @param outputManager to organise output writing
	 * @param notary to organise {@link Contract}s
	 * @param timeSeriesProvider that provides data of {@link TimeSeries}
	 * @param rngProvider providing random number generators
	 * @param addressBook to locate {@link Agent}s across processes */
	public LocalServices(Scheduler scheduler, PostOffice postOffice, OutputManager outputManager, Notary notary,
			TimeSeriesProvider timeSeriesProvider, RandomNumberGeneratorProvider rngProvider, AddressBook addressBook) {
		this.scheduler = scheduler;
		this.postOffice = postOffice;
		this.outputManager = outputManager;
		this.notary = notary;
		this.timeSeriesProvider = timeSeriesProvider;
		this.rngProvider = rngProvider;
		this.addressBook = addressBook;
	}

	/** @return local {@link PostOffice} */
	public PostOffice getPostOffice() {
		return postOffice;
	}

	/** @return local {@link TimeSeriesProvider} */
	public TimeSeriesProvider getTimeSeriesProvider() {
		return timeSeriesProvider;
	}

	/** Register agent at {@link Scheduler}, {@link PostOffice}, {@link OutputManager} and {@link Notary}
	 * 
	 * @param agent to register at local services
	 * @return {@link OutputBuffer} for that agent */
	public OutputBuffer registerAgent(Agent agent) {
		postOffice.registerAgent(agent);
		addressBook.registerAgent(agent);
		scheduler.registerAgent(agent);
		notary.registerLocalAgent(agent);
		return outputManager.registerAgent(agent);
	}

	/** @return Current simulation time */
	public TimeStamp getCurrentTime() {
		return scheduler.getCurrentTime();
	}

	/** Schedule an upcoming PlannedAction for the given Agent
	 * 
	 * @param agent that plans the given Action
	 * @param plannedAction to be scheduled */
	public void addActionAt(Agent agent, PlannedAction plannedAction) {
		scheduler.addActionAt(agent, plannedAction);
	}

	/** Send a message
	 * 
	 * @param senderId ID of sending agent
	 * @param receiverId ID of receiving Agent
	 * @param portable 0..1 {@link Portable} to include in message
	 * @param dataItems 0..N {@link DataItem}s to include in message */
	public void sendMessage(long senderId, long receiverId, Portable portable, DataItem... dataItems) {
		postOffice.sendMessage(senderId, receiverId, portable, dataItems);
	}

	/** Get a new seeded {@link Random} number generator. Seed is calculated from the Agent's ID and the simulation's random seed.
	 * 
	 * @param agentId ID of the agent, used to calculate seed
	 * @return an appropriately seeded random number generator */
	public Random getNewRandomNumberGeneratorForAgent(Long agentId) {
		return rngProvider.getNewRandomNumberGeneratorForAgent(agentId);
	}

	/** Register the given ActionBuilder for the specified Agent at the node's ActionActivator
	 * 
	 * @param agent the {@link Agent} that has actions to activate
	 * @param builder the {@link ActionBuilder} describing an Action ready for activation */
	public void registerActionBuilder(Agent agent, ActionBuilder builder) {
		actionActivator.registerBuilder(agent, builder);
	}

	/** activates all actions of all Agents */
	void armActions() {
		actionActivator.armAllActions();
	}
}