// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.message.MessageBuilder;
import de.dlr.gitlab.fame.communication.stats.CommTracking;
import de.dlr.gitlab.fame.communication.stats.FullTracking;
import de.dlr.gitlab.fame.communication.stats.NoTracking;
import de.dlr.gitlab.fame.communication.transfer.Composer;
import de.dlr.gitlab.fame.communication.transfer.PortableUidManager;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.service.output.FileWriter;
import de.dlr.gitlab.fame.service.output.OutputSerializer;
import de.dlr.gitlab.fame.service.scheduling.Schedule;
import de.dlr.gitlab.fame.service.scheduling.stats.FullRuntimeTracking;
import de.dlr.gitlab.fame.service.scheduling.stats.NoRuntimeTracking;
import de.dlr.gitlab.fame.service.scheduling.stats.RuntimeTracking;
import de.dlr.gitlab.fame.setup.Setup;

/** Runs the simulation ticks and coordinates all {@link Service Services}
 * 
 * @author Christoph Schimeczek */
public class Simulator {
	private static final int MAX_WARM_UP_TICKS = 100;

	static final String ERR_WARM_UP_MISSING = "Warm-up not completed for ";
	static final String ERR_WARM_UP_INCOMPLETE = "Simulation warm-up not completed after step " + MAX_WARM_UP_TICKS;
	static final String COMPLETION_MESSAGE = "%s:: Simulation completed after executing %,d ticks in %,.2f seconds.";
	static final String DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";

	private static final Logger logger = LoggerFactory.getLogger(Simulator.class);
	private final boolean processIsRoot;
	private final OutputManager outputManager;
	private final Scheduler scheduler;
	private final PostOffice postOffice;
	private final AddressBook addressBook;

	/** Sets up a simulation
	 * 
	 * @param mpiManager MpiManager to coordinate inter-process communication
	 * @param inputManager with all required input data
	 * @param setup configuration options for the simulation */
	public Simulator(MpiManager mpiManager, InputManager inputManager, Setup setup) {
		processIsRoot = mpiManager.isRoot();
		var javaPackageNames = inputManager.getPackageNames();

		boolean isOutputProcess = mpiManager.isInputOutputProcess();
		OutputSerializer outputSerialiser = isOutputProcess ? new OutputSerializer(new FileWriter(setup)) : null;
		outputManager = new OutputManager(mpiManager, outputSerialiser);

		RuntimeTracking runtimeTracking = setup.isTrackRuntime() ? new FullRuntimeTracking() : new NoRuntimeTracking();
		SimulationParam config = inputManager.getConfig();
		scheduler = new Scheduler(mpiManager, runtimeTracking, new Schedule(config.getStartTime(), config.getStopTime()));

		List<String> portablePackageNames = javaPackageNames.getPortablePackages();
		TimeSeriesProvider timeSeriesProvider = new TimeSeriesProvider(inputManager.getTimeSeries());
		PortableUidManager portableUidManager = new PortableUidManager(portablePackageNames);
		Composer composer = new Composer(portableUidManager, timeSeriesProvider, setup.getPortableDepth());
		MessageBuilder messageBuilder = new MessageBuilder(composer, javaPackageNames.getDataItemPackages());
		CommTracking commTracking = setup.isTrackExchange() ? new FullTracking() : new NoTracking();
		addressBook = new AddressBook(mpiManager);
		postOffice = new PostOffice(mpiManager, messageBuilder, commTracking, addressBook);

		List<String> agentPackages = javaPackageNames.getAgentPackages();
		outputManager.setOutputParameters(setup, agentPackages);
		outputManager.writeInputData(inputManager.getInputDataStorage());
		outputManager.logEnvironmentData();

		long randomSeed = inputManager.getConfig().getRandomSeed();
		RandomNumberGeneratorProvider rngProvider = new RandomNumberGeneratorProvider(randomSeed);

		Notary notary = new Notary(mpiManager, agentPackages, inputManager.getAgentConfigs());
		LocalServices localServices = new LocalServices(scheduler, postOffice, outputManager, notary, timeSeriesProvider,
				rngProvider, addressBook);

		AgentBuilder agentBuilder = new AgentBuilder(mpiManager, agentPackages, timeSeriesProvider.getMapOfTimeSeries(),
				localServices);
		agentBuilder.createAgentsFromConfig(inputManager.getAgentConfigs());
		localServices.armActions();

		notary.setInitialContracts(inputManager.getContractPrototypes());
	}

	/** executes initial steps of agents, until all agents report "ready to run" */
	public void warmUp() {
		addressBook.synchroniseAddressBookUpdates();
		scheduler.initialiseSchedules();

		int warmUpTick = 0;
		int numberOfAgentsWithWarmUp = 0;
		while (scheduler.needsFurtherWarmUp()) {
			postOffice.deliverMessages();
			numberOfAgentsWithWarmUp = scheduler.executeWarmUp();
			scheduler.sychroniseWarmUp();

			warmUpTick++;
			if (warmUpTick > MAX_WARM_UP_TICKS) {
				for (Agent agent : scheduler.getRemainingAgentsForWarmUp()) {
					logger.error(Logging.LOGGER_FATAL, ERR_WARM_UP_MISSING + agent);
				}
				throw new RuntimeException(ERR_WARM_UP_INCOMPLETE);
			}
		}
		scheduler.sychronise();
		if (processIsRoot) {
			if (warmUpTick > 1 || numberOfAgentsWithWarmUp > 0) {
				System.out.println("Warm-up completed after " + warmUpTick + " ticks.");
			}
		}
	}

	/** executes the simulation */
	public void run() {
		long startTime = System.currentTimeMillis();

		long tick = 0;
		proceedWithSchedule();
		while (scheduler.hasTasksRemaining()) {
			tick++;
			addressBook.synchroniseAddressBookUpdates();
			postOffice.deliverMessages();
			outputManager.tickBuffersAndWriteOutData(tick, scheduler.getCurrentTime());
			proceedWithSchedule();
		}
		outputManager.flushAll(scheduler.getCurrentTime());

		long stopTime = System.currentTimeMillis();
		long walltimeInMillis = stopTime - startTime;
		outputManager.logRuntimeData(formatDate(walltimeInMillis), walltimeInMillis, tick);
		outputManager.close();
		if (processIsRoot) {
			printFinalMessages(walltimeInMillis, tick);
		}
	}

	/** schedules next actions & synchronises schedulers, so that each scheduler knows the time of the next simulation step. */
	private void proceedWithSchedule() {
		scheduler.scheduleNext();
		scheduler.sychronise();
	}

	/** Prints final messages on simulation completion and simulation statistics to console */
	private void printFinalMessages(long walltimeInMillis, long ticks) {
		System.out.println(getCompletionMessage(walltimeInMillis, ticks));
		String postOfficeStats = postOffice.getStats();
		if (postOfficeStats != null && !postOfficeStats.isEmpty()) {
			System.out.println(postOfficeStats);
		}
		String schedulerStats = scheduler.getStats();
		if (schedulerStats != null && !schedulerStats.isEmpty()) {
			System.out.println(schedulerStats);
		}
	}

	/** returns compiled completion message */
	static String getCompletionMessage(long walltimeInMillis, long ticks) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);
		String dateString = dateFormat.format(new Date());
		return String.format(Locale.ENGLISH, COMPLETION_MESSAGE, dateString, ticks, walltimeInMillis / 1000.);
	}

	static String formatDate(long timeInMillies) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return "UTC " + dateFormat.format(new Date(timeInMillies));
	}
}