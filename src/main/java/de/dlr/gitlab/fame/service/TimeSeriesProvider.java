// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import java.util.Map;
import de.dlr.gitlab.fame.data.TimeSeries;

/** Provides {@link TimeSeries} on this process to other components
 * 
 * @author Christoph Schimeczek, Marc Deissenroth */
public class TimeSeriesProvider {
	static final String MISSING_SERIES = "TimeSeries not found for ID: ";

	private final Map<Integer, TimeSeries> mapOfSeries;

	/** An Exception hinting that a {@link TimeSeries} could not be provided
	 * 
	 * @author Christoph Schimeczek */
	public static class MissingSeriesException extends Exception {
		private static final long serialVersionUID = 1L;

		/** Create new {@link MissingSeriesException}
		 * 
		 * @param message associated with this Exception */
		public MissingSeriesException(String message) {
			super(message);
		}
	}

	/** Construct a {@link TimeSeriesProvider}
	 * 
	 * @param mapOfSeries assigning a unique integer to each TimeSeries */
	public TimeSeriesProvider(Map<Integer, TimeSeries> mapOfSeries) {
		this.mapOfSeries = mapOfSeries;
	}

	/** Gets the {@link TimeSeries} with the given ID
	 * 
	 * @param timeSeriesId id of the TimeSeries that is to be returned
	 * @return {@link TimeSeries} with given ID
	 * @throws MissingSeriesException if no TimeSeries was assigned the given ID */
	public TimeSeries getSeriesById(int timeSeriesId) throws MissingSeriesException {
		ensureSeriesExists(timeSeriesId);
		return mapOfSeries.get(timeSeriesId);
	}

	/** @throws MissingSeriesException if specified TimeSeries by ID is not listed in {@link #mapOfSeries} */
	private void ensureSeriesExists(int timeSeriesId) throws MissingSeriesException {
		if (!mapOfSeries.containsKey(timeSeriesId)) {
			throw new MissingSeriesException(MISSING_SERIES + timeSeriesId);
		}
	}

	/** @return Map of all TimeSeries and their corresponding IDs */
	Map<Integer, TimeSeries> getMapOfTimeSeries() {
		return mapOfSeries;
	}
}