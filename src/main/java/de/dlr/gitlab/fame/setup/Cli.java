// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.setup;

import java.util.Collections;
import java.util.List;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;

/** Setup constants
 * 
 * @author Christoph Schimeczek */
public final class Cli {
	/** Default values for all CLI parameters */
	public static final class Defaults {
		public static final int PORTABLE_DEPTH = 100;
		public static final String[] MPI_ARGS = new String[0];
		public static final MpiMode MPI_MODE = MpiMode.SINGLE_CORE;
		public static final String INPUT_FILE = "input.pb";
		public static final String OUTPUT_FILE = "./output.pb";
		public static final int OUTPUT_INTERVAL = 100;
		public static final boolean ADD_TIMESTAMP = false;
		public static final boolean OUTPUT_COMPLEX = false;
		public static final List<String> OUTPUT_AGENTS = Collections.emptyList();
		public static final boolean TRACK_EXCHANGE = false;
		public static final boolean TRACK_RUNTIME = false;
	}

	/** Help texts for all CLI parameters */
	public static final class Help {
		public static final String HELP = "show a help message";
		public static final String MPI_ARGS = "3 positional arguments required in PARALLEL mpi-mode only; provided by mpi starter - do not specify manually!";
		public static final String MPI_MODE = "select execution mode: [SINGLE_CORE, PARALLEL]; default: ${DEFAULT-VALUE}";
		public static final String INPUT_FILE = "path to input protobuf file; default: ${DEFAULT-VALUE}";
		public static final String PORTABLE_DEPTH = "maximum number of inner elements for Portables; default: ${DEFAULT-VALUE}";
		public static final String OUTPUT_FILE = "(path to) the output file; file and path will be created if they do not yet exist; file gets overridden if exting; adds '.pb' file ending if missing; default: ${DEFAULT-VALUE}";
		public static final String ADD_TIMESTAMP = "enable to attach a timestamp to the output file name; default: ${DEFAULT-VALUE}";
		public static final String OUTPUT_COMPLEX = "enable to also write complex output to file; default: ${DEFAULT-VALUE}";
		public static final String OUTPUT_INTERVAL = "tick interval after which stored outputs are flushed to file; default: ${DEFAULT-VALUE}";
		public static final String OUTPUT_AGENTS = "Comma-separated list of Agent classes whose outputs will be written to file - leave out to activate output of all agent classes; do not add spaces between list items; default: ${DEFAULT-VALUE}";
		public static final String TRACK_EXCHANGE = "enable to track exchange of message between agents; mpi-mode=SingleCore only; default: ${DEFAULT-VALUE}";
		public static final String TRACK_RUNTIME = "enable to track runtime of agent actions; mpi-mode=SingleCore only; default: ${DEFAULT-VALUE}";
	}
}