// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.setup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import picocli.CommandLine.IDefaultValueProvider;
import picocli.CommandLine.Model.ArgSpec;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Model.OptionSpec;
import picocli.CommandLine.Model.PositionalParamSpec;

/** Provides default values for command line parsing from a given file in YAML format. Some code copied from
 * <a href="https://picocli.info/">picocli's</a> class PropertiesDefaultProvider
 * 
 * @author Christoph Schimeczek */
public class YamlDefaultsProvider implements IDefaultValueProvider {
	static final String ERR_CANNOT_READ_FILE = "Could not access command-line defaults in file: ";
	static final String ERR_DISALLOWED_COMPLEX = "Expected primitive or list, but found complex value: ";
	static final String WARN_NO_CHILDREN = "Could not access defaults for subcommand: ";
	static final String INFO_NO_FILE = "No command-line defaults file found.";
	static final String INFO_NO_DEFAULT = "No default provided in file for parameter or sub-command: ";

	static Logger logger = LoggerFactory.getLogger(YamlDefaultsProvider.class);

	private final Map<String, Object> defaults;

	/** Creates a new {@link YamlDefaultsProvider} to read the given file and extract default values
	 * 
	 * @param file to read the command-line defaults from */
	public YamlDefaultsProvider(File file) {
		if (file != null && file.exists()) {
			defaults = (new Yaml()).load(getReader(file));
		} else {
			defaults = null;
			logger.info(INFO_NO_FILE);
		}
	}

	/** Creates an returns a new FileReader for the given file
	 * 
	 * @param file to read
	 * @return a new FileReader for the given file
	 * @throws RuntimeException if file cannot be opened for any reason */
	private FileReader getReader(File file) {
		try {
			return new FileReader(file);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(ERR_CANNOT_READ_FILE + file.toString());
		}
	}

	/** Creates a new {@link YamlDefaultsProvider} using the provided default value mappings
	 * 
	 * @param defaults mapping of parameter names to default values */
	YamlDefaultsProvider(Map<String, Object> defaults) {
		this.defaults = defaults;
	}

	/** Based on <a href="https://picocli.info/">picocli's</a> class PropertiesDefaultProvider */
	@Override
	public String defaultValue(ArgSpec argSpec) throws Exception {
		if (defaults == null || defaults.isEmpty()) {
			return null;
		}
		return argSpec.isOption() ? optionDefaultValue((OptionSpec) argSpec)
				: positionalDefaultValue((PositionalParamSpec) argSpec);
	}

	/** Copied from <a href="https://picocli.info/">picocli's</a> class PropertiesDefaultProvider */
	private String optionDefaultValue(OptionSpec option) {
		String result = getValue(option.descriptionKey(), option.command());
		result = result != null ? result : getValue(stripPrefix(option.longestName()), option.command());
		return result;
	}

	/** Copied from <a href="https://picocli.info/">picocli's</a> class PropertiesDefaultProvider */
	private static String stripPrefix(String prefixed) {
		for (int i = 0; i < prefixed.length(); i++) {
			if (Character.isJavaIdentifierPart(prefixed.charAt(i))) {
				return prefixed.substring(i);
			}
		}
		return prefixed;
	}

	/** Copied from <a href="https://picocli.info/">picocli's</a> class PropertiesDefaultProvider */
	private String positionalDefaultValue(PositionalParamSpec positional) {
		String result = getValue(positional.descriptionKey(), positional.command());
		result = result != null ? result : getValue(positional.paramLabel(), positional.command());
		return result;
	}

	/** Based on <a href="https://picocli.info/">picocli's</a> class PropertiesDefaultProvider */
	private String getValue(String key, CommandSpec spec) {
		if (key == null || key.isEmpty()) {
			return null;
		}

		String result = null;
		if (spec != null) {
			String cmd = spec.qualifiedName(".").replaceAll("<main class>", "");
			String qualifiedName = cmd == "" ? cmd : cmd + "." + key;
			if (!qualifiedName.isBlank()) {
				result = getDefault(defaults, qualifiedName.split("\\."));
			}
		}
		if (result != null) {
			return result;
		}
		return getDefault(defaults, key);
	}

	/** @return the default value (if existing) for the command-qualified name of a command-line parameter, or null if no parameter
	 *         with that address is found */
	@SuppressWarnings("unchecked")
	private String getDefault(Map<String, Object> defaults, String... address) {
		if (defaults.containsKey(address[0])) {
			Object value = defaults.get(address[0]);
			if (address.length > 1) {
				if (value instanceof Map) {
					return getDefault((Map<String, Object>) value, Arrays.copyOfRange(address, 1, address.length));
				} else {
					logger.warn(WARN_NO_CHILDREN + address[0]);
					return null;
				}
			} else {
				return unpack(value);
			}
		} else {
			logger.debug(INFO_NO_DEFAULT + address[0]);
			return null;
		}
	}

	/** @return String representation of given value if value is not null, null otherwise */
	private String unpack(Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof List) {
			return stringRepresentationOfList((List<?>) value);
		}
		if (value instanceof Map) {
			logger.error(ERR_DISALLOWED_COMPLEX + value);
			return null;
		}
		return value.toString();
	}

	/** @return string representation of given list of objects that has no brackets at beginning or end, as well as no spaces
	 *         between item */
	private String stringRepresentationOfList(List<?> valueList) {
		StringBuilder builder = new StringBuilder();
		for (Object item : valueList) {
			builder.append(item.toString()).append(",");
		}
		String repr = builder.toString();
		return repr.substring(0, repr.length() - 1);
	}
}
