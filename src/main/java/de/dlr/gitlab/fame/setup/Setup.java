// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.setup;

import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.setup.Cli.Defaults;
import de.dlr.gitlab.fame.setup.Cli.Help;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/** Unpacks command line arguments and associates them with a parameter in this class. If a parameter is not given, its default is
 * used. Standard defaults from {@link Cli} are used but can be overridden if specified in the fameSetup.yaml file.
 * 
 * @author Christoph Schimeczek */
public class Setup implements Runnable {
	@Option(names = {"-h", "--help"}, usageHelp = true, description = Help.HELP) private boolean helpRequested = false;

	@Parameters(paramLabel = "mpi-args", description = Help.MPI_ARGS, hidden = true) String[] mpiArgs = Defaults.MPI_ARGS;

	@Option(names = {"-m", "--mpi-mode"}, description = Help.MPI_MODE) private MpiMode mpiMode = Defaults.MPI_MODE;

	@Option(names = {"-f", "--input-file"}, description = Help.INPUT_FILE) private String inputFile = Defaults.INPUT_FILE;

	@Option(names = {"-d", "--portable-depth"}, description = Help.PORTABLE_DEPTH,
			hidden = true) private int portableDepth = Defaults.PORTABLE_DEPTH;

	@Option(names = {"-o", "--output-file"},
			description = Help.OUTPUT_FILE) private String outputFile = Defaults.OUTPUT_FILE;

	@Option(names = {"-t", "--add-timestamp"},
			description = Help.ADD_TIMESTAMP) private boolean addTimeStamp = Defaults.ADD_TIMESTAMP;

	@Option(names = {"-c", "--output-complex"},
			description = Help.OUTPUT_COMPLEX) private boolean outputComplex = Defaults.OUTPUT_COMPLEX;

	@Option(names = {"-n", "--output-interval"}, description = Help.OUTPUT_INTERVAL,
			hidden = true) private int outputInterval = Defaults.OUTPUT_INTERVAL;

	@Option(names = {"-a", "--output-agents"}, split = ",",
			description = Help.OUTPUT_AGENTS) private List<String> outputAgents = Defaults.OUTPUT_AGENTS;

	@Option(names = {"-x", "--track-exchange"},
			description = Help.TRACK_EXCHANGE) private boolean trackExchange = Defaults.TRACK_EXCHANGE;

	@Option(names = {"-r", "--track-runtime"},
			description = Help.TRACK_RUNTIME) private boolean trackRuntime = Defaults.TRACK_RUNTIME;

	static Logger logger = LoggerFactory.getLogger(Setup.class);
	static final String WARN_PARALLEL_DISALLOWED = "Tracking option disabled, as it is only allowed in mode=SINGLE_CORE: ";

	/** Constructs a new {@link Setup} with default parameter values */
	public Setup() {}

	Setup(MpiMode mpiMode, boolean trackExchange, boolean trackRuntime) {
		this.mpiMode = mpiMode;
		this.trackExchange = trackExchange;
		this.trackRuntime = trackRuntime;
	}

	@Override
	public void run() {}

	public boolean isHelpRequested() {
		return helpRequested;
	}

	public String[] getMpiArgs() {
		return mpiArgs;
	}

	public MpiMode getMpiMode() {
		return mpiMode;
	}

	public String getInputFile() {
		return inputFile;
	}

	public int getPortableDepth() {
		return portableDepth;
	}

	public String getOutputFile() {
		return outputFile;
	}

	public boolean isAddTimeStamp() {
		return addTimeStamp;
	}

	public boolean isOutputComplex() {
		return outputComplex;
	}

	public int getOutputInterval() {
		return outputInterval;
	}

	public List<String> getOutputAgents() {
		return outputAgents;
	}

	public boolean isTrackExchange() {
		if (mpiMode != MpiMode.SINGLE_CORE && trackExchange) {
			trackExchange = false;
			logger.warn(WARN_PARALLEL_DISALLOWED + "--track-exchange");
		}
		return trackExchange;
	}

	public boolean isTrackRuntime() {
		if (mpiMode != MpiMode.SINGLE_CORE && trackRuntime) {
			trackRuntime = false;
			logger.warn(WARN_PARALLEL_DISALLOWED + "--track-runtime");
		}
		return trackRuntime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("helpRequested: ").append(helpRequested).append("; ");
		builder.append("mpiArgs: ").append(Arrays.toString(mpiArgs)).append("; ");
		builder.append("mpiMode: ").append(mpiMode).append(";");
		builder.append("inputFile: ").append(inputFile).append(";");
		builder.append("outputFile: ").append(outputFile).append(";");
		builder.append("addTimeStamp: ").append(addTimeStamp).append(";");
		builder.append("outputComplex: ").append(outputComplex).append(";");
		builder.append("outputInterval: ").append(outputInterval).append(";");
		builder.append("outputAgents: ").append(outputAgents).append(";");
		builder.append("trackExchange: ").append(trackExchange).append(";");
		builder.append("trackRuntime: ").append(trackRuntime).append(";");
		builder.append("hierarchyDepth: ").append(portableDepth).append(";");
		return builder.toString();
	}
}