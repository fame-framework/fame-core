// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.setup;

import java.io.File;
import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiInstantiator;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.service.InputManager;
import de.dlr.gitlab.fame.service.Simulator;
import picocli.CommandLine;

/** Main class to read command line arguments and execute FAME
 * 
 * @author Christoph Schimeczek */
public class FameRunner {
	/** Name of the file that stores CLI defaults */
	public static final String DEFAULTS_FILE = "fameSetup.yaml";

	/** Execute FAME model
	 * 
	 * @param arguments for MPI and named options */
	public static void main(String[] arguments) {
		Setup setup = unpackCommandLineArguments(arguments);

		MpiFacade mpi = MpiInstantiator.getMpi(setup.getMpiMode());
		mpi.initialise(setup.getMpiArgs());
		MpiManager mpiManager = new MpiManager(mpi);

		InputManager inputManager = loadAndDistributeInputData(new InputManager(mpiManager), setup.getInputFile());
		Simulator simulator = new Simulator(mpiManager, inputManager, setup);
		simulator.warmUp();
		simulator.run();

		mpi.invokeFinalize();
		System.exit(0);
	}

	/** Unpacks given command line arguments and writes them to a new {@link Setup} file. If a command line argument is not
	 * explicitly specified, its default is used. Standard hard-coded defaults in specified {@link Cli} can be overridden in a the
	 * `fameSetup.yaml` file located at {@link #DEFAULTS_FILE}.
	 * 
	 * @param arguments from command line
	 * @return Setup with parameters associated with command line arguments or their defaults */
	private static Setup unpackCommandLineArguments(String[] arguments) {
		Setup setup = new Setup();
		CommandLine cmd = new CommandLine(setup);
		cmd.setDefaultValueProvider(new YamlDefaultsProvider(new File(DEFAULTS_FILE)));
		cmd.execute(arguments);
		return setup;
	}

	/** Use given input manager to load input data from file and distribute them among all nodes
	 * 
	 * @param inputManager {@link InputManager} to load and distribute the data
	 * @param pathToInputFile file to be read
	 * @return {@link InputManager} created and loaded with input data */
	public static InputManager loadAndDistributeInputData(InputManager inputManager, String pathToInputFile) {
		inputManager.read(pathToInputFile);
		inputManager.distribute();
		return inputManager;
	}
}