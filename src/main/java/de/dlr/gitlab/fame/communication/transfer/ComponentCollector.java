// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import java.util.ArrayList;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem.Builder;
import de.dlr.gitlab.fame.data.TimeSeries;

/** Collects components and primitives of {@link Portable} objects to enable their transfer to other {@link Agent}s; acts as a
 * Facade for underlying protobuf structure
 * 
 * @author Christoph Schimeczek, Marc Deissenroth */
public class ComponentCollector {
	private Builder builder;
	private ArrayList<Portable> storedComponents = new ArrayList<>();

	/** Creates a new {@link ComponentCollector}
	 * 
	 * @param builder for protobuf NestedItems */
	public ComponentCollector(Builder builder) {
		this.builder = builder;
	}

	/** Adds boolean variables to be serialised
	 * 
	 * @param booleans any number of variables of type boolean or Boolean to be serialised in order */
	public void storeBooleans(boolean... booleans) {
		for (boolean bool : booleans) {
			builder.addBoolValues(bool);
		}
	}

	/** Adds integer variables to be serialised
	 * 
	 * @param integers any number of variables of type int or Integer to be serialised in order */
	public void storeInts(int... integers) {
		for (int integer : integers) {
			builder.addIntValues(integer);
		}
	}

	/** Adds long variables to be serialised
	 * 
	 * @param longs any number of variables of type long or Long to be serialised in order */
	public void storeLongs(long... longs) {
		for (long longValue : longs) {
			builder.addLongValues(longValue);
		}
	}

	/** Adds float variables to be serialised
	 * 
	 * @param floats any number of variables of type float or Float to be serialised in order */
	public void storeFloats(float... floats) {
		for (float floatValue : floats) {
			builder.addFloatValues(floatValue);
		}
	}

	/** Adds double variables to be serialised
	 * 
	 * @param doubles any number of variables of type double or Double to be serialised in order */
	public void storeDoubles(double... doubles) {
		for (double doubleValue : doubles) {
			builder.addDoubleValues(doubleValue);
		}
	}

	/** Adds string variables to be serialised
	 * 
	 * @param strings any number of variables of type String to be serialised in order */
	public void storeStrings(String... strings) {
		for (String string : strings) {
			builder.addStringValues(string);
		}
	}

	/** Adds time series variables to be serialised
	 * 
	 * @param timeSeries any number of variables of type TimeSeries to be serialised in order */
	public void storeTimeSeries(TimeSeries... timeSeries) {
		for (TimeSeries aTimeSeries : timeSeries) {
			builder.addTimeSeriesIds(aTimeSeries.getId());
		}
	}

	/** Adds {@link Portable} components to be serialised
	 * 
	 * @param components any number of {@link Portable} components to be serialised in order */
	public void storeComponents(Portable... components) {
		for (Portable component : components) {
			storedComponents.add(component);
		}
	}

	/** @return all inner components stored to this {@link ComponentCollector} */
	ArrayList<Portable> getSubComponents() {
		return storedComponents;
	}
}