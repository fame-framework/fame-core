// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import de.dlr.gitlab.fame.agent.Agent;

/** Interface for complex objects that can be transfered to other {@link Agent}
 * 
 * @author Christoph Schimeczek */
public interface Portable {
	/** Adds all fields that are required for decomposition to given {@link ComponentCollector}
	 * 
	 * @param collector to be filled with the component data */
	public void addComponentsTo(ComponentCollector collector);

	/** Populates all required fields with given components
	 * 
	 * @param provider provides the field's data to construct a Portable from */
	public void populate(ComponentProvider provider);
}