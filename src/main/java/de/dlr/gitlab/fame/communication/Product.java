// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** Use to Annotate Products that can be used for {@link Contract}s
 * 
 * @author Christoph Schimeczek */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface Product {}