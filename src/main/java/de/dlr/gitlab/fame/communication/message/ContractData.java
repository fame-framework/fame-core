// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.message;

import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.time.TimeStamp;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;

/** Indicates which {@link Contract} is to be fulfilled the {@link Message} containing this DataItem and for what TimeStamp
 * 
 * @author Christoph Schimeczek */
public class ContractData extends DataItem {
	/** Unique ID of the associated {@link Contract} */
	public final long contractId;
	/** Time of fulfilment of the associated Contract that this message refers to */
	public final TimeStamp deliveryTime;

	/** Constructs a new {@link ContractData}
	 * 
	 * @param contractId id of the associated contract
	 * @param deliveryTime time at which the contract shall be fulfilled */
	public ContractData(long contractId, TimeStamp deliveryTime) {
		this.contractId = contractId;
		this.deliveryTime = deliveryTime;
	}

	/** Constructs a new {@link ContractData} from ProtoBuffer */
	ContractData(ProtoDataItem protoData) {
		this.contractId = protoData.getLongValues(0);
		this.deliveryTime = new TimeStamp(protoData.getLongValues(1));
	}

	@Override
	protected void fillDataFields(Builder builder) {
		builder.addLongValues(contractId);
		builder.addLongValues(deliveryTime.getStep());
	}

	/** @return id of associated {@link Contract} */
	public long getContractId() {
		return contractId;
	}

	/** @return {@link TimeStamp} of delivery */
	public TimeStamp getDeliveryTime() {
		return deliveryTime;
	}
}