// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem.Builder;
import de.dlr.gitlab.fame.service.TimeSeriesProvider;
import de.dlr.gitlab.fame.service.TimeSeriesProvider.MissingSeriesException;

/** Manages the composition and decomposition of {@link Portable} objects, their components and primitives data
 * 
 * @author Christoph Schimeczek */
public class Composer {
	static Logger logger = LoggerFactory.getLogger(Composer.class);
	static final String MAX_DEPTH_EXCEEDED = "Max hierarchy depth exceeded. Check for loop of references.";
	static final String INIT_FAIL = "Could not instantiate - ensure default constructor exists for ";

	private final int maxHierarchyDepth;
	private final PortableUidManager portableUidManager;
	private final TimeSeriesProvider timeSeriesProvider;

	/** Create new {@link Composer}
	 * 
	 * @param portableUidManager knows UIDs for all {@link Portable} types in the simulation
	 * @param timeSeriesProvider holds all {@link TimeSeries} available from input
	 * @param maxHierarchyDepth maximum depth of nesting allowed for {@link Portable} objects */
	public Composer(PortableUidManager portableUidManager, TimeSeriesProvider timeSeriesProvider, int maxHierarchyDepth) {
		this.maxHierarchyDepth = maxHierarchyDepth;
		this.portableUidManager = portableUidManager;
		this.timeSeriesProvider = timeSeriesProvider;
	}

	/** Decomposes any given {@link Object} implementing the {@link Portable}-interface and returns a {@link NestedItem} ready to be
	 * transported in a {@link Message} to any Agent
	 * 
	 * @param item {@link Portable} object to be decomposed into a {@link NestedItem}
	 * @return {@link NestedItem} containing all data necessary to construct the given Object using {@link #compose(NestedItem)} */
	public NestedItem decompose(Portable item) {
		int typeId = portableUidManager.getTypeIdOfClass(item.getClass());
		Builder builder = NestedItem.newBuilder().setDataTypeId(typeId);
		decompose(0, builder, item);
		return builder.build();
	}

	/** Recursive function to decompose a {@link Portable} item by adding all its components and primitives to the given
	 * {@link Builder} */
	private void decompose(int depth, Builder parentBuilder, Portable parentItem) {
		ensureDepthBelowMax(depth);
		ComponentCollector collector = new ComponentCollector(parentBuilder);
		parentItem.addComponentsTo(collector);
		for (Portable subComponent : collector.getSubComponents()) {
			int subTypeId = portableUidManager.getTypeIdOfClass(subComponent.getClass());
			Builder subComponentBuilder = NestedItem.newBuilder().setDataTypeId(subTypeId);
			decompose(depth + 1, subComponentBuilder, subComponent);
			parentBuilder.addComponents(subComponentBuilder);
		}
	}

	/** Ensures that the given depth is equal to or lower than {@link #maxHierarchyDepth}
	 * 
	 * @throws RuntimeException if depth exceeds {@link #maxHierarchyDepth} */
	private void ensureDepthBelowMax(int depth) {
		if (depth > maxHierarchyDepth) {
			throw Logging.logFatalException(logger, MAX_DEPTH_EXCEEDED);
		}
	}

	/** Builds a {@link Portable} composed from the given {@link NestedItem}, including all relevant components and primitive data
	 * 
	 * @param item {@link NestedItem} to build the {@link Portable} from
	 * @return {@link Portable} built from given item */
	public Portable compose(NestedItem item) {
		ComponentProvider componentProvider = new ComponentProvider();
		componentProvider.setPrimitives(item);
		componentProvider.setTimeSeriesArray(fetchTimeSeries(item.getTimeSeriesIdsList()));
		componentProvider.setComponentArray(getComponentsFor(item.getComponentsList()));
		Constructor<? extends Portable> constructor = portableUidManager.getConstructorById(item.getDataTypeId());
		return readyInstance(constructor, componentProvider);
	}

	/** @return List of {@link TimeSeries} which entries are specified in the given List of TimeSeries-ids */
	private List<TimeSeries> fetchTimeSeries(List<Integer> timeSeriesIdList) {
		ArrayList<TimeSeries> timeSeriesList = new ArrayList<>(timeSeriesIdList.size());
		for (int timeSeriesId : timeSeriesIdList) {
			TimeSeries timeSeries;
			try {
				timeSeries = timeSeriesProvider.getSeriesById(timeSeriesId);
			} catch (MissingSeriesException e) {
				throw Logging.logFatalException(logger, e.getMessage());
			}
			timeSeriesList.add(timeSeries);
		}
		return timeSeriesList;
	}

	/** @return {@link List} of {@link Portable} components composed from the given list of {@link NestedItem}s */
	private List<Portable> getComponentsFor(List<NestedItem> nestedComponents) {
		ArrayList<Portable> components = new ArrayList<>();
		for (NestedItem component : nestedComponents) {
			components.add(compose(component));
		}
		return components;
	}

	/** Creates and {@link Portable#populate(ComponentProvider) populates} a {@link Portable} object using given
	 * {@link ComponentProvider} filled with all required components and {@link Primitive}s
	 * 
	 * @return created {@link Portable} object */
	private Portable readyInstance(Constructor<? extends Portable> constructor, ComponentProvider componentProvider) {
		Portable instance = instantiate(constructor);
		instance.populate(componentProvider);
		return instance;
	}

	/** @return new instance of given class type */
	private Portable instantiate(Constructor<? extends Portable> constructor) {
		try {
			return constructor.newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw Logging.logFatalException(logger, INIT_FAIL + constructor.getDeclaringClass().getSimpleName());
		}
	}

	/** FAME-internal method -- <b>Do not call in simulation</b>!<br>
	 * Return type-ID of given class of {@link Portable}
	 * 
	 * @param classType class of Portable
	 * @return ID matching given classType */
	public int getIdOfDataType(Class<? extends Portable> classType) {
		return portableUidManager.getTypeIdOfClass(classType);
	}
}