// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.util.Reflection;
import de.dlr.gitlab.fame.util.Reflection.ReflectionType;

/** Creates and stores unique identification numbers for all class types implementing {@link Portable}
 * 
 * @author Christoph Schimeczek, Marc Deissenroth */
public class PortableUidManager {
	static final String NO_DEFAULT_CONSTRUCTOR = "Default constructor missing for ";
	static final String CLASS_NOT_KNOWN = "Class not registered - ensure portablePackages cover class ";

	private static Logger logger = LoggerFactory.getLogger(PortableUidManager.class);
	private static final Comparator<Class<?>> CLASS_COMPARATOR = new Comparator<Class<?>>() {
		@Override
		public int compare(Class<?> c1, Class<?> c2) {
			return c1.getName().compareTo(c2.getName());
		}
	};

	private HashMap<Class<? extends Portable>, Integer> mapClassToId;
	private ArrayList<Constructor<? extends Portable>> constructorsById;

	/** Create new {@link PortableUidManager}
	 * 
	 * @param portablePackageNames packages to be scanned for {@link Portable} classes */
	public PortableUidManager(List<String> portablePackageNames) {
		Reflection reflection = new Reflection(ReflectionType.PORTABLE, portablePackageNames);
		Set<Class<? extends Portable>> portableTypes = reflection.getSubTypesOf(Portable.class);
		ArrayList<Class<? extends Portable>> classList = sortClassTypesByName(portableTypes);
		mapClassToId = createClassToIdMap(classList);
		constructorsById = createConstructorList(classList);
	}

	/** Orders a Set of classes by their name
	 * 
	 * @param <T> class type of parent class for Set and List
	 * @param dataItemClasses Set of classes to be ordered
	 * @return {@link ArrayList} of the given {@link Set} of {@link Class}es sorted by name */
	public static <T> ArrayList<Class<? extends T>> sortClassTypesByName(Set<Class<? extends T>> dataItemClasses) {
		ArrayList<Class<? extends T>> classes = new ArrayList<>(dataItemClasses.size());
		for (Class<? extends T> item : dataItemClasses) {
			classes.add(item);
		}
		Collections.sort(classes, PortableUidManager.CLASS_COMPARATOR);
		return classes;
	}

	/** Creates a HashMap of class types to match unique IDs
	 * 
	 * @param <T> parent type of class
	 * @param classTypes list of classes to be assigned with an ID
	 * @return a {@link HashMap} mapping {@link Class}es to an int &ge; 0 based on their order in the given {@link ArrayList} */
	public static <T> HashMap<Class<? extends T>, Integer> createClassToIdMap(ArrayList<Class<? extends T>> classTypes) {
		HashMap<Class<? extends T>, Integer> map = new HashMap<>();
		int index = 0;
		for (Class<? extends T> type : classTypes) {
			map.put(type, index);
			index++;
		}
		return map;
	}

	/** @return an {@link ArrayList} of {@link Constructor}s for given list of {@link Class}es in matching order */
	private ArrayList<Constructor<? extends Portable>> createConstructorList(
			ArrayList<Class<? extends Portable>> classTypes) {
		ArrayList<Constructor<? extends Portable>> constructors = new ArrayList<>();
		for (Class<? extends Portable> type : classTypes) {
			try {
				constructors.add(type.getConstructor());
			} catch (NoSuchMethodException | SecurityException e) {
				throw Logging.logFatalException(logger, NO_DEFAULT_CONSTRUCTOR + type.getSimpleName());
			}
		}
		return constructors;
	}

	/** Get the unique ID of the given class of Portable
	 * 
	 * @param type class of Portable to be matched
	 * @return unique dataTypeId for given class type */
	public int getTypeIdOfClass(Class<? extends Portable> type) {
		Integer result = mapClassToId.get(type);
		if (result == null) {
			throw new RuntimeException(CLASS_NOT_KNOWN + type.getSimpleName());
		}
		return result;
	}

	/** Get {@link Constructor} for a class identified by its unique typeID
	 * 
	 * @param dataTypeId unique id of this class type
	 * @return constructor for Portable class associated with the given typeID */
	public Constructor<? extends Portable> getConstructorById(int dataTypeId) {
		return constructorsById.get(dataTypeId);
	}
}