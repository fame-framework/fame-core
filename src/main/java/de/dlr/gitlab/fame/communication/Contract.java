// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.protobuf.Field.NestedField;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;

/** An information delivery contract between two agents
 * 
 * @author Christoph Schimeczek */
public class Contract {
	static final String SERIES_NOT_ALLOWED = "TIME_SERIES not supported for Contract payload.";
	static final String INCORRECT_PAYLOAD = "More than one data type on single field of Contract payload.";

	static final String ADDRESS_SEPARATOR = ".";

	private final long contractId;
	private final long senderId;
	private final long receiverId;
	private final Enum<?> product;
	private final TimeStamp firstDeliveryTime;
	private final TimeSpan deliveryInterval;
	private final TimeStamp expirationTime;
	private final Map<String, List<?>> data;

	/** Create new {@link Contract}
	 * 
	 * @param protoType protobuf representation of the {@link Contract} to be created
	 * @param contractId unique ID of the {@link Contract} to be created
	 * @param product to which this {@link Contract} refers */
	public Contract(ProtoContract protoType, long contractId, Enum<?> product) {
		this.contractId = contractId;
		this.product = product;
		senderId = protoType.getSenderId();
		receiverId = protoType.getReceiverId();
		firstDeliveryTime = new TimeStamp(protoType.getFirstDeliveryTime());
		deliveryInterval = new TimeSpan(protoType.getDeliveryIntervalInSteps());
		expirationTime = new TimeStamp(protoType.getExpirationTime());
		data = protoType.getFieldsCount() == 0 ? Collections.emptyMap() : new HashMap<>();
		extractFieldData("", protoType.getFieldsList());
	}

	/** Adds (nested) information from list of NestedField to given dataContainer under the specified path */
	private void extractFieldData(String path, List<NestedField> innerFields) {
		for (NestedField field : innerFields) {
			String elementPath = path + field.getFieldName();
			if (field.getFieldsCount() > 0) {
				extractFieldData(elementPath + ADDRESS_SEPARATOR, field.getFieldsList());
			} else {
				data.put(elementPath, getValueList(field));
			}
		}
	}

	/** @return list of similar primitives extracted from given field */
	private List<?> getValueList(NestedField field) {
		if (field.hasSeriesId()) {
			throw new RuntimeException(SERIES_NOT_ALLOWED);
		}
		List<?> values = replaceFirstIfSecondIsNotEmpty(Collections.EMPTY_LIST, field.getIntValuesList());
		values = replaceFirstIfSecondIsNotEmpty(values, field.getStringValuesList());
		values = replaceFirstIfSecondIsNotEmpty(values, field.getDoubleValuesList());
		values = replaceFirstIfSecondIsNotEmpty(values, field.getLongValuesList());
		return values;
	}

	/** @return Second list if it is not empty else first list
	 * @throws RuntimeException if Second list is not empty but first list is also not empty */
	private List<?> replaceFirstIfSecondIsNotEmpty(List<?> first, List<?> second) {
		if (second != null && second.size() > 0) {
			if (first.size() > 0) {
				throw new RuntimeException(INCORRECT_PAYLOAD);
			}
			return second;
		}
		return first;
	}

	/** @return unique ID of this {@link Contract} */
	public long getContractId() {
		return contractId;
	}

	/** @return ID of {@link Agent} that sends the Product in this {@link Contract} */
	public long getSenderId() {
		return senderId;
	}

	/** @return ID of {@link Agent} that receives the Product in this {@link Contract} */
	public long getReceiverId() {
		return receiverId;
	}

	/** Returns the {@link Product} of the sender agent this contract refers to
	 * 
	 * @return {@link Product} of the sender agent this contract refers to */
	public Enum<?> getProduct() {
		return product;
	}

	/** Returns the {@link TimeStamp} of the first delivery, i.e. where the sender guarantees the receiver to have received the
	 * contract's information
	 * 
	 * @return {@link TimeStamp} of first delivery */
	public TimeStamp getFirstDeliveryTime() {
		return firstDeliveryTime;
	}

	/** Returns the {@link TimeSpan} of the delivery interval: The sender guarantees the delivery of update informations each
	 * "delivery interval" times
	 * 
	 * @return {@link TimeStamp} delivery interval of promised information */
	public TimeSpan getDeliveryInterval() {
		return deliveryInterval;
	}

	/** Returns the {@link TimeStamp} at which the contract will automatically expire and end without further notice, i.e. no
	 * further information is to be delivered <b>after</b> the expiration time.
	 * 
	 * @return expiration TimeStamp */
	public TimeStamp getExpirationTime() {
		return expirationTime;
	}

	/** Returns the next {@link TimeStamp} where at this contract guarantees delivery <b>after</b> the given TimeStamp
	 * 
	 * @param givenTime the given TimeStamp
	 * @return next {@link TimeStamp} where at this contract guarantees delivery <b>after</b> the given TimeStamp. <br>
	 *         If the contract will be expired by then, the largest possible future time is returned<br>
	 *         If the given TimeStamp is before the first delivery time, the initial first time of delivery is returned. */
	public TimeStamp getNextTimeOfDeliveryAfter(TimeStamp givenTime) {
		long timeAfterContractBegin = givenTime.getStep() - firstDeliveryTime.getStep();
		if (timeAfterContractBegin < 0) {
			return firstDeliveryTime;
		} else if (timeAfterContractBegin == 0) {
			TimeStamp nextDelivery = firstDeliveryTime.laterBy(deliveryInterval);
			return nextDelivery.isLessEqualTo(expirationTime) ? nextDelivery : TimeStamp.LATEST;
		} else {
			long timeAfterLastDelivery = timeAfterContractBegin % deliveryInterval.getSteps();
			long remainingTimeToNextDelivery = deliveryInterval.getSteps() - timeAfterLastDelivery;
			TimeStamp nextDelivery = givenTime.laterBy(new TimeSpan(remainingTimeToNextDelivery));
			return nextDelivery.isLessEqualTo(expirationTime) ? nextDelivery : TimeStamp.LATEST;
		}
	}

	/** Returns true if given AgentId is sender in this Contract
	 * 
	 * @param agentId id to be checked
	 * @return true if given Id is the sender */
	public boolean isSender(long agentId) {
		return agentId == senderId;
	}

	/** Returns true if this {@link Contract} is active at the given {@link TimeStamp time}, i.e. the given time is greater than or
	 * equal to the {@link #firstDeliveryTime} and lesser than or equal to {@link #expirationTime}
	 * 
	 * @param time at which the Contract is tested for activity
	 * @return true, if the Contract is active at the given time */
	public boolean isActiveAt(TimeStamp time) {
		return time.isLessEqualTo(expirationTime) && time.isGreaterEqualTo(firstDeliveryTime);
	}

	/** Returns a {@link ContractData} item that can be added to a {@link Message} to indicate that this {@link Contract} is to be
	 * fulfilled; the TimeStamp of fulfilment is searched based on the given {@link TimeStamp}: the next greater time of contract
	 * fulfilment is selected.
	 * 
	 * @param time used to search the next time of delivery after this TimeStamp
	 * @return {@link ContractData} matching this {@link Contract} */
	public ContractData fulfilAfter(TimeStamp time) {
		return new ContractData(getContractId(), getNextTimeOfDeliveryAfter(time));
	}

	/** Return payload at specified path
	 * 
	 * @param path specifying the payload
	 * @return any payload stored at the given Path or an empty list if path is missing or has no data */
	public List<?> getPayload(String path) {
		return data.getOrDefault(path, Collections.emptyList());
	}

	/** @return true if any additional payload is available for this {@link Contract} */
	public boolean hasPayload() {
		return !data.isEmpty();
	}

	@Override
	public String toString() {
		return "Agent " + senderId + "-->" + receiverId + " <" + product + "> [steps " + firstDeliveryTime.getStep() + ":"
				+ expirationTime.getStep() + " each " + deliveryInterval.getSteps() + "]";
	}
}