// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;

/** Provides components of a {@link Portable} object, allowing its population with values
 * 
 * @author Christoph Schimeczek */
public final class ComponentProvider {
	private ComponentIterator<Boolean> booleanIterator = new ComponentIterator<>();
	private ComponentIterator<Integer> intIterator = new ComponentIterator<>();
	private ComponentIterator<Long> longIterator = new ComponentIterator<>();
	private ComponentIterator<Float> floatIterator = new ComponentIterator<>();
	private ComponentIterator<Double> doubleIterator = new ComponentIterator<>();
	private ComponentIterator<String> stringIterator = new ComponentIterator<>();
	private ComponentIterator<TimeSeries> timeSeriesIterator = new ComponentIterator<>();
	private ComponentIterator<Portable> componentIterator = new ComponentIterator<>();

	/** saves values of all primitives to this component provider */
	void setPrimitives(NestedItem item) {
		booleanIterator.iterateOver(item.getBoolValuesList());
		intIterator.iterateOver(item.getIntValuesList());
		longIterator.iterateOver(item.getLongValuesList());
		floatIterator.iterateOver(item.getFloatValuesList());
		doubleIterator.iterateOver(item.getDoubleValuesList());
		stringIterator.iterateOver(item.getStringValuesList());
	}

	void setTimeSeriesArray(List<TimeSeries> timeSeries) {
		timeSeriesIterator.iterateOver(timeSeries);
	}

	void setComponentArray(List<Portable> components) {
		componentIterator.iterateOver(components);
	}

	/** Return next stored boolean value
	 * 
	 * @return next stored boolean value
	 * @throws NoSuchElementException if no more elements are stored */
	public boolean nextBoolean() {
		return booleanIterator.next();
	}

	/** Return next stored int value
	 * 
	 * @return next stored int value
	 * @throws NoSuchElementException if no more elements are stored */
	public int nextInt() {
		return intIterator.next();
	}

	/** Return next stored long value
	 * 
	 * @return next stored long value
	 * @throws NoSuchElementException if no more elements are stored */
	public long nextLong() {
		return longIterator.next();
	}

	/** Return next stored float value
	 * 
	 * @return next stored float value
	 * @throws NoSuchElementException if no more elements are stored */
	public float nextFloat() {
		return floatIterator.next();
	}

	/** Return next stored double value
	 * 
	 * @return next stored double value
	 * @throws NoSuchElementException if no more elements are stored */
	public double nextDouble() {
		return doubleIterator.next();
	}

	/** Return next stored String
	 * 
	 * @return next stored String
	 * @throws NoSuchElementException if no more elements are stored */
	public String nextString() {
		return stringIterator.next();
	}

	/** Return next stored {@link TimeSeries}
	 * 
	 * @return next stored TimeSeries
	 * @throws NoSuchElementException if no more elements are stored */
	public TimeSeries nextTimeSeries() {
		return timeSeriesIterator.next();
	}

	/** Test for any remaining component(s)
	 * 
	 * @return true if this {@link ComponentProvider} contains further components */
	public boolean hasNextComponent() {
		return componentIterator.hasNext();
	}

	/** Test if next component is of given type
	 * 
	 * @param type class of {@link Portable} in question
	 * @return <b>true</b> if the next component to provide can be cast to given class type<br>
	 *         <b>false</b> otherwise or if no next component exists */
	public boolean hasNextComponent(Class<? extends Portable> type) {
		if (componentIterator.hasNext()) {
			Portable nextItem = componentIterator.previewNext();
			return type.isAssignableFrom(nextItem.getClass());
		} else {
			return false;
		}
	}

	/** Return next stored component and casts to given class type; <br>
	 * Test with {@link #hasNextComponent(Class)} whether the next element can be cast to that type
	 * 
	 * @param<T> type of Portable
	 * @param type class of {@link Portable} to be extracted
	 * @return next stored component
	 * @throws NoSuchElementException if no more components are stored
	 * @throws ClassCastException if the next element cannot be cast to given class type */
	public <T extends Portable> T nextComponent(Class<T> type) {
		return type.cast(componentIterator.next());
	}

	/** Returns a List of all the next components that are of the given class type, until a component of a different class type or
	 * no further component is found.
	 * 
	 * @param<T> type of Portable
	 * @param type class of the {@link Portable}(s) to be extracted
	 * @return List of components of similar class type; list may be empty if the next component is not of given class type or if no
	 *         more components are stored. */
	public <T extends Portable> ArrayList<T> nextComponentList(Class<T> type) {
		ArrayList<T> list = new ArrayList<>();
		while (hasNextComponent(type)) {
			list.add(nextComponent(type));
		}
		return list;
	}
}