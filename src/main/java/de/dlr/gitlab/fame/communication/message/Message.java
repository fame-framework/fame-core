// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.message;

import java.util.ArrayList;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage.Builder;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.transfer.Composer;
import de.dlr.gitlab.fame.communication.transfer.Portable;

/** Container class for messages between agents: Transmission information and any amount of {@link DataItem}s and
 * {@link NestedItem}s
 * 
 * @author Christoph Schimeczek, Ben Fuchs */
public class Message {
	/** ID of {@link Agent} that sends this message */
	public final long senderId;
	/** ID of {@link Agent} that receives this message */
	public final long receiverId;
	private final ProtoDataItem[] dataItems;
	private final NestedItem[] nestedItems;
	private static Composer composer;

	/** sets the specified {@link Composer} to be use by all {@link Message}s for composition of {@link Portable}s */
	static void setComposer(Composer composer) {
		Message.composer = composer;
	}

	/** Constructs a new {@link Message} */
	Message(long senderId, long receiverId, ProtoDataItem[] dataItems, NestedItem[] nestedItems) {
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.dataItems = dataItems;
		this.nestedItems = nestedItems;
	}

	/** <b>FAME-internal routine - do not call</b>!<br>
	 * Constructs a new {@link Message} from its protobuf representation
	 * 
	 * @param messagePrototype protobuf representation of message */
	public Message(ProtoMessage messagePrototype) {
		senderId = messagePrototype.getSenderId();
		receiverId = messagePrototype.getReceiverId();
		dataItems = messagePrototype.getDataItemsList().toArray(new ProtoDataItem[0]);
		nestedItems = messagePrototype.getNestedItemsList().toArray(new NestedItem[0]);
	}

	/** <b>FAME-internal routine - do not call</b>!<br>
	 * 
	 * @return protobuf {@link ProtoMessage} representing this {@link Message} */
	public ProtoMessage createProtobufRepresentation() {
		return createProtobufRepresentationWithReceiver(receiverId);
	}

	/** @return protobuf {@link ProtoMessage} representing this {@link Message} with the specified receiver */
	private ProtoMessage createProtobufRepresentationWithReceiver(long newReceiverId) {
		Builder messageBuilder = ProtoMessage.newBuilder();
		messageBuilder.setSenderId(senderId).setReceiverId(newReceiverId);
		for (ProtoDataItem dataItem : dataItems) {
			messageBuilder.addDataItems(dataItem);
		}
		for (NestedItem nestedItem : nestedItems) {
			messageBuilder.addNestedItems(nestedItem);
		}
		return messageBuilder.build();
	}

	/** Returns a deep copy of this {@link Message} targeted at the specified receiverId
	 * 
	 * @param newReceiverId id of the receiver of the copied message1
	 * @return deep copy of this {@link Message} with the specified receiverId */
	public Message createCopyWithNewReceiver(long newReceiverId) {
		ProtoMessage serialisedMessage = createProtobufRepresentationWithReceiver(newReceiverId);
		return new Message(serialisedMessage);
	}

	/** Returns a deep copy of this {@link Message} without any changes, see also {@link #createCopyWithNewReceiver(long)}
	 * 
	 * @return deep copy of this Message */
	public Message deepCopy() {
		return createCopyWithNewReceiver(receiverId);
	}

	/** @return Id of this {@link Message}'s sender */
	public long getSenderId() {
		return senderId;
	}

	/** Returns true if this {@link Message} contains at least one {@link DataItem} of the given type
	 * 
	 * @param classType type of DataItem to search for in this message
	 * @return true if this {@link Message} contains at least one {@link DataItem} of the given type, false otherwise */
	public boolean containsType(Class<? extends DataItem> classType) {
		int idOfrequestedType = DataItem.getIdOfType(classType);
		for (ProtoDataItem item : dataItems) {
			if (item.getDataTypeId() == idOfrequestedType) {
				return true;
			}
		}
		return false;
	}

	/** Returns a {@link DataItem}s matching the given type
	 * 
	 * @param <T> type of DataItem
	 * @param classType type of DataItem to return
	 * @return {@link DataItem}s matching the given type; if no matching item is found, <code>null</code> is returned */
	@SuppressWarnings("unchecked")
	public <T extends DataItem> T getDataItemOfType(Class<T> classType) {
		int idOfrequestedType = DataItem.getIdOfType(classType);
		for (ProtoDataItem item : dataItems) {
			if (item.getDataTypeId() == idOfrequestedType) {
				return (T) DataItem.createFromProtobuf(item);
			}
		}
		return null;
	}

	/** Returns List of all {@link Portable} objects of the specified {@link Class} in this {@link Message}<br>
	 * if no matching objects are found, an empty list is returned
	 * 
	 * @param <T> type of Portable
	 * @param classType type of Portable to return
	 * @return {@link ArrayList} of {@link Portable}s extending given {@link Class}type */
	@SuppressWarnings("unchecked")
	public <T extends Portable> ArrayList<T> getAllPortableItemsOfType(Class<T> classType) {
		ArrayList<T> portables = new ArrayList<>();
		int requestedDataTypeId = composer.getIdOfDataType(classType);
		for (NestedItem nestedItem : nestedItems) {
			if (nestedItem.getDataTypeId() == requestedDataTypeId) {
				portables.add((T) composer.compose(nestedItem));
			}
		}
		return portables;
	}

	/** Returns the first {@link Portable} Object of the specified {@link Class} in this {@link Message} <br>
	 * if no matching object is found, <code>null</code> is returned.
	 * 
	 * @param <T> type of Portable
	 * @param classType type of Portable to return
	 * @return single {@link Portable} object of specified class type if present, else null */
	@SuppressWarnings("unchecked")
	public <T extends Portable> T getFirstPortableItemOfType(Class<T> classType) {
		int requestedDataTypeId = composer.getIdOfDataType(classType);
		for (NestedItem nestedItem : nestedItems) {
			if (nestedItem.getDataTypeId() == requestedDataTypeId) {
				return (T) composer.compose(nestedItem);
			}
		}
		return null;
	}
}