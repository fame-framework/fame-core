// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.stats;

import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;

/** Creates communication statistics from assessed messages
 * 
 * @author Christoph Schimeczek */
public interface CommTracking {

	/** Assesses characteristics of given message
	 * 
	 * @param message to be assessed */
	public void assessMessage(ProtoMessage message);

	/** @return measures statistics in JSON format */
	public String getStatsJson();
}
