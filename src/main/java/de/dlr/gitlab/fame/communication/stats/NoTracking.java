// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.stats;

import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;

/** Dummy implementation of {@link CommTracking} without any actual tracking of statistics
 * 
 * @author Christoph Schimeczek */
public class NoTracking implements CommTracking {
	@Override
	public void assessMessage(ProtoMessage message) {}

	@Override
	public String getStatsJson() {
		return "";
	}

}
