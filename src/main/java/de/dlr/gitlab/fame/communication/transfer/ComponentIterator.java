// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.transfer;

import java.util.List;
import java.util.NoSuchElementException;

/** Iterator for lists of {@link Portable} components of given type
 * 
 * @param <U> type of elements stored in the list that backs this iterator and that is returned by calls to {@link #next()} */
public class ComponentIterator<U> {
	static final String NO_MORE_ELEMENTS = "No more elements to iterate over.";

	private List<U> list;
	private int readPosition;

	/** Sets to iterate over the given list and resets the iterator's reading position to the beginning.
	 * 
	 * @param list to be iterated over from its start */
	public void iterateOver(List<U> list) {
		this.list = list;
		this.readPosition = -1;
	}

	/** Returns the value at the next position in the list that backs this iterator. This value could be null. Move the iterator to
	 * the next reading position. Thus, continued calls to {@link #next()} will iterate through the list until reaching the end of
	 * the list.
	 * 
	 * @throws NoSuchElementException if no more elements exist */
	public U next() {
		readPosition++;
		if (!hasElementAt(readPosition)) {
			throw new NoSuchElementException(NO_MORE_ELEMENTS);
		}
		return list.get(readPosition);
	}

	/** @return true if the list that backs this iterator has an element at the given position */
	private boolean hasElementAt(int position) {
		return list.size() > position;
	}

	/** Returns true if the list that backs this iterator has an element stored at the next position. This element could be null,
	 * though. Returns false if no more elements are stored in the list backed by this iterator.
	 * 
	 * @return true if more elements can be read with {@link #next()} */
	public boolean hasNext() {
		return hasElementAt(readPosition + 1);
	}

	/** Returns the value at the next position in the list that backs this iterator. This value could be null. Does <b>not</b>
	 * change the iterator reading position. Thus, continued calls to {@link #previewNext()} will return the same list element and
	 * not stop.
	 * 
	 * @throws NoSuchElementException if next element does not exist */
	public U previewNext() {
		int nextPosition = readPosition + 1;
		if (!hasElementAt(nextPosition)) {
			throw new NoSuchElementException(NO_MORE_ELEMENTS);
		}
		return list.get(nextPosition);
	}
}
