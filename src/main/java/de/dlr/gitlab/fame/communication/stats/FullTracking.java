// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.communication.stats;

import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.util.AgentInteractionStatistic;

/** Tracks number of messages between agents and cumulated size of their messages
 * 
 * @author Christoph Schimeczek */
public class FullTracking implements CommTracking {
	static final String KEY_COUNT = "MessageCountFromTo";
	static final String KEY_SIZE = "TransferredDataFromToInB";

	private final AgentInteractionStatistic messageCounts;
	private final AgentInteractionStatistic transferredBytes;

	/** Create a new {@link FullTracking} using given {@link AgentInteractionStatistic}s */
	FullTracking(AgentInteractionStatistic messageCounts, AgentInteractionStatistic transferredBytes) {
		this.messageCounts = messageCounts;
		this.transferredBytes = transferredBytes;
	}

	/** Create a new {@link FullTracking} */
	public FullTracking() {
		this(new AgentInteractionStatistic(KEY_COUNT), new AgentInteractionStatistic(KEY_SIZE));
	}

	@Override
	public void assessMessage(ProtoMessage message) {
		long senderId = message.getSenderId();
		long receiverId = message.getReceiverId();
		long sizeInBytes = message.getSerializedSize();

		messageCounts.recordInteraction(senderId, receiverId, 1);
		transferredBytes.recordInteraction(senderId, receiverId, sizeInBytes);
	}

	@Override
	public String getStatsJson() {
		StringBuilder builder = new StringBuilder().append("{");
		builder.append(messageCounts.getJSON()).append(",");
		builder.append(transferredBytes.getJSON()).append("}");
		return builder.toString();
	}
}
