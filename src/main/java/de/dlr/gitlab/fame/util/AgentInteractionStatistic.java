// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.util;

import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

/** Tracks Long-type data related to interactions of two agents
 * 
 * @author Christoph Schimeczek */
public class AgentInteractionStatistic {
	private final String name;
	private final HashMap<Long, HashMap<Long, Long>> agentInteractions = new HashMap<>();

	/** Creates a new {@link AgentInteractionStatistic}
	 * 
	 * @param name of this statistic */
	public AgentInteractionStatistic(String name) {
		this.name = name;
	}

	/** add given value to any data previously recorded for the two specified agents
	 * 
	 * @param agentId the ID of an Agent
	 * @param otherAgentId the ID of another agent
	 * @param value to be added to any previously recorded recorded value for the two specified agents */
	public void recordInteraction(long agentId, long otherAgentId, long value) {
		agentInteractions.putIfAbsent(agentId, new HashMap<>());
		HashMap<Long, Long> receiverInteractions = agentInteractions.get(agentId);
		receiverInteractions.compute(otherAgentId, (__, v) -> v == null ? value : v + value);
	}

	/** Returns a JSON representation of the recorded data
	 * 
	 * @return JSON representation of the recorded data in the form <code>"name":{"agentId": {"otherAgentId": value, }}</code> with
	 *         agentIDs in increasing order */
	public String getJSON() {
		StringBuilder builder = new StringBuilder();
		builder.append("\"").append(name).append("\":{");
		for (Long agentId : getSortedKeys(agentInteractions)) {
			HashMap<Long, Long> receiverInteractions = agentInteractions.get(agentId);
			builder.append("\"").append(agentId).append("\":{");
			for (Long otherAgentId : getSortedKeys(receiverInteractions)) {
				long value = receiverInteractions.get(otherAgentId);
				builder.append("\"").append(otherAgentId).append("\":").append(value).append(",");
			}
			builder.deleteCharAt(builder.length() - 1).append("},");
		}
		if (agentInteractions.size() > 0) {
			builder.deleteCharAt(builder.length() - 1);
		}
		return builder.append("}").toString();
	}

	/** @return keys of given map sorted in ascending order */
	private <T> Set<Long> getSortedKeys(HashMap<Long, T> map) {
		return new TreeSet<Long>(map.keySet());
	}
}
