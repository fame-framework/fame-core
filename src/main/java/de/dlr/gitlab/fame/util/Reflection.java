// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.reflections.Reflections;
import org.reflections.ReflectionsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.AgentAbility;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.service.output.ComplexIndex;

/** Encapsulates reflections from org.reflections for a specified {@link ReflectionType type} of packages
 * 
 * @author Christoph Schimeczek, A. Achraf El Ghazi */
public class Reflection {
	static final String AGENT_PACKAGE_NAME = "de.dlr.gitlab.fame.agent";
	static final String MESSAGE_PACKAGE_NAME = "de.dlr.gitlab.fame.communication.message";
	static final String PORTABLE_PACKAGE_NAME = "de.dlr.gitlab.fame.time";

	/** Type of class a {@link Reflection} refers to */
	public enum ReflectionType {
		/** {@link Reflection} refers to classes derived from {@link Agent} or {@link AgentAbility} */
		AGENT,
		/** {@link Reflection} refers to classes derived from {@link DataItem} */
		COMMUNICATION,
		/** {@link Reflection} refers to classes derived from {@link Portable} */
		PORTABLE
	};

	public static final String ERR_ILLEGAL_PACKAGE = "Invalide package name: ";
	public static final String ERR_MODIFIERS_MISSING = " : ComplexIndex definitions must be 'static final' in class: ";
	public static final String ERR_FIELD_ACCESS = " : ComplexField could not be read of class: ";
	public static final String ERR_NOT_ENUM = ": is not an enum type, but was annotated with @";
	public static final String ERR_DOUBLE_DECLARATION = " is already used for another enum in class hierarchy of: ";
	public static final String ERR_MULTI_USE = ": annotation was used more than once in class: ";
	public static final String ERR_BROKEN_CODE = " enum could not be read. Check for compilation errors in enclosing class: ";

	public static final HashMap<ReflectionType, String> FAME_PACKAGES = new HashMap<>();
	static {
		FAME_PACKAGES.put(ReflectionType.AGENT, AGENT_PACKAGE_NAME);
		FAME_PACKAGES.put(ReflectionType.COMMUNICATION, MESSAGE_PACKAGE_NAME);
		FAME_PACKAGES.put(ReflectionType.PORTABLE, PORTABLE_PACKAGE_NAME);
	}

	private static Logger logger = LoggerFactory.getLogger(Reflection.class);
	private final ArrayList<Reflections> reflectionList = new ArrayList<>();

	/** Creates a {@link Reflection} for the specified list of package names; the given List is not modified; the FAME-specific
	 * package names are added to the given list, depending on the specified {@link ReflectionType}; if the given List is empty or
	 * null, only FAME's base packages are used
	 * 
	 * @param type of Reflection, select from {@link ReflectionType}
	 * @param givenPackageNames list of fully qualified package names to consider */
	public Reflection(ReflectionType type, List<String> givenPackageNames) {
		List<String> packageNames = new ArrayList<String>();
		packageNames.add(getFamePackageNames(type));
		if (givenPackageNames != null) {
			packageNames.addAll(givenPackageNames);
		}
		for (String packageName : packageNames) {
			if (packageName != null) {
				reflectionList.add(createReflections(packageName));
			}
		}
	}

	/** @return {@link Reflections} for given packageName
	 * @throws RuntimeException if scan for any Object within given packageName raises an Exception */
	private Reflections createReflections(String packageName) {
		Reflections reflections = new Reflections(packageName);
		try {
			reflections.getSubTypesOf(Object.class);
		} catch (ReflectionsException e) {
			throw Logging.logFatalException(logger, ERR_ILLEGAL_PACKAGE + packageName);
		}
		return reflections;
	}

	/** @return number of considered individual reflections based in package names during initialisation */
	public int getPackageCount() {
		return reflectionList.size();
	}

	/** Get name of default package in FAME for given {@link ReflectionType}
	 * 
	 * @param type of Reflection to get the FAME default package name for
	 * @return path to FAME's packages depending on the specified {@link ReflectionType} */
	public static String getFamePackageNames(ReflectionType type) {
		return FAME_PACKAGES.get(type);
	}

	/** Returns map for the {@link Class#getSimpleName() SimpleName} of all child classes in hierarchy to their given class type
	 * 
	 * @param <T> type of class
	 * @param classType parent class to search children for down through the class hierarchy
	 * @return HashMap of all child-classes of Agent mapping the classes SimpleName to the class */
	public <T> HashMap<String, Class<? extends T>> mapChildrenToClassName(Class<T> classType) {
		HashMap<String, Class<? extends T>> classNameToClass = new HashMap<>();
		Set<Class<? extends T>> allClassesExtendingClassType = getSubTypesOf(classType);
		for (Class<? extends T> clas : allClassesExtendingClassType) {
			Class<? extends T> classObject = clas;
			classNameToClass.put(classObject.getSimpleName(), classObject);
		}
		return classNameToClass;
	}

	/** Returns set of all classes in hierarchy that are a subtype of the specified class in the packages of this {@link Reflection}
	 * 
	 * @param <T> type of parent class
	 * @param classType parent class of which to find children
	 * @return Set of all child classes in the considered packages */
	public <T> Set<Class<? extends T>> getSubTypesOf(Class<T> classType) {
		Set<Class<? extends T>> subTypes = new HashSet<Class<? extends T>>();
		for (Reflections reflections : reflectionList) {
			subTypes.addAll(reflections.getSubTypesOf(classType));
		}
		return subTypes;
	}

	/** Returns all non-abstract child classes of the specified class type in the packages of this {@link Reflection}
	 * 
	 * @param <T> parent class type
	 * @param classType parent class to search for instantiable children
	 * @return list of all non-abstract child classes of the specified class */
	public <T> ArrayList<Class<? extends T>> findAllInstantiableChildrenOf(Class<T> classType) {
		ArrayList<Class<? extends T>> instantiableChilds = new ArrayList<>();
		Set<Class<? extends T>> allClassesExtendingClassType = getSubTypesOf(classType);
		for (Class<? extends T> classObject : allClassesExtendingClassType) {
			if (!Modifier.isAbstract(classObject.getModifiers())) {
				instantiableChilds.add(classObject);
			}
		}
		return instantiableChilds;
	}

	/** Returns a {@link HashMap} that maps
	 * <ul>
	 * <li>each child of the specified class type considered in this {@link Reflection}</li>
	 * <li>to a Set of all {@link Enum}s annotated with the given annotationClass and defined in
	 * <ul>
	 * <li>this class,</li>
	 * <li>any of its parent classes,</li>
	 * <li>any direct interface (but not its interface parents) extending {@link AgentAbility} on this class or any of its parent
	 * classes</li>
	 * </ul>
	 * </ul>
	 * 
	 * @param <T> type extracted from parent class
	 * @param annotationClass type of annotation that enum(s) must be annotated with to be found
	 * @param parentClass whose children are to be inspected
	 * @return HashMap matching each child class to all enums with given annotation defined in this class, any super class, or
	 *         direct interfaces of this class or of any super class */
	public <T> HashMap<Class<? extends T>, List<Enum<?>>> findAnnotatedEnumsInHierarchyForChildrenOf(
			Class<? extends Annotation> annotationClass, Class<T> parentClass) {
		HashMap<Class<?>, ArrayList<Enum<?>>> annotatedEnumsInClass = mapAnnotatedEnumsToEnclosingClass(annotationClass);
		Set<Class<? extends T>> classesInHierarchy = getSubTypesOf(parentClass);
		return joinEnumsByHierarchy(annotatedEnumsInClass, classesInHierarchy);
	}

	/** @return HashMap mapping a List of all {@link Enum}s with given {@link Annotation} to their enclosing class */
	private HashMap<Class<?>, ArrayList<Enum<?>>> mapAnnotatedEnumsToEnclosingClass(
			Class<? extends Annotation> annotation) {
		HashMap<Class<?>, ArrayList<Enum<?>>> annotatedEnumsInClass = new HashMap<>();
		for (Class<? extends Enum<?>> enumType : getEnumTypesAnnotatedWith(annotation)) {
			Class<?> enclosingClass = enumType.getEnclosingClass();
			if (annotatedEnumsInClass.containsKey(enclosingClass)) {
				throw Logging.logFatalException(logger,
						annotation.getSimpleName() + ERR_MULTI_USE + enclosingClass.getSimpleName());
			}
			annotatedEnumsInClass.put(enclosingClass, getEnumConstants(enumType));
		}
		return annotatedEnumsInClass;
	}

	/** @return {@link Set} of Enum types within packages covered by this {@link Reflection} that are annotated with the given
	 *         annotation
	 * @throws RuntimeException if any annotated type is not {@link Enum} */
	@SuppressWarnings("unchecked")
	private List<Class<? extends Enum<?>>> getEnumTypesAnnotatedWith(Class<? extends Annotation> annotation) {
		List<Class<? extends Enum<?>>> annotatedEnums = new ArrayList<Class<? extends Enum<?>>>();
		for (Reflections reflections : reflectionList) {
			for (Class<?> clas : reflections.getTypesAnnotatedWith(annotation)) {
				if (!clas.isEnum()) {
					throw Logging.logFatalException(logger, clas.getSimpleName() + ERR_NOT_ENUM + annotation.getSimpleName());
				}
				annotatedEnums.add((Class<? extends Enum<?>>) clas);
			}
		}
		return annotatedEnums;
	}

	/** @return list of all constants of the given enum type */
	private ArrayList<Enum<?>> getEnumConstants(Class<? extends Enum<?>> enumType) {
		ArrayList<Enum<?>> enumConstants = new ArrayList<>();
		Enum<?>[] enumEntries = enumType.getEnumConstants();
		if (enumEntries == null) {
			String message = enumType.getSimpleName() + ERR_BROKEN_CODE + enumType.getEnclosingClass().getCanonicalName();
			throw Logging.logFatalException(logger, message);
		}
		for (Enum<?> entry : enumEntries) {
			enumConstants.add(entry);
		}
		return enumConstants;
	}

	/** Joins all given {@link ArrayList}s of {@link Enum}s based on the hierarchy of the {@link Set} of given classes;<br />
	 * All given enums defined in super classes are added to the list of their child classes.
	 * 
	 * @return HashMap of classes in hierarchy mapped to the joined ArrayLists of Enums based of the class hierarchy */
	private <T> HashMap<Class<? extends T>, List<Enum<?>>> joinEnumsByHierarchy(
			HashMap<Class<?>, ArrayList<Enum<?>>> enumsByEnclosingClass, Set<Class<? extends T>> classesToConsider) {
		HashMap<Class<? extends T>, List<Enum<?>>> enumsOfClassAndParents = new HashMap<>();
		for (Class<? extends T> clas : classesToConsider) {
			List<Enum<?>> enums = new ArrayList<Enum<?>>();
			getAllEnumsOfClassAndSuperClasses(clas, enumsByEnclosingClass, enums);
			enumsOfClassAndParents.put(clas, enums);
		}
		return enumsOfClassAndParents;
	}

	/** adds all Enum entries of a given class and, recursively, all its super-classes to the given ArrayList */
	private <T> void getAllEnumsOfClassAndSuperClasses(Class<? extends T> currentClass,
			HashMap<Class<?>, ArrayList<Enum<?>>> enumsByEnclosingClass, List<Enum<?>> enums) {
		addEnumsOfClass(currentClass, enumsByEnclosingClass, enums);
		Class<?> superClass = currentClass.getSuperclass();
		if (superClass != Object.class) {
			getAllEnumsOfClassAndSuperClasses(superClass, enumsByEnclosingClass, enums);
		}
		for (Class<?> directInterface : currentClass.getInterfaces()) {
			if (AgentAbility.class.isAssignableFrom(directInterface)) {
				addEnumsOfClass(directInterface, enumsByEnclosingClass, enums);
			}
		}
	}

	/** adds all Enum entries of given class (read from given map) to the given enum list if not yet present
	 * 
	 * @throws RuntimeException if a constant from a different enum with similar name to an given enum is added */
	private <T> void addEnumsOfClass(Class<? extends T> currentClass,
			HashMap<Class<?>, ArrayList<Enum<?>>> enumsByEnclosingClass, List<Enum<?>> enums) {
		ArrayList<Enum<?>> ownEnums = enumsByEnclosingClass.getOrDefault(currentClass, new ArrayList<>(0));
		addingLoop: for (Enum<?> enumType : ownEnums) {
			String name = enumType.name();
			for (Enum<?> enumInList : enums) {
				if (name.equals(enumInList.name()) && enumInList != enumType) {
					throw Logging.logFatalException(logger, enumType + ERR_DOUBLE_DECLARATION + currentClass.getSimpleName());
				} else if (enumInList == enumType) {
					continue addingLoop;
				}
			}
			enums.add(enumType);
		}
	}

	/** For all children of {@link Agent}, returns map of class names to a set of all ComplexIndex created in
	 * <ul>
	 * <li>this class,</li>
	 * <li>any of its parent classes,</li>
	 * <li>any direct interface (but not its interface parents) extending {@link AgentAbility} on this class or any of its parent
	 * classes</li>
	 * </ul>
	 *
	 * @return HashMap of Agent class name to Set of ComplexIndex */
	public HashMap<String, List<ComplexIndex<? extends Enum<?>>>> findComplexIndexHierarchyForAgents() {
		Set<Class<? extends Agent>> classesInHierarchy = getSubTypesOf(Agent.class);
		HashMap<String, List<ComplexIndex<? extends Enum<?>>>> result = new HashMap<>();
		for (Class<? extends Agent> clas : classesInHierarchy) {
			List<ComplexIndex<? extends Enum<?>>> indices = new ArrayList<>();
			result.put(clas.getSimpleName(), indices);
			addComplexIndicesOfClassAndSuperClasses(clas, indices);
		}
		return result;
	}

	/** Adds ComplexIndexes to the give List, that are created in the given Agent class, one of its super classes, or any direct
	 * interface of this class or of any super class (but not interface ancestors)
	 * 
	 * @param clas (and its super classes) and their interfaces to search for ComplexIndex definitions
	 * @param indices list to add the found ComplexIndexes to */
	@SuppressWarnings("unchecked")
	private void addComplexIndicesOfClassAndSuperClasses(
			Class<? extends Agent> clas, List<ComplexIndex<? extends Enum<?>>> indices) {
		addComplexIndicesOfClass(clas, indices);
		Class<?> superClass = clas.getSuperclass();
		if (Agent.class.isAssignableFrom(superClass)) {
			addComplexIndicesOfClassAndSuperClasses((Class<? extends Agent>) superClass, indices);
		}
		for (Class<?> directInterface : clas.getInterfaces()) {
			if (AgentAbility.class.isAssignableFrom(directInterface)) {
				addComplexIndicesOfClass(directInterface, indices);
			}
		}
	}

	/** Adds any complex index defined in given class to given set of {@link ComplexIndex}es - ignore indexes already present */
	private void addComplexIndicesOfClass(Class<?> clas, List<ComplexIndex<? extends Enum<?>>> indices) {
		for (Field field : clas.getDeclaredFields()) {
			if (ComplexIndex.class.isAssignableFrom(field.getType())) {
				ComplexIndex<?> complexIndex = getComplexIndexFromField(clas, field);
				if (!indices.contains(complexIndex)) {
					indices.add(complexIndex);
				}
			}
		}
	}

	/** @return the ComplexIndex defined in the given static field */
	@SuppressWarnings("unchecked")
	private ComplexIndex<?> getComplexIndexFromField(Class<?> clas, Field field) {
		int fieldModifiers = field.getModifiers();
		if (!Modifier.isFinal(fieldModifiers) || !Modifier.isStatic(fieldModifiers)) {
			throw new RuntimeException(field.getName() + ERR_MODIFIERS_MISSING + clas.getSimpleName());
		}

		try {
			field.setAccessible(true);
			return (ComplexIndex<? extends Enum<?>>) field.get(null);
		} catch (IllegalAccessException | RuntimeException e) {
			throw new RuntimeException(field.getName() + ERR_FIELD_ACCESS + clas.getSimpleName(), e);
		}
	}
}