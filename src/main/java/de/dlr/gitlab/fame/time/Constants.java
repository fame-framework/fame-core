// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.time;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/** Time constants
 * 
 * @author Christoph Schimeczek */
public final class Constants {
	/** Pre-defined time intervals in fame */
	public static enum Interval {
		/** in seconds */
		SECONDS,
		/** in minutes */
		MINUTES,
		/** in hours */
		HOURS,
		/** in days */
		DAYS,
		/** in weeks */
		WEEKS,
		/** in months */
		MONTHS,
		/** in years */
		YEARS
	};

	/** Number of simulation steps corresponding to one real-world second */
	static final int STEPS_PER_SECOND = 1;
	/** Reference year Zero for FAME {@link TimeStamp}s */
	static final int START_YEAR = 2000;
	/** Date-time of the reference time (time=Zero) in FAME */
	static final ZonedDateTime START_IN_REAL_TIME = ZonedDateTime.of(START_YEAR, 1, 1, 0, 0, 0, 0,
			ZoneId.of("UTC+01:00"));
	/** Formatter for {@link TimeStamp}s in FAME */
	static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	/** Definition: 60 seconds per minute */
	public static final int SECONDS_PER_MINUTE = 60;
	/** Definition: 60 minutes per hour */
	public static final int MINUTES_PER_HOUR = 60;
	/** Definition: 24 hours per day */
	public static final int HOURS_PER_DAY = 24;
	/** Definition: 7 days per week */
	public static final int DAYS_PER_WEEK = 7;
	/** Definition: 365 days per year */
	public static final int DAYS_PER_YEAR = 365;
	/** Definition: 12 months per year */
	public static final int MONTHS_PER_YEAR = 12;

	/** Definition: 168 hours per week */
	public static final int HOURS_PER_WEEK = HOURS_PER_DAY * DAYS_PER_WEEK;
	/** Definition: 8760 hours per year */
	public static final int HOURS_PER_NORM_YEAR = HOURS_PER_DAY * DAYS_PER_YEAR;
	/** Definition: 730 hours per month */
	public static final int HOURS_PER_NORM_MONTH = HOURS_PER_NORM_YEAR / MONTHS_PER_YEAR;

	/** Definition: 60 time steps per minute */
	static final long STEPS_PER_MINUTE = STEPS_PER_SECOND * SECONDS_PER_MINUTE;
	/** Definition: 3,600 time steps per hour */
	public static final long STEPS_PER_HOUR = STEPS_PER_MINUTE * MINUTES_PER_HOUR;
	/** Definition: 86,400 time steps per day */
	public static final long STEPS_PER_DAY = STEPS_PER_HOUR * HOURS_PER_DAY;
	/** Definition: 604,800 time steps per week */
	static final long STEPS_PER_WEEK = STEPS_PER_DAY * DAYS_PER_WEEK;
	/** Definition: 2,628,000 time steps per month */
	static final long STEPS_PER_MONTH = STEPS_PER_HOUR * HOURS_PER_NORM_MONTH;
	/** Definition: 31,536,000 time steps per year */
	static final long STEPS_PER_YEAR = STEPS_PER_MONTH * MONTHS_PER_YEAR;

	/** Returns simulation step equivalent of specified simulation time {@link Interval}
	 * 
	 * @return simulation step equivalent of specified simulation time {@link Interval} */
	static long calcPeriodInSteps(Interval period) {
		switch (period) {
			case SECONDS:
				return STEPS_PER_SECOND;
			case MINUTES:
				return STEPS_PER_MINUTE;
			case HOURS:
				return STEPS_PER_HOUR;
			case DAYS:
				return STEPS_PER_DAY;
			case WEEKS:
				return STEPS_PER_WEEK;
			case MONTHS:
				return STEPS_PER_MONTH;
			case YEARS:
				return STEPS_PER_YEAR;
			default:
				throw new RuntimeException("Period " + period + " not implemented.");
		}
	}
}