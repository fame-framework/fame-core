// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.time;

import java.time.ZonedDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.logging.Logging;

/** Immutable representation of a point in simulation time
 * 
 * @author Ulrich Frey, Benjamin Fuchs, Christoph Schimeczek */
public final class TimeStamp implements Comparable<TimeStamp>, Portable {
	static final String ERROR_INVALID = "You constructed an invalid TimeStamp! Only use Constructor TimeStamp(long)!";
	static final String ERR_NOT_POSITIVE = "Multipliers must be a positive integer!";
	private static Logger logger = LoggerFactory.getLogger(TimeStamp.class);

	/** Represents the latest time that can be addressed in the simulation, i.e. July 14th 292,471,210,677 13h54m8s */
	public static final TimeStamp LATEST = new TimeStamp(Long.MAX_VALUE);
	private long step;
	boolean isValid = true;

	/** Constructs a {@link TimeStamp} that represents the specified number of simulation steps
	 * 
	 * @param step numeric representation of simulation time */
	public TimeStamp(long step) {
		this.step = step;
	}

	/** <b>Do not use this</b> -- required for use in {@link Portable}s */
	public TimeStamp() {
		isValid = false;
	};

	/** <b>Do not use this</b> -- required for use in {@link Portable}s */
	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeLongs(getStep());
	}

	/** <b>Do not use this</b> -- required for use in {@link Portable}s */
	@Override
	public void populate(ComponentProvider provider) {
		step = provider.nextLong();
		isValid = true;
	}

	/** Returns the number if simulation steps equivalent to this {@link TimeStamp}
	 * 
	 * @return simulation time step */
	public long getStep() {
		if (!isValid) {
			throw new RuntimeException(ERROR_INVALID);
		}
		return step;
	}

	/** Return {@link ZonedDateTime} corresponding to the represented <b>real-world</b> time
	 * 
	 * @return
	 *         <ul>
	 *         <li><b>Months</b>: Real-world months have different lengths than "norm months", which are used internally</li>
	 *         <li>In <b>leap years</b>: December 31st is missing, since years are always 365 days long</li>
	 *         <li><b>Daylight saving time</b>: Extra hours / missing hours are not accounted for -- days are always 24h long</li>
	 *         </ul>
	 */
	private ZonedDateTime getRealTime() {
		long stepInYear = getStep() % Constants.STEPS_PER_YEAR;
		long yearOffset = getStep() / Constants.STEPS_PER_YEAR;
		return Constants.START_IN_REAL_TIME.plusYears(yearOffset).plusSeconds(stepInYear / Constants.STEPS_PER_SECOND);
	}

	/** Returns formatted <b>real-world</b> time correspondence of this simulation time.<br>
	 * **Warning**: FAME uses norm months of 730 hours, ignores leap years and daylight saving time. However, a real-world calendar
	 * is use to depict the this TimeStamp. Thus, the actual time of day / actual month within the simulation may deviate from the
	 * returned content. To get a correct representation of internal time, use {@link #toString()}
	 * 
	 * @return see also {@link #getRealTime()} */
	public String toCalendar() {
		return Constants.DATE_TIME_FORMATTER.format(getRealTime());
	}

	/** Returns correct internal representation of TimeStamp, see {@link Formatter#formatTimeStamp(long)}
	 * 
	 * @return formatted String */
	@Override
	public String toString() {
		return Formatter.formatTimeStamp(getStep());
	}

	/** Returns new {@link TimeStamp} shifted towards the future by the given {@link TimeSpan}
	 * 
	 * @param timeSpan duration for which this TimeStamp is to be shifted forward
	 * @return a new {@link TimeStamp} shifted towards the future by the given {@link TimeSpan}; this TimeStamp is not modified */
	public TimeStamp laterBy(TimeSpan timeSpan) {
		return laterBy(timeSpan.getSteps());
	}

	/** Get new {@link TimeStamp} shifted in time;<br>
	 * <i>positive</i> values: shift towards future<br>
	 * <i>negative</i> values: shift towards past
	 * 
	 * @param delta simulation time steps to shift the TimeStamp by
	 * @return new TimeStamp shifted by the given step delta */
	private TimeStamp laterBy(Long delta) {
		return new TimeStamp(getStep() + delta);
	}

	/** Returns new {@link TimeStamp} shifted towards the future 'multiplier' times the given {@link TimeSpan}
	 * 
	 * @param timeSpan the base TimeSpan
	 * @param multiplier the number of times the timeSpan is taken to shift the TimeStamp
	 * @return new shifted TimeStamp */
	public TimeStamp laterBy(TimeSpan timeSpan, int multiplier) {
		ensurePositive(multiplier);
		return laterBy(timeSpan.getSteps() * multiplier);
	}

	/** @throws IllegalArgumentException if given value is not positive */
	private void ensurePositive(int value) {
		if (value < 1) {
			Logging.logAndThrowFatal(logger, new IllegalArgumentException(ERR_NOT_POSITIVE));
		}
	}

	/** Returns a {@link TimeStamp} shifted towards the past by the given 'multiplier' times the given {@link TimeSpan}
	 * 
	 * @param timeSpan the base TimeSpan
	 * @param multiplier the number of times the TimeSpan is taken to shift the TimeStamp
	 * @return new shifted TimeStamp */
	public TimeStamp earlierBy(TimeSpan timeSpan, int multiplier) {
		ensurePositive(multiplier);
		return laterBy((-1) * timeSpan.getSteps() * multiplier);
	}

	/** Returns a new {@link TimeStamp} shifted towards the future by one time unit
	 * 
	 * @return new {@link TimeStamp} shifted towards the future by one time unit */
	public TimeStamp laterByOne() {
		return laterBy(1L);
	}

	/** Returns a new {@link TimeStamp} shifted towards the past by the given {@link TimeSpan}
	 * 
	 * @param timeSpan to shift towards the past
	 * @return a new {@link TimeStamp} shifted towards the past by the given {@link TimeSpan}; this TimeStamp is not modified */
	public TimeStamp earlierBy(TimeSpan timeSpan) {
		return laterBy(-timeSpan.getSteps());
	}

	/** Returns a new {@link TimeStamp} shifted towards the past by one time unit
	 * 
	 * @return new {@link TimeStamp} shifted towards the past by one time unit */
	public TimeStamp earlierByOne() {
		return laterBy(-1L);
	}

	/** Returns true if this {@link TimeStamp} represents an earlier time or time equal to the other {@link TimeStamp}
	 * 
	 * @param other TimeStamp to compare this one to
	 * @return true if this TimeStamp represents an earlier time or time equal to the other TimeStamp */
	public boolean isLessEqualTo(TimeStamp other) {
		return getStep() <= other.getStep();
	}

	/** Returns true if this {@link TimeStamp} represents an earlier time to the other {@link TimeStamp}
	 * 
	 * @param other TimeStamp to compare this one to
	 * @return true if this TimeStamp represents an earlier time to the other TimeStamp */
	public boolean isLessThan(TimeStamp other) {
		return getStep() < other.getStep();
	}

	/** Returns true if this {@link TimeStamp} represents a later time to the other {@link TimeStamp}
	 * 
	 * @param other TimeStamp to compare this one to
	 * @return true if this TimeStamp represents a later time to the other TimeStamp */
	public boolean isGreaterThan(TimeStamp other) {
		return getStep() > other.getStep();
	}

	/** Returns true if this {@link TimeStamp} represents an equal or later time to the given {@link TimeStamp}
	 * 
	 * @param other TimeStamp to compare this one to
	 * @return true if this {@link TimeStamp} represents an equal or later time to the given {@link TimeStamp} */
	public boolean isGreaterEqualTo(TimeStamp other) {
		return getStep() >= other.getStep();
	}

	@Override
	public int compareTo(TimeStamp other) {
		return Long.compare(getStep(), other.getStep());
	}

	/** Returns the next {@link TimeStamp} matching the specified {@link TimeOfDay}; if this TimeStamp exactly matches the specified
	 * TimeOfDay, the next day's matching TimeStamp is returned
	 * 
	 * @param timeOfDay to be matched by the returned TimeStamp
	 * @return next {@link TimeStamp} matching the specified {@link TimeOfDay} */
	public TimeStamp nextTimeOfDay(TimeOfDay timeOfDay) {
		long stepsOfToday = getStep() % Constants.STEPS_PER_DAY;
		long beginningOfToday = getStep() - stepsOfToday;
		long stepsOfDayTime = timeOfDay.getSteps();
		if (stepsOfToday >= stepsOfDayTime) {
			long beginningOfTomorrow = beginningOfToday + Constants.STEPS_PER_DAY;
			return new TimeStamp(beginningOfTomorrow + stepsOfDayTime);
		} else {
			return new TimeStamp(beginningOfToday + stepsOfDayTime);
		}
	}

	/** Returns a {@link Object#hashCode()} for this {@link TimeStamp}; <b>Important:</b> {@link #equals(Object) matching}
	 * TimeStamps yield <b>the same</b> hashCode
	 * 
	 * @return hashCode for this TimeStamp */
	@Override
	public int hashCode() {
		return Long.hashCode(getStep());
	}

	/** Return true if this {@link TimeStamp} represents the same simulation time as the specified TimeStamp
	 * 
	 * @return true if this {@link TimeStamp} represents the same simulation time as the specified TimeStamp */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TimeStamp) {
			final TimeStamp other = (TimeStamp) obj;
			return getStep() == other.getStep();
		}
		return false;
	}
}