// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.time;

import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.time.Constants.Interval;
import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.logging.Logging;

/** Immutable representation of a duration of simulation time
 * 
 * @author Christoph Schimeczek */
public class TimeSpan implements Portable {
	static final String ERR_NOT_POSITIVE = "TimeSpans can only be 0 or positive.";

	private static Logger logger = LoggerFactory.getLogger(TimeSpan.class);
	/** Number of time steps that this {@link TimeSpan} represents */
	protected long steps;

	/** <b>Do not use this</b> -- required for use in {@link Portable}s */
	public TimeSpan() {}

	/** Create {@link TimeSpan} based on specified number of simulation steps
	 * 
	 * @param steps long representation of TimeSpan in Steps */
	public TimeSpan(long steps) {
		ensureNotNegative(steps);
		this.steps = steps;
	}

	/** Create {@link TimeSpan} based on given simulation time {@link Interval}s
	 * 
	 * @param amount multiplier of interval duration; non-negative
	 * @param period predefined time {@link Interval} */
	public TimeSpan(long amount, Interval period) {
		ensureNotNegative(amount);
		this.steps = amount * Constants.calcPeriodInSteps(period);
	}

	/** @throws IllegalArgumentException if the specified time span is not positive */
	private void ensureNotNegative(long timeStep) {
		if (timeStep < 0) {
			Logging.logAndThrowFatal(logger, new IllegalArgumentException(ERR_NOT_POSITIVE));
		}
	}

	/** Returns the number of simulations steps equivalent to this {@link TimeSpan}
	 * 
	 * @return number of simulations steps equivalent to this {@link TimeSpan} */
	public long getSteps() {
		return steps;
	}

	/** Returns new {@link TimeSpan} equal to the sum of the given TimeSpans
	 * 
	 * @param spans 0..N {@link TimeSpan}s of any duration that are to be summed up
	 * @return new {@link TimeSpan} equal to the sum of the given TimeSpans */
	public static TimeSpan combine(TimeSpan... spans) {
		long newSteps = 0;
		for (TimeSpan span : spans) {
			newSteps += span.steps;
		}
		return new TimeSpan(newSteps);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TimeSpan) {
			final TimeSpan other = (TimeSpan) obj;
			return this.steps == other.steps;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(steps);
	}

	/** Returns String representation of TimeSpan, see {@link Formatter#formatTimeSpan(long)}
	 * 
	 * @return formatted String */
	@Override
	public String toString() {
		return Formatter.formatTimeSpan(steps);
	}

	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeLongs(steps);
	}

	@Override
	public void populate(ComponentProvider provider) {
		steps = provider.nextLong();
	}
}