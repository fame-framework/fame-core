// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.time;

import java.util.Objects;
import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;

/** Immutable representation of a simulation time period defined by start moment and duration
 *
 * @author Christoph Schimeczek */
public class TimePeriod implements Comparable<TimePeriod>, Portable {
	static final String UNALLOWED_COMPARISON = "TimePeriods with different durations may not be compared!";

	private TimeStamp firstValidTime;
	private TimeSpan duration;
	private TimeStamp lastValidTime;

	/** <b>Do not use this</b> -- required for use in {@link Portable}s */
	public TimePeriod() {}

	/** Creates a new TimePeriod
	 * 
	 * @param firstValidTime the first moment of simulation time that falls into this TimePeriod
	 * @param duration the TimeSpan for how long this TimePeriod lasts */
	public TimePeriod(TimeStamp firstValidTime, TimeSpan duration) {
		this.firstValidTime = firstValidTime;
		this.duration = duration;
		this.lastValidTime = calcLastValidTime(firstValidTime, duration);
	}

	private TimeStamp calcLastValidTime(TimeStamp firstValidTime, TimeSpan duration) {
		return new TimeStamp(firstValidTime.getStep() + duration.getSteps() - 1L);
	}

	/** Returns first valid {@link TimeStamp} marking the start of this {@link TimePeriod}
	 * 
	 * @return first valid {@link TimeStamp} */
	public TimeStamp getStartTime() {
		return firstValidTime;
	}

	/** Returns last {@link TimeStamp} <b>included in</b> this {@link TimePeriod}.
	 * 
	 * @return last valid {@link TimeStamp} marking the end of this {@link TimePeriod} */
	public TimeStamp getLastTime() {
		return lastValidTime;
	}

	/** Returns the duration of this {@link TimePeriod}
	 * 
	 * @return {@link TimeSpan} that equals the duration of this {@link TimePeriod} */
	public TimeSpan getDuration() {
		return duration;
	}

	/** Creates a new TimePeriod, shifted by its duration by the given count.<br>
	 * Example: If shiftByDuration(2) is called on a TimePeriod starting at 2 o'clock with a duration of 15 minutes, the resulting
	 * TimePeriod would start at 2:30. If count equals zero, this object is returned.
	 * 
	 * @param n <i>zero or positive integer</i>, number of times {@link #firstValidTime} is shifted by duration towards the future
	 * @return new {@link TimePeriod} that is shifted by the duration n times */
	public TimePeriod shiftByDuration(int n) {
		if (n == 0) {
			return this;
		}
		final TimeSpan timeSpan = new TimeSpan(duration.getSteps() * n);
		final TimeStamp newStartTime = firstValidTime.laterBy(timeSpan);
		return new TimePeriod(newStartTime, duration);
	}

	/** Compares two TimePeriod. Returns a negative integer, zero, or positive integer as this TimePeriod starts before, at the same
	 * time step or after the specified TimePeriod. Note that both TimePeriods <b>must</b> have the same duration, otherwise they
	 * are not comparable. One can test their comparability with {@link #isComparableTo(TimePeriod)}.
	 * 
	 * @param other the TimePeriod to compare this one with
	 * @return a negative integer, zero, or positive integer as this TimePeriod starts before, at the same time step or after the
	 *         specified TimePeriod
	 * @throws RuntimeException if the duration of the two TimePeriods does not match */
	@Override
	public final int compareTo(TimePeriod other) {
		if (!isComparableTo(other)) {
			throw new RuntimeException(UNALLOWED_COMPARISON);
		}
		return this.firstValidTime.compareTo(other.firstValidTime);
	}

	/** Tells whether the given TimePeriod can be compared with this one
	 * 
	 * @param other TimePeriod tested for comparability
	 * @return true when both TimePeriods match in duration, false otherwise */
	public final boolean isComparableTo(TimePeriod other) {
		return this.duration.equals(other.duration);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof TimePeriod) {
			final TimePeriod other = (TimePeriod) object;
			return isComparableTo(other) && firstValidTime.equals(other.firstValidTime);
		}
		return false;
	}

	@Override
	public String toString() {
		return firstValidTime + " - " + lastValidTime;
	}

	@Override
	public int hashCode() {
		return Objects.hash(firstValidTime, duration);
	}

	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeLongs(firstValidTime.getStep());
		collector.storeLongs(duration.getSteps());
	}

	@Override
	public void populate(ComponentProvider provider) {
		this.firstValidTime = new TimeStamp(provider.nextLong());
		this.duration = new TimeSpan(provider.nextLong());
		this.lastValidTime = calcLastValidTime(firstValidTime, duration);
	}
}