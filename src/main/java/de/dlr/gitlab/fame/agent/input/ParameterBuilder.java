// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import de.dlr.gitlab.fame.agent.input.Make.Type;
import de.dlr.gitlab.fame.util.Util;

/** Builds specifications for a single input {@link Parameter}
 *
 * @author Christoph Schimeczek */
public final class ParameterBuilder extends ElementBuilder {
	static final String ENUM_REQUIRED = "It is required to call setEnum() since DataType.ENUM is selected for Parameter: ";
	static final String DATA_TYPE_NEEDED = "DataType must be defined but was null.";
	static final String LIST_DISALLOWED = "List not supported for Type: ";

	private String defaultValue;
	private String comment;
	private Type dataType;
	private Enum<?>[] valueList;

	ParameterBuilder setType(Type dataType) {
		Util.ensureNotNull(dataType, DATA_TYPE_NEEDED);
		this.dataType = dataType;
		return this;
	}

	/** @return data type of the Parameter represented by this {@link ParameterBuilder} */
	Type getType() {
		return dataType;
	}

	/** {@inheritDoc}
	 * 
	 * @throws RuntimeException for {@link #dataType} timeseries */
	@Override
	protected void checkListAbility() {
		if (dataType == Type.TIMESERIES) {
			throw new RuntimeException(LIST_DISALLOWED + dataType);
		}
	}

	/** Sets this Parameter to fill in a default value for configuration in e.g. FAME-Io.<br>
	 * The given String is not type-checked.<br>
	 * No default is provided if this method is not called or if given String is null or empty.
	 * 
	 * @param defaultValue String to be used as default during configuration
	 * @return the ParameterBuilder of which this method was call from */
	public ParameterBuilder fill(String defaultValue) {
		this.defaultValue = defaultValue;
		return this;
	}

	/** Sets this Parameter to provide a help text for configuration e.g. in FAME-Gui.<br>
	 * No help text is provided if this method is not called or if given String is null or empty.
	 * 
	 * @param helpText to be shown during configuration
	 * @return the ParameterBuilder of which this method was call from */
	public ParameterBuilder help(String helpText) {
		comment = helpText;
		return this;
	}

	/** <b>Must</b> be called if this Parameter is of type {@link Type#ENUM} - Must <b>not</b> be called otherwise.<br>
	 * Defines which Enum values are valid for this parameter.<br>
	 * This also restricts the options during configuration with FAME-Io or FAME-Gui.
	 * 
	 * @param <T> any Enum can be used
	 * @param type the class of the Enum to be used for this Parameter
	 * @return the ParameterBuilder of which this method was call from */
	<T extends Enum<T>> ParameterBuilder setEnum(Class<T> type) {
		valueList = type.getEnumConstants();
		return this;
	}

	Enum<?>[] getAllowedValues() {
		return valueList;
	}

	@Override
	protected Parameter build() {
		Parameter parameter = new Parameter();
		super.setAttributes(parameter);
		parameter.setDefaultValue(defaultValue);
		parameter.setComment(comment);
		parameter.setDataType(dataType);
		if (dataType == Type.ENUM && valueList == null) {
			throw new RuntimeException(ENUM_REQUIRED + parameter.getAddress());
		}
		parameter.setAllowedValues(valueList);
		return parameter;
	}

	@Override
	protected ParameterBuilder setName(String name) {
		return (ParameterBuilder) super.setName(name);
	}

	@Override
	public ParameterBuilder list() {
		return (ParameterBuilder) super.list();
	}

	@Override
	public ParameterBuilder optional() {
		return (ParameterBuilder) super.optional();
	}
}
