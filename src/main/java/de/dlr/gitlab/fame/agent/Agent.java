// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.MessageManager.MessageManagementException;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.service.LocalServices;
import de.dlr.gitlab.fame.service.PostOffice;
import de.dlr.gitlab.fame.service.Scheduler;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.OutputBuffer;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Abstract representation of an autonomous agent in the simulation
 * 
 * @author Christoph Schimeczek, Marc Deissenroth */
public abstract class Agent implements AgentAbility {
	static final String ERR_UNHANDLED_MESSAGE = "Unhandled message from: ";
	static final String ERR_MESSAGES_MISSING = " is missing input messages to execute action: ";
	static final String ERR_NO_ACTION = " has no action for reason ";
	static final String LOG_EXECUTION = " executes contract action at ";
	static final String INFO_NO_FOLLOW_UP = " schedules no direct subsequent action(s) at ";
	private static final long MESSAGE_OVERFLOW_THRESHOLD = (long) 1E5;

	/** Tells upon the Agent's status during warm-up */
	public enum WarmUpStatus {
		/** Warm-up not yet completed */
		INCOMPLETE,
		/** No more warm-up iterations required */
		COMPLETED,
		/** No warm-up required at all */
		UNNECESSARY,
	};

	/** Logger for {@link Agent} class */
	protected static Logger logger = LoggerFactory.getLogger(Agent.class);
	private final long id;
	private final LocalServices localServices;
	private final ActionManager actionManager = new ActionManager();
	private final MessageManager messageManager = new MessageManager(MESSAGE_OVERFLOW_THRESHOLD);
	/** Manages all of the Agent's contracts */
	protected final ContractManager contractManager;
	private OutputBuffer outputBuffer;

	/** Construct an agent from the specified data provider object
	 * 
	 * @param dataProvider input for this agent read from file */
	public Agent(DataProvider dataProvider) {
		id = dataProvider.getAgentId();
		contractManager = new ContractManager(id);
		localServices = dataProvider.getLocalServices();
		outputBuffer = localServices.registerAgent(this);
	}

	@Override
	public final long getId() {
		return id;
	}

	/** Add a {@link Contract} to this {@link Agent}'s list of Contracts
	 * 
	 * @param contract associated with this agent */
	public final void addContract(Contract contract) {
		contractManager.addContract(contract);
		messageManager.registerContract(contract);
	}

	@Override
	public final void store(Enum<?> column, double value) {
		outputBuffer.store(column, value);
	}

	@Override
	public final void store(ComplexIndex<?> complexField, Object value) {
		outputBuffer.store(complexField, value);
	}

	/** Internal FAME function - <b>do not call</b> in simulations!<br>
	 * Trigger an action of this agent at a given {@link TimeStamp}.
	 * 
	 * @param reasons for why this Agent is being scheduled */
	public final void executeActions(List<Enum<?>> reasons) {
		TimeStamp currentTime = localServices.getCurrentTime();
		for (Enum<?> reason : reasons) {
			executeAction(reason, currentTime);
		}
		ArrayList<PlannedAction> nextActions = contractManager.updateAndGetNextContractActions(currentTime);
		transmitPlannedActionsToScheduler(currentTime, nextActions);
	}

	/** Executes an Action for a specific scheduling reason at the current time */
	private final void executeAction(Enum<?> reason, TimeStamp currentTime) {
		BiConsumer<ArrayList<Message>, List<Contract>> action = actionManager.getActionFor(reason);
		if (action == null) {
			throw new RuntimeException(this + ERR_NO_ACTION + reason);
		}
		ArrayList<Message> inputMessages = getMessagesForTrigger(reason, currentTime);
		List<Contract> contractsToFulfil = contractManager.getContractsForProduct(reason);
		if (logger.isDebugEnabled()) {
			logger.debug(this + LOG_EXECUTION + currentTime);
		}
		action.accept(inputMessages, contractsToFulfil);
	}

	/** @return message list for given reason with times less than or equal to currentTime - empty if no messages are found */
	private ArrayList<Message> getMessagesForTrigger(Enum<?> reason, TimeStamp currentTime) {
		ArrayList<Message> inputMessages;
		try {
			inputMessages = messageManager.getMessagesForTrigger(reason, currentTime);
		} catch (MessageManagementException e) {
			logger.warn(this + ERR_MESSAGES_MISSING + reason);
			inputMessages = new ArrayList<Message>();
		}
		return inputMessages;
	}

	/** Reports given list of planned actions to {@link Scheduler}; issues a warning if no actions reported */
	private void transmitPlannedActionsToScheduler(TimeStamp currentTime, ArrayList<PlannedAction> nextActions) {
		if (nextActions.size() == 0) {
			logger.info(this + INFO_NO_FOLLOW_UP + currentTime);
		} else {
			for (PlannedAction plannedAction : nextActions) {
				localServices.addActionAt(this, plannedAction);
			}
		}
	}

	/** Internal FAME function - <b>do not call</b> in simulations!<br>
	 * Called by {@link PostOffice} to deliver a {@link Message} to this agent.<br>
	 * Messages regarding active {@link Contract}s are handled by the {@link MessageManager}
	 * 
	 * @param message to be received and handled */
	public final void receive(Message message) {
		message = messageManager.handleMessage(message);
		if (message != null) {
			handleMessage(message);
		}
	}

	/** Function to react to non-contract messages;<br>
	 * By default, such messages are not handled and an error is logged. Override this method to handle non-contract messages (i.e.
	 * that have no {@link ContractData} item).
	 * 
	 * @param message message to be handled */
	protected void handleMessage(Message message) {
		logger.error(ERR_UNHANDLED_MESSAGE + message.getSenderId());
	}

	/** Manually dispatch a non-contract {@link Message}. To send a message connected with a contract, use
	 * {@link #fulfilNext(Contract, DataItem...)} instead.
	 * 
	 * @param receiverId the targeted agent to receive this message
	 * @param dataItems the contents of this message */
	protected final void sendMessageTo(long receiverId, DataItem... dataItems) {
		localServices.sendMessage(id, receiverId, null, dataItems);
	}

	/** Manually dispatch a non-contract {@link Message}. To send a message connected with a contract, use
	 * {@link #fulfilNext(Contract, DataItem...)} instead.
	 * 
	 * @param receiverId the targeted agent to receive this message
	 * @param portable a single complex {@link Portable} content of this message
	 * @param dataItems the contents of this message */
	protected final void sendMessageTo(long receiverId, Portable portable, DataItem... dataItems) {
		localServices.sendMessage(id, receiverId, portable, dataItems);
	}

	@Override
	public final void fulfilNext(Contract contract, DataItem... dataItems) {
		fulfilNext(contract, null, dataItems);
	}

	@Override
	public final void fulfilNext(Contract contract, Portable portable, DataItem... dataItems) {
		DataItem[] extendedItems = new DataItem[dataItems.length + 1];
		extendedItems[0] = contract.fulfilAfter(now());
		System.arraycopy(dataItems, 0, extendedItems, 1, dataItems.length);
		sendMessageTo(contract.getReceiverId(), portable, extendedItems);
	}

	/** Internal FAME function - <b>do not call</b> in simulations!<br>
	 * 
	 * @return the {@link PostOffice} that handle's this agents messages */
	public final PostOffice getPostOffice() {
		return localServices.getPostOffice();
	}

	@Override
	public String toString() {
		return "Agent(" + getClass().getSimpleName() + ", " + id + ")";
	}

	/** Initialises the {@link Agent}'s action schedule. <br>
	 * This function is called by the {@link Scheduler} and must not be called by the {@link Agent} itself!<br>
	 * 
	 * @param initialTime first time step to be considered by the simulation */
	public final void initialiseActions(TimeStamp initialTime) {
		ArrayList<PlannedAction> nextActions = contractManager.scheduleDeliveriesBeginningAt(initialTime);
		transmitPlannedActionsToScheduler(initialTime, nextActions);
	}

	/** Executes warm-up functionalities at the beginning of the simulation.<br>
	 * This function is called by the {@link Scheduler} and must not be called by the {@link Agent} itself!<br>
	 * In order to create agent-specific warm-up behaviour override function {@link #warmUp(TimeStamp)}.
	 * 
	 * @param initialTime first time step to be considered by the simulation
	 * @return {@link WarmUpStatus#COMPLETED} if warm-up has been completed<br>
	 *         {@link WarmUpStatus#INCOMPLETE} if more warm-up steps are required */
	public final WarmUpStatus executeWarmUp(TimeStamp initialTime) {
		WarmUpStatus needsFurtherWarmUp = warmUp(initialTime);
		return needsFurtherWarmUp;
	}

	/** <b>Override</b> this method to define agent-specific warm-up functionality at the beginning of the simulation. <br>
	 * It is called by the {@link Scheduler} and must not be called by the {@link Agent} itself!<br>
	 * 
	 * @param initialTime first time step to be considered by the simulation
	 * @return {@link WarmUpStatus#COMPLETED} if warm-up has been completed, this method is not called again;<br>
	 *         {@link WarmUpStatus#INCOMPLETE} if more warm-up steps are required, this method is called again after exchanging
	 *         messages with all other {@link Agent}s */
	protected WarmUpStatus warmUp(TimeStamp initialTime) {
		return WarmUpStatus.UNNECESSARY;
	}

	@Override
	public final TimeStamp now() {
		return localServices.getCurrentTime();
	}

	@Override
	public final ActionBuilder call(BiConsumer<ArrayList<Message>, List<Contract>> actionToExecute) {
		ActionBuilder builder = new ActionBuilder(actionToExecute);
		localServices.registerActionBuilder(this, builder);
		return builder;
	}

	/** Register the scheduled action defined in this builder */
	void activateAction(ActionBuilder builder) {
		builder.arm(contractManager, actionManager, messageManager);
	}

	@Override
	public final Random getNextRandomNumberGenerator() {
		return localServices.getNewRandomNumberGeneratorForAgent(id);
	}
}