// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import javax.lang.model.SourceVersion;
import de.dlr.gitlab.fame.agent.input.Make.Type;

/** Builds a {@link TreeElement}
 *
 * @author Christoph Schimeczek */
public abstract class ElementBuilder extends ElementOrBuilder {
	static final String NAME_NEEDED = "Name must not be null or empty.";
	static final String NAME_ILLEGAL = "Name must be a valid Java identifier but is: ";

	/** Name of this input element */
	protected String name;
	private boolean isList = false;
	private boolean isOptional = false;

	@Override
	protected TreeElement get() {
		return build();
	}

	/** @return {@link TreeElement} built from this builder */
	protected abstract TreeElement build();

	/** @return name of the element to build */
	String getName() {
		return name;
	}

	/** Sets {@link #name} to give value if not null or empty
	 * 
	 * @param name to be assigned to this element
	 * @return the ElementBuilder this function is called from */
	protected ElementBuilder setName(String name) {
		ensureNotNullOrEmpty(name);
		ensureIsValidIdentifier(name);
		this.name = name;
		return this;
	}

	/** @throws RuntimeException if given String is null or empty
	 * @param name to be checked */
	protected void ensureNotNullOrEmpty(String name) {
		if (name == null || name.isEmpty()) {
			throw new RuntimeException(NAME_NEEDED);
		}
	}

	/** @throws RuntimeException if given String has illegal characters */
	private void ensureIsValidIdentifier(String name) {
		if (!SourceVersion.isName(name)) {
			throw new RuntimeException(NAME_ILLEGAL + name);
		}
	}

	/** Sets this Element to be treated as List
	 * <p>
	 * If applied to a <b>Group</b>: all inner elements can be repeated in a list format. Do not call this method if inner items
	 * shall be unique. Note that the root group annotated with {@link Input} cannot be a list itself.
	 * </p>
	 * <p>
	 * If applied to a <b>Parameter</b>: sets this Parameter to be treated as List: Multiple values of same {@link Type} can be
	 * assigned. {@link Type#TIMESERIES} does not support this feature. Do not call this method if only one value shall be assigned
	 * to this parameter.
	 * </p>
	 * 
	 * @return ElementBuilder of which this method was call from **/
	public ElementBuilder list() {
		checkListAbility();
		isList = true;
		return this;
	}

	/** Checks if this type of ElementBuilder supports list ability. Base implementation performs no check - override in child
	 * 
	 * @throws RuntimeException if this element cannot be made a list */
	protected void checkListAbility() {}

	/** Sets this Element to be optional, thus it can be omitted during configuration.<br>
	 * If this Element shall be mandatory, simply do not call this method.<br>
	 * If a <b>Group</b> is declared optional, the group as a whole can be omitted even if some of its inner elements are not
	 * optional.
	 * 
	 * @return the ElementBuilder of which this method was call from */
	public ElementBuilder optional() {
		isOptional = true;
		return this;
	}

	/** Copies this Builder's attributes (name, isList) to the given Element
	 * 
	 * @param element to filled with this Builder's attributes */
	protected void setAttributes(TreeElement element) {
		element.setName(name);
		element.setIsList(isList);
		element.setIsOptional(isOptional);
	}
}
