// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import de.dlr.gitlab.fame.time.TimeStamp;

/** An action planned at a certain {@link TimeStamp} for a List of scheduling reasons
 * 
 * @author Ben Fuchs, Ulrich Frey, Christoph Schimeczek */
public class PlannedAction {
	private final TimeStamp timeStamp;
	private final Enum<?> schedulingReason;

	/** Create new {@link PlannedAction}
	 * 
	 * @param timeStamp at which the action is to be executed
	 * @param schedulingReason associated with the action */
	public PlannedAction(TimeStamp timeStamp, Enum<?> schedulingReason) {
		this.timeStamp = timeStamp;
		this.schedulingReason = schedulingReason;
	}

	/** @return time at which this {@link PlannedAction} is scheduled for */
	public TimeStamp getTimeStamp() {
		return timeStamp;
	}

	/** @return what reason this {@link PlannedAction} is associated with */
	public Enum<?> getSchedulingReason() {
		return schedulingReason;
	}
}