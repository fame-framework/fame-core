// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import java.util.ArrayList;
import java.util.List;

/** Builds a parameter tree group that contains inner parameter groups and or elements
 *
 * @author Christoph Schimeczek */
public final class GroupBuilder extends ElementBuilder {
	static final String NAME_NOT_UNIQUE = "Name was already used within this group: ";

	private final List<ElementOrBuilder> innerElementsOrBuilders = new ArrayList<>();

	/** Adds the given Element(s) or their respective Builders to be inner elements of this group
	 * 
	 * @param items any amount of items to be added to this group as inner elements
	 * @return the GroupBuilder of which this method was call from */
	public GroupBuilder add(ElementOrBuilder... items) {
		for (ElementOrBuilder item : items) {
			ensureUnique(item.getName());
			innerElementsOrBuilders.add(item);
		}
		return this;
	}

	/** Adds copy of the given element under the given name to this group - can be used to insert another parameter tree
	 * 
	 * @param name the new name attached to the given element
	 * @param element (root) element from another parameter tree
	 * @return the GroupBuilder of which this method was call from */
	public GroupBuilder addAs(String name, TreeElement element) {
		ensureNotNullOrEmpty(name);
		ensureUnique(name);
		TreeElement elementCopy = element.deepCopy();
		elementCopy.setName(name);
		innerElementsOrBuilders.add(elementCopy);
		return this;
	}

	/** @throws RuntimeException if given name is already used for an element in this group */
	private void ensureUnique(String name) {
		for (ElementOrBuilder elementOrBuilder : innerElementsOrBuilders) {
			if (name.equals(elementOrBuilder.getName())) {
				throw new RuntimeException(NAME_NOT_UNIQUE + name);
			}
		}
	}

	@Override
	protected Group build() {
		Group group = new Group();
		fillGroup(group);
		return group;
	}

	private void fillGroup(Group group) {
		super.setAttributes(group);
		ArrayList<TreeElement> innerElements = new ArrayList<>();
		for (ElementOrBuilder elementOrBuilder : innerElementsOrBuilders) {
			TreeElement innerElement = elementOrBuilder.get();
			innerElement.setParent(group);
			innerElements.add(innerElement);
		}
		group.setInnerElements(innerElements);
	}

	/** Returns a new {@link Tree} built from this group and all its inner elements
	 * 
	 * @return created {@link Tree} */
	public Tree buildTree() {
		name = null;
		Tree tree = new Tree();
		fillGroup(tree);
		return tree;
	}

	@Override
	protected GroupBuilder setName(String name) {
		return (GroupBuilder) super.setName(name);
	}

	@Override
	public GroupBuilder list() {
		return (GroupBuilder) super.list();
	}

	@Override
	public GroupBuilder optional() {
		return (GroupBuilder) super.optional();
	}
}
