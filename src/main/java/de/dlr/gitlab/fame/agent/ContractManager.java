// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.service.Scheduler;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Handles all {@link Contract}s of an {@link Agent}, associated {@link Product}s and scheduling times
 * 
 * @author Christoph Schimeczek, Marc Deissenroth */
public class ContractManager {
	static final String ERR_CONTRACTS_OUT_OF_SYNC = "The delivery intervals for these contracts and products do not match!";
	static final String NO_ACTION_DEFINED = "No Action has been defined for product: ";
	static final String ERR_MULTIPLE_INITIALISATION = "DeliverySchedule cannot be initialised more than once!";
	private static Logger logger = LoggerFactory.getLogger(Contract.class);

	private final long ownAgentId;
	private final ArrayList<Contract> contracts = new ArrayList<>();
	private final ProductTimes productTimes = new ProductTimes();
	/** Product -> ListOfContractsWithThatProduct */
	private final HashMap<Enum<?>, ArrayList<Contract>> productContracts = new HashMap<>();
	private final HashSet<Enum<?>> productsForSchedule = new HashSet<>();

	/** Creates new {@link ContractManager}
	 * 
	 * @param agentId of the {@link Agent} this {@link ContractManager} serves */
	public ContractManager(long agentId) {
		ownAgentId = agentId;
	}

	/** Adds the given {@link Contract} to this {@link ContractManager}
	 * 
	 * @param contract to be handled */
	public void addContract(Contract contract) {
		contracts.add(contract);
		addContractToProductList(contract);
	}

	/** Adds the given {@link Contract} to {@link #productContracts} and ensures synchronous behaviour of Contracts with identical
	 * {@link Product}s */
	private void addContractToProductList(Contract contract) {
		Enum<?> product = contract.getProduct();
		if (!productContracts.containsKey(product)) {
			productContracts.put(product, new ArrayList<Contract>());
		}
		if (productContracts.get(product).isEmpty()) {
			productContracts.get(product).add(contract);
		} else {
			ensureContractsAreSynchronized(contract);
			productContracts.get(product).add(contract);
		}
	}

	/** Ensures synchronous behaviour of given {@link Contract} to all previously added Contracts with the same {@link Product} */
	private void ensureContractsAreSynchronized(Contract contract) {
		Contract referenceContract = productContracts.get(contract.getProduct()).get(0);
		ensureSameDeliveryInterval(contract, referenceContract);
		ensureScheduleTimesAreSynchronous(contract, referenceContract);
	}

	/** Compares delivery intervals of given two {@link Contract}s;
	 * 
	 * @throws RuntimeException if delivery intervals do not match */
	private void ensureSameDeliveryInterval(Contract contractA, Contract contractB) {
		TimeSpan intervalA = contractA.getDeliveryInterval();
		TimeSpan intervalB = contractB.getDeliveryInterval();
		if (intervalB.getSteps() != intervalA.getSteps()) {
			throw Logging.logFatalException(logger, ERR_CONTRACTS_OUT_OF_SYNC);
		}
	}

	/** Ensures that the delta between firstDeliveryTimes of both {@link Contract}s is an integer multiple of one of the Contract's
	 * {@link Contract#getDeliveryInterval() deliveryInterval}
	 * 
	 * @throws RuntimeException if the contract delivery times are not aligned */
	private void ensureScheduleTimesAreSynchronous(Contract contractA, Contract contractB) {
		TimeStamp firstDeliveryTimeA = contractA.getFirstDeliveryTime();
		TimeStamp firstDeliveryTimeB = contractB.getFirstDeliveryTime();
		long deltaSteps = firstDeliveryTimeB.getStep() - firstDeliveryTimeA.getStep();
		TimeSpan deliveryInterval = contractB.getDeliveryInterval();
		if (deltaSteps % deliveryInterval.getSteps() != 0) {
			throw Logging.logFatalException(logger, ERR_CONTRACTS_OUT_OF_SYNC);
		}
	}

	/** Get all {@link Agent}s that have active contracts with the {@link ContractManager}'s agent that are active at the given
	 * TimeStamp; if no currently active contracts are found, an empty List is returned
	 * 
	 * @param timeStamp at which the Contracts are to be checked if they are active
	 * @return list of UUIDs of partner Agents with active Contracts */
	public ArrayList<Long> getContractPartners(TimeStamp timeStamp) {
		ArrayList<Long> partners = new ArrayList<>();
		for (Contract contract : contracts) {
			long partner = contract.getSenderId() == ownAgentId ? contract.getReceiverId() : contract.getSenderId();
			if (contract.isActiveAt(timeStamp)) {
				partners.add(partner);
			}
		}
		return partners;
	}

	/** For all {@link Contract}s that schedule {@link PlannedAction}s at the given time, the next later PlannedAction is created
	 * and returned; be sure to call {@link #scheduleDeliveriesBeginningAt(TimeStamp)} before first use of this method
	 * 
	 * @param currentTime time of action scheduling
	 * @return {@link ArrayList} of {@link PlannedAction}s that are next up to fulfil for schedules {@link Contract}s */
	public ArrayList<PlannedAction> updateAndGetNextContractActions(TimeStamp currentTime) {
		ArrayList<Enum<?>> products = productTimes.drawProductsForTime(currentTime);
		ArrayList<PlannedAction> nextPlannedActions = new ArrayList<>();
		if (products != null) {
			for (Enum<?> product : products) {
				Contract contract = productContracts.get(product).get(0); // Assumed: All contracts have the same scheduling times
				TimeStamp nextTime = calcNextProductActionTime(currentTime, contract);
				nextPlannedActions.add(new PlannedAction(nextTime, product));
				productTimes.linkProductToTime(nextTime, product);
			}
		}
		return nextPlannedActions;
	}

	/** @return next {@link TimeStamp} to execute the given {@link Contract} after the specified current simulation time,
	 *         considering the role of the associated agent and the time shift between sender and receiver of this contract */
	private TimeStamp calcNextProductActionTime(TimeStamp currentTime, Contract contract) {
		TimeStamp contractedTimeOfDelivery = contract.isSender(ownAgentId) ? currentTime.laterByOne() : currentTime;
		TimeStamp nextContractedTimeOfDelivery = contract.getNextTimeOfDeliveryAfter(contractedTimeOfDelivery);
		TimeStamp nextTimeOfProductAction = contract.isSender(ownAgentId) ? nextContractedTimeOfDelivery.earlierByOne()
				: nextContractedTimeOfDelivery;
		return nextTimeOfProductAction;
	}

	/** Prepares {@link PlannedAction}s for all {@link Contract}s with {@link #consider(Enum) considered} {@link Product}s; also
	 * saves the scheduled Products and Times internally for later matching
	 * 
	 * @param currentTime of the simulation
	 * @return on first call: List of PlannedActions to be sent to {@link Scheduler}; on additional calls: an empty list
	 * @throws RuntimeException if called more than once */
	public ArrayList<PlannedAction> scheduleDeliveriesBeginningAt(TimeStamp currentTime) {
		if (!productTimes.isEmpty()) {
			throw Logging.logFatalException(logger, ERR_MULTIPLE_INITIALISATION);
		}
		ArrayList<PlannedAction> plannedActions = new ArrayList<>();
		for (Entry<Enum<?>, ArrayList<Contract>> entry : productContracts.entrySet()) {
			Contract contract = entry.getValue().get(0);
			Enum<?> product = entry.getKey();
			if (productsForSchedule.contains(product)) {
				PlannedAction plannedAction = buildActionAfter(contract, currentTime);
				plannedActions.add(plannedAction);
				productTimes.linkProductToTime(plannedAction.getTimeStamp(), product);
			} else {
				logger.debug(NO_ACTION_DEFINED + product + " of agent " + ownAgentId);
			}
		}
		return plannedActions;
	}

	/** Creates a {@link PlannedAction} corresponding to the next {@link TimeStamp time} after the given one for the given
	 * {@link Contract} */
	private PlannedAction buildActionAfter(Contract contract, TimeStamp currentTime) {
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(currentTime);
		TimeStamp nextTime = contract.isSender(ownAgentId) ? nextDeliveryTime.earlierByOne() : nextDeliveryTime;
		return new PlannedAction(nextTime, contract.getProduct());
	}

	/** schedule actions for all {@link Contract}s associated with the given {@link Product} */
	void consider(Enum<?> product) {
		productsForSchedule.add(product);
	}

	/** Returns List of Contracts associated with the given Product. If no Contract is linked, an empty List is returned;
	 * 
	 * @return unmodifiable {@link List} of {@link Contract}s associated with the specified {@link Product} */
	List<Contract> getContractsForProduct(Enum<?> product) {
		ArrayList<Contract> contracts = productContracts.get(product);
		if (contracts == null) {
			contracts = new ArrayList<Contract>(0);
		}
		return Collections.unmodifiableList(contracts);
	}
}