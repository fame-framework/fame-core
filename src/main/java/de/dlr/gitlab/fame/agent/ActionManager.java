// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.communication.message.Message;

/** Links actions of an {@link Agent} to its possible scheduling reasons
 * 
 * @author Christoph Schimeczek */
public class ActionManager {
	static final String OVERRIDE = "Overriding method binding for trigger ";
	private static Logger logger = LoggerFactory.getLogger(ActionManager.class);
	private final HashMap<Enum<?>, BiConsumer<ArrayList<Message>, List<Contract>>> reasonToAction = new HashMap<>();

	/** Connects the given scheduling reason to the execution of the specified method's reference; any existing binding for the
	 * specified reason is overridden.
	 * 
	 * @param reason product that causes this action to be executed
	 * @param action function to be executed */
	public void configure(Enum<?> reason, BiConsumer<ArrayList<Message>, List<Contract>> action) {
		if (reasonToAction.containsKey(reason)) {
			logger.info(OVERRIDE + reason);
		}
		reasonToAction.put(reason, action);
	}

	/** Returns action associated with the given reason of scheduling, i.e. {@link Product} of a {@link Contract}
	 * 
	 * @param reason the scheduling reason that is assigned to the action
	 * @return {@link BiConsumer} representing the associated action, or null if no action is associated. */
	public BiConsumer<ArrayList<Message>, List<Contract>> getActionFor(Enum<?> reason) {
		return reasonToAction.get(reason);
	}
}