// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Base interface for actions {@link Agent}s can do
 * 
 * @author Christoph Schimeczek */
public interface AgentAbility {
	/** Returns unique ID of the agent
	 * 
	 * @return Id of agent unique in this simulation */
	long getId();

	/** Write a value to the agent's associated output file.<br>
	 * Value will appear in the specified column and with the time stamp matching the simulation time when this method is called.
	 * <b>Warning:</b> If you call this method multiple times on the same time stamp and column, the value will be overwritten.
	 * 
	 * @param column an Enum denoting the column to associate the value with; Enum definition must be annotated with {@link Output}
	 * @param value to be written to the specified column of the output file */
	void store(Enum<?> column, double value);

	/** Writes a value to the agent's associated output file.<br>
	 * Value will be assigned to a multi-level {@link ComplexIndex} plus the agent's ID and time stamp matching the simulation time
	 * when this method is called. <b>Warning:</b> If you call this method multiple times on the same time stamp with the exact same
	 * ComplexIndex, the value will be overwritten.
	 * 
	 * @param complexIndex that contains the data to write - will be reset during this call - no extra call to clear() is required.
	 * @param value to be stored in the given column - will be converted to its string representation */
	void store(ComplexIndex<?> complexIndex, Object value);

	/** Fulfils the given {@link Contract} by sending a contract-related {@link Message} to the receiver of the given contract, by
	 * adding to the specified {@link DataItem}s a {@link ContractData}-item which matches the next contracted {@link TimeStamp
	 * time} after the current time.
	 * 
	 * @param contract to be fulfilled
	 * @param dataItems to be sent to the contracted receiver */
	void fulfilNext(Contract contract, DataItem... dataItems);

	/** Fulfils the given {@link Contract} by sending a contract-related {@link Message} to the receiver of the given contract, by
	 * adding to the specified {@link DataItem}s a {@link ContractData}-item which matches the next contracted {@link TimeStamp
	 * time} after the current time.
	 * 
	 * @param contract to be fulfilled
	 * @param portable a single complex {@link Portable} to be sent to the receiver
	 * @param dataItems to be sent to the contracted receiver */
	void fulfilNext(Contract contract, Portable portable, DataItem... dataItems);

	/** @return the {@link TimeStamp} of the current time as defined by the scheduler */
	TimeStamp now();

	/** Returns a new random number generator with a new seed. Seeds of the returned generator differ for each call to this method.
	 * The seeds are generated based on the simulation seed specified in the config file and the agent's UUID. Thus, the sequence of
	 * returned number generators is identical for every execution of the simulation with the same simulation seed - regardless of
	 * the number of computational nodes that are used to run the simulation.
	 * 
	 * @return a {@link Random} seeded with an new value that is reproducible independent of the structure of employed computation
	 *         nodes */
	public Random getNextRandomNumberGenerator();

	/** Creates an ActionBuilder to define a new scheduled Action. A scheduled Action will call the specified method when the
	 * defined {@link ActionBuilder#on(Enum) trigger} is met and provide all messages with the (optionally) defined
	 * {@link ActionBuilder#use(Enum...) product(s)}.
	 * 
	 * @param actionToExecute a function pointer defining which function to be executed when conditions are met
	 * @return new ActionBuilder for the given method */
	public ActionBuilder call(BiConsumer<ArrayList<Message>, List<Contract>> actionToExecute);
}
