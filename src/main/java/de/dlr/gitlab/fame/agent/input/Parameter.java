// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import java.util.Collections;
import java.util.List;
import de.dlr.gitlab.fame.agent.input.Make.Type;

/** A {@link TreeElement} with no inner elements but specifications for one input parameter
 * 
 * @author Christoph Schimeczek */
public final class Parameter extends TreeElement {
	private String defaultValue;
	private String comment;
	private Enum<?>[] allowedValues;
	private Type dataType;

	String getDefaultValue() {
		return defaultValue;
	}

	void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	String getComment() {
		return comment;
	}

	void setComment(String comment) {
		this.comment = comment;
	}

	Enum<?>[] getAllowedValues() {
		return allowedValues;
	}

	void setAllowedValues(Enum<?>[] allowedValues) {
		this.allowedValues = allowedValues;
	}

	Type getDataType() {
		return dataType;
	}

	void setDataType(Type dataType) {
		this.dataType = dataType;
	}

	@Override
	Parameter deepCopy() {
		Parameter copy = new Parameter();
		super.setAttributes(copy);
		copy.setDefaultValue(defaultValue);
		copy.setComment(comment);
		copy.setDataType(dataType);
		copy.setAllowedValues(allowedValues);
		return copy;
	}

	@Override
	protected List<TreeElement> getInnerElements() {
		return Collections.emptyList();
	}
}
