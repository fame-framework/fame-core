// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import java.util.ArrayList;
import java.util.List;

/** A parameter group that contains inner elements but cannot hold a value on its own
 * 
 * @author Christoph Schimeczek */
public class Group extends TreeElement {
	private List<TreeElement> innerElements;

	/** Sets inner elements of this {@link Group} to the given ones
	 * 
	 * @param innerElements that define the inner elements of this Group */
	protected void setInnerElements(List<TreeElement> innerElements) {
		this.innerElements = innerElements;
	}

	@Override
	protected List<TreeElement> getInnerElements() {
		return innerElements;
	}

	@Override
	Group deepCopy() {
		Group copy = new Group();
		super.setAttributes(copy);
		ArrayList<TreeElement> copiedElements = new ArrayList<>();
		for (TreeElement innerElement : innerElements) {
			TreeElement elementCopy = innerElement.deepCopy();
			elementCopy.setParent(copy);
			copiedElements.add(elementCopy);
		}
		copy.setInnerElements(copiedElements);
		return copy;
	}
}
