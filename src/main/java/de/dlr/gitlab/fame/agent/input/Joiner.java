// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import java.util.HashMap;
import java.util.List;
import de.dlr.gitlab.fame.protobuf.Field.NestedField;

/** Creates a new {@link ParameterData} by joining data from given {@link DataProvider} with given {@link Tree}
 *
 * @author Christoph Schimeczek, Achraf El Ghazi */
public final class Joiner {
	static final String EX_PARAM_MISSING = "Missing input data for mandatory parameter ";
	static final String DONT_INSTANTIATE = "Do not instantiate ";

	private Joiner() {
		throw new IllegalStateException(DONT_INSTANTIATE + Joiner.class.getSimpleName());
	}

	/** Creates new {@link Joiner}
	 * 
	 * @param tree defines the expected structure of the input data
	 * @param dataProvider holds the input data read from file
	 * @return inputs read from file matched to their expected tree representation */
	public static ParameterData join(Tree tree, DataProvider dataProvider) {
		HashMap<String, Object> data = new HashMap<>();
		extractData("", tree, dataProvider.getFields(), dataProvider, data);
		return new ParameterData(data, "");
	}

	/** Extracts data from the given dataProvider and stores it to the provided HashMap - for all inner elements (and all of their
	 * inner elements as well) of the given TreeElement using the corresponding list of {@link NestedField}s.
	 * 
	 * @param path Path to parent field
	 * @param element TreeElement that is parent to all Parameters with respect to the given nested fields
	 * @param fields List of nested fields that contain data corresponding to the {@link #innerParameters} of the given Parameter
	 * @param provider provides the actual data from the input file
	 * @param data HashMap to store each parameter with its associated address */
	private static void extractData(String path, TreeElement element, List<NestedField> fields, DataProvider provider,
			HashMap<String, Object> data) {
		if (element.isList()) {
			for (int index = 0; index < fields.size(); index++) {
				String subPath = path + index + TreeElement.ADDRESS_SEPARATOR;
				NestedField subGroupField = searchMatchingField(fields, String.valueOf(index));
				for (TreeElement innerElement : element.getInnerElements()) {
					String elementPath = subPath + innerElement.getName();
					extractElement(elementPath, innerElement, subGroupField.getFieldsList(), provider, data);
				}
			}
		} else {
			for (TreeElement innerElement : element.getInnerElements()) {
				String elementPath = path + innerElement.getName();
				extractElement(elementPath, innerElement, fields, provider, data);
			}
		}
	}

	/** Returns {@link NestedField} from given list matching the given name
	 * 
	 * @param nestedFields list of fields to be searched
	 * @param name FieldName searched for
	 * @return NestedField with matching name, null if no field name matches given name */
	private static NestedField searchMatchingField(List<NestedField> nestedFields, String name) {
		for (NestedField nestedField : nestedFields) {
			if (nestedField.getFieldName().equals(name)) {
				return nestedField;
			}
		}
		return null;
	}

	/** Extracts data from the given dataProvider and stores it to the provided HashMap by searching a match among the provided
	 * fields for the given element. If the element is a group, its inner elements are extracted in turn.
	 * 
	 * @param path Path to given TreeElement
	 * @param element TreeElement that is to be matched with one of the provided fields
	 * @param fields List of nested fields that contain data corresponding to the given TreeElement
	 * @param provider provides the actual data from the input file
	 * @param data HashMap to store each parameter with its associated address
	 * @throws RuntimeException if a non-optional parameter is missing in input */
	private static void extractElement(String path, TreeElement element, List<NestedField> fields, DataProvider provider,
			HashMap<String, Object> data) {
		NestedField matchingField = searchMatchingField(fields, element.getName());
		if (matchingField == null) {
			if (!element.isOptional()) {
				throw new RuntimeException(EX_PARAM_MISSING + element);
			}
		} else {
			if (element instanceof Parameter) {
				data.put(path, provider.getValue((Parameter) element, matchingField));
			} else {
				extractData(path + TreeElement.ADDRESS_SEPARATOR, element, matchingField.getFieldsList(), provider, data);
			}
		}
	}
}
