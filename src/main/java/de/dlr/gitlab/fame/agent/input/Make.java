// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

/** Provides access to parameter tree element builders
 *
 * @author Christoph Schimeczek */
public final class Make {
	static final String DONT_INSTANTIATE = "Do not instantiate ";

	/** Supported input data types */
	public enum Type {
		/** 64-bit floating point numbers */
		DOUBLE,
		/** 32-bit signed integer numbers */
		INTEGER,
		/** 64-bit signed integer numbers */
		LONG,
		/** 64-bit signed integer numbers representing a time */
		TIMESTAMP,
		/** Multiple pairs of times and values representing 1-dimensional functions over time */
		TIMESERIES,
		/** Strings of any kind or length */
		STRING,
		/** Strings restricted to values defined at compile time */
		ENUM,
		/** Strings restricted to values defined at simulation time */
		STRINGSET
	}

	private Make() {
		throw new IllegalStateException(DONT_INSTANTIATE + Make.class.getCanonicalName());
	}

	/** Returns new {@link GroupBuilder} to create a new parameter tree group with the provided name. This {@link GroupBuilder}
	 * should be added to any other GroupBuilder (or root GroupBuilder) without calling its get() method.<br>
	 * Call {@link GroupBuilder#buildTree()} only in case a stand-alone parameter group shall be obtained, e.g. for reuse in
	 * multiple parameter trees.
	 * 
	 * @param name of the parameter group to be created - must not be empty or null
	 * @return new Builder to create the group */
	public static GroupBuilder newGroup(String name) {
		return new GroupBuilder().setName(name);
	}

	/** Returns new {@link GroupBuilder} to create a root group of a new parameter tree.<br>
	 * To complete parameter tree definition call {@link GroupBuilder#buildTree()}.
	 * 
	 * @return new {@link GroupBuilder} to create an unnamed group serving as root element of a new parameter tree */
	public static GroupBuilder newTree() {
		return new GroupBuilder();
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter. This {@link ParameterBuilder} should be added to any
	 * GroupBuilder without calling its get() method.<br />
	 * Call {@link ParameterBuilder#get()} only in case a stand-alone Parameter shall be obtained, e.g. for reuse in multiple
	 * parameter trees.<br />
	 * To take effect, Parameters or their Builders need to be added to ParameterGroups or their Builders.<br>
	 * Use the {@link Type}-specific ShortCuts {@link #newInt(String)}, {@link #newDouble(String)}, {@link #newString(String)}
	 * {@link #newSeries(String)} or {@link #newEnum(String, Class)} instead of this method.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @param type {@link Type} of the parameter - if {@link Type#ENUM} is used ParameterBuilder
	 * @return a new {@link ParameterBuilder} to create the parameter */
	private static ParameterBuilder newParam(String name, Type type) {
		return new ParameterBuilder().setName(name).setType(type);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#INTEGER}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newInt(String name) {
		return newParam(name, Type.INTEGER);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#DOUBLE}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newDouble(String name) {
		return newParam(name, Type.DOUBLE);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#STRING}. String parameters can take any value
	 * and are not validated. If values shall be validated, consider using {@link #newStringSet(String)} or
	 * {@link #newEnum(String, Class)}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newString(String name) {
		return newParam(name, Type.STRING);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#STRINGSET}. A StringSet refers to a shared
	 * concept across multiple agents. Thus, StringSet parameters can take any value (as opposed to Enums), but StringSet input
	 * parameters with the <i>same name</i> are expected to refer to a <i>shared list</i> of possible values. StringSets can be
	 * useful, if multiple agents refer to the same keywords (e.g. type of fuel) but the list of keywords is not a closed set. If no
	 * validation is required, consider using {@link #newString(String)}, or, if the set of keywords is restricted by the code, use
	 * {@link #newEnum(String, Class)}.
	 * 
	 * @param name of the StringSet - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newStringSet(String name) {
		return newParam(name, Type.STRINGSET);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#ENUM}. Enums can take only values contained in
	 * the enum definition. If the list of keywords that is to be reflected is not closed, consider using
	 * {@link #newStringSet(String)}.
	 * 
	 * @param <T> any Enum can be used
	 * @param name of the new parameter - must not be empty or null
	 * @param enumType class of the Enum that the parameter corresponds to
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static <T extends Enum<T>> ParameterBuilder newEnum(String name, Class<T> enumType) {
		return newParam(name, Type.ENUM).setEnum(enumType);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#TIMESERIES}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newSeries(String name) {
		return newParam(name, Type.TIMESERIES);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#LONG}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newLong(String name) {
		return newParam(name, Type.LONG);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#TIMESTAMP}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newTimeStamp(String name) {
		return newParam(name, Type.TIMESTAMP);
	}
}
