// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.agent.input;

import java.util.List;

/** One element of a parameter tree definition
 * 
 * @author Christoph Schimeczek */
public abstract class TreeElement extends ElementOrBuilder {
	static final String ADDRESS_SEPARATOR = ".";

	private TreeElement parent;
	private String name;
	private boolean isList;
	private boolean isOptional;

	/** @return {@link #deepCopy()} of this TreeElement */
	@Override
	protected final TreeElement get() {
		return deepCopy();
	}

	/** @return a deep copy of this TreeElement without a parent */
	abstract TreeElement deepCopy();

	/** Sets this members values to the provided copy
	 * 
	 * @param copy to be filled with this elements attributes */
	protected final void setAttributes(TreeElement copy) {
		copy.setName(name);
		copy.setIsList(isList);
		copy.setIsOptional(isOptional);
	}

	/** @return parent element if any - or null if element {@link #isRoot()} */
	final TreeElement getParent() {
		return parent;
	}

	/** Defines the given Element as parent of this Element
	 * 
	 * @param parent element to be the parent of this */
	protected final void setParent(TreeElement parent) {
		this.parent = parent;
	}

	/** @return name of this element or null if {@link #isRoot()} */
	protected final String getName() {
		return name;
	}

	final void setName(String name) {
		this.name = name;
	}

	/** @return true if this element or its inner Elements can be repeatedly defined */
	final boolean isList() {
		return isList;
	}

	final void setIsList(boolean isList) {
		this.isList = isList;
	}

	/** @return true if this element can be omitted */
	final boolean isOptional() {
		return isOptional;
	}

	final void setIsOptional(boolean isOptional) {
		this.isOptional = isOptional;
	}

	/** @return root element of the parameter tree this element is part of; returns itself if it is the root element */
	final TreeElement getRoot() {
		return isRoot() ? this : parent.getRoot();
	}

	/** @return true if element has no {@link #parent} */
	final boolean isRoot() {
		return parent == null;
	}

	/** @return address of this element, with all names of parent elements prepended and separated by "."; root elements return an
	 *         empty String */
	final String getAddress() {
		if (isRoot()) {
			return "";
		} else if (parent.isRoot()) {
			return name;
		} else {
			return parent.getAddress() + ADDRESS_SEPARATOR + name;
		}
	}

	/** @return List of inner elements - or an empty List if no inner elements are present */
	abstract List<TreeElement> getInnerElements();

	@Override
	public String toString() {
		return getAddress();
	}
}
