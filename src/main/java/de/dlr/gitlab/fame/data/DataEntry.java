// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.data;

/** A data entry of a {@link TimeSeries}; specifies a value at a time step
 * 
 * @author Christoph Schimeczek */
public class DataEntry implements Comparable<DataEntry> {
	final long timeStep;
	final double value;

	/** Creates a {@link DataEntry} from the specified time step and given value
	 * 
	 * @param timeStep time to which the value is associated with
	 * @param value associated to the given time */
	public DataEntry(long timeStep, double value) {
		this.timeStep = timeStep;
		this.value = value;
	}

	/** @return time step of this {@link DataEntry} */
	public long getTimeStep() {
		return timeStep;
	}

	/** @return value assigned to the associated time */
	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "(" + timeStep + ", " + value + ")";
	}

	@Override
	public int compareTo(DataEntry otherItem) {
		return Long.compare(this.timeStep, otherItem.timeStep);
	}
}