// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Represents a time series of double values, offering different interpolation mechanics
 * 
 * @author Christoph Schimeczek */
public class TimeSeries {
	static final String WARN_EXTRAPOLATING_LOWER = ": TimeSeries extrapolated before first element. Requested time: ";
	static final String WARN_EXTRAPOLATING_HIGHER = ": TimeSeries extrapolated after last element. Requested time: ";
	static final String ERR_DUPLICATION = "Duplicate time steps found in series ";
	static final String ERR_TOO_MANY_EXTRAPOLATIONS = "Many extrapolations detected. Please check your timeseries and/or update FAME-Io to at least v2.1";
	static final String ERR_INCOMPLETE_DATA = "Corrupt series! Number of time steps is not matching number of values in series: ";
	static final int MAX_WARNING_COUNT = 1000;

	private static int warningCount = 0;
	static Logger logger = LoggerFactory.getLogger(TimeSeries.class);
	private final ArrayList<DataEntry> data = new ArrayList<>();
	private final String name;
	private final int id;
	private final int extrapolation_index_high;

	/** Construct a {@link TimeSeries} from its protobuf {@link TimeSeriesDao representation}
	 * 
	 * @param rawData protobuf representation of a TimeSeries */
	public TimeSeries(TimeSeriesDao rawData) {
		name = rawData.getSeriesName();
		id = rawData.getSeriesId();
		addRowData(rawData.getTimeStepsList(), rawData.getValuesList());
		Collections.sort(data);
		checkNoDuplicates();
		extrapolation_index_high = -(data.size() + 1);
	}

	/** Adds data entries from given List of time steps and associated values to {@link #data} */
	private void addRowData(List<Long> timeSteps, List<Double> values) {
		if (timeSteps.size() != values.size()) {
			throw Logging.logFatalException(logger, ERR_INCOMPLETE_DATA + name);
		}
		for (int index = 0; index < timeSteps.size(); index++) {
			data.add(new DataEntry(timeSteps.get(index), values.get(index)));
		}
	}

	/** Ensures no duplicate items for same timeStep exist; assumes that {@link DataEntry}s are sorted by timeStep
	 * 
	 * @throws RuntimeException in case duplicate entries are found */
	private void checkNoDuplicates() {
		for (int i = 1; i < data.size(); i++) {
			long timeStep = data.get(i).timeStep;
			long previousTimeStep = data.get(i - 1).timeStep;
			if (timeStep == previousTimeStep) {
				throw Logging.logFatalException(logger, ERR_DUPLICATION + name);
			}
		}
	}

	@Override
	public String toString() {
		return "Series(" + id + ": " + name + ")";
	}

	/** Returns value for the specified timeStamp:
	 * <ul>
	 * <li>If no value is stored for the specified time the value of the next earlier time is returned.</li>
	 * <li>If no element exists for the next earlier time, the value of the first element is returned.</li>
	 * </ul>
	 * 
	 * @param timeStamp the time for which to return a value
	 * @return Value for specified time, next earlier time or otherwise first time element stored */
	public double getValueEarlierEqual(TimeStamp timeStamp) {
		return getValueLowerEqual(timeStamp.getStep());
	}

	/** @return Value for specified time step, next lower time step or lowest time element stored */
	private double getValueLowerEqual(long timeStep) {
		int arrayIndex = getArrayIndexLowerEqual(timeStep);
		return data.get(arrayIndex).value;
	}

	/** Returns the index of a {@link DataEntry} for the specified (next-lower) time:
	 * <ul>
	 * <li>If no DataItem with the specified time exists, the index of the next earlier time will be returned.</li>
	 * <li>If the specified time is earlier as the earliest DataItem the index of the earliest DataItem is returned.</li>
	 * <li>If the specified time is later as the latest DataItem the index of the latest DataItem is returned.</li>
	 * </ul>
	 *
	 * @param timeStep to search for
	 * @return index */
	private int getArrayIndexLowerEqual(long timeStep) {
		int searchResult = Collections.binarySearch(data, new DataEntry(timeStep, 0L));
		if (searchResult < 0) {
			if (searchResult == -1) {
				log_extrapolation(true, timeStep);
			} else if (searchResult == extrapolation_index_high) {
				log_extrapolation(false, timeStep);
			}
			int lowerElementIndex = -(searchResult + 2);
			return lowerElementIndex >= 0 ? lowerElementIndex : 0;
		} else {
			return searchResult;
		}
	}

	/** logs a warning or an error about extrapolation: If total number of warnings is not yet exceeded on this node a warning is
	 * issues. If these total count of warnings is exceeded, an error is thrown once */
	private void log_extrapolation(boolean lower, long timeStep) {
		if (warningCount < MAX_WARNING_COUNT) {
			String message = lower ? WARN_EXTRAPOLATING_LOWER : WARN_EXTRAPOLATING_HIGHER;
			logger.warn(name + message + new TimeStamp(timeStep));
			warningCount++;
		} else if (warningCount == MAX_WARNING_COUNT) {
			logger.error(ERR_TOO_MANY_EXTRAPOLATIONS);
			warningCount++;
		}
	}

	/** Returns value for the specified timeStamp;
	 * <p>
	 * If no value is stored for the specified time the value of the next later time is returned.
	 * <p>
	 * If no element exists for the next later time, the value of the last element is returned.
	 * 
	 * @param timeStamp the time for which to return a value
	 * @return Value for specified time, next later time or otherwise latest time element stored */
	public double getValueLaterEqual(TimeStamp timeStamp) {
		return getValueHigherEqual(timeStamp.getStep());
	}

	/** @return Value for specified time step, next higher time step or highest time element stored */
	private double getValueHigherEqual(long timeStep) {
		int arrayIndex = getArrayIndexLowerEqual(timeStep);
		if (isLastDataIndex(arrayIndex)) {
			return data.get(arrayIndex).value;
		}
		long timeOfElement = data.get(arrayIndex).timeStep;
		if (timeStep > timeOfElement) {
			return data.get(arrayIndex + 1).value;
		}
		return data.get(arrayIndex).value;
	}

	/** @return true if the specified index is the last one in the {@link #data} array */
	private boolean isLastDataIndex(int index) {
		return index == (data.size() - 1);
	}

	/** Returns value for the specified timeStamp;
	 * <p>
	 * If no value is stored for the specified time the value is interpolated from direct neighbouring elements
	 * <p>
	 * If no element is stored for earlier times, the value of the earliest element is returned.
	 * <p>
	 * If no element is stored for later times, the value of the latest element is returned.
	 * 
	 * @param timeStamp the time for which to return a value
	 * @return Interpolated value for specified time, earliest time or latest time stored */
	public double getValueLinear(TimeStamp timeStamp) {
		return getValueLinear(timeStamp.getStep());
	}

	private double getValueLinear(long timeStep) {
		int arrayIndex = getArrayIndexLowerEqual(timeStep);
		if (isLastDataIndex(arrayIndex)) {
			return data.get(arrayIndex).value;
		}
		long timeOfElement = data.get(arrayIndex).timeStep;
		if (arrayIndex == 0 && (timeStep < timeOfElement)) {
			return data.get(arrayIndex).value;
		}
		return interpolateBetween(timeStep, data.get(arrayIndex), data.get(arrayIndex + 1));
	}

	/** @return Interpolated value at timeStep, where timeStep lies between time of lowerTimeItem and upperTimeItem or at the
	 *         boundary of this interval */
	private double interpolateBetween(long requestedTimeStep, DataEntry lowerTimeItem, DataEntry upperTimeItem) {
		long t0 = lowerTimeItem.timeStep;
		long t1 = upperTimeItem.timeStep;
		double y0 = lowerTimeItem.value;
		double y1 = upperTimeItem.value;
		double gradient = (y1 - y0) / (t1 - t0);
		long timeDelta = requestedTimeStep - t0;
		return y0 + gradient * timeDelta;
	}

	/** @return unique Id of this {@link TimeSeries} as specified in the input file */
	public int getId() {
		return id;
	}

	/** Reset the warning count for extrapolations to 0 - use for testing purposes only */
	static void resetExtrapolationWarningCount() {
		warningCount = 0;
	}
}