<!-- SPDX-FileCopyrightText: 2025 German Aerospace Center <fame@dlr.de>

SPDX-License-Identifier: CC0-1.0 -->
# Changelog

## [2.0.2](https://gitlab.com/fame-framework/fame-core/-/releases/v2.0.2) - 2025-02-27
### Fixed
- Fixed buggy dependency by updating to fame-mpi v1.0.2 #141 (@dlr-cjs)
- Fixed buggy test of RuntimeProperties #143 (@dlr-cjs)

## [2.0.1](https://gitlab.com/fame-framework/fame-core/-/releases/v2.0.1) - 2024-12-03
### Fixed
- Fixed vulnerability in protobuf 3.25.4 inherited from FAME-Protobuf v2.0.2 #139 (@dlr-cjs)
- Fixed wrong target folder for javadoc creation #140 (@dlr-cjs)

## [2.0.0](https://gitlab.com/fame-framework/fame-core/-/releases/v2.0.0) - 2024-12-02
_If you are upgrading: please see [`UPGRADING.md`](UPGRADING.md)_

### Changed
- **Breaking**: Replace content of `fameSetup.yaml` to be consistent with names of command-line arguments #74 (@dlr-cjs)
- **Breaking**: Renaming of existing command-line arguments #74 (@dlr-cjs)
- **Breaking**: Input Groups and GroupLists do no longer derive their optionality from their inner elements. They can be made optional individually #60 (@dlr-cjs)
- **Breaking**: Rename TimeSeries methods `getValueHigherEqual` and `getValueLowerEqual ` to `getValueLaterEqual` and `getValueEarlierEqual`, respectively #120 (@dlr-cjs)
- **Breaking**: Drop support for Java versions 8, 9 and 10 #115 (@dlr-cjs)
- **Breaking**: Definition of Timeseries in input protobuf format adapted to new fame-protobuf v2.0.2 #123, #137 (@dlr-cjs)
- **Breaking**: Change DataItem field names to plural due to new fame-protobuf v2.0.1 #135 (@dlr-cjs)
- Implement own iterator for `Portable` components #125 (@dlr-cjs)
- `CommUtils`: Add extraction of messages with requested type of `Portable` #138 (@dlr-cjs)
- Deprecate `DataItem` and mark for later removal #129 (@dlr-cjs)
- Move java package name transfer from package `Setup` to `Input` #117 (@dlr-cjs)
- Move `MissingSeriesException` to `TimeSeriesProvider` #124 (@dlr-cjs)

### Added
- Add new options to command line interface that cover all allowed setup parameters #74 (@dlr-cjs)
- Add new `ParameterData.GetOptionalGroupList` method to extract optional group lists without need for exception handling #60 (@dlr-cjs)
- Add new `Make.newStringSet` method denoting Strings that are to be harmonised in a joint set #121 (@dlr_fn, @dlr-cjs)
- Add ability to lead defaults for command-line arguments from `fameSetup.yaml` #74 (@dlr-cjs)
- Add new method `onAndUse` for ActionBuilder that provides a shorter notation of response actions #133 (@dlr-cjs)
- Add warning if output file name has no ending `.pb` #74 (@dlr-cjs)
- Add Portable interface to `TimeSpan` and `TimePeriod` (@dlr-cjs)
- Add test that CHANGELOG.md is updated #122 !115 (@dlr-cjs)

### Removed
- **Breaking**: Drop support for protobuf files written with older versions of FAME-Core or FAME-Io version <3.0.0 #134 (@dlr-cjs)
- **Breaking**: Remove support of java package definition in `setup.yaml` #123 (@dlr-cjs)
- **Breaking**: Drop command-line argument `-s`, `--setup` - this name can no longer be changed dynamically #74 (@dlr-cjs)
- **Breaking**: Drop support of Channels #136 (@dlr-cjs)
- Delete static methods from Setup #116 (@dlr-cjs)
- Delete SetupManager class since the transfer of Setup information between nodes is no longer required, as all nodes now read the information from the command line #74 (@dlr-cjs)
- Delete package `agent.exception` #124 (@dlr-cjs)

### Fixed
- Fix missing output information transfer across nodes using new new fame-protobuf v2.0.0 format #123 (@dlr-cjs)
- Fix overly sensitive error messages on not-contracted triggers spamming console #51 (@dlr-cjs)
- Fix tests on JDK21
- Correct wrong email in SPDX headers #131 (@dlr-cjs)

## [1.7.0](https://gitlab.com/fame-framework/fame-core/-/releases/v1.7.0) - 2024-05-24
### Changed
- Tie root node to all input and output processes #109 (@dlr-cjs)
- Move output-interval definition from scenario.yaml to fameSetup.yaml #109 (@dlr-cjs)
- Update FAME-Protobuf version to v1.5.0 #37 (@dlr-cjs)
- Update CI to run fewer pipelines when tests fail #113 (@dlr-cjs)

### Added
- Trigger a warning if timeseries are extrapolated beyond their original regime of definition #67 (@dlr-cjs)
- Add execution details to output #82 (@dlr-cjs)
- Define set of classes with active output in fameSetup.yaml #109 (@dlr-cjs)
- Add tracking of communication amount between agents #105 (@dlr-cjs)
- Add option in setup to enable communication tracking #105 (@dlr-cjs)
- Add tracking of action runtimes and tick coincidence for agents #104 (@dlr-cjs)
- Add option in setup to enable runtime tracking #104 (@dlr-cjs)
- Add option to disable all complex outputs #83 (@dlr-cjs)
- Add optional reading of package names from input file #37 (@dlr-cjs)
- Write complete first (input) DataStorage of input file to output file #109 (@dlr-cjs)
- REUSE compatibility of repository #110 (@dlr-cjs)

### Fixed
- Allow re-usage of same parameter group multiple times #73 (@dlr-cjs)

## 1.6.3 - 2024-04-05
### Fixed
- Fix coverage pipeline #111 (@dlr-cjs)

## 1.6.2 - 2024-04-04
### Fixed
- Fix wrong version hint in README.md !92 (@dlr-cjs)

## 1.6.1 - 2024-04-03
### Fixed
- Fix broken `nexus-staging-maven-plugin` #108 (@dlr-cjs)

## [1.6.0](https://gitlab.com/fame-framework/fame-core/-/releases/v1.6.0) - 2024-04-03
### Changed
- Changed the level for missing outputs to INFO #103 (@dlr-cjs, @dlr_elghazi)
- Notary & InputManager: improved testing !82 (@dlr-cjs)
- New message on simulation completion #75 (@dlr-cjs)
- Improved warning if an action is missing input messages #90 (@dlr-cjs)

### Added
- Contract: may have a payload that is read from input and can be extracted by Contract owners #15 (@dlr-cjs)
- InputManager: added support for reading files with header #106 (@dlr-cjs)

### Fixed
- Fixed missing error report on crash due to missing Agent in classpath #77 (@dlr-cjs)
- OutputSerialiser: fixed broken tests #106 (@dlr-cjs)

## [1.5.0](https://gitlab.com/fame-framework/fame-core/-/releases/v1.5.0) - 2023-07-27
### Changed
- Changelog style adapted #99 (@dlr-cjs)
- Console output at end of simulation #88 (@dlr-cjs)

### Added
- AgentAbility interface - can be used to define common Products, Outputs and functions the across Agent hierarchy #98 (@dlr-cjs)
- ParameterData: method "getOptionalGroup" returns null if group is missing instead of throwing an exception #84 (@dlr-cjs)
- TimeSpan: Meaningful conversion to String #92 (@dlr-cjs)
- Contract: Meaningful string representation #91 (@dlr-cjs)
- Citation.cff #98 (@dlr-cjs)

## [1.4.3](https://gitlab.com/fame-framework/fame-core/-/releases/v1.4.3) - 2023-05-03
### Changed
- Updated FAME-Protobuf version to v1.2.1

### Fixed
- Removed (wrong) redundant dependency to protobuf-java-util

## [1.4.2](https://gitlab.com/fame-framework/fame-core/-/releases/v.1.4.2) - 2022-12-02
### Fixed
- Output of last tick is now written to file
- Removed log4j.properties from packaged files

## [1.4](https://gitlab.com/fame-framework/fame-core/-/releases/v1.4) - 2022-07-08
### Changed
- **Breaking:** Format of output files - now contains a file header and stores multiple protobuf messages
- **Breaking:** Compatibility with FAME-Protobuf - minimum required version is 1.2
- **Breaking:** Compatibility with FAME-Io - minimum required version is 1.6
- Performance improvements for systems with more than 1000 agents

### Added
- ComplexIndex mechanics: Enables storing of more than one value per column for one agent and time step
