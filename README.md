<!-- SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>

SPDX-License-Identifier: Apache-2.0 -->
# FAME-Core
FAME's core library written in Java. 
Minimum required JDK version is 11.

## What is FAME?
FAME is an open **F**ramework for distributed **A**gent-based **M**odels of **E**nergy systems.
Its purpose is to support the rapid development and fast execution of complex agent-based energy system simulations.
All components of FAME are open source.
These components are:
- **FAME-Core**: A Java library that provides classes and methods to create and run your own agent-based simulations in single-core or multi-core mode.
- **FAME-Io**: A set of Python tools that enable you to feed input data to your simulations and to extract simulation results in a convenient way.
- **FAME-Gui**: A graphical user interface for convenient configuration of FAME-based models.
- **FAME-Protobuf**: Basic definitions for input and output formats of FAME-Core.
- **FAME-Demo**: A simplistic FAME-based simulation demonstrating the most important features of FAME and their application in energy systems analysis.
- **FAME-Mpi**: Components for parallelisation of FAME-Core applications.

Please visit the [FAME-Wiki](https://gitlab.com/fame-framework/wiki/-/wikis/home) to access detailed manuals and explanations of these FAME-components.

## Statement of need
FAME is a general framework intended for, but not exclusive to, energy systems analysis.
It facilitates the development of complex agent-based simulations.
FAME enables parallel execution of simulations without any prior knowledge about parallelisation techniques.
Models built with FAME can be executed on computer systems of any capacity – from your laptop to high-performance computing clusters.
Furthermore, FAME applications are binary data files that are portable across Windows and Linux systems.
FAME's highly adaptable contracting system offers broad configuration options to control simulations without changing its code.

FAME-Core has been thoroughly tested to fulfil the demand of scientific rigor and to provide a stable and reliable simulation framework.
It provides thoroughly tested functionality common to all simulations (e.g. input, output, scheduling, communication), resulting in

1. less code &rarr; less errors &rarr; less maintenance, 
2. higher transparency of existing models due to common interfaces and concepts of the respective framework, and 
3. a lower threshold for developing new models by providing a well-defined starting point.

## Installation instructions
FAME-Core can be installed easily by using [Apache Maven](https://maven.apache.org/).
In your Java project, just add the FAME-Core dependency to your `pom.xml`:

```
<dependency>
    <groupId>de.dlr.gitlab.fame</groupId>
    <artifactId>core</artifactId>
    <version>2.0.0</version>
</dependency>
```

Please see [Maven-central](https://mvnrepository.com/artifact/de.dlr.gitlab.fame/core) for the latest version.
Have a look at the [Getting Started](https://gitlab.com/fame-framework/wiki/-/wikis/GetStarted/Getting-started) section in the Wiki to understand how to set up a model using FAME-Core.  

## Available Support
This is a purely scientific project, hence there will be no paid technical support.
Limited support is available, e.g. to enhance FAME, fix bugs, etc. 
To report bugs or pose enhancement requests, please file issues following the provided templates (see also [CONTRIBUTING.md](CONTRIBUTING.md))
For substantial enhancements, contact us via [fame@dlr.de](mailto:fame@dlr.de) for working together on the code in joint projects or towards collaborative publications.

## Citing FAME-Core
If you use FAME-Core in your scientific work please cite:

Christoph Schimeczek, Marc Deissenroth-Uhrig, Ulrich Frey, Benjamin Fuchs, A. Achraf El Ghazi, Manuel Wetzel & Kristina Nienhaus (2023).
FAME-Core: An open Framework for distributed Agent-based Modelling of Energy systems.
Journal of Open Source Software. [doi: 10.21105/joss.05087](https://doi.org/10.21105/joss.05087)
