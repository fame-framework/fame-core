---
title: 'FAME-Core: An open Framework for distributed Agent-based Modelling of Energy systems'
tags:
  - Java
  - agent-based
  - energy systems
  - modelling framework
  - parallelisation
authors:
  - name: Christoph Schimeczek^[corresponding author]
    orcid: 0000-0002-0791-9365
    affiliation: 1
  - name: Marc Deissenroth-Uhrig
    orcid: 0000-0002-9103-418X
    affiliation: "1, 2"
  - name: Ulrich Frey
    orcid: 0000-0002-9803-1336
    affiliation: 1
  - name: Benjamin Fuchs
    orcid: 0000-0002-7820-851X
    affiliation: 1
  - name: A. Achraf El Ghazi
    orcid: 0000-0001-5064-9148
    affiliation: 1
  - name: Manuel Wetzel
    orcid: 0000-0001-7838-2414
    affiliation: 1
  - name: Kristina Nienhaus
    orcid: 0000-0003-4180-6767
    affiliation: 1

affiliations:
 - name: German Aerospace Center (DLR), Institute of Networked Energy Systems, Curiestr. 4, 70563 Stuttgart, Germany
   index: 1
 - name: School of Engineering, University of Applied Sciences Saarbrücken, Goebenstr. 40, 66117 Saarbrücken, Germany
   index: 2

date: 20 September 2022
bibliography: paper.bib
---

# Summary
In this paper we introduce FAME-Core, a Java library supporting development and execution of agent-based simulation models (ABM).
It is a main component of the open **F**ramework for distributed **A**gent-based **M**odels of **E**nergy systems.
FAME-Core offers rapid development and fast simulation execution capabilities while reducing required programming skills and efforts for creating and running complex ABM.

Within the domain of energy systems, ABMs are employed to investigate the dynamics of these systems and their emerging variables, e.g., electricity prices.
Designing such models requires in-depth domain knowledge, e.g., of relevant actors, their interactions, and decision-making processes.
In addition, enhanced programming skills are necessary to ensure numeric integrity and fast execution of the models' implementations.
However, if models require parallelisation to keep computation times within acceptable limits, highly specialised programming skills are required and program complexity rises tremendously.
FAME-Core removes the need for these specialised programming skills by providing parallelisation capability out of the box.
It makes the code base of ABMs less complex by managing common tasks like reading inputs, gathering outputs, and scheduling of agents.
Thus, FAME-Core eases the burden of ABM development and maintenance.

FAME-Core is one of two main components of FAME, a software suite to support creators and users of ABM simulations.
Hence, the FAME repository^[https://gitlab.com/fame-framework/] is split into several components.
Each component addresses one aspect within FAME's model development and usage workflow, see \autoref{fig:components}.
Of these, FAME-Core and FAME-Io [@FAME_IO] are the most important ones.
While the first is a Java library supporting development and execute ABM simulations, the latter is a Python package^[https://pypi.org/project/fameio/] to configure complex simulations and manage files associated with FAME.
Other FAME components, like FAME-Mpi, FAME-Protobuf, and FAME-Prepare are supporting tools used by either FAME-Io or FAME-Core.
In addition, FAME-Gui – a graphical user interface to FAME-Io – is currently under development.
Lastly, the demonstration model FAME-Demo introduces the most important features of FAME-Core and FAME-Io and their combined workflow.

![Components of FAME and their interactions.\label{fig:components}](Components.png)

# Statement of need
In the field of energy systems analysis (ESA), optimisation-based models are by far the most common [@Hoekstra2017].
However, this type of modelling is not ideally suited to capture real-world market imperfections, as was pointed out by, e.g., @Trutnevyte2016.
The social-planner approach employed in optimisation models often disregards, e.g., restricted foresight, complex (and maybe non-rational) actor behaviour, and market distortions due to policy instruments.
Thus, results from such models may significantly deviate from outcomes of ABM simulations that consider such aspects, see @TorralbaDiaz2020.

Yet, ABMs are not new to the field of ESA.
Many research questions were addressed with this modelling technique [@Zhou2009; @Ringler2016].
However, most models are not actively developed over a longer period.
One possible cause might be the high effort of keeping models up to date to an ever-evolving energy system with diverse actors ranging from power plant owners over policymakers to household customers.
A framework to design ABM in ESA that reduces coding overhead could help with both keeping existing models up-to date more easily and creating new models more quickly.
Such a framework, however, must be flexible enough to consider highly diverse actors that, e.g., decide on different time scales, ranging from seconds (e.g., grid-stabilisation) to decades (e.g., investment decisions) and must also consider that some decisions might be addressed with computationally cheap rule sets, whereas other decisions might require complex and demanding numerical algorithms (e.g., dynamic programming).

Many platforms, languages, and libraries already exist for the development of ABM, see the CoMSES Network^[https://www.comses.net/].
We assessed 42 candidate ABM frameworks for the purpose of modelling energy systems.
However, it was difficult to find frameworks that are both actively developed and open source, support diverse actors of inhomogeneous numerical complexity, and are able to execute code in parallel for high performance computing applications.
An additional requirement was that the framework should use Java as programming language since it offers high performance.
The most promising candidates regarding these criteria were:

* Akka^[https://akka.io/]: a powerful and concurrent actor-model framework, 
* DEVS-Suite^[https://acims.asu.edu/devs-suite/]: a modelling framework for discrete event simulations and cellular automata,
* Jade^[https://jade.tilab.com]: a wide-spread peer-to-peer platform for agent-based modelling,
* Jadex^[https://github.com/actoron/jadex]: a framework to create applications using a service-oriented architecture,
* JIAC^[https://www.jiac.de/]: a multi-agent framework for distributed applications, 
* MASS^[http://depts.washington.edu/dslab/MASS]: a parallel computing library for multi-agent simulations,
* OMS^[https://alm.engr.colostate.edu/cb/project/oms]: a simulation framework for connected components, and 
* Repast^[https://repast.github.io]: a mature and well-known ABM suite.

A close inspection of these most promising candidates, however, revealed some drawbacks.
None of the frameworks supports agent scheduling with discrete events (except for DEVS-suite).
This scheduling approach might be significantly faster compared to a linear time progression if agents act on different time scales.
DEVS-Suite, however, offered almost no documentation on its functionalities and its examples included but a selection of screenshots.
We deemed Akka inappropriate for scientific modelling, as it does not guarantee the delivery of messages between agents.
Repast offered parallelisation only with the C++ language, which is not wide-spread in the ESA community - Python support was added only recently.
Jadex was licensed under GPL v3.0, and was thus not suitable for our research institute.
MASS required spatial assignment of agents and organised parallelisation around their spatial distribution.
For market models in ESA like AMIRIS [@AMIRIS], this seemed unfitting as their agents are often unrelated to spatial coordinates.
The linear data-flow driven design of OMS seemed not suited for depicting autonomous acting agents and their complex interactions.
JIAC's syntax for defining agents is complex, the XML configuration files are bloated, and it relies on physical time intervals for agent scheduling.
In conclusion, we deemed Jade the best candidate. 
However, it is designed around the FIPA^[http://www.fipa.org/] specifications which, after a close examination, seemed to not benefit the design of scientific models in ESA, but to add additional complexity to the model code. 

We therefore developed FAME to feature discrete-event scheduling and guaranteed message delivery without the need of spatial assignment for agents.
In addition, we aimed at supporting heavy-duty simulations with slim configuration files and simple model code under a non-copy left license.
Furthermore, we wanted to provide easy setup procedures and extensive documentation.
In our view, this combination of features could facilitate the development of new ABM in ESA and improve their longevity and interoperability.

In essence, FAME addresses management of data input, simulation output, scheduling of agent actions, and agent-to-agent communication.
Java model code created with FAME-Core defines the structure of input and output files in YAML and CSV format and enable FAME-Io and FAME-Gui to easily configure such models.
FAME supports flexible scheduling of agents, i.e., the order of agent actions can be configured instead of hard-coded.
Actions are either triggered via configured contracts that represent repeated actions, or alternatively, upon receiving of messages.

All FAME-Core models are ready for parallel execution by default and can be run either in single-core or multicore mode.
Hence, model developers do not need to consider any aspects of parallelisation when using FAME.
FAME’s multicore mode is based on the Message Passing Interface^[https://www.mpi-forum.org/] and automatically coordinates agent scheduling and message exchange across computing nodes.
It enables fast execution of computationally intense simulation models featuring inhomogeneous agents with regard to time and complexity.
For small to medium-sized simulations, however, single-core execution is recommended to avoid parallelisation overhead.

# Use Case
FAME was designed to support modelling of energy systems.
However, it is not restricted to this domain and may be employed for ABM simulations of other domains with similarly structured problems.
Albeit FAME-Core is intended to drive numerically complex simulations, its rapid-prototyping features also support designing simpler or less demanding models.

For a first experience with FAME-Core, we recommend to inspect the code of the FAME-Demo^[https://gitlab.com/fame-framework/fame-demo] project.
It may serve as a convenient starting point for any new model developments.

A full-fledged example of a FAME-Core based simulation is AMIRIS [@Deissenroth2017; @AMIRIS].
It was ported to FAME in 2020.
This did not only reduce the required lines of code in AMIRIS, but also opened new configuration options to the model, which weren't possible before.
FAME-Core proved to be numerically efficient introducing only minor overhead in comparison to its previous implementation.
In single-core mode, FAME-Core executes the AMIRIS model with low overhead – performing a simulation of the German wholesale electricity market for one year in hourly resolution on a desktop computer within about 20 seconds.
In multicore mode, FAME demonstrated high parallelisation efficiency for a setup of 16 computationally heavy agents: Computation wall time was roughly proportional to $1/n$ as $n \in {1,2,4,8}$ cores were utilised.

# Acknowledgements
This project was funded by the German Aerospace Center (DLR).

# References
