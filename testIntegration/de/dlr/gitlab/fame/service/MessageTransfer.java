// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.DummyAgent;
import de.dlr.gitlab.fame.communication.message.MessageBuilder;
import de.dlr.gitlab.fame.communication.stats.CommTracking;
import de.dlr.gitlab.fame.communication.transfer.Composer;
import de.dlr.gitlab.fame.communication.transfer.PortableUidManager;
import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.mpi.MpiInstantiator;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;

public class MessageTransfer {
	public static final int AGENT_COUNT = 7;
	private static MpiFacade mpi = MpiInstantiator.getMpi(MpiMode.SINGLE_CORE);
	private static MpiManager mpiManager;
	private static ArrayList<ArrayList<Long>> globalAddressBook = new ArrayList<>();
	private static ArrayList<Agent> localAgents = new ArrayList<>();

	public static void main(String[] args) {
		mpi.initialise(args);
		setupAddessBook();
		MessageTransfer.mpiManager = new MpiManager(mpi);

		TimeSeriesProvider mockSeriesProvider = mock(TimeSeriesProvider.class);
		PortableUidManager portableUidManager = new PortableUidManager(new ArrayList<>());
		Composer composer = new Composer(portableUidManager, mockSeriesProvider, 100);
		MessageBuilder messageBuilder = new MessageBuilder(composer, new ArrayList<>());
		CommTracking commTracking = mock(CommTracking.class);

		AddressBook addressBook = new AddressBook(mpiManager);
		PostOffice postOffice = new PostOffice(mpiManager, messageBuilder, commTracking, addressBook);
		LocalServices localServices = new LocalServices(mock(Scheduler.class), postOffice, mock(OutputManager.class),
				mock(Notary.class), mockSeriesProvider, null, addressBook);

		for (Long agentId : globalAddressBook.get(mpiManager.getRank())) {
			DummyAgent agent = new DummyAgent(AgentDao.newBuilder().setId(agentId).setClassName("DummyAgent").build(),
					localServices);
			localAgents.add(agent);
			postOffice.registerAgent(agent);

		}
		addressBook.synchroniseAddressBookUpdates();
		assertAddressBooksAreSychnronized(addressBook);

		int tickCount = 10;
		for (int tick = 0; tick < tickCount; tick++) {
			for (Agent agent : localAgents) {
				agent.executeActions(Collections.emptyList());
			}
			postOffice.deliverMessages();
		}
		assertCorrectSendMessageCount(tickCount);
		assertCorrectReceiveMessageCount(tickCount);

		mpi.invokeFinalize();
	}

	private static void setupAddessBook() {
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(0L, 1L, 2L, 3L)));
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(4L, 5L)));
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(6L)));
		globalAddressBook.add(new ArrayList<Long>());
	}

	private static void assertAddressBooksAreSychnronized(AddressBook addressbook) {
		for (int processId = 0; processId < mpiManager.getProcessCount(); processId++) {
			for (Long agentId : globalAddressBook.get(processId)) {
				assertEquals(processId, addressbook.getProcessId(agentId));
			}
		}
	}

	private static void assertCorrectSendMessageCount(int tickCount) {
		for (Agent agent : localAgents) {
			DummyAgent dummy = (DummyAgent) agent;
			long expextedSent = (agent.getId() + 1) * tickCount;
			long actualSent = (long) dummy.getNumerOfSentMessages();
			assert expextedSent == actualSent : "Sent message was " + actualSent + " but expected: " + expextedSent;
		}
	}

	private static void assertCorrectReceiveMessageCount(int tickCount) {
		for (Agent agent : localAgents) {
			DummyAgent dummy = (DummyAgent) agent;
			long senderId = agent.getId() - 1;
			senderId = senderId < 0 ? senderId + AGENT_COUNT : senderId;
			long expextedReceived = (senderId + 1) * (tickCount - 1); // no receive in first tick
			long actualReceived = (long) dummy.getNumerOfReceivedMessages();
			assert expextedReceived == actualReceived : "Received message was " + actualReceived + " but expected: "
					+ expextedReceived;
		}
	}
}