// SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
//
// SPDX-License-Identifier: Apache-2.0
package de.dlr.gitlab.fame.service;

import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import java.util.HashMap;
import de.dlr.gitlab.fame.agent.WarmUpAgent;
import de.dlr.gitlab.fame.communication.message.MessageBuilder;
import de.dlr.gitlab.fame.communication.stats.CommTracking;
import de.dlr.gitlab.fame.communication.transfer.Composer;
import de.dlr.gitlab.fame.communication.transfer.PortableUidManager;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.mpi.MpiInstantiator;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.service.scheduling.Schedule;
import de.dlr.gitlab.fame.service.scheduling.stats.NoRuntimeTracking;
import de.dlr.gitlab.fame.setup.Setup;

/** Tests warm-up of simulation: MPI, Scheduler */
public class SimulationWarmUp {
	private static final int MAX_WARM_UP_TICKS = 100;
	private static MpiFacade mpi = MpiInstantiator.getMpi(MpiMode.SINGLE_CORE);

	public static void main(String[] args) {
		mpi.initialise(args);
		MpiManager mpiManager = new MpiManager(mpi);

		Setup setup = new Setup();
		TimeSeriesProvider timeSeriesProvider = new TimeSeriesProvider(new HashMap<Integer, TimeSeries>());
		PortableUidManager portableUidManager = new PortableUidManager(new ArrayList<>());
		Composer composer = new Composer(portableUidManager, timeSeriesProvider, setup.getPortableDepth());
		MessageBuilder messageBuilder = new MessageBuilder(composer, new ArrayList<>());
		CommTracking commTracking = mock(CommTracking.class);
		AddressBook addressBook = new AddressBook(mpiManager);
		PostOffice postOffice = new PostOffice(mpiManager, messageBuilder, commTracking, addressBook);

		Scheduler scheduler = new Scheduler(mpiManager, new NoRuntimeTracking(), new Schedule(0L, 0L));
		UuidGenerator uuidManager = new UuidGenerator(mpiManager.getRank());

		ArrayList<WarmUpAgent> localAgents = new ArrayList<>();
		for (int i = 0; i <= mpiManager.getRank(); i++) {
			AgentDao dao = AgentDao.newBuilder().setId(uuidManager.generateNext()).setClassName("WarmUpAgent").build();
			WarmUpAgent agent = new WarmUpAgent(dao, mpiManager.getRank() + i);
			localAgents.add(agent);
			scheduler.registerAgent(agent);
		}

		addressBook.synchroniseAddressBookUpdates();
		scheduler.initialiseSchedules();

		int warmUpTick = 0;
		int numberOfAgentsWithWarmUp = 0;
		while (scheduler.needsFurtherWarmUp()) {
			postOffice.deliverMessages();
			numberOfAgentsWithWarmUp = scheduler.executeWarmUp();
			scheduler.sychroniseWarmUp();

			warmUpTick++;
			if (warmUpTick > MAX_WARM_UP_TICKS) {
				throw new RuntimeException(Simulator.ERR_WARM_UP_INCOMPLETE);
			}
		}

		for (WarmUpAgent agent : localAgents) {
			assert agent.warmUpPerformedAsNeeded() : "Warm-up not performed as expected";
		}
		if (warmUpTick > 1 || numberOfAgentsWithWarmUp > 0) {
			System.out.println("Warm-up completed as expected after " + warmUpTick + " ticks.");
		}
		mpi.invokeFinalize();
	}
}