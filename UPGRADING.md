<!-- SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>

SPDX-License-Identifier: Apache-2.0 -->
# Upgrading

## [2.0.0]
### New command-line interface
This version provides a complete revision of the command-line interface (CLI).
All relevant parameters can now be configured via the CLI.
Each parameter has now been assigned a default value, allowing users to run FAME applications without any CLI arguments.
Furthermore, users can override the default settings by modifying the `fameSetup.yaml` file.
Please use the `--help` argument of the CLI to get an overview of all available parameters and their defaults.

#### Changed CLI arguments
These arguments were already present previously but are now renamed or removed:

| old argument    | new argument         |
|-----------------|----------------------|
| `-m`, `--mode`  | `-m`, `--mpi-mode`   |
| `-f`, `--file`  | `-f`, `--input-file` |
| `-s`, `--setup` | _removed_            |

#### New CLI arguments
Options that were previously only accessible via editing `fameSetup.yaml` are now accessible via CLI as well.
However, all their names are updated:

| old name in `fameSetup.yaml`     | new name in `fameSetup.yaml` | new CLI argument         | changed behaviour                 |
|----------------------------------|------------------------------|--------------------------|-----------------------------------|
| _not available_                  | mpi-mode                     | see above                | no change                         |
| _not available_                  | input-file                   | see above                | no change                         |
| outputPath                       | _removed_                    | _none_                   |                                   |
| outputFilePrefix                 | output-file                  | `-o`, `--output-file`    | now takes a path                  |
| outputFileTimeStamp              | add-timestamp                | `-t`, `--add-timestamp`  | time stamp prepended to file name |
| outputActiveClassNames           | output-agents                | `-a`, `--output-agents`  | no changes                        |
| outputComplexDisabled            | output-complex               | `-c`, `--output-complex` | activates complex output          |
| performanceTrackingRuntimes      | track-runtime                | `-r`, `--track-runtime`  | no changes                        |
| performanceTrackingCommunication | track-exchange               | `-x`, `--track-exchange` | no changes                        |

**Warning**: Some parameters have had their default values changed.
For further details, please refer to the CLI's `--help` page, which provides information on the new defaults.

#### Hidden CLI arguments
Some CLI arguments are hidden to prevent users from making unintended changes that could disrupt the functionality of the program.
The following arguments should not be changed without a very good reason:
* `-d`, `--portable-depth`: defines how many nesting levels Portable message data components can have
* `-n`, `--output-interval`: replaces `outputInterval` in `fameSetup.yaml`

### Explicit group optionality
This version makes @Input Groups optionality explicit.
Thus, if a Group is optional is no longer derived from its inner elements.
Instead, if a group can be omitted as a whole, add `.optional()` to the group builder.
Since some groups in your code were possibly considered optional before, since all their inner elements were optional, your existing code might break and might require manual adjustment to explicitly state optional groups.
You can now also use the new method `ParameterData.GetOptionalGroupList` to safely access optional group lists without the need of catching exceptions.

### Renamed TimeSeries methods
This version renames TimeSeries methods `getValueHigherEqual` and `getValueLowerEqual ` to `getValueLaterEqual` and `getValueEarlierEqual`.
If your code refers to those, you will need to rename the calls to these methods.
There is no change to these methods from their behaviour - so no other action than renaming is required.

### Update to FAME-Protobuf v2.0
This version is based on [FAME-Protobuf v2.0](https://gitlab.com/fame-framework/fame-protobuf).
Input protobuf files created with versions of [FAME-Io < v3.0](https://gitlab.com/fame-framework/fame-io) are not compatible with simulations based on this version of FAME-Core.
Thus, you will need to also upgrade to a FAME-Io release >= v3.0.
If you want to run an existing protobuf file compiled with FAME-Io v2, first extract its inputs with FAME-Io v2 and the recompile the input protobuf with FAME-Io v3.

#### DataItems: Plural names for protobuf fields
FAME-Protobuf no follows the official protobuf style guide for field names.
Thus, when using DataItems, their field names have to be updated and add the additional "s" when using their methods.
Hence, in `fillDataFields` replace, e.g., `builder.addIntValue` with `builder.addIntValues`, and correspondingly for all data types.
Also, in your protobuf constructor replace, e.g., `protoData.getIntValue` with `protoData.getIntValues`, and correspondingly for all data types.

### Old JDK support dropped
This release drops support for older Java versions 8, 9, and 10, making JDK version 11 the new minimum requirement.
Check your version with `java -version` and update if your Java version is below 11, see [adoptium.net](https://adoptium.net/).
